const webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const CompressionPlugin = require('compression-webpack-plugin');

const port = process.env.PORT || 8086;

module.exports = {
    mode: 'development',
    entry: './src/index.js',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: '[name].[contenthash].js',
      },
      optimization: {
        runtimeChunk: 'single',
        splitChunks: {
          chunks: 'all',
          maxInitialRequests: Infinity,
          maxSize: 0,
          cacheGroups: {
            vendor: {
              test: /[\\/]node_modules[\\/]/,
              name(module) {
                // get the name. E.g. node_modules/packageName/not/this/part.js
                // or node_modules/packageName
                const packageName = module.context.match(/[\\/]node_modules[\\/](.*?)([\\/]|$)/)[1];
    
                // npm package names are URL-safe, but some servers don't like @ symbols
                return `npm.${packageName.replace('@', '')}`;
              },
            },
          },
        },
    },
    module: {
        rules: [
            {
                test: /\.(js)$/,
                exclude: /node_modules/,
                use: ['babel-loader']
            },
            {
                test: /\.tsx?$/,
                loader: 'ts-loader'
            },
            {
                test: /\.css$/,
                use: [
                    {
                        loader: 'style-loader'
                    },
                    {
                        loader: 'css-loader',
                        options: {
                            //modules: true,
                            //camelCase: true,
                            //sourceMap: true
                        }
                    },
                ]
            },
            {
                test: /\.less$/,
                // loader: 'style!css!less!postcss'
                use: [
                    {
                        loader: 'style-loader', // creates style nodes from JS strings
                    },
                    {
                        loader: 'css-loader', // translates CSS into CommonJS
                    },
                    {
                        loader: 'less-loader', // compiles Less to CSS
                    },
                ],
            },
            {
                test: /\.(png|jpg|woff|woff2|eot|ttf|svg|gif|otf|mp4)$/,
                exclude: /node_modules/,
                loader: 'file-loader',
                options: {
                    limit: 1024,
                    name: '[name].[ext]',
                    publicPath: './dist/',
                    outputPath: './dist/'
                }
            }
        ]
    },
    devtool: 'inline-source-map',
    plugins: [
        new HtmlWebpackPlugin({
            template: 'public/index.html',
          //  favicon: 'public/favicon.ico'
        }),
        new webpack.DefinePlugin({
            WEBPACK_CONFIG_API_URL: JSON.stringify("http://10.15.19.30:8085"),
            WEBPACK_CONFIG_API_QUARTZ: JSON.stringify("http://10.15.19.30:8070"),
        }),
        new CompressionPlugin({
            algorithm: 'gzip',
            test: /\.js$/,
        }),
        new webpack.HashedModuleIdsPlugin(),
        new BundleAnalyzerPlugin(),
    ],
    devServer: {
        host: 'localhost',
        port: port,
        open: true,
        historyApiFallback: true
    },
    resolve: {
        modules: [path.resolve(__dirname, './src'), 'node_modules'],
        extensions: ['.js', '.json', 'css', '.tsx', '.ts'],
        alias: {
            components: path.resolve(__dirname, './src/components'),
            modules: path.resolve(__dirname, './src/modules'),
            style: path.resolve(__dirname, './src/style'),
            utils: path.resolve(__dirname, './src/utils'),
        }
    },
};
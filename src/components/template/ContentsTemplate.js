import React, { Component } from 'react'
import PropTypes from 'prop-types';
import $ from 'jquery';
import 'jquery-ui/ui/core';
import ReactDOM from 'react-dom';
import 'components/template/contents_style.css';

class ContentsTemplate extends Component {

    margin = 0;

    constructor(props) {
        super(props);

        this.state = {
            height: 'calc(100% - 50px)',
            // height: '100%',
            padding: '6px 10px 15px',
            // width: '100%',
            isMounted: undefined,
            background: '#fff',
            isShadow: false,
        }
    }

    componentDidMount() {
        const own = ReactDOM.findDOMNode(this);
        own.addEventListener("updatesize", this.updateContentsSize);

        this.initialize();
        this.updateContentsSize();
        window.addEventListener('resize', this.updateContentsSize);
        setTimeout(() => {
            if (this._isMounted) {
                this.updateContentsSize();
            }
        }, 0.1);
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
        const own = ReactDOM.findDOMNode(this);
        own.removeEventListener("updatesize", this.updateContentsSize);
        window.removeEventListener('resize', this.updateContentsSize);
    }

    initialize = () => {
        const own = ReactDOM.findDOMNode(this);

        // 자식 중에 "비율" 설정이 있으면 background: transparent로 설정
        let existsRatioChildren = false;
        if (this.props.background == "#fff" && own.hasChildNodes()) {
            const nodes = own.childNodes;
            for (let i = 0; i < nodes.length; i++) {
                const classList = nodes[i].classList;
                for (let j = 0; j < classList.length; j++) {
                    if (classList[j].length > 0 && classList[j].indexOf("ratio") >= 0) {
                        existsRatioChildren = true;
                        break;
                    }
                }
            }
        }

        if (this.props.shadow == true && !this.isExistParentContentsWrap(own, 'contents_wrap')) {
            this.setState({ isShadow: true });
        }

        if (existsRatioChildren) {
            this.setState({ background: 'transparent', isShadow: false });
        } else {
            this.setState({ background: this.props.background, isShadow: this.props.shadow == false ? false : true });
        }

        // 부모가 없으면 width: 100% 설정
        if (own.parentNode == null) {
            this.setState({ width: '100%' });
        }

        // 자식중에 ContentsTemplate이 존재하면 padding 0px
        if (this.isExistContentsWrap(own)) {
            this.setState({ padding: '0px' });
        }

        if (this.props.shadow == true && this.isExistParentContentsWrap(own, 'contents_wrap')) {
            this.setState({ isShadow: true });
        }

        // SModal에서는 무조건 padding 처리
        if (own.length <= 1 || own.length == undefined) { // IE 오류 방지
            if (own.hasChildNodes() && own.childNodes[0].className == "pop_panel_body") {
                this.setState({ padding: '6px 10px' });
            }
        }

        // props으로 넘어온 noMargin 처리
        if (this.props.noMargin == true) {
            this.setState({ padding: '0px' });
        }

        // STab이 자식일 때는 border 그리지 않는다
        if (own.hasChildNodes() && own.childNodes.length == 1) {
            if (own.childNodes[0].classList.contains('tab_item')) {
                this.setState({ isShadow: false });
            }
        }

        if (this.isExistParentContentsWrap(own, 'tab_item')) {
            this.setState({ isShadow: false });
        }
    }

    /**
     * 내 부모중에 "className"이 있는지 확인
     */
    isExistParentContentsWrap = (element, findClassName = 'contents_wrap') => {
        const parentNode = element.parentNode;
        if (parentNode) {
            if (parentNode.classList && parentNode.classList.contains(findClassName)) {
                return true;
            }
            if (parentNode.parentNode) {
                if (this.isExistParentContentsWrap(parentNode, findClassName)) {
                    return true;
                }
            }
        }
    }

    updateContentsSize = () => {
        const own = ReactDOM.findDOMNode(this);
        const parent = own.parentNode;
        const children = parent.childNodes;

        const parentX = parent.getBoundingClientRect().left;
        const ownX = own.getBoundingClientRect().left;
        const parentLeftPadding = parseInt($(parent).css("padding-left").replace('px', ''));
        const margin = this.state.isShadow ? this.margin * 2 : 0

        // 현재 컨트롤 같은 가로 레벨에 컨트롤이 있는지 확인하여 해당 영역을 빼고 계산
        if (parentX + parentLeftPadding != ownX) {
            this.setState({ width: `calc(100% - ${ownX - (parentX + parentLeftPadding)}px)` });
        } else {
            this.setState({ width: '100%' });
        }

        const parentTopPadding = parseInt($(parent).css("padding-top").replace('px', ''));
        const parentTop = this.getAbsoluteTop(parent);
        let contentsTop;
        let remainderHeight = 0;

        let findCheck = false;
        for (let i = 0; i < children.length; i++) {
            if (findCheck == true) {
                remainderHeight += children[i].getBoundingClientRect().height;
            }
            if (own == children[i]) {
                contentsTop = this.getAbsoluteTop(children[i]);
                // contentsTop = hildren[i].getBoundingClientRect().top;

                findCheck = true;
            }
        }

        contentsTop -= parentTopPadding;
        this.setState({ height: `calc(100% - ${contentsTop - parentTop}px - ${remainderHeight}px - ${margin}px)` });
    }

    isExistContentsWrap = (element) => {
        // 내 자식중에 ContentsTemplate이 존재하는지 확인
        if (element.hasChildNodes) {
            for (let i = 0; i < element.childNodes.length; i++) {
                const item = element.childNodes[i];

                if (item.getElementsByClassName('contents_wrap').length > 0) {
                    return true;
                } else {
                    if (item.hasChildNodes) {
                        if (this.isExistContentsWrap(item.childNodes)) {
                            return true;
                        }
                    }
                }
            }
        }

        return false;
    }

    //요소의 절대좌표 구하기.
    getAbsoluteTop(element) {

        //// 출처: https://mommoo.tistory.com/85
        //// 부모요소의 시작지점을 기준으로한 상대좌표
        // const parentElement = element.parentElement;
        // const parentAbsoluteTop = this.getAbsoluteTop(parentElement);
        // const absoluteTop = this.getAbsoluteTop(element);
        // const relativeTop = absoluteTop - parentAbsoluteTop;
        // return absoluteTop;

        return window.pageYOffset + element.getBoundingClientRect().top;
    }

    render() {
        const { height, id, overflowY, overflowX, overflow, minWidth, minHeight, background, float } = this.props;
        const { isShadow } = this.state;

        return (
            <div id={id} className='contents_wrap fl'
                type={isShadow ? 'shadow' : undefined}
                style={{
                    height: (height || this.state.height)
                    , width: this.state.width
                    , minWidth: minWidth
                    , minHeight: minHeight
                    , overflowY: overflow == 'hidden' ? undefined : overflowY
                    , overflowX: overflow == 'hidden' ? undefined : overflowX
                    // , overflow: overflow
                    // , clear: 'both'
                    , padding: this.state.padding
                    , backgroundColor: this.state.background
                    , boxShadow: 'inset -2px -2px 4px 0px rgb(223 224 233)'
                }}
            >
                {/* <div style={{ width: '100%', height: '100%', clear: 'both' }}> */}
                {this.props.children}
                {/* </div> */}
            </div>
        );
    };
}

export default ContentsTemplate;

// Type 체크
ContentsTemplate.propTypes = {
    overflowY: PropTypes.string
}

// Default 값 설정
ContentsTemplate.defaultProps = {
    overflowY: 'auto',
    overflowX: 'auto',
    minWidth: '10px',
    minHeight: '10px',
    noMargin: false,
    background: '#fff',
    // float: 'left',
    shadow: false
}
// import React from 'react';
// import 'components/template/search_style.css';

// const SearchTemplate = ({ searchItem }) => {
//     return (
//         <div className="search_wrap">
//             <div className="search_line_wrap">                
//                 {searchItem}
//             </div>
//         </div>
//     );
// };

// export default SearchTemplate;

import React, { PureComponent } from 'react';
import 'components/template/search_style.css';

class SearchTemplate extends PureComponent {

    render() {

        return (
            <div className={"search_wrap"}
                type={this.props.shadow == true ? "shadow" : "noShadow"}
                style={{
                    // boxShadow: '0 1px 4px 0 rgba(0, 0, 0, 0.14)',
                    // borderRadius: '6px',
                    // margin: '0 1px 3px 0',
                    // width: 'calc(100% - 1px)',
                    width: this.props.width,
                }}>
                <div className="search_line_wrap">
                    {/* {this.props.searchItem} */}
                    {this.props.children}
                </div>
            </div>
        );
    };

    getParams = () => {
        const params = [];

        if (!this.props.children) {
            return;
        }

        // console.log('children', this.props.children, this.props.children.map)

        if (this.props.children.map) {

            this.props.children.map((item, index) => {
                const { id, value } = item.props;
                if (value != undefined) {
                    params.push({ id: id, value: value });
                }
            })
        } else {
            // if (this.props.children._owner.child) {
            //     this.makeParameter(this.props.children._owner.child)
            // }
        }

        if (this.props.onChange) {
            this.props.onChange({ id: 'params', value: params })
        }
    }

    makeParameter = (children) => {
        console.log('make', children);
    }
}
// const SearchTemplate = ({ searchItem }) => {
//     console.log()
// };

export default SearchTemplate;

SearchTemplate.defaultProps = {
    shadow: true,
}
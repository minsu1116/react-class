import React, { Component, Fragment } from 'react'
import 'components/template/middle_style.css'
/**
 * @expandTargetId 확장/축소할 element id
 * @length 데이터의 길이. 0일 경우 자동 축소된다.
 */
class ContentsMiddleTemplate extends Component {

    constructor(props) {
        super(props);
        this.state = {
            on: true,
            length: 0,
        };
    }

    componentDidMount() {
        this._isMounted = true;
        const { expandTargetId, length } = this.props;
        if (expandTargetId != undefined && length != undefined) {
            const on = length > 0 ? true : false;
            this.setState({ on: on });
            this.setDisplay(on);
        }
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    expandToggle = async () => {
        const on = this.state.on ? false : true;
        this.setState({ on: on });
        this.setDisplay(on);
    }

    setDisplay = (on) => {
        const { expandTargetId } = this.props;

        let contentsTemplate = document.getElementById(expandTargetId);
        if (contentsTemplate) {
            if (!on) {
                contentsTemplate.style.display = 'none';
            } else {
                contentsTemplate.style.display = 'block';

                var event;
                if(typeof(Event) === 'function') {
                    event = new Event('updatesize');
                } else{
                    event = document.createEvent('Event');
                    event.initEvent('updatesize', true, true);
                }
                
                contentsTemplate.dispatchEvent(event);
            }
        }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        if (nextProps.length != prevState.length) {
            return { length: nextProps.length };
        }
        return null;
    }

    getSnapshotBeforeUpdate(prevProps, prevState) {
        if (prevState.length != this.state.length) {
            const on = this.state.length > 0 ? true : false;
            this.setState({ on: on });
            this.setDisplay(on);
        }

        return null;
    }

    componentDidUpdate(prevProps, prevState, snapshot) {

    }

    render() {
        const { subTitle, expandTargetId, length, align, desc } = this.props;
        const { on } = this.state;

        const rightChildren = [];
        let leftChildren;
        React.Children.map(this.props.children, x => {
            if (x) {
                if (x.type == "div" && x.props.className == "left_btn_wrap") {
                    leftChildren = x;
                } else {
                    rightChildren.push(x);
                }
            }
        });

        let css;
        if (align == "center") {
            css = "btn_align_center";
        } else {
            css = "btn_wrap";
        }

        let updownClass = "updown";
        if (on) {
            updownClass += " icon-up-dir";
        } else {
            updownClass += " icon-down-dir";
        }

        return (
            <div className="middle_wrap" type={this.props.shadow == true ? "shadow" : "noShadow"}>
                {subTitle ? (
                    <div className="sub_tit">
                        {subTitle}
                        {
                            length != undefined ?
                                <Fragment>
                                    {` (${length})`}
                                </Fragment>
                                :
                                null
                        }
                    </div>)
                    : (null)
                }
                {expandTargetId ? (
                    <div className="up_down_warp" style={{ float: "right" }}>
                        <div className="up_down_icon">
                            <i className={updownClass} onClick={this.expandToggle} />
                        </div>
                    </div>
                )
                    : (null)
                }
                {desc ? (<div className="sub_tit_desc">{desc}</div>) : (null)}
                {leftChildren}
                <ul className={css}>
                    {rightChildren}
                </ul>
            </div>
        );
    }
}

export default ContentsMiddleTemplate;

ContentsMiddleTemplate.defaultProps = {
    shadow: true,
}
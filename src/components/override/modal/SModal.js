import React, { Component } from 'react'
import PropTypes from 'prop-types';
import Modal from 'react-modal';
import Draggable from 'react-draggable';
import btn_pop_close from 'style/img/btn_pop_close.png';
import 'components/override/modal/style.css';
import { SModalProvider } from 'utils/context/SModalContext';
import { SConsumer } from 'utils/context/SContext'
import { Resizable } from 'react-resizable';
import 'components/override/modal/resize.css';

export default class SModal extends Component {

    _init = false;

    initWidth = 0;
    initHeight = 0;

    constructor(props) {
        super(props);

        this.state = {
            sizeInfo: {
                parentWidth: 0,
                parentHeight: 0,
            },
            width: 300,
            height: 300,
        }
        Modal.setAppElement('body');
    }

    componentDidMount() {
        this._isMounted = true;
        this.windowResize(true);
        window.addEventListener('resize', this.windowResize);

        if (this._init == false) {
            setTimeout(() => {
                if (this._isMounted) {
                    const children = document.getElementById(`modal_${this.props.id}`);
                    if (children) {
                        const childrenRect = children.getBoundingClientRect();
                        this.initHeight = childrenRect.height + 50;
                        this.setState({ height: this.initHeight });
                    }
                }
            }, 0.1);
            this._init = true;
        }
    }

    componentWillUnmount() {
        this._isMounted = false;
        window.removeEventListener('resize', this.windowResize);
    }

    getSnapshotBeforeUpdate(prevProps, prevState) {
        if (prevProps.width != this.props.width) {
            return { propsChange: true }
        }
        return null;
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (snapshot) {
            if (snapshot.propsChange == true) {
                this.windowResize();
            }
        }
    }

    windowResize = (init) => {
        if (this.props.isOpen == true || init) {
            const { width, height, id } = this.props;
            const container = document.getElementById('container');

            const widthValue = Number(width.toString().replace(/[^0-9]/g, ""));
            const heightValue = height ? Number(height.toString().replace(/[^0-9]/g, "")) : 0;
            let widthText = width.toString().replace(widthValue, '');
            let heightText = height ? height.toString().replace(heightValue, '') : "";
            widthText += widthText.length > 0 ? "" : "px";
            heightText += heightText.length > 0 ? "" : "px";

            const containerHeight = container.offsetHeight - 10;

            this.setState({
                sizeInfo: {
                    parentWidth: container.offsetWidth,
                    parentHeight: container.offsetHeight - 10,
                }
            });

            if (init) {
                this.initWidth = widthValue;
                this.setState({ width: widthValue });
            }
        }
    }

    handleRequestClose = () => {
        if (this.props.onRequestClose) {
            this.props.onRequestClose(null);
        }
    }

    _handleResize = (event, { element, size, handle }) => {
        this.setState({ width: size.width, height: size.height });
    }

    render() {

        const {
            isOpen    // 오픈 여부
            // , onRequestClose  // modal 닫기 함수
            , contentLabel  // modal 라벨
            , contents // modal contents
            , onAfterOpen // modal open 후 실행될 함수
            , closeTimeoutMS // modal 닫기 버튼 후 대기 시간 (ms)
            , width
            , height
            , minWidth
            , minHeight
            , id
        } = this.props;

        const modalStyle = {
            overlay: {
                position: 'fixed',
                top: 0,
                left: 0,
                right: 0,
                bottom: 0,
                backgroundColor: 'rgba(0,0,0,0.2)',
                zIndex: 9999,
                // pointerEvents: 'none',
            },
            content: {
                backgroundColor: 'transparent',
                border: '0px',
                padding: '0px',
                overflow: 'visible',
                position: 'relative',
                top: 10,
                left: 0,
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center',
                filter: 'drop-shadow(3px 3px 4px black)',
            }
        }

        const { parentHeight, parentWidth } = this.state.sizeInfo;

        return (
            <React.Fragment>
                <SConsumer>
                    {
                        (context) => {
                            return (
                                <SModalProvider formId={id} auth={context ? context.auth : null}>
                                    <Modal
                                        style={modalStyle}
                                        isOpen={isOpen}
                                        onRequestClose={this.handleRequestClose}
                                        contentLabel={contentLabel}
                                        onAfterOpen={this.props.onAfterOpen}
                                        shouldCloseOnOverlayClick={false} // modal 바깥 부분 클릭 시 닫기 기능 없애기 
                                    >
                                        <Draggable bounds="body" handle=".cursor" cancel="li">
                                            <Resizable
                                                width={this.state.width}
                                                height={this.state.height}
                                                minConstraints={[this.initWidth, this.initHeight]}
                                                onResize={this._handleResize}
                                                axis="x"
                                            >
                                                <div id={id} className="wrap_pop_up"
                                                    style={{
                                                        width: this.state.width,
                                                        height: this.state.height,
                                                        background: "white",
                                                    }}
                                                >
                                                    <strong className="cursor">
                                                        <div className="pop_panel_head">
                                                            <div className="pop_panel_tit">{contentLabel}</div>
                                                            <div className="pop_close">
                                                                <li className="no-cursor" onClick={this.handleRequestClose} style={{ width: '30px' }}>
                                                                    <img alt="" onClick={this.handleRequestClose} src={btn_pop_close} />
                                                                </li>
                                                            </div>
                                                        </div>
                                                    </strong>

                                                    <div className="pop_panel_body" id={`modal_${this.props.id}`} style={{ overflow: 'visible !important', width: this.state.width, maxHeight: parentHeight - 60 }}>
                                                        {/* position: 'absolute', */}
                                                        {this.props.children}
                                                    </div>
                                                </div>
                                            </Resizable>
                                        </Draggable>
                                    </Modal>
                                </SModalProvider>
                            )
                        }
                    }
                </SConsumer>
            </React.Fragment>
        );
    }
}

SModal.propTypes = {
    minWidth: PropTypes.number,
    minHeight: PropTypes.number,
}

SModal.defaultProps = {
    width: 600,
    id: '',
    customClassName: "pop_panel_body",
}
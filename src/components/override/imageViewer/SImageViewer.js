import React, { Component } from 'react';
import Viewer from 'react-viewer';
import { BaseConsumer } from 'utils/base/BaseComponent'
import { getValue } from 'utils/object/ContextManager';
import 'components/override/imageViewer/style.css';

export default class SImageViewer extends Component {

    constructor(props) {
        super(props);
        this.state = {
            visible: false,
            images: [],
        }
    }

    componentDidMount() {
        const { id } = this.props;
        if (this.baseContext) {
            const initValue = getValue(this.baseContext, id);
            if (initValue) {
                this.setState({ images: initValue });
            }
        }
    }

    setVisible = (value) => {
        this.setState({ visible: value });
    }

    _handleOpenImage = (index) => {
        this.setState({ activeIndex: index });
        this.setVisible(true);
    }

    render() {

        const { images, visible, activeIndex } = this.state;
        const { id } = this.props;

        return (
            <React.Fragment>
                {!this.baseContext ?
                    <BaseConsumer>
                        {(context) => { this.baseContext = context; }}
                    </BaseConsumer>
                    :
                    (null)
                }
                <div className={"image_items"}>
                    {images.map((image, index) => (
                        <div className={"image_item"} key={`img-${id}-${index}`} onClick={() => this._handleOpenImage(index)}>
                            <img
                                className={"image"}
                                src={image.src}
                                // width="100px"
                                // height="100px"
                                // key={`img-${id}-${index}`}
                                alt={image.alt}
                                align={"center"}
                            />
                        </div>
                    ))}

                    <Viewer
                        visible={visible}
                        onClose={() => { this.setVisible(false) }}
                        activeIndex={activeIndex}
                        images={images}
                    />
                </div>
            </React.Fragment>
        )
    }
}
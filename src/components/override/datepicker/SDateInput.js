import React, { Component } from 'react'
import PropTypes from 'prop-types';
import NumberFormat from 'react-number-format';
import { BaseConsumer } from 'utils/base/BaseComponent'
import 'utils/object/DateFormat'
import { reaction } from 'mobx';
import { getValue } from 'utils/object/ContextManager.js'
import 'components/override/datepicker/style.css'
import 'components/override/numericinput/style.css';

export default class SDateInput extends Component {

    beforeValue = "";
    constructor(props) {
        super(props);

        this.state = {
            value: "",
        }

        if (this.props.format == "yyyy-MM") {
            this.mask = "####-##";
        } else if (this.props.format == "yyyy-MM-dd HH:mm") {
            this.mask = "####-##-## ##:##"
        } else if (this.props.format == "HH:mm") {
            this.mask = "##:##"
        } else if (this.props.format == "HH:mm:ss") {
            this.mask = "##:##:##"
        } else {
            this.mask = "####-##-##"
        }
    }

    componentDidMount() {
        this._isMounted = true;
        const { id } = this.props;

        const value = getValue(this.baseContext, id);
        this.beforeValue = value;
        this.setState({ value });

        const ids = id.split('.');

        reaction(
            () => ids.length == 2 ? this.baseContext[ids[0]][ids[1]] : this.baseContext[id],
            (value) => {
                if (this._isMounted) {
                    this.setState({ value: value })
                }
            }
        );
    }

    componentWillUnmount() {
        this._isMounted = false
    }

    _handleChange = (e) => {
        const value = e.target.value;

        if (String.isDateTime(value, this.props.format)) {
            let data;
            if (this.props.format == "yyyy-MM-dd HH:mm") {
                data = value;
                this.setState({ value: value })
            } else {
                data = value.replace(/[^0-9]/g, '');
            }

            this.setValue(data);
        }
    }

    _handleBlur = (e) => {
        const value = e.target.value;

        if (String.isDateTime(value, this.props.format) == false) {
            this.setValue(this.beforeValue);

        } else {
            if (this.props.onChange) {             
                this.props.onChange(value);
            }
        }

    }

    _handleFocus = (e) => {
        this.input.select();
    }

    setValue = (data) => {
        this.setState({ value: data })
        if (this.baseContext) {
            this.baseContext._handleChange({ "id": this.props.id, "value": data });
        }
    }

    render() {
        const { id, contentWidth, readOnly } = this.props;

        let css;
        if (readOnly) {
            css = "input_box date readOnly";
        } else {
            css = "input_box date";
        }

        return (
            <React.Fragment>
                <BaseConsumer>
                    {
                        (context) => {
                            if (this.baseContext == null) {
                                this.baseContext = context;
                            }
                        }
                    }
                </BaseConsumer>


                <div className="numeric_input_item">
                    <NumberFormat
                        id={id}
                        className={css}
                        style={{
                            // textAlign: 'center !important',
                            width: contentWidth,
                            textAlign: 'center',
                        }}
                        value={this.state.value}
                        decimalScale={0}
                        fixedDecimalScale={false}
                        onChange={this._handleChange}
                        onBlur={this._handleBlur}
                        onFocus={this._handleFocus}
                        format={this.mask}
                        readOnly={readOnly}
                        mask={"_"}
                        getInputRef={(el) => this.input = el}
                    />
                </div>
            </React.Fragment>
        );
    }
}

// Type 체크
SDateInput.propTypes = {
    id: PropTypes.string,
}

// Default 값 설정
SDateInput.defaultProps = {
    format: "yyyy-MM-dd",
    contentWidth: 110,
    readOnly: false,
} 
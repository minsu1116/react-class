
import React, { Component } from 'react'
import PropTypes from 'prop-types';
import 'components/override/datepicker/style.css'
import 'utils/object/DateFormat'
import NumberFormat from 'react-number-format';

export default class InputDatePicker extends Component {

    constructor(props) {
        super(props);
        this.state = {
            value: "",
            mounted: false,
            beforeValue: "",
        }
    }

    static getDerivedStateFromProps(nextProps, prevState) {

        if (prevState.value != nextProps.value) {
            if (prevState.mounted == false) {
                return { mounted: true, value: nextProps.value, beforeValue: nextProps.value }
            }

            if (!nextProps.value) {
                return { value: undefined, beforeValue: undefined };
            }

            if (prevState.value != "") {
                if ((prevState.value == undefined || String.isDateTime(prevState.value, nextProps.dateFormat)) && String.isDateTime(nextProps.value, nextProps.dateFormat)) {
                    return { value: nextProps.value, beforeValue: nextProps.value };
                };
            }
        }

        return null;
    }

    _handleChange = (e) => {
        const value = e.target.value;
        e.preventDefault();

        if (String.isDateTime(value, this.props.dateFormat)) {
            this.props.onChange(value);
            this.setState({ value });
            this.setState({ beforeValue: value })
        } else {
            this.setState({ value });
        }
    }

    _handleBlur = (e) => {
        e.preventDefault();
        const value = e.target.value;
        if (String.isDateTime(value, this.props.dateFormat)) {

        } else {
            // this.setState({ value: this.state.beforeValue });
            // this.props.onChange(this.state.beforeValue);
            if (this.props.nullable == true) {
                this.setState({ value: undefined });
                this.props.onChange("");
            } else {
                this.setState({ value: this.state.beforeValue });
                this.props.onChange(this.state.beforeValue);
            }
        }
    }

    _handleFocus = (e) => {
        this.input.select();
    }

    render() {
        return (
            <NumberFormat
                id={this.props.id}
                className={this.props.customClassName}
                style={{
                    width: this.props.width,
                    paddingLeft: '5px',
                    textAlign: 'center',
                }}

                

                value={this.state.value || ""}
                decimalScale={0}
                fixedDecimalScale={false}
                onChange={this._handleChange}
                onBlur={this._handleBlur}
                onFocus={this._handleFocus}
                format={this.props.dateFormat == "yyyy-MM" ? "####-##" : "####-##-##"}
                readOnly={this.props.readOnly}
                getInputRef={(el) => this.input = el}
                mask={"_"}
            />
        );
    }
}

// Type 체크
InputDatePicker.propTypes = {
    width: PropTypes.number
}

// Default 값 설정
InputDatePicker.defaultProps = {
    width: 75
}

import React, { Component } from 'react'
import PropTypes from 'prop-types';
import DatePicker, { registerLocale } from 'react-datepicker';
import ko from 'date-fns/locale/ko';
registerLocale('ko', ko)
import ObjectUtility from 'utils/object/ObjectUtility';
import DatePickerIcon from 'components/override/datepicker/DatePickerIcon';
import InputDatePicker from "components/override/datepicker/InputDatePicker";
import 'components/override/datepicker/style.css'
import "react-datepicker/dist/react-datepicker.css";
import { observer } from 'mobx-react';
import { BaseConsumer } from 'utils/base/BaseComponent';
import { getValue } from 'utils/object/ContextManager';
import { observable, toJS, when } from 'mobx';

/**
 * @id ** DatePicker의 값을 Binding할 Store의 Field명.
 * @readOnly [default: false]
 * @onChange 변경 감지 이벤트. [parameter: 선택한 날짜]
 * @dateFormat 표현할 format. [oneOf: yyyy-MM-dd|yyyy=MM] [defalt: yyyy-MM-dd]
 * @width 가로길이 [default: 110]
 */
export default class SDatePicker extends Component {

    constructor(props) {
        super(props);

        this.state = {
            date: null,
            baseContext: null,
        };
    }

    componentDidMount() {
        if (this.baseContext) {
            this.setState({ baseContext: this.baseContext });
            delete this.baseContext;
        }
    }

    _handleChanageRaw = (date) => {
        this._handleChange(date);
    }

    _handleChange = (date) => {
        const { id, onChange, dateFormat } = this.props;

        this.setState({ date });

        let dateText;
        if (typeof date === 'object' && date instanceof Date) {
            dateText = ObjectUtility.convertToYYYYMMDDFromDate(date);
        } else {
            if (String.isDateTime(date, dateFormat)) {
                dateText = date;
            } else {
                if (this.props.nullable == true) {

                } else {
                    dateText = ObjectUtility.convertToYYYYMMDDFromDate(new Date());
                }
            }
        }

        if (dateText) {
            dateText = dateText.replace(/[^0-9]/g, '');
            if (dateFormat == "yyyy-MM") {
                dateText = dateText.substring(0, 6);
            }
        }

        const data = { "id": id, "value": dateText };
        // const data = { "id": id, "value": dateText || "" };
        if (this.state.baseContext) {
            this.state.baseContext._handleChange(data);
        }

        if (onChange) {
            onChange(dateText);
        }
    }

    convertDate = (value) => {
        if (value && value != "") {
            if (String.isDateTime(value, this.props.dateFormat)) {
                if (typeof value === 'object' && value instanceof Date) {
                    return value;
                }
                const dateText = ObjectUtility.convertToDateFromYYYYMMDD(value);
                return dateText;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    render() {
        const { id, dateFormat, width, readOnly, marginLeft, marginRight, title} = this.props;
        const { baseContext } = this.state;
        let css = "";
        let customContentWidth = width;
        if (readOnly) {
            css = "input_box date readOnly";
        } else {
            css = "input_box date";
            customContentWidth = customContentWidth - 25;  // 버튼크기 제외
        }

        let value = getValue(baseContext, id);
        value = this.convertDate(value);
        return (
            <React.Fragment>
                {!this.baseContext ?
                    <BaseConsumer>
                        {(context) => { this.baseContext = context; }}
                    </BaseConsumer>
                    :
                    (null)
                }

                <div className="fl" style={{ marginLeft: `${marginLeft}px`, marginRight: `${marginRight}px`, height: '28px' }}>
                    {title != "" && title != "~" ?
                        <label className="item_lb w100">{title}</label> : 
                        title == '~' ? <label className="item_lb">{title}</label> :
                        null
                    }
                    <ul className="fl">
                        <DatePicker
                            customInput={<InputDatePicker {...this.props} id={id} customClassName={css} width={customContentWidth} readOnly={readOnly} value={value} />}
                            selected={value}
                            onChange={this._handleChange}
                            onClick={this._handleClick}
                            onChangeRaw={this._handleChanageRaw}
                            dateFormat={dateFormat}
                            popperPlacement="center"
                            showMonthYearPicker={dateFormat != "yyyy-MM-dd" ? true : false}
                            locale="ko"
                            showMonthDropdown
                            showYearDropdown
                            readOnly={readOnly}
                        />
                    </ul>
                    {readOnly == false ?
                        <ul className="fl" style={{ cursor: 'pointer' }}>
                            <DatePicker
                                customInput={<DatePickerIcon />}
                                selected={value}
                                onChange={this._handleChange}
                                popperPlacement="center"
                                showMonthYearPicker={dateFormat != "yyyy-MM-dd" ? true : false}
                                locale="ko"
                                showMonthDropdown
                                showYearDropdown
                                readOnly={readOnly}
                            />
                        </ul>
                        :
                        null
                    }
                </div>
            </React.Fragment>
        );
    }
}

// Type 체크
SDatePicker.propTypes = {
    id: PropTypes.string,
    dateFormat: PropTypes.string,
    width: PropTypes.oneOf(PropTypes.number, PropTypes.string),
    title: PropTypes.string
}

// Default 값 설정
SDatePicker.defaultProps = {
    dateFormat: "yyyy-MM-dd",
    width: 110,
    readOnly: false,
    marginLeft: 0,
    marginRight: 0,
    nullable: false,
    title: "",
}
import React, { useState, useRef, Component } from 'react';
import ReactQuill, { Mixin, Toolbar, Quill } from "react-quill";
import Dropzone, { ImageFile } from "react-dropzone";
import "./style.css";
import "react-quill/dist/quill.snow.css";
import ContentsTemplate from 'components/template/ContentsTemplate';

const __ISMSIE__ = navigator.userAgent.match(/Trident/i) ? true : false;
const __ISIOS__ = navigator.userAgent.match(/iPad|iPhone|iPod/i) ? true : false;

export default class SQuill extends Component {

    modules = {
        toolbar: {
            container: [
                ["bold", "italic", "underline", "strike", "blockquote"],
                [{ size: ["small", false, "large", "huge"] }, { color: [] }],
                [
                    { list: "ordered" },
                    { list: "bullet" },
                    { indent: "-1" },
                    { indent: "+1" },
                    { align: [] }
                ],
                ["link", "image", "video"],
                ["clean"]
            ],
            handlers: { image: this.imageHandler }
        },
        clipboard: { matchVisual: false }
    };

    formats = [
        "header",
        "bold",
        "italic",
        "underline",
        "strike",
        "blockquote",
        "size",
        "color",
        "list",
        "bullet",
        "indent",
        "link",
        "image",
        "video",
        "align"
    ];

    constructor(props) {
        super(props);
        this.state = {
            subject: "",
            contents: "", //__ISMSIE__ ? "<p>&nbsp;</p>" : "",
            workings: {},
            fileIds: []
        };
    }

    onChangeContents = (contents) => {
        let _contents = null;
        if (__ISMSIE__) {
            if (contents.indexOf("<p><br></p>") > -1) {
                _contents = contents.replace(/<p><br><\/p>/gi, "<p>&nbsp;</p>");
            }
        }
        this.setState({ contents: _contents || contents });
    }

    _handleBlur = (e, e1) => {
        console.log("_handleBlur", this.quillRef, this.state.contents, e, e1);
    }

    render() {
        return (
            // <div className="main-panel">
            //     <div className="navbar">
            //         ReactQuill Typescript with IE11, iOS, Korean Support
            // </div>
            <ContentsTemplate>
                <div className="main-content" style={{ width: "100%", height: "100%" }}>
                    <ReactQuill
                        onRef={(el) => (this.quillRef = el)}
                        value={this.state.contents}
                        onChange={this.onChangeContents}
                        // onKeyUp={this.onKeyUp}
                        // onFocus={this.onFocus}
                        onBlur={this._handleBlur}
                        theme="snow"
                        modules={this.modules}
                        formats={this.formats}
                    />
                    {/* <Dropzone
                        ref={(el) => (this.dropzone = el)}
                        style={{ width: 0, height: 0 }}
                        onDrop={this.onDrop}
                        accept="image/*"
                    />a */}
                </div>
            </ContentsTemplate>
        );
    }

}
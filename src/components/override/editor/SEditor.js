import React, { useState, useRef, Component } from 'react';
// import Dropzone, { ImageFile } from "react-dropzone";
import "components/override/editor/style.css";
import { BaseConsumer } from 'utils/base/BaseComponent'
import { getValue } from 'utils/object/ContextManager';
import { reaction } from 'mobx';

import 'froala-editor/css/froala_editor.pkgd.min.css';
import 'froala-editor/css/froala_style.css';
import 'froala-editor/js/plugins.pkgd.min.js';

import "froala-editor/css/plugins/quick_insert.min.css";
import "froala-editor/css/plugins/char_counter.min.css";
import "froala-editor/css/plugins/code_view.min.css";
import "froala-editor/css/plugins/colors.min.css";
import "froala-editor/css/plugins/draggable.min.css";
import "froala-editor/css/plugins/emoticons.min.css";
import "froala-editor/css/plugins/file.min.css";
import "froala-editor/css/plugins/fullscreen.min.css";
import "froala-editor/css/plugins/help.min.css";
import "froala-editor/css/plugins/image_manager.min.css";
import "froala-editor/css/plugins/image.min.css";
import "froala-editor/css/plugins/line_breaker.min.css";
import "froala-editor/css/plugins/quick_insert.min.css";
import "froala-editor/css/plugins/special_characters.min.css";
import "froala-editor/css/plugins/table.min.css";
import "froala-editor/css/plugins/video.min.css";
import "froala-editor/js/languages/ko.js";


// import "froala-editor/css/plugins/quick_insert.min.css";

import FroalaEditor from 'react-froala-wysiwyg';
import Froalaeditor from 'froala-editor';
import FroalaEditorView from 'react-froala-wysiwyg/FroalaEditorView';
import CompanySavePModel from 'modules/pages/system/master/company/popup/CompanySavePModel';

export default class SEditor extends Component {

    config = {
        // key: "IGLGTD1DMJf1BWLb1PO== ",
        // key: "IB2A6A1B1D3D1rD1H5G4A3B2C10A7E2E5D3eUh1QBRVCDLPAZMBQ==",
        key: "",
        height: '100%',
        attribution: false,
        charCounterCount: false,
        language: window.localStorage.getItem("locale").toString().toLowerCase() == "kr" ? 'ko' : window.localStorage.getItem("locale").toString().toLowerCase(),
        quickInsertEnabled: false,
        tabSpaces: 4,

        //image
        imageDefaultWidth: 0, //auto
        imageDefaultAlign: 'left',
        imageUpload: false,

        listAdvancedTypes: true,
        toolbarButtons: [
        ['fontSize', 'textColor', 'backgroundColor', 'emoticons', 'specialCharacters'],
        ['insertTable'],
        ['bold', 'italic', 'underline', 'strikeThrough'],
        ['alignLeft', 'alignCenter', 'alignRight', 'formatOL', 'formatUL', 'outdent', 'indent'],
        ['undo', 'redo'],
        ],
        events: {
            "initialized": function (a) {
                const wrapper = this.$wp[0];
                const viewer = this.$el[0];
                const editor = this;
                if (wrapper) {
                    const func = function (event) {
                        if (editor.node.hasFocus(viewer) == false) {
                            editor.selection.setAtEnd(viewer);
                            editor.selection.restore();
                        }
                    }
                    editor.__wrapperClick = func;
                    wrapper.addEventListener('click', func);
                }
            },
            'destroy': function () {
                const wrapper = this.$wp[0];
                if (wrapper) {
                    wrapper.removeEventListener('click', this.__wrapperClick);
                }
            },
            // image가 파일 drag&drop으로 삽입될 경우 blob이 아닌 base64형태로 추가
            "image.beforeUpload": function (files) {
                const editor = this;
                if (files.length) {
                    for (let i = 0; i < files.length; i++) {
                        // Create a File Reader.
                        const reader = new FileReader();
                        // Set the reader to insert images when they are loaded.
                        reader.onload = function (e) {
                            var result = e.target.result;
                            editor.image.insert(result, null, null, editor.image.get());
                        };
                        // Read image as base64.
                        reader.readAsDataURL(files[i]);
                    }
                }
                editor.popups.hideAll();
                // Stop default upload chain.
                return false;
            },
            // 'image.beforePasteUpload': function (img) {
            //     // Do something here.
            //     // console.log("image.beforePasteUpload", img);
            // },
            // "image.error": function (error, response) {
            //     // console.log("image.error", error, response, this);
            // },
            // "click": function (e) {
            //     // console.log("CLICK", this, e);
            // },
            // 'mousedown': function (mousedownEvent) {
            //     // Do something here.
            //     // this is the editor instance.
            //     // console.log('mousedown', this);
            // },
        }
    };

    constructor(props) {
        super(props);
        this.state = {
            subject: "",
            contents: "",
            workings: {},
            fileIds: [],
            visible: false,
            editor: undefined,
            readOnly: props.readOnly,
        };
    }

    componentDidMount() {
        this._isMounted = true;
        const { id } = this.props;
        if (this.baseContext) {
            const initValue = getValue(this.baseContext, id);
            if (initValue) {
                if (this._isMounted) {
                    this.setState({ contents: initValue });
                }
            }
        }

        const ids = id.split('.');

        let index;
        const regex = /\[[0-9]*\]/;
        const result = id.match(regex);
        if (result) {
            ids[0] = ids[0].replace(result[0], "");
            index = result[0].replace(/[^0-9]/g, "");
        }

        reaction(
            () => ids.length == 2 ?
                (
                    index ? this.baseContext[ids[0]][index][ids[1]] : this.baseContext[ids[0]][ids[1]]
                )
                : this.baseContext[id],
            (value) => {
                if (this._isMounted) {
                    if (value != this.state.contents) {
                        if (!this.originData) {
                            this.originData = document.createElement("originData");
                        }
                        this.originData.innerHTML = value;

                        this.setState({ contents: value });
                    }
                }
            }
        );

        setTimeout(() => {
            if (this._isMounted) {
                this.setState({ visible: true });
            }
        }, 0.1);

        // var toolbar = document.getElementsByClassName("ql-toolbar");
        // console.log("toolbar", toolbar);
        // toolbar.querySelector('button.ql-bold').setAttribute('title', 'Bold');
    }

    componentWillUnmount = () => {
        this._isMounted = false;
    }

    // static getDerivedStateFromProps(nextProps, prevState) { 
    //     if (nextProps.readOnly !== prevState.readOnly) {
    //         const { editor } = prevState;
    //         document.getElementsByClassName('fr-view')[0].focus();
    //         // editor.commands.selectAll();
    //         // var elements = editor.selection.blocks();
    //         if (nextProps.readOnly == true) {
    //             // for (let i = 0; i < elements.length; i++) {
    //             //     elements[i].contentEditable = "false";
    //             // }
    //             editor.edit.off();
    //         } else {
    //             // for (let i = 0; i < elements.length; i++) {
    //             //     console.log("ON?", i, elements[i].contentEditable);
    //             //     elements[i].contentEditable = "true";
    //             // }
    //             editor.edit.on();
    //         }
    //         return { readOnly: nextProps.readOnly };
    //     }

    //     return null;
    // }

    // onChangeContents = (contents) => {
    //     if (this._isMounted) {
    //         this.setState({ contents: contents });
    //     }
    // }

    _handleBlur = (model) => {
        if (this.state.contents == model) {
            return;
        }
        if (this._isMounted) {
            if (model) {
                this.setState({
                    contents: model,
                });
            }
        }

        // 태그만 있는지 테스트
        if (this.state.contents.indexOf("<img") < 0) {
            const regExp = /(<([^>]+)>)/ig;
            const emptyText = this.state.contents.replace(regExp, "");
            if (emptyText.trimLeft().trimRight() == "") {
                if (this.baseContext) {
                    const { id } = this.props;
                    this.baseContext._handleChange({ id: id, value: "" });
                }
                return;
            }
        }

        const newData = document.createElement("newData");
        newData.innerHTML = this.state.contents;
        const newImgArray = newData.getElementsByTagName("img");

        if (this.originData) {
            // 새로운 데이터에 이미지가 있으면
            if (newImgArray && newImgArray.length > 0) {
                const originImgArray = this.originData.getElementsByTagName("img");

                const originLength = originImgArray.length;
                const newLength = newImgArray.length;

                for (let i = 0; i < originLength; i++) {
                    const img = originImgArray[i];

                    if (img.id) {
                        for (let j = 0; j < newLength; j++) {
                            const newImg = newImgArray[j];
                            if (img.src == newImg.src) {
                                newImg.id = img.id;
                            }
                        }
                    }
                }
            }
        }

        if (!this.originData) {
            this.originData = document.createElement("originData");
        }
        this.originData.innerHTML = newData.innerHTML;

        if (this.baseContext) {
            const { id } = this.props;
            this.baseContext._handleChange({ id: id, value: newData.innerHTML });
        }
    }

    handleManualController = (initControls) => {
        initControls.initialize();
        this.setState({ editor: initControls.getEditor() });
    }

    render() {

        const { readOnly } = this.props

        if(readOnly) {
            this.config["toolbarButtons"] = [];
            this.config["events"]["initialized"] =  function (a) {
                const wrapper = this.$wp[0];
                const viewer = this.$el[0];
                const editor = this;
                if (wrapper) {
                    const func = function (event) {
                        if (editor.node.hasFocus(viewer) == false) {
                            editor.selection.setAtEnd(viewer);
                            editor.selection.restore();
                        }
                    }
                    editor.__wrapperClick = func;
                    wrapper.addEventListener('click', func);
                    
                }
                
                editor.edit.off();
            }
        }

        return (
            <React.Fragment>
                {!this.baseContext ?
                    <BaseConsumer>
                        {(context) => { this.baseContext = context; }}
                    </BaseConsumer>
                    :
                    (null)
                }
                <FroalaEditor
                    // tag='textarea'
                    model={this.state.contents}
                    onManualControllerReady={this.handleManualController}
                    onModelChange={this._handleBlur}
                    // contentChanged={thi}
                    config={this.config}
                // config={{
                //     key: "IGLGTD1DMJf1BWLb1PO==",
                //     height: '100%',
                //     attribution: false,
                //     charCounterCount: false,
                //     language: "ko",
                //     quickInsertEnabled: false,
                //     tabSpaces: 4,

                //     //image
                //     imageDefaultWidth: 0, //auto
                //     imageDefaultAlign: 'left',

                //     listAdvancedTypes: true,
                //     toolbarButtons: [
                //         ['bold', 'italic', 'underline', 'strikeThrough', 'quote', 'clearFormatting', '|', 'alignLeft', 'alignCenter', 'formatOLSimple', 'alignRight', 'formatOL', 'formatUL', '|', 'fontSize', 'textColor', 'backgroundColor', 'emoticons', 'specialCharacters'],
                //         // ['alignLeft', 'alignCenter', 'formatOLSimple', 'alignRight', 'formatOL', 'formatUL'],
                //         // ['fontSize', 'textColor', 'backgroundColor', 'emoticons', 'specialCharacters'],
                //         ['insertTable'],
                //         ['undo', 'redo'],
                //     ],
                //     events: {
                //         "initialized": function (a) {
                //             const wrapper = this.$wp[0];
                //             const viewer = this.$el[0];
                //             const editor = this;
                //             if (wrapper) {
                //                 const func = function(event) {
                //                     if (editor.node.hasFocus(viewer) == false) {
                //                         editor.selection.setAtEnd(viewer);
                //                         editor.selection.restore();
                //                     }
                //                 }
                //                 editor.__wrapperClick = func;
                //                 wrapper.addEventListener('click', func);
                //             }
                //         },
                //         'destroy': function () {
                //             const wrapper = this.$wp[0];
                //             if (wrapper) {
                //                 wrapper.removeEventListener('click', this.__wrapperClick);
                //             }
                //         },
                //         // image가 파일 drag&drop으로 삽입될 경우 blob이 아닌 base64형태로 추가
                //         "image.beforeUpload": function (files) {
                //             const editor = this;
                //             if (files.length) {
                //                 for (let i = 0; i < files.length; i++) {
                //                     // Create a File Reader.
                //                     const reader = new FileReader();
                //                     // Set the reader to insert images when they are loaded.
                //                     reader.onload = function (e) {
                //                         var result = e.target.result;
                //                         editor.image.insert(result, null, null, editor.image.get());
                //                     };
                //                     // Read image as base64.
                //                     reader.readAsDataURL(files[i]);
                //                 }
                //             }
                //             editor.popups.hideAll();
                //             // Stop default upload chain.
                //             return false;
                //         },
                //         "click": function (e) {
                //             // console.log("CLICK", this, e);
                //         },
                //         'mousedown': function (mousedownEvent) {
                //             // Do something here.
                //             // this is the editor instance.
                //             // console.log('mousedown', this);
                //         }
                //     }
                // }}
                />
            </React.Fragment>
        );
    }
}

// Default 값 설정
SEditor.defaultProps = {
    readOnly: false,
}
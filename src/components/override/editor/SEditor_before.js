import React, { useState, useRef, Component } from 'react';
import ReactQuill, { Mixin, Toolbar, Quill } from "react-quill";
// import Dropzone, { ImageFile } from "react-dropzone";
import "./style.css";
import "react-quill/dist/quill.snow.css";
import { BaseConsumer } from 'utils/base/BaseComponent'
import { getValue } from 'utils/object/ContextManager';
import { reaction } from 'mobx';
// import ImageResize from 'quill-image-resize-module';
// Quill.register('modules/imageResize', ImageResize);

// import QuillBetterTable from "quill-better-table";
// import "quill-better-table/dist/quill-better-table.css";
// Quill.register({ "modules/better-table": QuillBetterTable }, true);

const __ISMSIE__ = navigator.userAgent.match(/Trident/i) ? true : false;
const __ISIOS__ = navigator.userAgent.match(/iPad|iPhone|iPod/i) ? true : false;

export default class SEditor_before extends Component {

    quill = React.createRef();
    quillEditor;

    modules = {
        // "better-table": {
        //     operationMenu: {
        //         items: {
        //             unmergeCells: {
        //                 text: "Another unmerge cells name"
        //             }
        //         }
        //     }
        // },
        // keyboard: {
        //     bindings: QuillBetterTable.keyboardBindings
        // },
        // imageResize: true,
        toolbar: {
            container: [
                ["bold", "italic", "underline", "strike", "blockquote"],
                [{ size: ["small", false, "large", "huge"] }, { color: [] }],
                [
                    { list: "ordered" },
                    { list: "bullet" },
                    { indent: "-1" },
                    { indent: "+1" },
                    { align: [] }
                ],
                ["link", "image", "video"],
                ["clean"]
            ],
        },
        clipboard: { matchVisual: true }
    };

    formats = [
        "header",
        "bold",
        "italic",
        "underline",
        "strike",
        "blockquote",
        "size",
        "color",
        "list",
        "bullet",
        "indent",
        "link",
        "image",
        "video",
        "align"
    ];


    // 저장되어 있는 이미지의 경우 <img>태그 안에 "id"로 FILE_GRP_ID가 부여되어 데이터가 저장되는데
    // react-quill에서 <img>태그를 초기화해서 id가 없어짐.
    // 기존 id를 설정하기 위하여 originData를 보관한다.
    originData;

    constructor(props) {
        super(props);
        this.state = {
            subject: "",
            contents: "", //__ISMSIE__ ? "<p>&nbsp;</p>" : "",
            workings: {},
            fileIds: [],
            visible: false,
        };
    }

    componentDidMount() {
        this._isMounted = true;
        const { id } = this.props;
        if (this.baseContext) {
            const initValue = getValue(this.baseContext, id);
            if (initValue) {
                this.setState({ contents: initValue });
            }
        }

        const ids = id.split('.');

        let index;
        const regex = /\[[0-9]*\]/;
        const result = id.match(regex);
        if (result) {
            ids[0] = ids[0].replace(result[0], "");
            index = result[0].replace(/[^0-9]/g, "");
        }

        reaction(
            () => ids.length == 2 ?
                (
                    index ? this.baseContext[ids[0]][index][ids[1]] : this.baseContext[ids[0]][ids[1]]
                )
                : this.baseContext[id],
            (value) => {
                if (this._isMounted) {
                    if (!this.originData) {
                        this.originData = document.createElement("originData");
                    }
                    this.originData.innerHTML = value;

                    this.setState({ contents: value });
                }
            }
        );

        setTimeout(() => {
            if (this._isMounted) {
                this.setState({ visible: true });
            }
        }, 0.1);

        // var toolbar = document.getElementsByClassName("ql-toolbar");
        // console.log("toolbar", toolbar);
        // toolbar.querySelector('button.ql-bold').setAttribute('title', 'Bold');
    }

    onChangeContents = (contents) => {
        if (this._isMounted) {
            this.setState({ contents: contents });
        }
    }

    _handleBlur = (e, e1) => {
        // this.quill.current.getEditor().getModule('better-table').insertTable(3, 3);

        // 태그만 있는지 테스트
        if (this.state.contents.indexOf("<img") < 0) {
            const regExp = /(<([^>]+)>)/ig;
            const emptyText = this.state.contents.replace(regExp, "");
            if (emptyText.trimLeft().trimRight() == "") {
                if (this.baseContext) {
                    const { id } = this.props;
                    this.baseContext._handleChange({ id: id, value: "" });
                }
                return;
            }
        }

        const newData = document.createElement("newData");
        newData.innerHTML = this.state.contents;
        const newImgArray = newData.getElementsByTagName("img");

        if (this.originData) {
            // 새로운 데이터에 이미지가 있으면
            if (newImgArray && newImgArray.length > 0) {
                const originImgArray = this.originData.getElementsByTagName("img");

                const originLength = originImgArray.length;
                const newLength = newImgArray.length;

                for (let i = 0; i < originLength; i++) {
                    const img = originImgArray[i];

                    if (img.id) {
                        for (let j = 0; j < newLength; j++) {
                            const newImg = newImgArray[j];
                            if (img.src == newImg.src) {
                                newImg.id = img.id;
                            }
                        }
                    }
                }
            }
        }

        if (!this.originData) {
            this.originData = document.createElement("originData");
        }
        this.originData.innerHTML = newData.innerHTML;

        if (this.baseContext) {
            const { id } = this.props;
            this.baseContext._handleChange({ id: id, value: newData.innerHTML });
        }
    }

    render() {
        const { readOnly } = this.props;

        return (
            <React.Fragment>
                {!this.baseContext ?
                    <BaseConsumer>
                        {(context) => { this.baseContext = context; }}
                    </BaseConsumer>
                    :
                    (null)
                }
                <div className="main-content" style={{ width: "100%", height: "100%" }}>
                    <ReactQuill
                        ref={this.quill}
                        value={this.state.contents}
                        onChange={this.onChangeContents}
                        onBlur={this._handleBlur}
                        theme="snow"
                        modules={this.modules}
                        formats={this.formats}
                        readOnly={readOnly}
                    />
                    {/* <Dropzone
                        ref={(el) => (this.dropzone = el)}
                        style={{ width: 0, height: 0 }}
                        onDrop={this.onDrop}
                        accept="image/*"
                    />a */}
                </div>
            </React.Fragment>
        );
    }
}

SEditor.defaultProps = {
    readOnly: false,
}


//index.html에 아래 script 선언 필요
//<!-- <script src="//tinymce.cachefly.net/4.2/tinymce.min.js"></script> -->
// import React, { useState, useRef, Component } from 'react';
// import { BaseConsumer } from 'utils/base/BaseComponent'
// import { getValue } from 'utils/object/ContextManager';
// import { reaction } from 'mobx';
// import TinyMCE from 'react-tinymce';

// export default class SEditor extends Component {

//     editor = React.createRef();

//     constructor(props) {
//         super(props);
//         this.state = {
//             content: "",
//         }
//     }

//     componentDidMount() {
//         this._isMounted = true;
//         const { id } = this.props;
//         if (this.baseContext) {
//             const initValue = getValue(this.baseContext, id);
//             if (initValue) {
//                 this.setState({ content: initValue });
//             }
//         }

//         const ids = id.split('.');

//         let index;
//         const regex = /\[[0-9]*\]/;
//         const result = id.match(regex);
//         if (result) {
//             ids[0] = ids[0].replace(result[0], "");
//             index = result[0].replace(/[^0-9]/g, "");
//         }

//         reaction(
//             () => ids.length == 2 ?
//                 (
//                     index ? this.baseContext[ids[0]][index][ids[1]] : this.baseContext[ids[0]][ids[1]]
//                 )
//                 : this.baseContext[id],
//             (value) => {
//                 if (this._isMounted) {
//                     this.setState({ content: value });
//                     // this.forceUpdate();
//                 }
//             }
//         );

//         setTimeout(() => {
//             if (this._isMounted) {
//                 this.setState({ visible: true });
//             }
//         }, 0.1);

//     }

//     componentWillUnmount() {
//         this._isMounted = false;
//     }

//     _handleBlur = (e) => {
//         const newContent = e.target.getContent();
//         this.setState({ content: newContent });

//         if (this.baseContext) {
//             const { id } = this.props;
//             this.baseContext._handleChange({ id: id, value: newContent });
//         }
//     }

//     handleEditorChange = (e) => {
//         this.setState({ content: e.target.getContent() });
//     }

//     render() {
//         const { content, config, visible } = this.state;

//         return (
//             <React.Fragment>
//                 {!this.baseContext ?
//                     <BaseConsumer>
//                         {(context) => { this.baseContext = context; }}
//                     </BaseConsumer>
//                     :
//                     (null)
//                 }
//                 <TinyMCE
//                     ref={this.editor}
//                     content={content}
//                     config={{
//                         plugins: 'autolink link image lists print preview',
//                         toolbar: 'undo redo | bold italic | alignleft aligncenter alignright'
//                     }}
//                     onChange={this.handleEditorChange}
//                     onBlur={this._handleBlur}
//                 />
//             </React.Fragment>
//         );
//     }
// }
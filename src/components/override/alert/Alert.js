
import Swal from 'sweetalert2'
import 'components/override/alert/style.css'

/**
 * !필수! 호출할 때 꼭 비동기로 호출해주세요!!
 */
class Alert {

    meg = async (message) => {
        await Swal.fire({
            html: `<font size="3">${message}</font>`
            , animation: false
        })
    }

    confirm = async (message) => {
        const { value } = await Swal.fire({
            html: `<font size="3">${message}</font>`
            , showCancelButton: true
            , cancelButtonColor: '#f1547f'
            , animation: false
        }).then((result) => {
            return result;
        });
        return value;
    }

    info() {
        console.log('error');
    }

    error() {
        console.log('error');
    }

    success() {
        console.log('success');
    }

    warning() {
        console.log('warning');
    }

}
export default new Alert();
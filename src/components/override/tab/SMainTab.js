import React, { Component, Suspense } from 'react';
import { Tabs, TabList, Tab, PanelList, Panel} from 'react-tabtab';
import 'components/override/tab/style.css'
import { inject } from 'mobx-react';

@inject(stores => ({
    closeAllMenu: stores.mainStore.closeAllMenu,
}))
export default class SMainTab extends Component {

    index = 0;

    constructor(props) {
        super(props);

        this.state = {
            template: { header: [], content: [] },
            tabs: [],
            activeIndex: undefined,
        }
    }

    handleEdit = ({ type, index }) => {
        const { onTabEdit } = this.props;

        if (onTabEdit) {
            onTabEdit({ type, index });
        }
    }

    handleTabChange = (index) => {
        const { onTabChange } = this.props;

        this.setState({ activeIndex: index });

        if (onTabChange) {
            onTabChange(index);
        }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        if (nextProps.tabs != prevState.tabs) {
            const newTabLength = nextProps.tabs.length;
            const existTabLength = prevState.tabs.length;

            // 신규로 추가된 탭이 있을 때
            if (existTabLength < newTabLength) {
                for (let i = 0; i < newTabLength; i++) {
                    const newTab = nextProps.tabs[i];
                    const tab = prevState.tabs.find(x => x.formClass == newTab.formClass);
                    if (!tab) {
                        function closeAllMenu(e) {
                            e.stopPropagation();
                            nextProps.closeAllMenu(true);
                        }
                        const tabComponent = (
                            <Tab key={`tab-${newTab.formClass}`} closable={newTab.title == "HOME" ? false : true}>
                                {newTab.title}
                                {
                                    newTab.title == "HOME" ?
                                        <i className="icon-cancel-circled" style={{ paddingLeft: '5px', cursor: 'pointer', color: '#9c6161bf' }} onClick={closeAllMenu} />
                                        :
                                        null
                                }
                            </Tab>
                        );
                        const panelComponent = (
                            <Panel key={`panel-${newTab.formClass}`} active={true}>
                                {newTab.content}
                            </Panel>
                        )

                        const header = prevState.template.header;
                        const content = prevState.template.content;

                        header.splice(i, 0, tabComponent);
                        content.splice(i, 0, panelComponent);

                        const template = { header: header, content: content };
                        return {
                            template: template,
                            tabs: nextProps.tabs,
                            activeIndex: header.length - 1 < nextProps.activeIndex ? header.length - 1 : nextProps.activeIndex, //i == 0 ? 0 : nextProps.activeIndex,
                        };
                    }
                }
            } else {
                // 삭제된 탭이 있을 떄!
                const header = prevState.template.header;
                const content = prevState.template.content;

                for (let i = existTabLength - 1; i >= 0; i--) {
                    const existTab = prevState.tabs[i];
                    const tab = nextProps.tabs.find(x => x.formClass == existTab.formClass);
                    if (!tab) {
                        header.splice(i, 1);
                        content.splice(i, 1);
                    }
                }
                const template = { header: header, content: content };
                let index = undefined;
                if (header.length <= 0) {
                    index = undefined;
                } else if (header.length - 1 < nextProps.activeIndex) {
                    index = header.length - 1;
                } else {
                    index = nextProps.activeIndex;
                }

                return {
                    template: template,
                    tabs: nextProps.tabs,
                    activeIndex: index,
                }
            }
        } else if (nextProps.activeIndex != prevState.activeIndex) {
            return { activeIndex: nextProps.activeIndex };
        }
        return null;
    }

    _handleAllClose = async () => {
        // if (await Alert.confirm("전체 탭을 닫으시겠습니까?")) {

        //     const { onChange, id, tabs } = this.props;

        //     let value = tabs;
        //     value.splice(1, tabs.length - 1);
        //     onChange({ id: id, value: toJS(value) });
        // }
    }

    render() {
        const { template, activeIndex } = this.state;
        return (
            <div className='basic'>
                {
                    activeIndex == undefined ?
                        null
                        :
                        <Suspense fallback={<div>로딩중...</div>}>
                            <Tabs onTabEdit={this.handleEdit}
                                onTabChange={this.handleTabChange}
                                activeIndex={activeIndex}
                            >
                                <TabList>
                                        {template.header}
                                </TabList>
                                <PanelList>
                                        {template.content}
                                </PanelList>
                            </Tabs>
                        </Suspense>
                }
            </div>
        )
    }
}

SMainTab.defaultProps = {
    tabs: [],
}
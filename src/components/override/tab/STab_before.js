import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Tabs, TabList, Tab, PanelList, Panel } from 'react-tabtab';
import 'components/override/tab/style.css'
import ReactDOM from "react-dom";
import styled from 'styled-components'
import { styled as tabtabStyle } from 'react-tabtab';
import { BaseConsumer } from 'utils/base/BaseComponent';
import { reaction, observable } from 'mobx';
import { observer } from 'mobx-react';

export {
    STab,
    STabPanel,
}

let { PanelStyle } = tabtabStyle;

@observer
class STab extends Component {

    currentHeight = 0;

    constructor(props) {
        super(props);

        this.state = {
            styled: undefined,
            index: 0,
            children: [],
            buttons: undefined,
            childrenData: {},
        }
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    async componentDidMount() {
        this._isMounted = true;
        this.updateTabSize();
        // window.addEventListener('resize', this.updateTabSize);

        // const children = [];
        // let buttons;
        // const childrenData = {}
        // React.Children.map(this.props.children, x => {
        //     if (x.type == "div" && x.props.className == "tab_button_item") {
        //         buttons = x;
        //     } else {
        //         if (x.props.visible != false) {
        //             if (x.props.data) {
        //                 childrenData[Object.keys(childrenData).length] = x.props.data;
        //             }

        //             children.push(x);
        //         }
        //     }
        // });

        // this.setState({ children });
        // this.setState({ buttons });

        setTimeout(() => {
            if (this._isMounted) {
                this.updateTabSize();
            }
        }, 0.1);

        const contextValue = this.baseContext[this.props.id];
        if (contextValue && contextValue != this.state.index) {
            this.setState({ index: contextValue })
        }

        reaction(
            () => this.baseContext[this.props.id],
            (index) => {
                if (this._isMounted && this.state.index != index) {
                    this.setState({ index: index })
                }
            }
        );
    }

    updateTabSize = () => {
        if (this._isMounted) {
            // 1) PanelTemplate의 Height 정의
            const getHeight = () => {
                // const me = ReactDOM.findDOMNode(this);
                // const parentRect = me.parentNode.getBoundingClientRect();
                // const top = me.getBoundingClientRect().top;
                // return parentRect.height - (top - parentRect.top) - 40; // -40은 TabHeader 길이

                const me = ReactDOM.findDOMNode(this);
                const top = me.getBoundingClientRect().top + 42; //42는 Tab Header Size + 10
                // const height = me.getBoundingClientRect().height; //42는 Tab Header Size + 10

                const parent = me.parentNode;
                const parentHeight = parent.getBoundingClientRect().height;
                console.log("getHeight #1", me.getBoundingClientRect(), me);
                console.log("getHeight #2", parent.getBoundingClientRect(), parent);
                // console.log("getHeight #2", window.outerHeight)

                return `calc(100vh - ${top}px)`;
                // return `calc(100% - 134px)`;
            }

            const getH = getHeight();
            if (this.currentHeight != getH) {
                this.currentHeight = getH;
                const CustomPanelStyle = styled(PanelStyle)`
                height: ${getH}px !important;
                background-color: transparent;
                padding: 0px; `;

                const customStyle = {
                    Panel: CustomPanelStyle
                }

                this.setState({ styled: customStyle });
            }
        }
    }

    handleTabChange = (index) => {
        const { id, onTabChange } = this.props;
        this.setState({ index: index });

        if (this.baseContext) {
            this.baseContext._handleChange({ "id": id, "value": index });
        }

        // Grid가 렌더링된 후에 실행될 수 있도록 componentDidUpdate()로 이동
        // if (onTabChange) {
        //     onTabChange(index);
        // }
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevState.index != this.state.index) {
            const { onTabChange } = this.props;
            if (onTabChange) {
                const gridCnt = ReactDOM.findDOMNode(this).getElementsByClassName('ag-theme-balham');

                // STab 내부에 SGrid가 없을 경우 onTabChange 이벤트 바로 실행
                if (gridCnt.length == 0) {
                    onTabChange(this.state.index, this.state.childrenData[this.state.index]);
                } else {
                    // STab 내부에 SGrid가 있을 경우 Grid가 모두 Ready 상태인 것을 확인한 후에 onTabChange 이벤트 실행
                    let gridList = [];
                    if (gridCnt.length > 1) {
                        for (let i = 0; i < gridCnt.length; i++) {
                            gridList.push(gridCnt[i].id);
                        }
                    } else {
                        gridList.push(gridCnt[0].id);
                    }

                    this.baseContext.__tabGridList = { gridList: gridList, onTabChange: () => onTabChange(this.state.index, this.state.childrenData[this.state.index]) };
                }
            }
        }
    }

    render() {

        // const { id } = this.props;
        // const { index, children, buttons } = this.state;
        // React.Children.map(x => {
        //     console.log("STab Render", x)
        // });

        const { id } = this.props;
        const { index } = this.state;

        const children = [];
        let buttons;
        const childrenData = {}
        React.Children.map(this.props.children, x => {
            if (x.type == "div" && x.props.className == "tab_button_item") {
                buttons = x;
            } else {
                if (x.props.visible != false) {
                    if (x.props.data) {
                        childrenData[Object.keys(childrenData).length] = x.props.data;
                    }

                    children.push(x);
                }
            }
        });

        return (
            <BaseConsumer>
                {
                    (context) => {
                        if (this.baseContext == null) {
                            this.baseContext = context;
                        }
                        return (
                            <React.Fragment>
                                {buttons}
                                <Tabs onTabChange={this.handleTabChange} activeIndex={index} customStyle={this.state.styled} showModalButton={false} showArrowButton={false}>
                                    <TabList>
                                        {children.map((x, index) => {
                                            return <Tab key={`tabHeader-${id}-${index}`}>{x.props.header}</Tab>
                                        })}
                                    </TabList>
                                    <PanelList>
                                        {children}
                                    </PanelList>
                                </Tabs>
                            </React.Fragment>
                        )
                    }
                }
            </BaseConsumer>
        )
    }
}

STab.propTypes = {
    id: PropTypes.string.isRequired,
    onTabChange: PropTypes.func,
    parentHeight: PropTypes.number,
}

STab.defaultProps = {
    parentHeight: 800,
}

/**
 * @data TabChangeEvent 발생 시 두번째 파라미터로 전달되는 object
 */
class STabPanel extends Panel {

}
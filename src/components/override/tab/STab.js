import React, { Component, PureComponent } from 'react';
import ReactDOM from "react-dom";
import { observer } from 'mobx-react';
import { action, observe, reaction } from 'mobx';
import 'components/override/tab/style.css'
import { BaseConsumer } from 'utils/base/BaseComponent';

export {
    STab,
    STabPanel,
}
@observer
class STab extends Component {

    constructor(props) {
        super(props);
        this.state = {
            index: 0,
            child: [],
            buttons: undefined,
        }
    }

    componentDidMount() {
        this._isMounted = true;
        const contextValue = this.baseContext[this.props.id];
        if (contextValue && contextValue != this.state.index) {
            this.setState({ index: contextValue });
            // this.makeChildren(contextValue);
        } else {
            // this.makeChildren(this.state.index);
        }

        reaction(
            () => this.baseContext[this.props.id],
            (index) => {
                if (this._isMounted && this.state.index != index) {
                    this.setState({ index: index });
                    // this.makeChildren(index);
                }
            }
        );
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    @action.bound
    _handleHeaderClick = (index) => {

        // const { id } = this.props;
        // const a = document.getElementById(id);

        // console.log("1) ", a);
        // console.log("2) ", a.scrollWidth, a.clientWidth);
        // console.log("1) ", a.getBoundingClientRect());

        // if (a.scrollWidth > a.clientWidth) {
        //     console.log("스크롤 있음 #######################");
        // }

        if (this.state.index != index) {
            // this.makeChildren(index);
            this.setState({ index });
            if (this.baseContext) {
                this.baseContext._handleChange({ id: this.props.id, value: index });
            }
        }
    }

    // makeChildren = (index) => {
    //     const child = [];
    //     let buttons;
    //     this.childrenData = {};

    //     React.Children.map(this.props.children, x => {
    //         if (x.type == "div" && x.props.className == "tab_button_item") {
    //             buttons = x;
    //         } else {
    //             if (x.props.visible != false) {
    //                 if (x.props.data) {
    //                     this.childrenData[Object.keys(this.childrenData).length] = x.props.data;
    //                 }

    //                 child.push(x);
    //             }
    //         }
    //     })

    //     this.setState({
    //         index: index,
    //         child: child,
    //         buttons: buttons,
    //     })
    // }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevState.index != this.state.index) {
            const { onTabChange } = this.props;
            if (onTabChange) {
                const gridCnt = ReactDOM.findDOMNode(this).getElementsByClassName('ag-theme-balham');
                // STab 내부에 SGrid가 없을 경우 onTabChange 이벤트 바로 실행
                if (gridCnt.length == 0) {
                    onTabChange(this.state.index, this.childrenData[this.state.index]);
                } else {
                    // STab 내부에 SGrid가 있을 경우 Grid가 모두 Ready 상태인 것을 확인한 후에 onTabChange 이벤트 실행
                    let gridList = [];
                    if (gridCnt.length > 1) {
                        for (let i = 0; i < gridCnt.length; i++) {
                            gridList.push(gridCnt[i].id);
                        }
                    } else {
                        gridList.push(gridCnt[0].id);
                    }
                    this.baseContext.__tabGridList = { gridList: gridList, onTabChange: () => onTabChange(this.state.index, this.childrenData[this.state.index]) };
                }
            }
        }
    }

    render() {
        const { id } = this.props;
        const { index } = this.state;

        const child = [];
        let buttons;
        this.childrenData = {};

        React.Children.map(this.props.children, x => {
            if (x.type == "div" && x.props.className == "tab_button_item") {
                buttons = x;
            } else {
                if (x.props.visible != false) {
                    if (x.props.data) {
                        this.childrenData[Object.keys(this.childrenData).length] = x.props.data;
                    }
                    
                    child.push(x);
                }
            }
        })

        return (
            <React.Fragment>
                <BaseConsumer>
                    {
                        (context) => {
                            if (this.baseContext == null) {
                                this.baseContext = context;
                            }
                        }
                    }
                </BaseConsumer>
                <div className="basic tab_item">
                    {buttons}
                    <div id={id} className={"tab_headers"}>
                        {child.map((x, i) => {
                            return <STabHeader key={`tabHeader-${id}-${i}`} header={x.props.header} index={i} active={index == i ? true : false} onClick={this._handleHeaderClick} />
                        })}
                    </div>
                    <div className="tab_border" style={{ width: '100%', height: '1px', background: '#7fb375' }}></div>
                    <div className="tab_content" style={{
                        width: '100%',
                        height: 'calc(100% - 39px)',
                        // background: this.props.background,
                    }}>
                        {child.map((x, i) => {
                            if (i == index) {
                                return child[i];
                            }
                        })}
                    </div>
                </div>
            </React.Fragment>
        )
    }
}

@observer
class STabPanel extends Component {
    render() {
        return (
            this.props.children
        )
    }
}

class STabHeader extends Component {

    _handleClick = () => {
        this.props.onClick(this.props.index);
    }

    render() {
        const { header, active, index } = this.props;

        let cn = "tab_header";
        if (active) cn += " tab_active";

        return (
            <div className={cn} onClick={this._handleClick}>
                <span className="tab_header_text">{header}</span>
            </div>
        )
    }
}

STab.defaultProps = {
    // background: '#fff',
}
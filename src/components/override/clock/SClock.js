import React, { Component } from 'react'
import moment from 'moment-timezone';
import PropTypes from 'prop-types';
import ico_kr from 'style/img/flag/ico_KR.png';
import ico_vt from 'style/img/flag/ico_CH.png';
import ico_ja from 'style/img/flag/ico_JA.png';
import ico_us from 'style/img/flag/ico_EN.png';

export default class SClock extends Component {

    dataPolling = undefined;

    constructor(props) {
        super(props);

        this.state = {
            timezone: '',
        }
    }

    componentDidMount() {
        this.setTimezone();
        this.dataPolling = setInterval(
            () => (this.setTimezone(), 60000)
        );
    }

    componentWillUnmount() {
        this.dataPolling = null;
    }

    setTimezone = () => {
        try {
            const targetTime = moment().tz(this.props.locale).format('YYYY-MM-DD, HH:mm');
            // const targetTime = moment().tz(this.props.locale).format('DD/MM/YY, HH:mm');
            // const targetTime = moment().tz(this.props.locale).format('DD/MM/YY, h:mm A');
            this.setState({ timezone: targetTime });
        } catch (ex) {
            console.warn(ex);
        }
    }

    render() {
        const { locale } = this.props;
        const { timezone } = this.state;
        const clock = timezone.split(',');
        const mmdd = clock[0];
        const hhmm = clock[1];

        let icon;


        switch (locale) {
            case "Asia/Seoul":
                icon = ico_kr;    
                break;
            case "Asia/Ho_Chi_Minh":
                icon = ico_vt;    
                break;
            case "Asia/Tokyo":
                icon = ico_ja;    
                break;
            case "America/New_York":
                icon = ico_us;    
                break;
            default:
                break;
        }

        return (
            <React.Fragment>
                <div className="clock_item" style={{ color: '#fff', fontWeight: '300' }}>
                    <div style={{float: 'left' }}>
                        <img src={icon} alt={this.cityName} data-tip='' data-for={this.props.targetDate} style={{ }} />
                    </div>
                    <div style={{ float: 'left', paddingLeft: '10px', paddingTop:'1px' }}>
                        <label style={{ fontSize: "14px", letterSpacing: "0px",color: '#aedaff' }}>
                            {mmdd}
                        </label>
                    </div>
                    <div style={{ float: 'left', paddingLeft: '5px' }}>
                        <label style={{ fontSize: "15px",fontWeight: '500' }}>
                            {hhmm}
                        </label>
                    </div>
                    
                </div>
            </React.Fragment>
        );
    }
}

SClock.propTypes = {
    locale: PropTypes.oneOf('Asia/Seoul', 'Asia/Ho_Chi_Minh'),
}

SClock.defaultProps = {
    locale: "Asia/Seoul",
}
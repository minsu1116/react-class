
import React, { Component } from 'react'
import { AgGridReact } from 'ag-grid-react';
import { observer, inject } from 'mobx-react';
import { toJS } from 'mobx';
import PropTypes from 'prop-types';
import CommonStore from 'modules/pages/common/service/CommonStore';
import NumericEditor from "components/override/grid/editor/NumericEditor";
import RowCellTextArea from 'components/override/grid/editor/RowCellTextArea';
import RowCellSelectBox from 'components/override/grid/editor/RowCellSelectBox';
import RowCellDatepicker from 'components/override/grid/editor/RowCellDatepicker';
import RowCellCheckbox from 'components/override/grid/editor/RowCellCheckbox';
import RowCellGroupCheckbox from 'components/override/grid/editor/RowCellGroupCheckbox';
import RowCellRadio from 'components/override/grid/editor/RowCellRadio';

import ContentsTemplate from 'components/template/ContentsTemplate';

import RowSpanCellRender from "components/override/grid/render/RowSpanCellRender";
import RowCellButtonRender from 'components/override/grid/render/RowCellButtonRender';
import RowCellFileLinkRender from 'components/override/grid/render/RowCellFileLinkRender';
import RowCellTextAreaRender from 'components/override/grid/render/RowCellTextAreaRender';
import CustomPinnedRowRenderer from 'components/override/grid/render/CustomPinnedRowRenderer'
import ColumnHeaderRenderer from 'components/override/grid/render/ColumnHeaderRenderer'
import GroupHeaderRenderer from 'components/override/grid/render/GroupHeaderRenderer';
import RowCheckboxHeaderRender from 'components/override/grid/render/RowCheckboxHeaderRender';
import RowCellFileDownloadRender from 'components/override/grid/render/RowCellFileDownloadRender';
import { RowCellCodeHelperRenderer } from 'components/override/grid/render/RowCellCodeHelperRenderer';
import RowCellIconButtonRender from 'components/override/grid/render/RowCellIconButtonRender';
import { SConsumer } from 'utils/context/SContext';
import { BaseConsumer } from 'utils/base/BaseComponent';
import GridUtil from 'components/override/grid/utils/GridUtil';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-balham.css';
import 'components/override/grid/style.css';
import 'components/override/grid/customStyle.css';
import 'components/template/middle_style.css';
import 'ag-grid-enterprise';
import { LicenseManager } from "ag-grid-enterprise";
import DailyCellRenderer from 'components/override/grid/render/DailyCellRenderer';
import { locale } from 'utils/word/locale'
// LicenseManager.setLicenseKey("");

import API from 'components/override/api/API';
import Alert from 'components/override/alert/Alert';
import FileUtil from 'components/atoms/file/FileUtil';

@observer
export default class SGrid extends Component {

    keyField = [];
    dataVariable = `__${this.props.grid}`;
    focusVariable = `__${this.props.grid}Focus`;
    _codeHelpers = {};

    componentDidMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
        this._isReady = false;
        if (this.gridApi) {
            if (!this.baseContext[this.dataVariable] || this.baseContext[this.dataVariable].length <= 0) {
                this.baseContext[this.focusVariable] = undefined;
            } else {
                const focusedCell = this.gridApi.getFocusedCell();
                if (focusedCell) {
                    if (!this.baseContext[this.focusVariable]) {
                        Object.defineProperty(this.baseContext, this.focusVariable, {
                            enumerable: true,
                            writable: true,
                            configurable: false,
                        })
                    }
                    this.baseContext[this.focusVariable] = this.gridApi.getFocusedCell();
                }
            }
        }
    }

    l10n = (wordCd) => {
        return locale[wordCd] ? locale[wordCd] : wordCd;
    }

    constructor(props) {
        super(props);

        this.state = {
            style: { width: '100%', height: '100%' }
            , sizeToFit: this.props.sizeToFit
            , rowDoubleClick: this.props.rowDoubleClick
            , pinnedBottomRowData: []
            , rowBuffer: this.props.rowBuffer
            , columnDefs: []
            , keyField: []
            , rowData: []
            , rowCount: 0
            , detailCellRendererParams: null
            , localeText: { noRowsToShow: " " }
            , sortable: this.props.sortable
            , editType: this.props.editType // edit 시 row 전체 수정 // fullRow, single
            , gridOptions: {
                suppressRowTransform: true // span 사용자 필요
                , stopEditingWhenGridLosesFocus: true // 그리드 외부 클릭 시 cell 편집 중지
                , enterMovesDownAfterEdit: true // enter 버튼 클릭 시 아래로 이동
                // , suppressContextMenu: true
                , rowSelection: 'multiple'
                , rowHeight: this.props.rowHeight
                , groupHeaderHeight: this.props.groupHeaderHeight
                , headerHeight: this.props.headerHeight
                , enableRangeSelection: this.props.enableRangeSelection
                , isRowSelectable: this.props.isRowSelectable
                , defaultColDef: {
                    resizable: true
                    , filter: this.props.filter // header filter
                    , editable: this.props.editable
                    //, tooltipComponent: "gridTooltip"
                    , sortable: this.props.sortable
                    , cellClass: this._getCellClass
                    , menuTabs: []
                    , pinnedRowCellRenderer: "customPinnedRowRenderer"
                }
                , defaultColGroupDef: {
                    headerGroupComponent: 'headerGroupComponent'
                }
                , autoGroupColumnDef: {
                    // minWidth: 200,
                    width: 30,
                    // filterValueGetter: function (params) {
                    //     var colGettingGrouped = params.colDef.showRowGroup;
                    //     var valueForOtherCol = params.api.getValue(
                    //         colGettingGrouped,
                    //         params.node
                    //     );
                    //     return valueForOtherCol;
                    // },
                }
                , columnTypes: {
                    "dateTime": {
                        valueFormatter: GridUtil.dateFormatter,
                    },
                    "dateTimeFull": {
                        valueFormatter: (params) => GridUtil.dateFormatter(params, 'dtm'),
                    },
                    "dateTimeSecond": {
                        valueFormatter: (params) => GridUtil.dateFormatter(params, 'dtms'),
                    },
                    "number": {
                        cellClass: 'ag-numeric-cell',
                        valueFormatter: GridUtil.numberFormatter,
                        valueParser: GridUtil.numberParser,
                    },
                    "checkbox": {
                        cellRenderer: 'rowCellCheckbox'
                    },
                    "checkboxAll": {
                        headerComponent: 'rowCheckboxHeaderRender',
                        cellRenderer: 'rowCellCheckbox',
                    },
                    "groupCheckbox": {
                        cellRenderer: 'rowCellGroupCheckbox',
                        cellClass: 'cell_align_center',
                    },
                    "radio": {
                        cellRenderer: 'rowCellRadio',
                    },
                    "button": {
                        cellRenderer: 'rowCellButtonRender'
                    },
                    "iconButton": {
                        cellRenderer: 'rowCellIconButtonRender'
                    },
                    "codeHelper": {
                        cellRenderer: 'rowCellCodeHelperRender',
                        editable: false,
                    },
                    "datePicker": {
                        cellEditor: 'rowCellDatepicker',
                        cellEditorParams: { format: "yyyy-MM-dd" },
                        valueFormatter: GridUtil.dateFormatter,
                        singleClickEdit: true,
                    },
                    "datePickerYYYYMM": {
                        cellEditor: 'rowCellDatepicker',
                        cellEditorParams: { format: "yyyy-MM" },
                        valueFormatter: (params) => GridUtil.dateFormatter(params, "yyyyMM"),
                        singleClickEdit: true,
                    },
                    "datePickerHHmm": {
                        cellEditor: 'rowCellDatepicker',
                        cellEditorParams: { format: "HH:mm" },
                        valueFormatter: (params) => GridUtil.dateFormatter(params, "HHmm"),
                        singleClickEdit: true,
                    },
                    "datePickerTime": {
                        cellEditor: 'rowCellDatepicker',
                        cellEditorParams: { format: "yyyy-MM-dd HH:mm" },
                        valueFormatter: (params) => GridUtil.dateFormatter(params, "dtm"),
                        singleClickEdit: true,
                    },
                    "textarea": {
                        cellEditor: "rowCellTextArea",
                        // cellRenderer: "rowCellTextAreaRender",
                        autoHeight: true,
                        cellStyle: { 'white-space': 'break-spaces !important' },
                    },
                    "download": {
                        cellRenderer: "rowCellFileDownloadRender",
                    },
                }

                , onRowEditingStarted: function (event) { // row 편집 시작

                }
                , onRowEditingStopped: function (event) { // row 편집 종료

                }
                , onGridSizeChanged: this._handleGridSizeChanged

                , onRowDoubleClicked: function (param) { // Row 더블 클릭 이벤트
                    if (props.rowDoubleClick) { //TODO 지우기
                        props.rowDoubleClick(param);
                    }

                    if (props.onRowDoubleClick) {
                        props.onRowDoubleClick(param);
                    }
                }
                // , onRowEditingStarted: function(param) {
                //     console.log("onRowEditingStarted", param)
                //     if (param.rowPinned == "bottom") {
                //         param.api.stopEditing();
                //     }
                // }
                , getRowClass: function (params) {
                    if (!params.data)
                        return null;

                    if (params.node.rowPinned == "bottom") {
                        return "grid-bottom-summary";
                    }

                    switch (params.data.rowType) {
                        case "TOTAL":
                            return "grid-row-total";
                        case "SUBTOTAL":
                            return "grid-row-subtotal";
                    }
                }
                , rowClassRules: {
                    'grid-edit': 'data.edit == true',
                }
                // , getRowHeight: function (params) {
                //     if (params.node && params.node.detail) {
                //         var offset = 80;
                //         var allDetailRowHeight = params.data.details.length * 26;
                //         return allDetailRowHeight + offset;
                //     } else {
                //         return 26;
                //     }
                // }
                , onCellValueChanged: this._handleCellValueChanged
                , onRowDataChanged: this._handleRowDataChanged
                , onRowDataUpdated: this._handleRowDataUpdated
                , onSelectionChanged: this._handleSelectionChanged
                , onRowSelected: this._handleRowSelected

                , suppressKeyboardEvent: this._suppressKeyboardEvent
                , frameworkComponents: {
                    rowSpanCellRender: RowSpanCellRender
                    , numericEditor: NumericEditor
                    , rowCellButtonRender: RowCellButtonRender
                    , rowCellTextArea: RowCellTextArea
                    , rowCellSelectBox: RowCellSelectBox
                    , rowCellDatepicker: RowCellDatepicker
                    , rowCellFileLinkRender: RowCellFileLinkRender
                    , rowCellCheckbox: RowCellCheckbox
                    , rowCellGroupCheckbox: RowCellGroupCheckbox
                    , rowCellTextAreaRender: RowCellTextAreaRender
                    , rowCellRadio: RowCellRadio
                    , customPinnedRowRenderer: CustomPinnedRowRenderer
                    , agColumnHeader: ColumnHeaderRenderer
                    , headerGroupComponent: GroupHeaderRenderer
                    , dailyCellRender: DailyCellRenderer
                    , rowCellCodeHelperRender: RowCellCodeHelperRenderer
                    , rowCheckboxHeaderRender: RowCheckboxHeaderRender
                    , rowCellFileDownloadRender: RowCellFileDownloadRender
                    , rowCellIconButtonRender: RowCellIconButtonRender
                }
                , overlayLoadingTemplate: '<span className="ag-overlay-loading-center">잠시만 기다려주세요.</span>' // 로딩 내용
            }
            , excelStyles: [
                {
                    id: "header",
                    alignment: {
                        horizontal: 'Center', vertical: 'Center'
                    },
                    font: {
                        fontName: 'Malgun Gothic',
                        size: 10,
                    },
                    interior: {
                        color: "#CCCCCC",
                        pattern: "Solid"
                    },
                },
                {
                    id: "cell_align_center",
                    alignment: {
                        horizontal: 'Center', vertical: 'Center'
                    },
                    font: {
                        fontName: 'Malgun Gothic',
                        size: 10,
                    },
                },
                {
                    id: "cell_align_left",
                    alignment: {
                        horizontal: 'Left', vertical: 'Center'
                    },
                    font: {
                        fontName: 'Malgun Gothic',
                        size: 10,
                    },
                },
                {
                    id: "cell_align_right",
                    alignment: {
                        horizontal: 'Right', vertical: 'Center'
                    },
                    font: {
                        fontName: 'Malgun Gothic',
                        size: 10,
                    },
                },
                {
                    id: "ag-numeric-cell",
                    alignment: {
                        horizontal: 'Right', vertical: 'Center'
                    },
                    font: {
                        fontName: 'Malgun Gothic',
                        size: 10,
                    },
                },
                {
                    id: "general",
                    alignment: {
                        horizontal: 'Left', vertical: 'Center'
                    },
                    font: {
                        fontName: 'Malgun Gothic',
                        size: 10,
                    },
                },
            ]
        }
    }

    // getRowNodeId = (data) => {
    //     if (this.state.keyField) {
    //         let id = "";
    //         this.state.keyField.map(key => {
    //             id += data[key];
    //         })
    //         return id;
    //     }
    //     return data[Object.keys(data)[0]];
    // }

    keyEditable(params) {
        if (params.data.newRow) {
            return true;
        } else {
            return false;
        }
    }
    _handleGridSizeChanged = (event) => {
        if (this.state.sizeToFit) {
            event.api.sizeColumnsToFit();
        }
    }

    _setColumn = (columnDefs) => {
        columnDefs.forEach(field => {
            if (field.children) {
                this._setColumn(field.children);
            }

            field.headerName = ""; //TODO제거: 다국어 필수 적용을 위해 headerName 강제 삭제

            if (field.word) {
                field.headerName = this.l10n(field.word);
            } else {
                field.headerName = field.name;
            }

            if (!field.tooltipField) {
                field.tooltipField = field.field;
            }

            if (field.key == true) {
                if (field.editable == undefined) {
                    field.editable = this.keyEditable;
                }
                field.essential = true;
                this.keyField = this.keyField.concat(field.field);

                // this.setState({ keyField: this.state.keyField.concat(field.field) })
                // const gridOptions = this.state.gridOptions;
                // gridOptions.getRowNodeId = this.getRowNodeId;
                // this.setState({ gridOptions: gridOptions })
            }

            if (field.getCdNm && field.getCdNm != "") {
                field.valueFormatter = function (params) { return GridUtil.getCodeNm(params, field.getCdNm); };
            }

            if (field.getCdNmByField && field.getCdNmByField != "") {
                field.valueFormatter = function (params) {
                    const cdGubn = params.data[field.getCdNmByField];
                    if (!cdGubn) {
                        return params.value;
                    }
                    return GridUtil.getCodeNm(params, cdGubn);
                };
            }

            if (field.deptCd) {

                // deptNm
                // buNm
                // bzipNm
                // teamNm
                // 등.. 원하는 부서 정보 Property명을 기입해주세요.

                field.valueFormatter = function (params) {
                    const { value } = params;
                    if (!value) {
                        return value;
                    }
                    const deptInfo = CommonStore.deptList.find(x => x.deptCd == value);
                    return deptInfo ? deptInfo[field.deptCd] : value;
                }
            }

            if (field.codeGroup && field.codeGroup != "") {
                let selectOption = GridUtil.convertSelectMap(toJS(CommonStore.codeObject[field.codeGroup]));

                let optionList = [];

                if (field.include) {
                    const includeKeys = field.include.split(',');
                    includeKeys.map(key => {
                        if (selectOption[key]) {
                            optionList[key] = selectOption[key];
                        }
                    });
                } else {
                    optionList = JSON.parse(JSON.stringify(selectOption));
                }

                if (field.addOption) {
                    const emptyOption = {};
                    emptyOption[""] = field.addOption;
                    optionList = Object.assign(emptyOption, optionList);
                }

                field.singleClickEdit = true;
                field.cellEditor = "agSelectCellEditor";
                field.cellEditorParams = { values: GridUtil.extractValues(optionList) };
                field.valueFormatter = function (params) { return GridUtil.lookupValue(optionList, params.value); };
                field.valueParser = function (params) { return GridUtil.lookupKey(optionList, params.newValue); };
            }

            if (field.optionGroup && field.optionGroup != "") {
                field.singleClickEdit = true;
                field.cellEditor = "agSelectCellEditor";
                const group = GridUtil.convertSelectMap(field.optionGroup, field.valueMember, field.displayMember);

                field.cellEditorParams = { values: GridUtil.extractValues(group) };
                field.valueFormatter = function (params) { return GridUtil.lookupValue(group, params.value); };
                field.valueParser = function (params) { return GridUtil.lookupKey(group, params.newValue); };
            }

            if (field.align && field.align != "") {
                if (field.align == "center") {
                    field.customCellClass = "cell_align_center";
                } else if (field.align == "right") {
                    field.customCellClass = "cell_align_right";
                }
            }

            if (field.type && field.type.indexOf('checkbox') >= 0) {
                if (field.word == undefined && field.name == undefined) {
                    field.headerName = "";
                }
                if (field.width == undefined) {
                    field.width = 36;
                }
                field.customCellClass = "cell_align_center";
                field.editable = false;
            }

            if (field.type == "download") {
                field.editable = false;
            }

            if (field.type == "radio") {
                field.editable = false;
                field.customCellClass = "cell_align_center";
            }

            if (field.type == 'codeHelper') {
                // field.cellRendererParams = field.helperParams;
                field.helperParams = field.helperParams;
                if (field.editable) {

                } else {
                    field.editable = false;
                }
                if (field.helperId) {
                    field.gridStore = this;
                }
            }

            if (field.type == "textarea") {
                this.setState({ editType: "single" });
            }

            if (field.format && field.format != "") {
                field.valueFormatter = function (params) {
                    if (params.node.rowPinned == "bottom") {
                        return;
                    }

                    const value = String.format(field.format, params.value);
                    return value;
                }
            }

            field.customEditable = field.editable;

            function editable(param) {
                const { colDef, data, node } = param;
                let result;

                // "합계" Row의 경우 editable: false 고정
                if (node.rowPinned == "bottom"
                    || (data.rowType && (data.rowType == "TOTAL" || data.rowType == "SUBTOTAL"))) {
                    result = false;
                } else {
                    if (colDef.customEditable != undefined) {
                        if (typeof colDef.customEditable == "function") {
                            result = colDef.customEditable(param);
                        } else {
                            result = colDef.customEditable;
                        }
                    } else {
                        result = this.props.editable;
                    }
                }
                return result;
            }
            field.editable = editable.bind(this);

            if (this.state.sortable) {
                if (field.rowSpan) {
                    this.setState({ sortable: false });
                    field.sortable = false;
                }
            } else {
                field.sortable = false;
            }
        })
    }

    setColumnDefs = (columnDefs) => {
        // STab에 들어있는 Grid의 경우 재로딩하기 때문에 소멸되었을 수도 있어서 체크.
        if (this.gridApi.gridCore.destroyed == false && this._isMounted) {
            if (columnDefs && columnDefs.length > 0) {
                if (columnDefs[0].rowStyle) {
                    this.setRowStyle(columnDefs[0].rowStyle);
                }
                if (columnDefs[0].pinnedBottomRowData) {
                    this.setState({ pinnedBottomRowData: columnDefs[0].pinnedBottomRowData })
                }
            }

            this._setColumn(columnDefs);
            this.columnDefs = columnDefs;
            this.gridApi.setColumnDefs(columnDefs);
        }
    }

    /**
     * @rowIndex 업데이트할 rowIndex. 없으면 현재 선택된 항목 중 첫 번째 항목에 UPDATE
     */
    setDataByRowIndex = (data, rowIndex) => {
        if (this.length() <= 0) return;

        let targetNodeData;
        if (rowIndex != undefined) {
            targetNodeData = GridUtil.getRowNodeByRowIndex(this.gridApi, rowIndex).data;
        } else {
            const nodes = this.gridApi.getSelectedNodes();
            if (nodes.length <= 0) return;
            targetNodeData = nodes[0].data;
        }

        Object.keys(data).map(key => {
            targetNodeData[key] = data[key];
        });

        targetNodeData.edit = true;

        this.gridApi.updateRowData({ update: [targetNodeData] });
    }

    /**
     * 그리드 데이터 업데이트
     * @rowData 변경할 rowArray
     */
    updateRowData = async (rowData) => {
        if (!rowData) return;
        rowData.forEach(x => {
            x.edit = true;
        });
        this.gridApi.updateRowData({ update: rowData });
    }

    setOption = (field, trigger, func) => {
        if (this.gridApi.gridCore.destroyed == true) return;

        let target = [];
        if (!Array.isArray(field)) {
            target.push(field);
        } else {
            target = field;
        }
        const columnDefs = this.columnDefs;
        if (field == "ALL") {
            target = columnDefs.map(x => { return x.field });
        }

        target.map(field => {
            this._setOption(columnDefs, field, trigger, func);
        })

        this.columnDefs = columnDefs;
        this.gridApi.setColumnDefs(columnDefs);
    }

    _setOption = (columnDefs, field, trigger, func) => {
        columnDefs.forEach(col => {
            if (col.children) {
                this._setOption(col.children, field, trigger, func);
            }

            if (col.field == field) {
                col[trigger] = func;
            }
        })
    }

    setSummary = (field) => {
        let target = [];
        if (!Array.isArray(field)) {
            target.push(field);
        } else {
            target = field;
        }

        const firstColumn = this.gridColumnApi.columnController.allDisplayedColumns[0];

        const pinned = [];
        target.map(field => {
            if (firstColumn) {
                pinned[firstColumn.colDef.field] = this.l10n("M03344", "합계");
            }
            pinned[field] = 0;
        })

        if (this._isMounted) {
            this.setState({ pinnedBottomRowData: [pinned] });
        }
    }

    setRowStyle = (func) => {
        if (this._isMounted) {
            this.setState(function (prevState, props) {
                const prevOption = prevState.gridOptions;
                prevOption.getRowStyle = func;
                return { gridOptions: prevOption };
            });
        }
    }

    setCellStyle = (field, func) => {
        this.setOption(field, "cellStyle", func);
    }

    setExpandedAll = (isExpand) => {
        this.gridApi.forEachNode(x => {
            if (x.group == true) {
                x.setExpanded(isExpand);
            }
        })
    }

    setMasterDetail = (detailField, detailColumnDefs) => {
        this._setColumn(detailColumnDefs);

        const detailCellRendererParams = {
            detailGridOptions: {
                columnDefs: detailColumnDefs,
                defaultColDef: {
                    resizable: true
                    , filter: this.props.filter
                    , editable: this.props.editable
                    , sortable: this.props.sortable
                    , cellClass: this._getCellClass
                    , menuTabs: []
                }
            }
            , getDetailRowData: function (params) {
                params.successCallback(params.data[detailField]);
            }
        };


        if (this._isMounted) {
            this.setState({
                detailCellRendererParams: detailCellRendererParams
                , sizeToFit: true
            });
        }
    }

    setNewRowEditable = (field) => {
        this.setOption(field, "editable", this.keyEditable);
    }

    /**
     * Row Merge
     * @field 적용할 Field
     * @refColumn 참조할 Field
     */
    setRowSpanning = (field, refColumn = null) => {
        let refTarget = undefined
        if (refColumn) {
            refTarget = [];
            if (!Array.isArray(refColumn)) {
                refTarget.push(refColumn);
            } else {
                refTarget = refColumn;
            }
        }

        const func = function (params) {
            return GridUtil.rowSpanning(params, refColumn ? false : true, refTarget)
        }

        this.setOption(field, "cellRenderer", "rowSpanCellRender")
        this.setOption(field, "rowSpan", func);
        this.setOption(field, "valueFormatter", undefined);
    }

    /**
     * 그리드에 바인딩되어 있는 모든 Data
     */
    getRows = () => {
        const rows = [];
        this.gridApi.getModel().rowsToDisplay.map(item => rows.push(item.data));
        return rows;
    }

    getCurrentIndex = () => {
        const node = this.gridApi.getSelectedNodes();
        if (node && node.length > 0) {
            return node[0].rowIndex;
        } else {
            return -1;
        }
    }

    max = (field) => {
        if (!field) return;
        if (!this.gridApi) return;
        const rows = [];

        this.gridApi.forEachNode((node) => {
            const cellValue = node.data[field];
            if (node.data.rowType && (node.data.rowType == "TOTAL" || node.data.rowType == "SUBTOTAL")) {
            } else {
                if (!isNaN(cellValue)) {
                    rows.push(node.data);
                }
            }
        });

        if (rows.length <= 0) return;
        return Math.max.apply(Math, rows.map(function (o) { return o[field]; }));
    }

    min = (field) => {
        if (!field) return;
        if (!this.gridApi) return;
        const rows = [];

        this.gridApi.forEachNode((node) => {
            const cellValue = node.data[field];
            if (node.data.rowType && (node.data.rowType == "TOTAL" || node.data.rowType == "SUBTOTAL")) {
            } else {
                if (!isNaN(cellValue)) {
                    rows.push(node.data);
                }
            }
        });

        if (rows.length <= 0) return;
        return Math.min.apply(Math, rows.map(function (o) { return o[field]; }));
    }

    sum = (field) => {
        if (!field) return;
        if (!this.gridApi) return;

        const rows = [];

        this.gridApi.forEachNode((node) => {
            const cellValue = node.data[field];
            if (node.data.rowType && (node.data.rowType == "TOTAL" || node.data.rowType == "SUBTOTAL")) {
            } else {
                if (!isNaN(cellValue)) {
                    rows.push(node.data);
                }
            }
        });

        if (rows.length <= 0) return 0;

        return rows.map(function (o) { return o[field]; }).reduce(function (accumulator, currentValue) {
            return Math.round((accumulator + Number(currentValue)) * 1e12) / 1e12;
        }, 0);
    }

    /**
     * 선택되어 있는 row 첫번째 항목의 Data
     */
    getSelectedRow = () => {
        return this.gridApi.getSelectedRows()[0];
    }

    /**
     * 선택되어 있는 모든 row의 Data
     */
    getSelectedRows = () => {
        const selectedRows = this.gridApi.getSelectedRows();
        return selectedRows.length > 0 ? selectedRows : undefined;
    }

    getCheckedRows = (field = "check") => {
        const rows = this.getRows();
        if (rows.length > 0) {
            return rows.filter(x => x[field] == "Y");
        } else {
            return null;
        }
    }

    getModifiedRows = () => {
        return this.getRows().filter(x => x.edit == true || x.newRow);
    }

    /**
     * 그리드에 바인딩되어 있는 Row Length
     */
    length = () => {
        return GridUtil.length(this.gridApi);
    }

    /**
     * 현재 그리드의 마지막 RowData를 가져온다.
     */
    getLastDisplayedRowData = () => {
        if (this.gridApi.getDisplayedRowCount() > 0) {
            return this.gridApi.getDisplayedRowAtIndex(this.gridApi.getDisplayedRowCount() - 1).data;
        } else {
            return null;
        }
    }

    clear = () => {
        this.setRowData([]);
    }

    setFocusByRowIndex = (rowIndex) => {
        if (this.length() == 0) return;
        GridUtil.setFocusByRowIndex(this.gridApi, rowIndex);
    }

    setFocusByKeyObject = (value) => {
        if (!value) return false;
        if (this.length() == 0) return false;
        if (this.keyField.length <= 0) return false;

        const targetValue = this.getKeyValue(value);

        let success = false;
        this.gridApi.forEachNode(rowNode => {
            if (success == false && this.getKeyValue(rowNode.data) == targetValue) {
                success = true;
                GridUtil.setFocus(this.gridApi, rowNode);
            }
        });

        return success;
    }

    /**
     * Grid Cell에 위치한 CodeHelper 팝업 Open 함수
     * @helperId ** 컬럼정의할 때에 전달한 helperId
     * @rowIndex CodeHelper를 열 rowIndex. 안넘기면 현재 포커싱 되어 있는 row의 CodeHelper가 열립니다.
     */
    showCodeHelper = async (helperId, rowIndex) => {
        if (!helperId) return;
        if (!rowIndex) {
            const focusedCell = this.gridApi.getFocusedCell();
            if (focusedCell) {
                rowIndex = focusedCell.rowIndex;
            } else {
                return;
            }
        }
        if (this._codeHelpers[`${rowIndex}-${helperId}`]) {
            const handler = this._codeHelpers[`${rowIndex}-${helperId}`];
            await handler();
        }
    }

    setRowData = (rowData, backup = false) => {
        // if (this._isMounted == false) {
        //     this.baseContext[this.dataVariable] = rowData;
        //     return;
        // }

        if (!this.baseContext[this.dataVariable]) {
            Object.defineProperty(this.baseContext, this.dataVariable, {
                enumerable: true,
                writable: true,
                configurable: false,
            })
        }

        if (this._isMounted && this.state.localeText.noRowsToShow == " ") {
            this.setState({ localeText: { noRowsToShow: this.l10n('M01617', '조회된 결과가 없습니다.') } })
        }

        if (backup == false) {
            this.baseContext[this.dataVariable] = rowData;
        }
        if (this.gridApi) {
            this.gridApi.setRowData(rowData);
        }
        // this.setState({ rowData: rowData });

        if (this.state.sizeToFit) {
            this.gridApi.sizeColumnsToFit();
        }

        if (this._isMounted && this.props.showRowCount) {
            this.setState({ rowCount: rowData.length })
        }
    }

    /**
     * 조회
     */
    search = async (path, params, selectedFirstRow = false) => {
        const result = await API.request.post(encodeURI(`/api${path}`), params);

        if (API.isLocal) {
            console.log('#search', `/api${path}`, { params: params }, { result: result });
        }

        if (this._isMounted && this.state.localeText.noRowsToShow == " ") {
            this.setState({ localeText: { noRowsToShow: this.l10n('M01617', '조회된 결과가 없습니다.') } })
        }

        if (result.data.success) {
            this.setRowData(result.data.data);
            if (selectedFirstRow) {
                if (result.data.data.length > 0) {
                    this.setFocusByRowIndex(0);
                } else {
                    if (this.props.onSelectionChanged) {
                        await this.props.onSelectionChanged();
                    }
                }
            }
            return true;
        } else {
            this.setRowData([]);
            return false;
        }
    }

    /**
     * 원하는 row 일반 삭제처리
     */
    deleteRow = (row) => {
        const removeResult = this.gridApi.updateRowData({ remove: [row] });
        const removeIndex = removeResult.remove[0].rowIndex;
        const focusIndex = this.length() == removeIndex ? removeIndex - 1 : removeIndex;
        if (focusIndex >= 0) {
            GridUtil.setFocusByRowIndex(this.gridApi, focusIndex);
        }
    }

    /**
     * 선택된 항목 삭제
     */
    deleteCheckedRows = async (field = "check") => {

        const deleteParams = this.getCheckedRows(field);
        const removeResult = this.gridApi.updateRowData({ remove: deleteParams });
    }

    /**
     * 선택되어 있는 row 첫번째 항목 삭제
     */
    deleteSelectedItem = async (path, searchParam = null, showMessage = true, msgShow = true) => {
        const deleteParam = this.getSelectedRow();

        if (!deleteParam || deleteParam.length <= 0) {
            await Alert.meg(this.l10n("M02418", "삭제할 항목을 선택해주세요."));
            return;
        }

        if (deleteParam.newRow) {
            if (API.isLocal) {
                console.log('#newRow-delete', { items: deleteParam });
            }
            const removeResult = this.gridApi.updateRowData({ remove: [deleteParam] });
            const removeIndex = removeResult.remove[0].rowIndex;
            GridUtil.setFocusByRowIndex(this.gridApi, this.length() == removeIndex ? removeIndex - 1 : removeIndex);
            return true;
        }

        if (msgShow) {
            const result = await Alert.confirm(this.l10n("M01492", "삭제하시겠습니까?"));
            if (!result) {
                return;
            }
        }

        return await this._delete(path, deleteParam, searchParam, showMessage);
    }

    /**
     * 선택되어 있는 row 전체 삭제
     */
    deleteSelectedItems = async (path, searchParam = null, showMessage = true) => {
        const deleteParams = this.getSelectedRows();
        if (!deleteParams || deleteParams.length <= 0) {
            await Alert.meg(this.l10n("M02418", "삭제할 항목을 선택해주세요."));
            return;
        }

        const result = await Alert.confirm(this.l10n("M01492", "삭제하시겠습니까?"));
        if (!result) {
            return;
        }
        return await this._delete(path, deleteParams, searchParam, showMessage);
    }

    /**
     * 체크되어 있는 row 전체 삭제
     */
    deleteCheckedItems = async (path, searchParam = null, showMessage = true, field = "check") => {
        const deleteParams = this.getCheckedRows(field);
        if (!deleteParams || deleteParams.length <= 0) {
            if (showMessage) {
                await Alert.meg(this.l10n("M02418", "삭제할 항목을 선택해주세요."));
            }
            return;
        }

        if (showMessage) {
            const result = await Alert.confirm(this.l10n("M01492", "삭제하시겠습니까?"));
            if (!result) {
                return;
            }
        }

        return await this._delete(path, deleteParams, searchParam, showMessage);
    }

    /**
     * 삭제할 row를 parameter로 전달받아 삭제
     */
    deleteItems = async (path, deleteParams, searchParam = null, showMessage = true) => {

        const result = await Alert.confirm(this.l10n("M01492", "삭제하시겠습니까?"));
        if (!result) {
            return;
        }

        if (!deleteParams || deleteParams.length <= 0) {
            await Alert.mes(this.l10n("M01324", "삭제할 항목이 없습니다."));
            return;
        }

        return await this._delete(path, deleteParams, searchParam, showMessage);
    }

    /**
     * 행 추가
     * @defaultValues 기본값으로 설정할 Object
     * @addOption 추가되는 위치
     */
    addRow = (defaultValues = null, addOption = "") => {
        const columnDefs = this.columnDefs;

        const addRow = {};

        columnDefs.map(item => {
            if (item.default && item.default != "") {
                addRow[item.field] = item.default;
            } else {
                addRow[item.field] = "";
            }

            if (defaultValues) {
                Object.keys(defaultValues).map(key => {
                    if (key == item.field) {
                        addRow[item.field] = defaultValues[key];
                    } else {
                        addRow[key] = defaultValues[key];
                    }
                })
            }

            // 컬럼이 select일 경우 첫번째 값 바인딩
            if ((!addRow[item.field] || addRow[item.field] == "") && (item.codeGroup || item.optionGroup)) {
                addRow[item.field] = item.cellEditorParams.values[0];
            }
        })

        let addIndex = 0;
        switch (addOption) {
            case "":
            case "lastIndex":
                addIndex = this.gridApi.getModel().length;
                break;
            case "firstIndex":
                addIndex = 0;
                break;
            case "selectedItemNextIndex":
                const nodes = this.gridApi.getSelectedNodes();
                if (nodes && nodes.length > 0) {
                    addIndex = nodes[0].rowIndex + 1;
                } else {
                    addIndex = this.gridApi.getModel().length;
                }
                break;
        }

        addRow.newRow = true;
        addRow.edit = true;

        if (this.findField("check")) {
            addRow.check = "Y";
        }

        const result = this.gridApi.updateRowData({ add: [addRow], addIndex: addIndex });
        GridUtil.setFocus(this.gridApi, result.add[0]);

        if (this.props.rowData) {
            this.props.rowData.push(addRow);
        }
    }

    /**
     * 추가/수정된 항목을 저장한다.
     * @path 저장 RestApi
     * @param 저장 후 조회하려면 조회 데이터를 넘긴다. 기본값 null
     */
    saveEditAll = async (path, param = null, showMessage = true) => {
        const editItems = GridUtil.getGridSaveArray(this.gridApi);
        let currentFocus = 0;
        if (this.getSelectedRow()) {
            currentFocus = this.gridApi.getSelectedNodes()[0].rowIndex;
        }

        if (!editItems || editItems.length <= 0) {
            if (showMessage) {
                await Alert.meg(this.l10n("M01323", "저장할 정보가 없습니다."));
            }
            return false;
        }

        if (showMessage) {
            const confirmResult = await Alert.confirm(this.l10n("M01490", "저장하시겠습니까?"));
            if (!confirmResult) {
                return;
            }
        }

        let params;

        if (param) {
            params = JSON.stringify({ param1: editItems, param2: param });
        } else {
            params = editItems;
        }

        const result = await API.request.post(encodeURI(`/api${path}`), params);

        if (API.isLocal) {
            console.log('#saveEditAll', `/api${path}`, { params: params }, { result: result });
        }

        if (result.data.success) {
            if (param) {
                this.setRowData(result.data.data);

                if (result.data.data.length > 0) {
                    GridUtil.setFocusByRowIndex(this.gridApi, this.length() == currentFocus ? currentFocus - 1 : currentFocus);
                }
            }
            if (showMessage) {
                await Alert.meg(this.l10n("M01060", "저장되었습니다."));
            }

            return true;
        } else {
            if (param) {
                this.setRowData([]);
            }
            return false;
        }
    }

    saveCheckedItems = async (path, searchParam = null, field = "check", showMessage = true) => {
        let check = false;
        const checkedItems = this.getCheckedRows(field);
        let currentFocus = 0;
        if (this.getSelectedRow()) {
            currentFocus = this.gridApi.getSelectedNodes()[0].rowIndex;
        }

        if (!checkedItems || checkedItems.length <= 0) {
            if (showMessage) {
                await Alert.meg(this.l10n("M01323", "저장할 정보가 없습니다."));
            }
            return false;
        }

        if (showMessage) {
            const confirmResult = await Alert.confirm(this.l10n("M01490", "저장하시겠습니까?"));
            if (!confirmResult) {
                return;
            }
        }

        let params;

        if (searchParam) {
            params = JSON.stringify({ param1: checkedItems, param2: searchParam });
        } else {
            params = checkedItems;
        }

        const result = await API.request.post(encodeURI(`/api${path}`), params);

        if (API.isLocal) {
            console.log('#saveCheckedItems', `/api${path}`, { params: params }, { result: result });
        }

        if (result.data.success) {
            if (searchParam) {
                this.setRowData(result.data.data);

                if (result.data.data.length > 0) {
                    GridUtil.setFocusByRowIndex(this.gridApi, this.length() == currentFocus ? currentFocus - 1 : currentFocus);
                }
            }
            if (showMessage) {
                await Alert.meg(this.l10n("M01060", "저장되었습니다."));
            }

            return true;
        } else {
            if (searchParam) {
                this.setRowData([]);
            }
            return false;
        }
    }

    /**
     * 저장할 row를 받아 저장한다.
     * @path 저장 RestApi
     * @param 저장 후 조회하려면 조회 데이터를 넘긴다. 기본값 null
     */
    saveItems = async (path, saveItems, searchParam = null, showMessage = true) => {

        if (!saveItems || saveItems.length <= 0) {
            await Alert.meg(this.l10n("M01323", "저장할 정보가 없습니다."));
            return false;
        }

        const confirmResult = await Alert.confirm(this.l10n("M01490", "저장하시겠습니까?"));
        if (!confirmResult) {
            return;
        }

        let params;

        if (searchParam) {
            params = JSON.stringify({ param1: saveItems, param2: searchParam });
        } else {
            params = saveItems;
        }

        const result = await API.request.post(encodeURI(`/api${path}`), params);

        if (API.isLocal) {
            console.log('#saveItems', `/api${path}`, { params: params }, { result: result });
        }

        if (result.data.success) {
            if (searchParam) {
                this.setRowData(result.data.data);

                if (result.data.data.length > 0) {
                    GridUtil.setFocusByRowIndex(this.gridApi, this.length() == currentFocus ? currentFocus - 1 : currentFocus);
                }
            }
            if (showMessage) {
                await Alert.meg(this.l10n("M01060", "저장되었습니다."));
            }

            return true;
        } else {
            if (searchParam) {
                this.setRowData([]);
            }
            return false;
        }
    }

    /**
     * @fileGrpId FILE_GRP_ID 정보를 전달. 없으면 전달하지 않아도 됩니다.
     * @fileInfoField 실제 파일정보가 들어있는 필드명. 기본값: fileInfo
     */
    saveFilesEditAll = async (fileGrpId = "", fileInfoField = "fileInfo") => {
        const editItems = GridUtil.getGridSaveArray(this.gridApi);
        if (!editItems || editItems.length <= 0) {
            await Alert.meg(this.l10n("M02017", "저장할 파일이 없습니다."));
            return;
        }

        try {
            const fileInfoItems = [];
            // let fileGrpId = "";
            editItems.map(item => {
                if (item[fileInfoField]) {
                    fileInfoItems.push(item[fileInfoField]);
                    // if (!fileGrpId) {
                    //     // 대표로 fileGrpId 설정
                    //     fileGrpId = item[fileGrpIdField];
                    // }
                }
            })

            return await FileUtil.fileGroupUpload(fileInfoItems, fileGrpId);
        } catch (exception) {
            console.warn("그리드 파일 저장 시 에러 발생: ", exception);
            await Alert.meg(this.l10n("M02018", "파일 저장 시 에러가 발생하였습니다."));
            return;
        }
    }

    /**
     * 필수값 입력 여부 및 기본값 중복 확인 확인
     * @checkDuplication 기본값 중복 확인 여부. 기본값 true
     */
    validation = async (checkDuplication = true) => {
        let duplicationResult = "";
        let duplicationIndex;

        let essentialResult = true;

        const columnDefs = this.columnDefs;
        const keyField = this.keyField;
        const essentialField = [];
        columnDefs.map(field => {
            if (field.essential) {
                essentialField.push(field.field);
            }
        })
        const rowData = this.getRows();

        for (let i = rowData.length - 1; i >= 0; i--) {
            // 키 값 중복 체크
            if (checkDuplication && keyField.length > 0) {
                const currentData = this._getKeysValue(rowData[i], keyField);
                const compareData = rowData.filter(x => this._getKeysValue(x, keyField) == currentData);

                if (compareData.length >= 2) {
                    duplicationIndex = i;

                    keyField.map(key => {
                        const field = this.findField(key, this.columnDefs);
                        if (field) {
                            duplicationResult = duplicationResult + `${field.headerName}: [${compareData[0][field.field]}]`;
                        }
                    });

                    break;
                }
            }

            // 필수값 체크
            if (!this._checkEmpty(rowData[i], essentialField)) {
                essentialResult = false;
                break;
            }
        }

        if (checkDuplication == true && duplicationResult != "") {
            await Alert.meg(String.format(this.l10n("M03383", "{0}<br/>값이 중복되었습니다."), duplicationResult));
            return false;
        } else if (essentialResult == false) {
            await Alert.meg(this.l10n("M03343", "필수입력항목을 확인하여 주십시오."));
            return false;
        } else {
            return true;
        }
    }

    findField = (field, columnDefs) => {
        if (!field) return;

        if (!columnDefs) {
            columnDefs = this.columnDefs;
        }

        let result;
        columnDefs.map(x => {
            if (x.children) {
                const r = this.findField(field, x.children);
                if (r) {
                    result = r;
                }
            } else {
                if (x.field == field) {
                    result = x;
                }
            }
        });

        return result;
    }

    /**
     * 해당 데이터에 그리드의 현재 설정되어 있는 key값 조합
     */
    getKeyValue = (data) => {
        const keys = this.keyField;
        let value = "";
        for (let i = 0; i < keys.length; i++) {
            value += data[keys[i]] || "";
        }
        return value;
    }

    /**
     * 해당 데이터에서 전달된 key값 조합
     */
    _getKeysValue = (data, keys) => {
        let value = "";
        for (let i = 0; i < keys.length; i++) {
            value += data[keys[i]] ? data[keys[i]] : "";
        }
        return value;
    }

    _checkEmpty = (data, keys) => {
        let check = true;
        for (let i = 0; i < keys.length; i++) {
            if (!data[keys[i]] || data[keys[i]] == "") {
                check = false;
                break;
            }
        }
        return check;
    }

    _delete = async (path, deleteParams, searchParam, showMessage) => {

        let params;
        if (searchParam) {
            params = JSON.stringify({ param1: deleteParams, param2: searchParam });
        } else {
            params = deleteParams;
        }

        let currentFocus = 0;
        if (this.getSelectedRow()) {
            currentFocus = this.gridApi.getSelectedNodes()[0].rowIndex;
        }

        const result = await API.request.post(encodeURI(`/api${path}`), params);

        if (API.isLocal) {
            console.log('#delete', `/api${path}`, { params: params }, { result: result });
        }

        if (result.data.success) {
            if (searchParam) {
                this.setRowData(result.data.data);

                if (result.data.data && result.data.data.length > 0) {
                    GridUtil.setFocusByRowIndex(this.gridApi, this.length() > currentFocus ? currentFocus : this.length() - 1);
                }
            }
            if (showMessage) {
                await Alert.meg(this.l10n("M01311", "삭제되었습니다."));
            }
            return true;
        } else {
            if (searchParam) {
                this.setRowData([]);
            }
            return false;
        }
    }

    _handleCellValueChanged = (event) => {
        this._bottomSummary(event);

        if (event.colDef.type == "number") {
            event.data[event.colDef.field] = Number(event.newValue);
        }

        if ((event.oldValue != event.newValue)) {
            event.data.edit = true;

            if (this.findField("check")) {
                event.data.check = "Y";
            }
            this.gridApi.redrawRows({ rowNodes: [event.node] });
        }

        if (this.props.onCellValueChanged) {
            this.props.onCellValueChanged(event);
        }
    }

    _handleRowDataUpdated = async (event) => {
        this._bottomSummary(event);
    }

    _handleRowDataChanged = async (event) => {
        const { api } = event;
        // rowSpan의 경우 rowBuffer 설정
        const rows = api.getModel().rowsToDisplay;

        if (rows && rows.length > 0) {

            const { rowBuffer, grid } = this.props;

            const max = Math.max.apply(null, rows.map(item => { return item.customRowSpan ? item.customRowSpan : 0 }));
            const buffer = Math.max(max, rowBuffer)
            if (this._isMounted && this.state.rowBuffer != buffer) {
                this.setState({ rowBuffer: buffer })

                if (API.isLocal) {
                    console.log('SGrid', grid, 'rowBuffer', buffer)
                }
            }
        }

        this._bottomSummary(event);

        api.setSortModel(null);

        // 임의로 포커스를 지정했을 경우 Clear하지 않는다.
        if (!event.api.setFocused) {
            event.api.clearFocusedCell();
        }
        api.setFocused = false;

        api.clearRangeSelection();

        if (this._isReady && this.baseContext[this.focusVariable]) {
            const { rowIndex, column } = this.baseContext[this.focusVariable];
            await this.__setFocus(rowIndex, column.colId);
            this.baseContext[this.focusVariable] = undefined;
        }

        if (this.props.onRowDataChanged) {
            this.props.onRowDataChanged(event);
        }
    }

    _navigateToNextCell = (params) => {
        const suggestedNextCell = params.nextCellPosition;
        const key = params.key;

        // KEY_UP, KEY_DOWN 시에도 selectionChanged Event 발생을 위하여 포커스 조정
        if (suggestedNextCell != null) {
            // KEY_LEFT, KEY_RIGHT는 제외함
            if (key != "37" && key != "39") {
                this.setFocusByRowIndex(suggestedNextCell.rowIndex);
            }
        }

        return suggestedNextCell;
    }

    _handleSelectionChanged = (event) => {
        const { suppressRowClickSelection } = this.props;

        if (event != null && suppressRowClickSelection) {
            event.api.clearRangeSelection();
        }
    }

    _handleRowSelected = async (event) => {
        if (event.node.selected) {
            const { onSelectionChanged } = this.props;

            // KEY_UP, KEY_DOWN 시에도 selectionChanged Event 발생을 위하여 포커스 조절할 때
            // unSelected일 때도 SelectionChanged Event가 중복으로 호출되어 rowSelected Event 이동
            if (onSelectionChanged) {
                if (this.gridApi._tabChangeFocus == true) {
                    this.gridApi._tabChangeFocus = false;
                } else {
                    await onSelectionChanged(event);
                }
            }
        }
    }

    currentRowIndex = undefined;

    _handleCellClicked = (event) => {
        const { onCellClicked } = this.props;

        const currentDef = event.colDef;

        // singleClickEdit: true 옵션으로 처리
        // // selectCell, datePickerCell일 경우에는 한번 눌렀을 때 editing Mode로 변경
        // if (currentDef.cellEditor == "agSelectCellEditor" || currentDef.cellEditor == "rowCellDatepicker") {
        //     event.api.startEditingCell({ rowIndex: event.rowIndex, colKey: currentDef.field });
        // }

        if (onCellClicked) {
            onCellClicked(event);
        }
    }

    _bottomSummary = (event) => {
        const grid = event.api;
        const { pinnedBottomRowData } = this.state;

        if (pinnedBottomRowData && pinnedBottomRowData.length > 0) {
            if (event.type == "cellValueChanged") {
                const changeColumn = event.column.colDef.field;

                if (Object.keys(pinnedBottomRowData[0]).indexOf(changeColumn) < 0) {
                    return;
                }
            }

            const bottomData = pinnedBottomRowData[0];
            for (let key in bottomData) {
                let sumArray = [];

                if (!isNaN(bottomData[key])) {
                    grid.forEachNode((node) => {
                        const cellValue = node.data[key];

                        if (node.data.rowType && (node.data.rowType == "TOTAL" || node.data.rowType == "SUBTOTAL")) {
                        } else {
                            if (!isNaN(cellValue)) {
                                sumArray.push(Number(cellValue));
                            }
                        }
                    });
                    const v = sumArray.reduce(function (accumulator, currentValue) {
                        return +(accumulator + currentValue).toFixed(8);
                    }, 0);
                    bottomData[key] = v;
                }
            }

            grid.setPinnedBottomRowData(pinnedBottomRowData);
        }
    }

    _getCellClass = (params) => {

        let cellClass = [];
        if (!this.props.cellReadOnlyColor) {

            cellClass.push('general');

            if (params.colDef.type == 'numericColumn') {
                cellClass.push('ag-numeric-cell');
            }

            if (params.colDef.customCellClass) {
                cellClass.push(params.colDef.customCellClass);
            }

            return cellClass;
        }

        let editable;
        if (typeof params.colDef.editable === "function") {
            editable = params.colDef.editable(params);
        } else {
            editable = params.colDef.editable;
        }

        if (!editable) {

            cellClass.push('general');

            if (params.colDef.type == 'numericColumn') {
                cellClass.push('ag-numeric-cell');
            }

            if (params.colDef.customCellClass) {
                cellClass.push(params.colDef.customCellClass);
            }

            cellClass.push('cell_readonly');

            return cellClass;
        }

        cellClass.push('general');
        return cellClass;
    }

    _suppressKeyboardEvent = (params) => {

        if (params.editing) {
            return false;
        }

        var event = params.event;
        var key = event.which;

        const KEY_C = 67;
        // let KEY_ENTER = 13;
        const KEY_DELETE = 46;
        const KEY_BACKSPACE = 8;
        // const KEY_SPACE = 32;

        // Ctrl+C 눌렀을 때 셀 하나만 선택되어 있으면 셀만 복사
        if (key == KEY_C && event.ctrlKey) {
            const rangeSelections = params.api.getCellRanges();

            if (rangeSelections.length == 1) {
                const rangeSelection = rangeSelections[0];
                if (rangeSelection.startRow == rangeSelection.endRow
                    && rangeSelection.columns.length == 1) {
                    params.api.copySelectedRangeToClipboard();
                    return true;
                }
            }
        }

        // CodeHelper의 경우 데이터 지우기
        if (key == KEY_DELETE || key == KEY_BACKSPACE) {
            if (params.colDef.type == "codeHelper") {
                const { api, data, colDef, node } = params;

                if (data[colDef.field] != "") {
                    data[colDef.field] = "";
                    data.edit = true;
                    if (this.findField("check")) {
                        data.check = "Y";
                    }

                    api.redrawRows({ rowNodes: [node] });

                    if (colDef.onHelperResult) {
                        colDef.onHelperResult();
                    }

                    return true;
                }
            }
        }

        return false;
    }

    _getContextMenuItems = (params, context) => {
        params.api.clearRangeSelection();
        var result = [
            {
                name: this.l10n("M02020", "엑셀저장 (.xlsx)"),
                action: () => this._exportExcel(params, (context.menuNm ? context.menuNm : "없음")),
            },
            // {
            //     name: "엑셀업로드 (.xlsx)",
            //     action: () => this._exportExcel(params, context.menuNm),
            // }
        ];
        return result;
    }

    _exportExcel = (params, menuNm) => {

        const today = new Date().format("yyyyMMddHHmm");
        const excelStyle = {
            fileName: `${today}_${menuNm}`,
            sheetName: `${today}_${menuNm}`,
            rowHeight: 16.5,
            headerRowHeight: 16.5,
        }

        var excelList = this.getRows();
        var filterList = this.columnDefs.filter(x => x.codeGroup != undefined)
        if (filterList) {
            excelList.forEach(element => {
                filterList.forEach(key => {
                    if (element.hasOwnProperty(key.field)) {
                        var valueChangeList = toJS(CommonStore.codeObject[key.codeGroup])
                        valueChangeList.forEach(value => {
                            if (element[key.field] == value.cd) {
                                element[key.field] = value.cdNm;
                            }
                        });
                    }
                });
            });
        }

        // 엑셀 다운로드
        params.api.exportDataAsExcel(excelStyle);

        if (filterList) {
            excelList.forEach(element => {
                filterList.forEach(key => {
                    if (element.hasOwnProperty(key.field)) {
                        var valueChangeList = toJS(CommonStore.codeObject[key.codeGroup])
                        valueChangeList.forEach(value => {
                            if (element[key.field] == value.cdNm) {
                                element[key.field] = value.cd;
                            }
                        });

                    }
                });
            });
        }
    }

    _onGridReady = params => {
        this.gridApi = params.api;
        this.gridColumnApi = params.columnApi;

        params.api.setDomLayout(this.props.domLayout); // normal, autoHeight
        this.props.gridApiCallBack(this);

        // 탭 내부에 있는 Grid가 미리 조회되었을 경우 rowData와 동기화
        const { rowData } = this.props;

        if (this.baseContext) {
            this.baseContext._loadedGrid(this.props.grid);
        }
        if (rowData && rowData.length > 0) {
            this.setRowData(rowData);
        } else {
            if (this.baseContext) {
                if (this.baseContext[this.dataVariable]) {
                    this.setRowData(this.baseContext[this.dataVariable], true);
                    // this.setState({ rowData: this.baseContext[this.dataVariable] });
                }
            }
        }

        this._isReady = true;
    };

    __setFocus = (rowIndex, colId) => {
        this.gridApi._tabChangeFocus = true;
        this.gridApi.setFocusedCell(rowIndex, colId);
        const rowNode = GridUtil.getRowNodeByRowIndex(this.gridApi, rowIndex);
        if (rowNode) {
            rowNode.setSelected(true);
        }
    }

    getSnapshotBeforeUpdate(prevProps, prevState) {
        // 탭 내부에 있는 Grid가 미리 조회되었을 경우 rowData와 동기화
        if (this.props.rowData && this.props.rowData != prevProps.rowData) {
            this.setRowData(this.props.rowData);
        }
        return null;
    }

    // _handleFirstDataRendered = (event) => {
    //     if (this.state.sizeToFit) {
    //         event.api.sizeColumnsToFit();
    //     }
    // }

    render() {
        const {
            columnDefs
            , grid
            , suppressRowClickSelection
            , domLayout
            , subTitle
            , showRowCount
            , isSmall
        } = this.props;
        const { rowData, rowBuffer, gridOptions, rowCount, editType } = this.state;

        let style = { width: "100%", height: "100%" };
        if (domLayout == "autoHeight") {
            style = {};
        }

        return (
            <React.Fragment>
                <BaseConsumer>
                    {
                        (context) => {
                            if (this.baseContext == null) {
                                this.baseContext = context;
                            }
                        }
                    }
                </BaseConsumer>

                <SConsumer>
                    {
                        (context) => {
                            return (
                                <div id={grid}
                                    ref={(div) => { this.gridDiv = div; }}
                                    className="ag-theme-balham"
                                    style={style}
                                    gridtype={isSmall ? "small" : undefined}
                                // gridtype="small"
                                >
                                    {subTitle ? (
                                        <div className="middle_wrap" type={"noShadow"}>
                                            <div className="sub_tit">
                                                {subTitle}
                                            </div>
                                        </div>)
                                        : (null)
                                    }
                                    <ContentsTemplate id={'grid_contents'} shadow={false} height={this.props.height}>
                                        <div style={this.state.style}>
                                            <AgGridReact
                                                // columnDefs={columnDefs}
                                                gridOptions={gridOptions}
                                                onGridReady={this._onGridReady}
                                                // suppressRowTransform={true}
                                                rowData={rowData}
                                                rowBuffer={rowBuffer}
                                                editType={editType}
                                                suppressKeyboardEvent={this.state.suppressKeyboardEvent}
                                                pinnedBottomRowData={this.state.pinnedBottomRowData}
                                                onCellClicked={this._handleCellClicked}
                                                navigateToNextCell={this._navigateToNextCell}
                                                suppressRowClickSelection={suppressRowClickSelection}
                                                getContextMenuItems={(params) => this._getContextMenuItems(params, context)}
                                                excelStyles={this.state.excelStyles}
                                                detailCellRendererParams={this.state.detailCellRendererParams}
                                                masterDetail={this.props.masterDetail}
                                                localeText={this.state.localeText}
                                                groupHideOpenParents={true}
                                                enableBrowserTooltips={true}
                                            // pagination ={true}
                                            // paginationPageSize = {5}
                                            // onFirstDataRendered={this._handleFirstDataRendered}
                                            />
                                        </div>
                                    </ContentsTemplate>
                                    {showRowCount == true ?
                                        <label className="item_lb r_count">{`RECORD COUNT: ${rowCount}`}</label>
                                        :
                                        null
                                    }
                                </div>
                            )
                        }
                    }
                </SConsumer>

            </React.Fragment>
        );
    }
}

// Type 체크
SGrid.propTypes = {
    rowHeight: PropTypes.number,
    editType: PropTypes.string
}

// Default 값 설정
SGrid.defaultProps = {
    rowHeight: 23,
    headerHeight: 26,
    groupHeaderHeight: 26,
    editable: true,
    editType: "fullRow",
    sizeToFit: false,
    rowData: undefined,
    // rowData: [],
    sortable: true,
    filter: false,
    cellReadOnlyColor: false,
    enableRangeSelection: true,
    suppressRowClickSelection: false, //그리드에 체크박스가 있으면 해당 옵션 true 설정
    domLayout: "normal",
    rowBuffer: 20,
    showRowCount: false,
    masterDetail: false,
    isSmall: false,
}
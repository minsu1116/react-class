import { toJS } from 'mobx'
import CommonStore from 'modules/pages/common/service/CommonStore'
import { locale } from 'utils/word/locale'

class GridUtil {

    constructor(root) {
        this.root = root;
    }

    convertSelectYnMap(data) {

        // 구분
        let checkCode = true;

        // 대륙별 구분
        let object05 = {}
        let object06 = {}
        let object07 = {}
        let object08 = {}

        // 결과 
        let result = {};

        for (let index = 0; index < data.length; index++) {
            const element = data[index];

            if (data[index].cdGubn == 'AC004') {
                switch (element.desc1) {
                    case '미주':
                        object05[element.cd] = element.cdNm
                        break;
                    case '유럽':
                        object06[element.cd] = element.cdNm
                        break;
                    case '동남아':
                        object07[element.cd] = element.cdNm
                        break;
                    case '중동':
                        object08[element.cd] = element.cdNm
                        break;
                }
            }
            else {
                checkCode = false;
                if (data[index].useFlag == 'Y') {
                    if (element.desc2 == "2306") {
                        object2306[element.cd] = element.cdNm;
                    } else if (element.desc2 == "5630") {
                        object5630[element.cd] = element.cdNm;
                    } else if (element.desc2 == "5720") {
                        object5720[element.cd] = element.cdNm;
                    }
                }
            }
        }

        if (checkCode) {
            result['05'] = object05;
            result['06'] = object06;
            result['07'] = object07;
            result['08'] = object08;
        }

        return result;
    }

    l10n = (wordCd) => {
        return locale[wordCd] ? locale[wordCd] : wordCd;
    }

    extractValues(mappings) {
        return Object.keys(mappings).sort((a,b) => (a === this.l10n("M00233", "선택") || a === this.l10n("M00322", "전체")) - (b === this.l10n("M00233", "선택") || b === this.l10n("M00322", "전체")) || (a - b));
    }

    extractKeys(mappings) {
        return Object.values(mappings).sort();
    }

    // LookUp 관련 Method

    convertSelectMap(data, cd = "cd", cdNm = "cdNm") {
        let result = new Object();
        
        for (let i = 0; i < data.length; i++) {
            const element = data[i];
            result[element[cd]] = element[cdNm];
        }

        return result;
    }

    lookupValue = (mappings, key) => {
        return mappings[key];
    }

    lookupKey = (mappings, name) => {
        for (var key in mappings) {
            if (mappings.hasOwnProperty(key)) {
                if (name === mappings[key]) {
                    return key;
                }
            }
        }
    }

    setSelectCell = (columnDef, list, cd = "cd", cdNm = "cdNm") => {

        columnDef["cellEditor"] = "agSelectCellEditor";

        columnDef["cellEditorParams"] = {
            
            values: this.extractValues(list)
        }

        columnDef["valueFormatter"] = (params) => {
            return this.lookupValue(list, params.value);
        }

        columnDef["valueParser"] = (params) => {
            return this.lookupKey(list, params.newValue);
        }
    }

    setSelectCellByCode = (columnDef, codeGroup) => {

        const selectOption = this.convertSelectMap(toJS(CommonStore.codeObject[codeGroup]));
        this.setSelectCell(columnDef, selectOption);
    }

    setSelectCellByList = (columnDef, optionGroup, cd = "cd", cdNm = "cdNm") => {

        const selectOption = this.convertSelectMap(toJS(optionGroup), cd, cdNm);
        this.setSelectCell(columnDef, selectOption);
    }

    numberOnly(param) {
        if (!param.value || param.value == '') return '';
        const number = Number(param.value);

        if (typeof param.value === 'string') {
            param.value = 0;
            return 0;
        }

        // if (typeof param.value === 'string') {
        //     return param.value.replace(/[^0-9\.]+/g, '');
        // }
        return param.value.toLocaleString(navigator.language, { minimumFractionDigits: 0 });
    }

    numberParser(params) {
        if (Number(params.newValue)) {
            const decimalPoint = params.colDef.decimalPoint;
            const strArr = params.newValue.split('.');
            if (strArr.length == 1) {
                return Number(params.newValue);
            } else {
                const point = strArr[1].substr(0, decimalPoint);
                return Number(`${strArr[0]}.${point}`);
            }
        } else {
            return 0;
        }
    }

    formatNumber(number) {
        if (!number) return;
        return Math.floor(number)
            .toString()
            .replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
    }

    numberFormatter = (params) => {
        const decimalPoint = params.colDef.decimalPoint ? params.colDef.decimalPoint : 0;
        return this.numberFormat(params.value, decimalPoint);
    }

    numberFormat = (value, decimalPoint = 0) => {

        // 기존 ::  소수점 길이를 지정하지 않은 경우, DB에서 읽어온 대로 보여줬는데,
        //          0 으로 처리하게 되면 소수점 이하 자리가 안 보임.
        //          일단 기존에 나오던 대로 처리함.
        if (!value) {
            return value;
            // params.value = 0; // 0
        }

        if (isNaN(value)) {
            return value; // null
        }
        let data = String(Number(value));
        const regx = new RegExp(/(-?\d+)(\d{3})/);
        let bExists = data.indexOf(".", 0);
        let strArr = data.split('.');
        while (regx.test(strArr[0])) {
            strArr[0] = strArr[0].replace(regx, "$1,$2");
        }

        // 소수점 자리수에 대해 고정되게 나오도록 처리 
        // decimalPoint = 2 인 경우 1.1 => 1.10
        if (decimalPoint == 0) {
            data = strArr[0];
        } else {
            let decimalValue = "";

            if (bExists > -1) {
                decimalValue = strArr[1];
            }

            if (decimalValue.length > decimalPoint) {
                decimalValue = decimalValue.substring(0, decimalPoint);
            } else {
                decimalValue = decimalValue.padEnd(decimalPoint, "0");
            }

            data = strArr[0] + "." + decimalValue;
        }

        return data;
    }

    numberValue(data) {
        if (isNaN(data.newValue) || data.newValue == "") {
            return 0;
        }
        return Number(data.newValue);
    }

    // 그리드에 선택된 row 업데이트
    updateGridOneRow(gridApi, updateData) {
        gridApi.getSelectedNodes()[0].setData(updateData);
    }

    // 그리드에서 선택된 값을 Array로 리턴
    getGridCheckArray(gridApi) {
        const selectedRows = gridApi.getSelectedRows();
        let arrayData = [];
        selectedRows.forEach(rowNode => {
            arrayData.push(rowNode);
        }
        );

        return arrayData;
    }

    // 그리드에 선택된 데이터 가져오기
    getGridCheckData(gridApi) {
        return gridApi.getSelectedRows()[0];
    }

    // 그리드 저장 시 Eidt 된 데이터 Array로 리턴
    getGridSaveArray(gridApi) {
        gridApi.stopEditing();

        let updateRows = [];

        gridApi.forEachNode(rowNode => {
            if (rowNode.data.edit || rowNode.data.newRow) {
                updateRows.push(rowNode.data);
            }
        }
        );
        return updateRows;
    }

    // 특정 column의 유효성 검사
    checkGridColumn(gridApi, column) {
        let result = true;
        gridApi.forEachNode(rowNode => {
            for (let i = 0; i < column.length; i++) {
                if (eval(`rowNode.data.${column[i]}`) == "") {
                    result = false;
                    break;
                }
            }
        })
        return result;
    }
    //그리드에 데이터 바인딩 시 전체 데이터를 수정 모드로 변경
    setGridAllEditRowData(gridApi, rowData, dataCheckColumn) {

        const length = rowData.length;
        const isCheck = dataCheckColumn ? true : false;

        if (isCheck) {
            for (let i = 0; i < length; i++) {
                if (eval(`rowData[i].${dataCheckColumn}`)) {
                    rowData[i]['edit'] = true;
                }
            }

        } else {
            for (let i = 0; i < length; i++) {
                rowData[i]['edit'] = true;
            }
        }

        gridApi.setRowData(rowData);
    }


    formatCurrency(param) {
        const value = param.value;
        if (!value && value != "0") {
            return "";
        }
        return value.toLocaleString(navigator.language, { minimumFractionDigits: 0 });
    }

    /**
     * 그리드 데이터 span 할 수 있도록 변환
     * @param {*} resultData 
     * @param {*} object 
     * @param {*} columnName 
     */
    convertData(resultData, object, columnName) {

        let current = null;
        let cnt = 0;

        const length = resultData.length;
        for (let i = 0; i < length; i++) {
            const element = resultData[i];
            let data = eval(`element.${columnName}`);

            if (data != current) {
                if (cnt > 0) {
                    object[current] = cnt;
                }
                current = data;
                cnt = 1;
            } else {
                cnt++;
                eval(`resultData[${i}].${columnName}=''`);
            }
        }
        if (cnt > 0) {
            object[current] = cnt;
        }
    }

    autoSizeAll(gridColumnApi) {
        var allColumnIds = [];
        gridColumnApi.getAllColumns().forEach(function (column) {
            allColumnIds.push(column.colId);
        });
        gridColumnApi.autoSizeColumns(allColumnIds);
    }

    rowSpanning(params, isOnlyTarget, refColumn) {
        let cols = params.columnApi.getAllColumns();
        let rows = params.api.getModel().rowsToDisplay;
        let columnNm = params.colDef.field;
        let rowIndex = params.node.rowIndex;
        let data = null;
        let prevData = null;
        let nextData = null;
        let result = 1;

        if (rows.length <= 0) {
            return result;
        }

        const valueCheck = rows[rowIndex].data[columnNm];
        if (!valueCheck || valueCheck == "") {
            return result;
        }

        if (refColumn) {
            if (refColumn.indexOf(columnNm) < 0) {
                refColumn.push(columnNm);
            }
        }
        data = this.cellValueCombine(cols, rows[rowIndex], columnNm, isOnlyTarget, refColumn);

        if (params.node.rowIndex > 0) {
            prevData = this.cellValueCombine(cols, rows[rowIndex - 1], columnNm, isOnlyTarget, refColumn);
        }

        if (data != prevData) {
            for (let i = 1; rowIndex + i < rows.length; i++) {
                nextData = this.cellValueCombine(cols, rows[rowIndex + i], columnNm, isOnlyTarget, refColumn);

                if (data == nextData) {
                    result = result + 1;
                } else {
                    break;
                }
            }
        }

        params.colDef[`customRowSpan_${rowIndex}`] = result; //RowSpanCellRender에서 autoHeight일 때 height 계산 시에 사용
        params.node.customRowSpan = Math.max(params.node.customRowSpan ? params.node.customRowSpan : 0, result); //SGrid rowBuffer 설정에서 사용

        return (result);
    }

    cellValueCombine(cols, row, toColunmnNm, isOnlyTo, refColumn) {
        let returnData = "";

        if (isOnlyTo == undefined || isOnlyTo == true) {
            returnData = row.data[toColunmnNm];
        } else {
            for (let i = 0; i < cols.length; i++) {
                if (cols[i].visible == false) {
                    continue;
                }

                if (refColumn == undefined) {
                    returnData = returnData + row.data[cols[i].colDef.field];
                } else {
                    for (let j = 0; j < refColumn.length; j++) {
                        if (cols[i].colDef.field == refColumn[j]) {
                            returnData = returnData + row.data[cols[i].colDef.field];
                        }
                    }
                }

                if (cols[i].colDef.field == toColunmnNm) {
                    // break;
                    continue;
                }
            }
        }

        return returnData;
    }

    colSpanning = (params) => {
        const { columnApi, data, column } = params;

        if (data == null) {
            return 1;
        }

        const allDisplayedColumns = columnApi.columnController.allDisplayedColumns;
        const currentColId = column.colId;
        const currentIndex = allDisplayedColumns.findIndex(x => x.colId == currentColId);
        const nextColumn = columnApi.getDisplayedColAfter(columnApi.getColumn(currentColId));
        let span = 1;

        if (nextColumn) {
            for (let i = currentIndex; i < allDisplayedColumns.length; i++) {
                const item = allDisplayedColumns[i];

                if (data[item.colDef.field] == null) {
                    break;
                }

                const _nextColumn = params.columnApi.getDisplayedColAfter(item);

                if (_nextColumn) {

                    if (data[item.colDef.field] == data[_nextColumn.colDef.field]) {
                        span++;
                    } else {
                        break;
                    }
                }
            }
        }
        return span;
    }

    getRowNodeByRowIndex(grid, rowIndex) {
        const model = grid.getModel();

        if (model == null) {
            return null;
        }

        return model.rowsToDisplay[rowIndex];
    }

    length(grid) {
        return grid.getDisplayedRowCount();
    }

    // targetRow: Grid에서 선택할 RowNode
    setFocus(grid, targetRow) {
        if (!targetRow) return;
        const row = grid.getRowNode(targetRow.id);

        // rowIndex에 행이 없을 경우
        if (!row) return;

        const focusedCell = grid.getFocusedCell();

        grid.clearRangeSelection();

        // 선택된 Cell이 있을 경우 Cell Focus
        if (focusedCell != null) {

            // 기존 SelectedRow 해제
            const currentSelectedRowIndex = focusedCell.rowIndex;
            if (currentSelectedRowIndex >= 0 && this.length(grid) > currentSelectedRowIndex) {
                this.getRowNodeByRowIndex(grid, currentSelectedRowIndex).setSelected(false);
            }
            grid.clearFocusedCell();
            grid.clearRangeSelection();

            const colId = focusedCell.column.colId;
            grid.setFocusedCell(targetRow.rowIndex, colId);
        }
        // 선택된 Cell이 없을 경우 Visible=true인 첫번째 Cell Focus
        else {
            const visibleColumns = grid.columnController.gridColumns ? grid.columnController.gridColumns.filter(x => x.visible == true) : undefined;

            if (!visibleColumns) return;

            grid.setFocusedCell(targetRow.rowIndex, visibleColumns[0].colId);
        }

        row.setSelected(true);
        grid.ensureIndexVisible(targetRow.rowIndex);
        grid.setFocused = true;
    }

    setFocusByRowIndex(grid, rowIndex) {
        this.setFocus(grid, this.getRowNodeByRowIndex(grid, rowIndex));
    }

    setFocusByKey(grid, key, value) {
        grid.forEachNode((rowNode, index) => {
            if (rowNode.data[key] == value) {
                this.setFocus(grid, rowNode);
            }
        })
    }

    setSelectByKey(grid, key, values) {
        grid.forEachNode(rowNode => {
            if (values.find(x => x[key] == rowNode.data[key])) {
                rowNode.setSelected(true);
            }
        })
    }

    setSelect(grid, from, to) {

        if (from > to) {
            return;
        }

        if (grid.getDisplayedRowCount() < to) {
            return;
        }

        grid.deselectAll();

        for (let i = from; i <= to; i++) {
            const row = grid.getDisplayedRowAtIndex(i - 1);
            row.setSelected(true);
        }
    }

    getCodeNm(params, cdGubn) {

        if (!params.value) {
            return params.value;
        }

        const codeGroup = CommonStore.codeObject[cdGubn];
        if (codeGroup) {
            const filtering = codeGroup.filter(x => x.cd == params.value);
            if (filtering.length > 0) {
                return filtering[0].cdNm;
            }
        }

        return params.value;
    }

    dateFormatter(params, type = '') {

        const { value } = params;

        try {

            if (type == 'dtm') {
                return value.substring(0, 16);
            }

            if (type == 'dtms') {
                return value.substring(0, 19);
            }

            if (type == "yyyyMM") {
                return `${value.substring(0, 4)}-${value.substring(4, 6)}`;
            }

            // nvarchar Date Case '20190101'
            if (value.length == 8) {
                return `${value.substring(0, 4)}-${value.substring(4, 6)}-${value.substring(6, 8)}`;
            }
            // DateTime Case
            else if (value.length > 10) {
                return value.substring(0, 10);
            }
            // nvarchar Time Case '0700'
            else if (value.length == 4) {
                return `${value.substring(0, 2)}:${value.substring(2, 4)}`;
            }
        } catch {
            return value;
        }

        return value;
    }

    getSelectedRowData(grid) {
        const selectedCell = grid.getFocusedCell();
        if (selectedCell) {
            return this.getRowNodeByRowIndex(grid, selectedCell.rowIndex).data;
        } else {
            return null;
        }
    }

    findColumnDef = (columnDefs, targetField) => {

        for (let i = 0; i < columnDefs.length; i++) {
            const item = columnDefs[i];

            if (item.children) {
                const find = this.findColumnDef(item.children, targetField);
                if (find) {
                    return find;
                }
            } else {
                if (item.field == targetField) {
                    return item;
                }
            }
        }
    }
}

//Singleton
const instance = new GridUtil();
export default instance
import React, { Component } from "react";

export default class RowCellRadio extends Component {

    constructor(props) {
        super(props);
        this.state = {
            value: false,
            visible: true,
        }
    }

    componentDidMount() {
        const { value, visible } = this.props;
        if (value == "Y") {
            this._dataInit();
            this.setState({ value: true })
        }
    }

    getSnapshotBeforeUpdate(prevProps, prevState) {
        if (prevProps.visible != this.props.visible) {
            return { visible: this.props.visible };
        }

        if (prevProps.value != this.props.value) {
            if (this.props.value == "Y") {
                this._dataInit();
                return { value: true }
            } else {
                return { value: false }
            }
        }

        return null;
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (snapshot) {
            if (snapshot.visible) {
                this.setState({ visible: snapshot.visible });
            }
            if (snapshot.value) {
                this.setState({ value: snapshot.value })
            }
        }

    }
    _handleChange = (event) => {
        //_handleClick Event에서 처리
    }

    _handleClick = (event) => {
        const { data, } = this.props;
        const { id, checked } = event.target;

        if (checked == true) {
            this.setState({ value: checked });

            data[id] = checked == true ? "Y" : "N";
            this._dataInit(data);
        }
    }

    _dataInit = () => {
        const { field } = this.props.colDef;
        const gridApi = this.props.api;
        const data = this._getRows(gridApi);

        const values = data.filter(x => x[field] == "Y" && x != this.props.data);

        values.forEach(x => x[field] = "N");
        gridApi.updateRowData({ update: values });
    }

    _getRows = (gridApi) => {
        return gridApi.getModel().rowsToDisplay.map(item => {
            return item.data;
        });
    }

    render() {
        const { field } = this.props.colDef;
        const { visible } = this.props;

        if (visible == false) {
            return null;
        } else {
            return (
                <input type='checkbox' ref="inputcheckbox" id={field} checked={this.state.value} onChange={this._handleChange} onClick={this._handleClick} />
            );
        }
    }
}
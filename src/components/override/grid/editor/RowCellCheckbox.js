import React, { Component } from "react";

export default class RowCellCheckbox extends Component {

    type = "checkbox";

    constructor(props) {
        super(props);
        this.state = {
            value: false,
            visible: true,
        }
        this.type = this.props.column.colDef.type;
    }

    componentDidMount() {
        const { value, visible } = this.props;
        if (value == "Y") {
            this.setState({ value: true })
        }

        // if (visible != undefined) {
        //     Object.defineProperty(this.props.data, '_checkboxVisible', {
        //         enumerable: false,
        //         // writable: false,
        //         // configurable: false,
        //         value: visible
        //     })
        // }
    }

    getSnapshotBeforeUpdate(prevProps, prevState) {
        // if (prevProps.visible != this.props.visible) {
        //     Object.defineProperty(this.props.data, '_checkboxVisible', {
        //         enumerable: false,
        //         // writable: false,
        //         // configurable: false,
        //         value: this.props.visible
        //     })
        //     return { visible: this.props.visible };
        // }

        if (prevProps.value != this.props.value) {
            if (this.props.value == true) {
                return { value: true }
            } else {
                return { value: false }
            }
        }

        return null;
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (snapshot) {
            // if (snapshot.visible) {
            //     this.setState({ visible: snapshot.visible });
            // }
            if (snapshot.value) {
                this.setState({ value: snapshot.value })
            }
        }
    }

    _handleChange = (event) => {
        //_handleClick Event에서 처리
    }

    _handleClick = (event) => {
        const { data, } = this.props;
        const { id, checked } = event.target;
        const { field, onChecked } = this.props.colDef;

        this.setState({ value: checked });

        const value = checked == true ? "Y" : "N";
        data[id] = value;
        data.edit = true;

        if (onChecked) {
            onChecked(value, data);
        }

        if (this.type == "checkboxAll") {
            this.props.column._onChecked(event.target.checked);
        }
    }

    render() {
        const { field } = this.props.colDef;
        const { visible } = this.props;

        if (visible == false || this.props.data.rowType != undefined) {
            return null;
        } else {
            return (
                <input type='checkbox' ref="inputcheckbox" id={field} checked={this.state.value} onChange={this._handleChange} onClick={this._handleClick} />
            );
        }
    }
}
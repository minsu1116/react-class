import React, { Component } from "react";
import { toJS } from "mobx";

export default class RowCellGroupCheckbox extends Component {

    constructor(props) {
        super(props);
        this.state = {
            value: false,
            visible: true,
        }
    }

    componentDidMount() {
        const { value, visible } = this.props;
        if (value == "Y") {
            this.setState({ value: true })
        }
    }

    getSnapshotBeforeUpdate(prevProps, prevState) {
        if (prevProps.value != this.props.value) {
            if (this.props.value == true || this.props.value == "Y") {
                return { value: true }
            } else {
                return { value: false }
            }
        }

        return null;
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (snapshot) {
            if (snapshot.value) {
                this.setState({ value: snapshot.value })
            }
        }
    }

    _handleChange = (event) => {
        //_handleClick Event에서 처리
    }

    _handleClick = (event) => {
        const { data, node } = this.props;
        const { id, checked } = event.target;
        const { field, onChecked } = this.props.colDef;

        this.setState({ value: checked });

        const value = checked == true ? "Y" : "N";

        if (node.group == true) {
            node.allLeafChildren.map(x => {
                x.data[id] = value;
                x.data.edit = true;
            })
        } else {
            if (data.depth == 0) {
                node.parent.allLeafChildren.map(x => {
                    x.data[id] = value;
                    x.data.edit = true;
                })

                this.props.api.redrawRows({ rowNodes: node.parent.allLeafChildren });
            } else {
                data[id] = value;
                data.edit = true;
            }
        }

        if (onChecked) {
            onChecked(value, field);
        }
    }

    render() {
        const { field } = this.props.colDef;
        const { visible } = this.props;

        if (visible == false) {
            return null;
        } else {
            return (
                <input type='checkbox' ref="inputcheckbox" id={field} checked={this.state.value} onChange={this._handleChange} onClick={this._handleClick} />
            );
        }
    }
}
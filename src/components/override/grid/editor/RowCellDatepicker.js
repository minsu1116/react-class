import React, { Component } from "react";
import NumberFormat from 'react-number-format';
import 'utils/object/DateFormat'

export default class RowCellDatepicker extends Component {

    mask = "";
    InitialValue = "";

    constructor(props) {
        super(props);

        this.state = {
            value: "",
        }

        if (this.props.format == "yyyy-MM") {
            this.mask = "####-##";
        } else if (this.props.format == "yyyy-MM-dd HH:mm") {
            this.mask = "####-##-## ##:##"
        } else if (this.props.format == "HH:mm") {
            this.mask = "##:##"
        } else {
            this.mask = "####-##-##"
        }
    }

    componentDidMount() {
        const { value } = this.props;
        this.InitialValue = value;
        this.setState({ value })
    }

    getValue = () => {
        return this.state.value;
    }

    isPopup() {
        return false;
    }

    date = "";
    _handleChange = (e) => {
        const value = e.target.value;

        if (String.isDateTime(value, this.props.format)) {
            if (this.props.format == "yyyy-MM-dd HH:mm") {
                this.setState({ value: value })
            } else {
                this.setState({ value: value.replace(/[^0-9]/g, '') })
            }
        } else if (!value) {
            if (this.props.colDef.essential == true) {
            } else {
                this.setState({ value: undefined });
            }
        }
    }

    _handleBlur = (e) => {
        const value = e.target.value;

        if (String.isDateTime(value, this.props.format) == false) {
            if (this.props.colDef.essential == true) {
                this.setState({ value: this.InitialValue });
            } else {
                this.setState({ value: undefined });
            }
        }
    }

    _handleFocus = (e) => {
        this.input.select();
    }

    render() {
        const { field } = this.props.colDef;
        return (
            <NumberFormat
                id={field}
                className={'input_box date'}
                style={{
                    textAlign: 'center !important',
                }}
                value={this.state.value}
                decimalScale={0}
                fixedDecimalScale={false}
                onChange={this._handleChange}
                onBlur={this._handleBlur}
                onFocus={this._handleFocus}
                format={this.mask}
                mask={"_"}
                getInputRef={(el) => this.input = el}
            />
        );
    }
};
import React, { Component } from "react";
import PropTypes from 'prop-types';
export default class RowCellTextArea extends Component {

    initValue = "";

    constructor(props) {
        super(props);
        this.state = {
            value: props.value,
        }
    }

    componentDidMount() {
        this._isMounted = true;
        this.initValue = this.props.value;
        this.textarea.addEventListener('keydown', this.onKeyDown);
    }

    afterGuiAttached() {
        this.textarea.focus();

        const length = this.textarea.value ? this.textarea.value.length : 0;
        if (length > 0) {
            this.textarea.setSelectionRange(length, length);
        }

        this.textarea.style.height = `${this.textarea.scrollHeight}px`;
    }

    componentWillUnmount() {
        this.textarea.removeEventListener('keydown', this.onKeyDown);
        this._isMounted = false;
        this.props.api.stopEditing();
        if (this.initValue != this.state.value) {
            setTimeout(() => this.reset(this.props.api, this.props.node), 1);
        }
    }

    reset = (api, node) => {
        api.resetRowHeights();

        // rowSpan처리된 rows면 모든 로우를 다시 그림
        if (node.customRowSpan) {
            api.redrawRows();
        }
    }

    getValue() {
        return this.state.value;
    }

    onKeyDown = (event) => {
        this.textarea.style.height = '26px';
        this.textarea.style.height = this.textarea.scrollHeight + 'px';

        if (event.keyCode == 13) {
            event.stopPropagation();
            return;
        }
    }

    handleChange = (event) => {
        this.setState({ value: event.target.value });
    }

    isPopup() {
        return true;
    }

    render() {
        const columnWidth = this.props.eGridCell.clientWidth;
        const style = {
            width: `${columnWidth}px`,
            overflow: "visible",
            maxHeight: "200px",
        }

        const { maxLength } = this.props;
        return (
            <div style={style}>
                <textarea
                    ref={(ref) => this.textarea = ref}
                    maxLength={maxLength}
                    style={style}
                    value={this.state.value || ""}
                    onChange={this.handleChange} />
            </div>
        );
    }
};

// Type 체크
RowCellTextArea.propTypes = {

};

// Default 값 설정
RowCellTextArea.defaultProps = {
    maxLength: 200
}
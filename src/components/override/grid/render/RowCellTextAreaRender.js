import React, { Component } from "react";

export default class RowCellTextAreaRender extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        let index = 0;
        let spanCtrl = '';

        if (this.props.value == null) {
            spanCtrl = (<span key={index++}></span>);
        }
        else {

            // rowSpan이 적용된 컬럼일 때 row-span class 추가
            if (this.props.column.colDef.rowSpan != null) {
                this.props.eGridCell.className = this.props.eGridCell.className + ' ag-row-span';
            }

            spanCtrl =
                this.props.value.split('\n').map((line, x) => {
                    return (<span key={index++}>{line}<br /></span>)
                });
        }

        return (
            <div>
                <div>
                    {spanCtrl}
                </div>
            </div>
        );
    }
};

// Type 체크
RowCellTextAreaRender.propTypes = {
};

// Default 값 설정
RowCellTextAreaRender.defaultProps = {

}

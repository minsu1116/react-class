import React, { Component } from "react";
import SButton from "components/atoms/button/SButton";
import CodeHelper from "modules/pages/common/codehelper/CodeHelper";

export { RowCellCodeHelperRenderer, }

class RowCellCodeHelperRenderer extends Component {

    constructor(props) {
        super(props);
        this.state = {
            newRowWriteOnly: undefined,
            searchField: undefined,
        }
    }

    componentDidMount() {
        this._isMounted = true;
        const { helperParams } = this.props.colDef;

        if (helperParams) {
            let result;
            if (typeof helperParams == "function") {
                result = helperParams();
            } else {
                result = helperParams;
            }

            if (result.newRowWriteOnly && this._isMounted) {
                this.setState({ newRowWriteOnly: result.newRowWriteOnly })
            }

            if (result.searchField && this._isMounted) {
                this.setState({ searchField: result.searchField })
            }
        }
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    _handleResult = (result, cdProp, cdNmProp) => {
        const { field, onHelperResult } = this.props.colDef;
        const { data } = this.props;

        if (result) {
            data[field] = result[cdProp];
            data.edit = true;

            this.props.api.redrawRows({ rowNodes: [this.props.node] });
        }

        if (onHelperResult) {
            onHelperResult(result, cdProp, cdNmProp);
        }
    }

    render() {
        const { visible, value, node, data } = this.props;
        const { field, business, helperParams, gridStore, helperId } = this.props.colDef;
        const { newRowWriteOnly, searchField } = this.state;

        let codeHelperReadOnly = false;
        if (newRowWriteOnly) {
            codeHelperReadOnly = data.newRow == true ? false : true;
        }

        let search;
        // 같은 Row의 특정 필드 값을 "조회조건"으로 설정하기 위한 옵션
        if (searchField) {
            search = data[searchField];
        }

        if (visible == false) {
            return <div style={{ float: 'left' }}>{value}</div>
        } else if (node.rowPinned == "bottom") {
            return null;
        } else {
            return (
                <React.Fragment>
                    <div style={{ float: 'left' }}>{value}</div>
                    <CodeHelper business={business} onResult={this._handleResult} codeVisible={false} codeNmVisible={false} readOnly={codeHelperReadOnly} {...this.props}
                        gridCell={true} codeHelperCell={search} helperParams={helperParams} gridStore={gridStore} helperId={helperId} rowIndex={node.rowIndex} />
                </React.Fragment>
            )
        }
    }
};
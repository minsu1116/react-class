import React, { Component } from "react";


export default class RowHeaderCheckBoxRender extends Component {

    constructor(props) {
        super(props);
        this.onClick = this.onClick.bind(this);
    }

    onCheckBoxChange() {
        this.selectAll = false;
        if (this.state.checkStatus == "inactive") {
            this.setState({
                checkStatus: "active"
            });
            this.selectAll = true;
        } else {
            this.setState({
                checkStatus: "inactive"
            });
        }
        
        try {
            if (this.props.column.colDef.HeaderChangeCallback) {
                this.props.column.colDef.HeaderChangeCallback(this.selectAll);
            }
        } catch (e) { }
    }
    render() {
        return (
            <div class="ag-header-cell" style={{ "padding": "4px 4px 4px 5px" }}>
                <div class="ag-header-cell-resize" />
                <div class="ag-header-select-all" />
                <div class="ag-header-component" />
                <input id="checkboxMember" type="checkbox" onClick={this.onCheckBoxChange.bind(this)} className="check" checked={this.state.checkStatus == "active" ? true : false} />
            </div>
        );
    }
};
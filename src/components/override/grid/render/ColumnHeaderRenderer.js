import React, { Component } from 'react';

export default class ColumnHeaderRenderer extends Component {

    _isMounted = false;

    constructor(props) {
        super(props);

        this.state = {
            ascSort: false,
            descSort: false,
            text: <span></span>,
        }
    }

    componentDidMount() {
        this._isMounted = true;
        this.props.agGridReact.gridOptions.api.addEventListener('rowDataChanged', this.handleRowDataChanged);

        const { displayName, api } = this.props;
        const { colId } = this.props.column;

        let headerLength = 0;
        if (displayName) {
            const names = String(displayName).split('\n');
            const length = names.length;
            const display = [];

            names.map((text, i) => {
                headerLength++;
                if (i == length - 1) {
                    display.push(<span key={`${colId}-${i}`}>{text}</span>);
                    return;
                }
                display.push(<span key={`${colId}-${i}`}>{text}<br /></span>);
            });

            this.setState({ text: display.map(x => { return x }) })
        } else {
            headerLength++;
            this.setState({ text: <span>{displayName}</span> })
        }

        if (headerLength > 1) {
            if (!api.headerLength || api.headerLength < headerLength) {
                api.headerLength = headerLength;

                const newHeight = ((26 - 4) * headerLength);
                api.setHeaderHeight(newHeight);
            }
        }
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    handleRowDataChanged = (params) => {
        if (this._isMounted == false) {
            return;
        }

        this.setState({ ascSort: false })
        this.setState({ descSort: false })
    }

    handleSortChanged = () => {
        if (this._isMounted == false) {
            return;
        }

        this.setState({ ascSort: false })
        this.setState({ descSort: false })

        const { column, api } = this.props;

        if (!column.colDef.sortable) {
            return;
        }

        const colId = column.colId;

        if (column.isSortAscending()) {
            this.setState({ descSort: true })
            var sort = [{ colId: colId, sort: "desc" }];
            api.setSortModel(api.getSortModel().concat(sort));

        } else if (column.isSortDescending()) {
            var sortModel = api.getSortModel();
            var currentIndex = sortModel.findIndex(x => x.colId == colId);
            sortModel.splice(currentIndex, 1);
            api.setSortModel(sortModel);

        } else {
            this.setState({ ascSort: true })
            var sort = [{ colId: colId, sort: "asc" }];
            api.setSortModel(api.getSortModel().concat(sort));
        }
    }

    render() {

        const { column } = this.props;

        // let editable = false;

        // if (typeof column.colDef.editable != "function") {
        //     editable = column.colDef.editable;s
        // }

        const essential = column.colDef.essential ? column.colDef.essential : false;

        let ascClassName;
        let descClassName;

        if (this.state.ascSort) {
            ascClassName = "ag-header-icon ag-sort-order";
        } else {
            ascClassName = "ag-header-icon ag-sort-order ag-hidden";
        }

        if (this.state.descSort) {
            descClassName = "ag-header-icon ag-sort-descending-icon";
        } else {
            descClassName = "ag-header-icon ag-sort-descending-icon ag-hidden";
        }

        return (
            <React.Fragment>
                <div className="ag-cell-label-container" role="presentation" onClick={this.handleSortChanged} style={{ cursor: 'hand', height: '100%' }}>
                    <div ref="eLabel" className="ag-header-cell-label" role="presentation" unselectable="on">
                        {/* {editable == true ?
                            <span style={{ color: '#F15234' }}>*</span>
                            :
                            null
                        } */}
                        {essential == true ?
                            <span style={{ color: '#F15234' }}>*</span>
                            :
                            null
                        }
                        <span ref="eText" className="ag-header-cell-text" role="columnheader" unselectable="on" title={this.props.displayName} style={{ height: '100%' }}>
                            {this.state.text}
                        </span>
                        <span ref="eSortAsc" className={ascClassName} aria-hidden={!this.state.ascSort}>
                            <span className="ag-icon ag-icon-asc" unselectable="on">
                            </span>
                        </span>
                        <span ref="eSortDesc" className={descClassName} aria-hidden={!this.state.descSort}>
                            <span className="ag-icon ag-icon-desc" unselectable="on">
                            </span>
                        </span>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

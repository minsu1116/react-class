import React, { Component } from "react";
import SButton from "components/atoms/button/SButton";

export default class RowCellButtonRender extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { buttonName, onButtonClick } = this.props.colDef;
        const { visible, on, text } = this.props;

        let btnName = buttonName;
        if (this.props.buttonName) {
            btnName = this.props.buttonName;
        }

        if (visible == false) {
            if (text) {
                return text;
            } else {
                return null;
            }
        } else {
            return (<SButton
                buttonName={btnName}
                onClick={onButtonClick}
                type={'default'}
                on={on}
            />);
        }
    }
};
import React, { Component } from 'react';

export default class RowCheckboxHeaderRender extends Component {

    _isMounted = false;

    constructor(props) {
        super(props);

        this.state = {
            checked: false,
        }
    }

    componentDidMount() {
        this._isMounted = true;
        this.props.column._onChecked = this._handleAllCheckbox;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    _handleAllCheckbox = (checked) => {
        if (checked == false) {
            if (this.state.checked == true) {
                this.setState({ checked: false })
            }
        } else {
            const field = this.props.column.colDef.field;
            const gridApi = this.props.api;
            const data = this.getRows(gridApi);
            const unSelectedItems = data.filter(x => !x[field] || x[field] == "N")
            if (unSelectedItems.length == 0) {
                this.setState({ checked: true });
            } else {
                this.setState({ checked: false });
            }
        }
    }

    _handleCheckClick = (event) => {
        const field = this.props.column.colDef.field;
        const gridApi = this.props.api;
        const data = this.getRows(gridApi);

        if (data.length <= 0) return;

        if (this.state.checked == true) {
            const unSelectedItems = data.filter(x => !x[field] || x[field] == "N")
            if (unSelectedItems.length > 0) {
                unSelectedItems.forEach(x => {
                    if (x.rowType == undefined) {
                        x[field] = "Y";
                    }
                })
                gridApi.updateRowData({ update: unSelectedItems });
                gridApi.redrawRows();
                this.setState({ checked: true });
            } else {
                data.forEach(x => {
                    if (x.rowType == undefined) {
                        x[field] = "N";
                    }
                })
                gridApi.updateRowData({ update: data });
                this.setState({ checked: false });
            }
        } else {
            data.forEach(x => {
                if (x.rowType == undefined) {
                    x[field] = "Y";
                }
            })
            gridApi.updateRowData({ update: data });
            gridApi.redrawRows();
            this.setState({ checked: true });
        }
    }

    getRows = (gridApi) => {
        const rows = [];
        gridApi.getModel().rowsToDisplay.map(item => rows.push(item.data));
        return rows;
    }

    render() {
        // const essential = column.colDef.essential ? column.colDef.essential : false;

        return (
            <div className="ag-cell-label-container" role="presentation">
                <input id="checkBoxAll" type="checkbox" className="check" onChange={this._handleCheckClick} checked={this.state.checked} style={{ width: '100%' }} />
            </div>
        )
    }
}

import React, { Component } from "react";
import FileUtil from 'components/atoms/file/FileUtil';
import Alert from "components/override/alert/Alert";
import API from "components/override/api/API";
import SModal from "components/override/modal/SModal";
import { FileDownloadHelper } from "modules/pages/common/codehelper/fileDownload/FileDownloadHelper";

export default class RowCellFileDownloadRender extends Component {

    helperList = undefined;
    docId = undefined;

    constructor(props) {
        super(props);
        this.state = {
            fileNameField: undefined,
            helperOpen: false,
            helperList: undefined,
        };
    }

    componentDidMount() {
        if (this.props.colDef.fileNm) {
            this.setState({ fileNameField: this.props.colDef.fileNm })
        }
    }

    _handleClick = async () => {
        const { value, byFileId } = this.props;
        const { docReadLog } = this.props.colDef;

        if (docReadLog) {
            this.docId = this.props.data.docId;
        }

        if (byFileId) {
            await this.fileDownload();
        } else {
            const result = await API.request.get(`/api/file/getfileinfo?fileGrpId=${value}`);
            const { data } = result;
            if (data.success) {
                if (data.data.length == 1) {
                    await this.fileDownload();
                } else if (data.data.length > 1) {
                    this.helperList = data.data;
                    this.setState({ helperOpen: true });
                } else {
                    Alert.meg('다운로드 중 문제가 발생 하였습니다.');
                    return;
                }
            } else {
                Alert.meg('다운로드 중 문제가 발생 하였습니다.');
                return;
            }
        }
    }

    fileDownload = async () => {
        await FileUtil.fileDownload(this.props.value, this.docId);
    }

    _handleModalClose = () => {
        this.setState({ helperOpen: false });
    }

    render() {
        const { value, data } = this.props;
        const { fileNameField } = this.state;

        let control;

        if (value) {
            if (fileNameField) {
                control = <span className={"download_cell"} onClick={this._handleClick} style={{ cursor: 'pointer !important' }}>{data[fileNameField]}</span>;
            } else {
                if (data.fileCnt && data.fileCnt >= 2) {
                    control = <span className={"download_cell"} onClick={this._handleClick} style={{ cursor: 'pointer !important' }}><i className={'icon-folder'} /></span>;
                } else {
                    control = <span className={"download_cell"} onClick={this._handleClick} style={{ cursor: 'pointer !important' }}><i className={'icon-download'} /></span>;
                }
            }
        } else {
            if (fileNameField) {
                control = <span>{data[fileNameField]}</span>;
            } else {
                (null)
            }
        }

        return (
            <React.Fragment>
                {control}
                <SModal
                    id={"downloadHelper"}
                    contentLabel={"파일 다운로드"}
                    isOpen={this.state.helperOpen}
                    width={625}
                    height={400}
                    onRequestClose={this._handleModalClose}
                >
                    <FileDownloadHelper excludeData={this.helperList} docId={this.docId} />
                </SModal>
            </React.Fragment>
        )
    }
}
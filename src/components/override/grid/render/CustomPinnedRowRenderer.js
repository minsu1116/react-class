import React, { Component } from 'react';
import GridUtil from 'components/override/grid/utils/GridUtil'

export default class CustomPinnedRowRenderer extends Component {

    render() {
        const { value, column } = this.props;

        return (
            <span>
                {GridUtil.numberFormat(value, column.colDef.decimalPoint)}
            </span>
        );
    }
};
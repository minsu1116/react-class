import React, { Component } from "react";
import ico_search from 'style/img/ico_search.svg';

/**
 * @buttonIcon \src\style\fonts\entypo\demo.html 에서 Icon 명칭.
 */
export default class RowCellIconButtonRender extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        const { buttonIcon, onButtonClick } = this.props.colDef;
        const { visible, value } = this.props;

        if (visible == false) {
            return null;
        } else {
            return (
                <React.Fragment>
                    {value}
                    <span onClick={onButtonClick} style={{ cursor: 'pointer !important' }}>
                        {
                            buttonIcon == "icon-search" ?
                                <span className="btn_ico cell_icon">
                                    <a style={{ cursor: "pointer" }}>
                                        <img className="ico" src={ico_search} alt="" />
                                    </a>
                                </span>
                                :
                                (
                                    <i className={`${buttonIcon} cell_icon`} />
                                )
                        }
                    </span>
                </React.Fragment>
            )
        }
    }
};
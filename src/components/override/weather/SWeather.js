import React, { Component } from 'react'

import cloud from 'style/img/weather/ico_cloud.png';
import cloudy from 'style/img/weather/ico_cloudy.png';
import foog from 'style/img/weather/ico_foog.png';
import rainy from 'style/img/weather/ico_rainy.png';
import snowy from 'style/img/weather/ico_snowy.png';
import sun from 'style/img/weather/ico_sun.png';
import thunder from 'style/img/weather/ico_thunder.png';
import tem from 'style/img/weather/ico_tem.png';
import windy from 'style/img/weather/ico_windy.png';
import humidy from 'style/img/weather/ico_humid.png';

export default class SWeather extends Component {

    constructor(props) {
        super(props);
    }


    render() {
        const { model, cityGroup } = this.props;
        
        let iconCd = "" ;
        let temper = model.main.temp;
        let humid  = model.main.humidity;
        let wind   = model.wind.speed;
        let cityNm = cityGroup.filter(x=> x.cd == model.cityNm)[0] ? cityGroup.filter(x=> x.cd == model.cityNm)[0].cdNm : '-';


        if(temper.toString().split('.').length > 1) {
            const temp = temper.toString().split('.');
            temper = temp[0];
        }

        if(model.weather[0]) {
            iconCd = model.weather[0].icon;
        }

        let imgSrc = undefined;
        let imgWidth  = '54%';
        switch(iconCd) {
            case '01d' :
            case '01n' :
                imgSrc = sun;
                imgWidth = "50%";
            break;
            case '02d' :
            case '02n' :
                imgSrc = cloudy;
                imgWidth = "48%";
            break;
            case '03d' :
            case '03n' :
            case '04d' :
            case '04n' :
                imgSrc = cloud;
            break;
            case '09d' :
            case '09n' :
            case '10d' :
            case '10n' :
                imgSrc = rainy;
                imgWidth = "50%";
            break;
            case '11d' :
            case '11n' :
                imgSrc = thunder;
                imgWidth = "50%";
            break;
            case '13d' :
            case '13n' :
                imgSrc = snowy
                imgWidth = "50%";
            break;
            case '50d' :
            case '50n' :
                imgSrc = foog
                imgWidth = "50%";
            break;
            
        }

        return (
            <React.Fragment>
                <div className="weather_item" >
                    <div className="weather_main">
                         <img className="weather_main_img" src={imgSrc} style={{width : imgWidth}} />
                    </div>
                    <div className="weather_middle">
                         <img className="weather_middle_img" src={tem} />
                         <span className="weather_middle_txt">{`${temper}`}</span><span style={{fontWeight: 900, fontSize: '15px'}}>℃</span>
                         <img className="weather_middle_img" src={humidy} />
                         <span className="weather_middle_txt" style={{marginLeft: "7px"}}>{`${humid}`}</span><span style={{fontWeight: 900, fontSize: '15px'}}>%</span>
                    </div>
                    <div className="weather_bottom">
                         <span className="weather_bottom_txt">{cityNm}</span>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

SWeather.defaultProps = {
    model: null,
    cityGroup : null
}
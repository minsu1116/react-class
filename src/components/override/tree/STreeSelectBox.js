import React, { Component } from 'react'
import TreeSelect, { SHOW_PARENT } from 'rc-tree-select';
import PropTypes from 'prop-types';
import CommonStore from 'modules/pages/common/service/CommonStore'
import { BaseConsumer } from 'utils/base/BaseComponent'
import { getValue } from 'utils/object/ContextManager.js'
import 'rc-tree-select/assets/index.css';
import 'components/override/tree/style.css'
import { reaction } from 'mobx';

export default class STreeSelectBox extends Component {

    constructor(props) {
        super(props);
        this.state = {
            value: [],
            optionList: [],
            simpleSearchValue: "",
            tooltip: "",
        };
    }

    async componentDidMount() {
        this._isMounted = true;
        const { codeGroup, optionGroup, id } = this.props;

        // codeGroup : 공통코드 가져와서 Binding.
        // optionGroup : Custom List Binding.
        if (codeGroup) {
            await this.getCodeGroup(codeGroup);
        } else {
            this.setList(optionGroup);
        }

        const ids = id.split('.');

        reaction(
            () => ids.length == 2 ? this.baseContext[ids[0]][ids[1]] : this.baseContext[id],
            (value) => {
                if (this._isMounted) {
                    this.setValue(value, this.state.optionList);
                }
            }
        );
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    setValue = (value, option) => {
        if (!value) return;

        const { titleMember } = this.props;

        if (option && option.length > 0) {
            const split = value.split(",");
            const result = [];
            let check = true;

            split.map(key => {
                const data = option.find(x => x.value == key);
                if (data) {
                    result.push({ label: data.title, value: data.value });
                }
            });

            this.setState({ value: result });
        }

    }
    getCodeGroup = async (codeGroup) => {
        const { excludeCode } = this.props;

        await CommonStore.getCodeObject().then(codeObject => {

            let option = codeObject[codeGroup].concat();

            if (excludeCode) {
                const removeItem = excludeCode.split(',');

                removeItem.map(item => {
                    const index = option.findIndex(x => x.cd == item);
                    if (index >= 0) {
                        option.splice(index, 1);
                    }
                });
            }

            this.setList(option);
        });
    }

    setList = (optionList) => {
        const { addOption, valueMember, displayMember } = this.props;

        // if (addOption && !optionList.some(x => x[valueMember] == "")) {
        //     let all = {};
        //     all[valueMember] = "";
        //     all[displayMember] = addOption;

        //     optionList.unshift(all);
        // }

        this.makeMultiObject(optionList);
        // this.setState({ optionList });
    }

    async UNSAFE_componentDidUpdate(nextProps) {
        if (nextProps.codeGroup) {
            if (this.props.codeGroup != nextProps.codeGroup) {
                await this.getCodeGroup(nextProps.codeGroup);
            }
        } else {
            if (JSON.stringify(this.props.optionGroup) != JSON.stringify(nextProps.optionGroup)) {
                this.setList(nextProps.optionGroup);
            }
        }
    }

    makeMultiObject = (codeObject) => {
        const { pIdMember, titleMember, valueMember, splitText, id } = this.props;

        const multiObject = (
            codeObject.map((item => {

                const key = eval(`item.${valueMember}`);
                let pId = eval(`item.${pIdMember}`);
                const title = eval(`item.${titleMember}`);
                const value = eval(`item.${valueMember}`);

                if (!pId) {
                    pId = 0;
                }

                return { key: key, pId: pId, title: title, value: value };
            }))
        )

        this.setState({ optionList: multiObject });
        if (this.baseContext) {
            const value = getValue(this.baseContext, id);
            this.setValue(value, multiObject);
        }
    }

    handleChange = (value) => {
        const { id, onChange, splitText, multiple } = this.props;

        let resultText = '';
        if (multiple) {
            let tooltip = '';
            for (let i = 0; i < value.length; i++) {
                resultText += value[i].value + splitText;
                tooltip += value[i].label + splitText;
            }
            resultText = resultText.slice(0, -1);
            tooltip = tooltip.slice(0, -1);
            this.setState({ value: value });
            this.setState({ tooltip });
        } else {
            if (this.state.value.length > 0 && this.state.value[0].value == value.value) {
                this.setState({ value: [] });
                resultText = "";
            } else {
                this.setState({ value: value });
                resultText = value.value;
            }
        }

        if (this.baseContext) {
            this.baseContext._handleChange({ "id": id, "value": resultText });
        }

        if (onChange) {
            onChange(value);
        }
    }

    render() {
        const { value, optionList } = this.state;
        const { width, readOnly, multiple, title } = this.props;

        return (
            <React.Fragment>
                <BaseConsumer>
                    {
                        (context) => {
                            if (this.baseContext == null) {
                                this.baseContext = context;
                            }
                        }
                    }
                </BaseConsumer>

                <div className="tree_item" title={this.state.tooltip}>
                    {
                        title != "" ?
                            <label className="item_lb w100">{title}</label>
                            : null
                    }
                    <TreeSelect
                        style={{ width: width }}
                        treeData={optionList}
                        value={value}
                        onChange={this.handleChange}
                        dropdownStyle={{ maxHeight: 400, overflow: 'auto' }}
                        treeLine
                        maxTagTextLength={10}
                        // inputValue={null}
                        disabled={readOnly}
                        treeDataSimpleMode={{ id: 'key', rootPId: 0 }}
                        labelInValue
                        multiple={false}
                        treeCheckable={multiple ? true : false}

                        //조회관련
                        showSearch={true}
                        treeNodeFilterProp="title"
                        searchPlaceholder={this.props.searchPlaceholder}
                        searchValue={this.state.simpleSearchValue}
                        onSearch={val => {
                            this.setState({ simpleSearchValue: val });
                        }}
                    />
                </div>
            </React.Fragment>
        );
    }
}

STreeSelectBox.propTypes = {
    width: PropTypes.number,
    title: PropTypes.string,
}

// Default 값 설정
STreeSelectBox.defaultProps = {
    splitText: ',',
    // keyMember: 'cd',
    pIdMember: 'pCd',
    titleMember: 'cdNm',
    valueMember: 'cd',
    width: 110,
    readOnly: false,
    searchPlaceholder: "Search Text",
    multiple: true,
    title: "",
}
import React, { Component } from 'react';
import Tree from 'rc-tree';
import PropTypes from 'prop-types';
import 'rc-tree/assets/index.less';
import 'components/override/tree/style.css';
import { observer } from 'mobx-react';
import { BaseConsumer } from 'utils/base/BaseComponent';
import { getValue } from 'utils/object/ContextManager.js'
import ObjectUtility from 'utils/object/ObjectUtility'
import { toJS, reaction, when } from 'mobx';

/**
 * @treeData tree에 바인딩 할 데이터. [default: []]
 * @keyMember 데이터 중 key로 사용할 property 명칭. [default: cd]
 * @pIdMember 데이터 중 Parent Key로 사용할 property 명칭. [default: pCd]
 * @titleMember 데이터 중 display할 property 명칭. [default: cdNm]
 * @checkable 체크 가능여부. [default: false]
 * @disableMember e데이터 중 비활성화를 결정할 property 명칭. "Y"일 때 비활성화.
 */
@observer
export default class STree extends Component {

    constructor(props) {
        super(props);
        this.state = {
            treeData: [],
            treeNode: [],
            // originTreeData: [],
            expandedKeys: [],
            selectedKeys: [],
            checkedKeys: [],
            noRowsToShow: false,
        }

        when(
            () => this.state.treeNode.length > 0,
            () => {
                this.setState({ noRowsToShow: true });
            }
        )
    }

    componentDidMount() {
        this._isMounted = true;
        const { selectedKeyId, expandedKeysId, checkedKeysId } = this.props;
        const result = this.createTree();

        this.setState({ treeData: this.props.treeData });

        if (result.treeNode) {
            this.setState({ treeNode: result.treeNode })
        }

        if (result.expandedKeys) {
            this.setState({ expandedKeys: result.expandedKeys })
        }

        if (result.selectedKeys) {
            this.setState({ selectedKeys: result.selectedKeys })
        }

        if (result.checkedKeys) {
            this.setState({ checkedKeys: result.checkedKeys })
        }

        if (selectedKeyId) {
            reaction(() => this.baseContext[selectedKeyId], selectedKey => {
                if (this._isMounted) {
                    this.setState({ selectedKeys: [selectedKey] })
                }
            });
        }
        if (expandedKeysId) {
            reaction(() => this.baseContext[expandedKeysId], expandedKeys => {
                if (this._isMounted) {
                    const keys = this.setKeys(expandedKeysId, expandedKeys);
                    this.setState({ expandedKeys: toJS(keys) })
                }
            });
        }

        if (checkedKeysId) {
            reaction(() => this.baseContext[checkedKeysId], checkedKeys => {
                if (this._isMounted) {
                    this.setState({ checkedKeys: toJS(checkedKeys) })
                }
            });
        }
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    /**
     * expandedKeys 설정을 위하여 선택한 값의 부모 찾기
     */
    findParent = (key, keys) => {
        if (!key) return;

        const { keyMember, pIdMember, treeData } = this.props;

        const findMe = treeData.find(x => x[keyMember] == key);

        if (findMe) {
            const parentValue = findMe[pIdMember];
            if (parentValue != null) {
                if (keys.indexOf(parentValue) < 0) {
                    keys.push(parentValue);
                }
                this.findParent(parentValue, keys);
            }
        }
    }

    /**
     * expandedKeys 설정을 위하여 선택한 값의 부모 찾기
     */
    setKeys = (keysId, values) => {
        if (values) {
            values.map(x => {
                this.findParent(x, values);
            })

            return values;
        }
    }

    /**
     * TreeNode 생성
     */
    setData = () => {
        const { treeData } = this.props;
        return this.makeTreeNode(treeData);
    }

    createTree = () => {
        const result = {};
        const data = this.setData();
        result.treeNode = data;

        const { selectedKeyId, expandedKeysId, checkedKeysId } = this.props;

        if (selectedKeyId) {
            const selectedKey = getValue(this.baseContext, selectedKeyId);
            if (selectedKey) {
                result.selectedKeys = [selectedKey];
            }
        }

        if (expandedKeysId) {
            const originValue = getValue(this.baseContext, expandedKeysId);
            const expandedKeys = this.setKeys(expandedKeysId, originValue);
            if (expandedKeys) {
                result.expandedKeys = toJS(expandedKeys);
            }
        }

        if (checkedKeysId) {
            const originValue = getValue(this.baseContext, checkedKeysId);
            if (originValue) {
                result.checkedKeys = toJS(originValue);
            }
        }

        return result;
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        if (nextProps.treeData != prevState.treeData) {
            return { treeData: nextProps.treeData };
        }

        return null;
    }

    getSnapshotBeforeUpdate(prevProps, prevState) {
        if (prevProps.treeData != this.props.treeData) {
            return this.createTree();
        }

        return null;
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (snapshot) {
            if (snapshot.treeData) {
                this.setState({ treeData: snapshot.treeData })
            }

            if (snapshot.treeNode) {
                this.setState({ treeNode: snapshot.treeNode })
            }

            if (snapshot.expandedKeys) {
                this.setState({ expandedKeys: snapshot.expandedKeys })
            }

            if (snapshot.selectedKeys) {
                this.setState({ selectedKeys: snapshot.selectedKeys })
            }

            if (snapshot.checkedKeys) {
                this.setState({ checkedKeys: snapshot.checkedKeys })
            }
        }
    }

    handleSelect = (selectedKeys, info) => {
        const { onSelect, selectedKeyId } = this.props;

        const selectedKey = selectedKeys.length > 0 ? selectedKeys[0] : "";
        if (selectedKeyId && this.baseContext._handleChange) {
            this.baseContext._handleChange({ id: selectedKeyId, value: selectedKey })
        }

        this.setState({ selectedKeys: selectedKeys });

        if (info.node.children && info.node.children.length > 0) {
            if (this.state.expandedKeys.indexOf(selectedKeys[0]) < 0) {
                this.setState(prevState => ({
                    expandedKeys: prevState.expandedKeys.concat(selectedKeys)
                }));
            }
        }

        if (onSelect) {
            onSelect(selectedKey, info.node);
        }
    }

    handleExpand = (expandedKeys, info) => {
        const { expandedKeysId } = this.props;

        if (expandedKeysId && this.baseContext._handleChange) {
            this.baseContext._handleChange({ id: expandedKeysId, value: expandedKeys })
        }

        this.setState({ expandedKeys });
    }

    handleCheck = (checkedKeys, info) => {
        const { checkedKeysId } = this.props;

        if (checkedKeysId && this.baseContext._handleChange) {
            this.baseContext._handleChange({ id: checkedKeysId, value: checkedKeys })
        }

        this.setState({ checkedKeys });

        const { onCheck } = this.props;

        if (onCheck) {
            onCheck(checkedKeys, info,);
        }
    }

    makeTreeNode = (data) => {
        const { keyMember, pIdMember, titleMember } = this.props;

        let makeTree = [];

        for (let i = 0; i < data.length; i++) {
            const item = data[i];

            const temp = { key: item[keyMember], title: item[titleMember] };
            const target = Object.assign(temp, item);

            this.makeChildren(data, target);

            if (item[pIdMember] == null) {
                makeTree = makeTree.concat(target);
            }
        }

        return makeTree;
    };

    makeChildren = (data, target) => {
        const { keyMember, pIdMember, titleMember, disableMember } = this.props;

        // 나(target)를 부모로 가지고 있는 자식 가져오기

        const children = data.filter(x => x[pIdMember] == target.key) 

        // 자식이 있으면
        if (children) {
            // children 초기값 설정
            target.children = [];

            for (let i = 0; i < children.length; i++) {
                const item = children[i];

                // 자식 object 생성
                const temp = { key: item[keyMember], title: item[titleMember] };
                const child = Object.assign(temp, item);

                // 자식 key에도 자식이 있는지 확인하여 자식이 있으면 makeChildren 재귀호출
                if (this.isExistChildren(data, child.key)) {
                    this.makeChildren(data, child);
                }

                // 자식 설정 후 target에 concat
                target.children = target.children.concat(child);
                if (disableMember) {
                    target.className = "icon_tree_node";
                }
            }
        }
    }

    isExistChildren = (data, pId) => {
        const { pIdMember } = this.props;
        // 자식이 있는지 확인 
        return data.some(x => x[pIdMember] == pId);``
    }

    render() {
        const { disableMember, checkable, border } = this.props;
        const { noRowsToShow, treeNode, expandedKeys, selectedKeys, checkedKeys } = this.state;

        return (
            <React.Fragment>
                {!this.baseContext ?
                    <BaseConsumer>
                        {(context) => { this.baseContext = context; }}
                    </BaseConsumer>
                    :
                    (null)
                }

                {noRowsToShow == true && treeNode.length > 0 ?
                    (<div className="basic tree_list_item" style={{
                        backgroundColor: '#fff',
                        height: 'calc( 100% - 1px )'
                    }}>
                        <Tree
                            treeData={treeNode}
                            checkable={checkable}
                            onCheck={this.handleCheck}
                            onSelect={this.handleSelect}
                            onExpand={this.handleExpand}
                            expandedKeys={expandedKeys}
                            selectedKeys={selectedKeys}
                            checkedKeys={checkedKeys}
                            showIcon={false}
                            showLine={true}
                            switcherIcon={disableMember ? (obj) => switcherIcon(obj, disableMember) : undefined}
                        />
                    </div>)
                    :
                    (<div className="basic" style={{
                        textAlign: 'center',
                        padding: '8px',
                        border: border ? '1px solid #ddd' : undefined
                    }}>
                        {ObjectUtility.l10n("M01617", "조회된 항목이 없습니다")}
                    </div>)
                }
            </React.Fragment>
        )
    }
}

STree.propTypes = {
    keyMember: PropTypes.string,
    pIdMember: PropTypes.string,
    titleMember: PropTypes.string,
    checkable: PropTypes.bool,
    onSelect: PropTypes.func,
    onCheck: PropTypes.func,
}

STree.defaultProps = {
    treeData: [],
    keyMember: 'cd',
    pIdMember: 'pCd',
    titleMember: 'cdNm',
    checkable: false,
    disableMember: undefined,
    border: false,
    
}

const switcherIcon = (obj, field) => {
    if (obj.isLeaf) {
        if (obj[field] == "Y") {
            return <i className={'icon-check'} />;
        } else {
            return <i className={'icon-cancel'} />;
        }
    } else {
        if (obj[field] == "Y") {
            return <i className={'icon-check'} style={{ marginLeft: "18px" }} />;
        } else {
            return <i className={'icon-cancel'} style={{ marginLeft: "18px" }} />;
        }
    }
};

// const Icon = ({ selected, node }) => (
//     <span
//         className={classNames(
//             'customize-icon',
//             selected && 'selected-icon',
//         )}
//     />
//     <i className={'icon-plus-circled'} />
//     <span style={{background: 'green', width: '10px', height: '10px'}}>T</span>
// );
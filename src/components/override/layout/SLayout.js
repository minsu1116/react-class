import React, { Component } from 'react';
// import { Responsive, WidthProvider } from "react-grid-layout";
// const ResponsiveReactGridLayout = WidthProvider(Responsive);
import RGL, { WidthProvider } from "react-grid-layout";
const ReactGridLayout = WidthProvider(RGL);
// import GridLayout from 'react-grid-layout';
import "components/override/layout/style.css";


export default class SLayout extends Component {

    _handleLayoutChange = (layout) => {
    }

    render() {
        return (
            <ReactGridLayout
                className="layout"
                // cols={{ lg: 10, md: 10, sm: 10, xs: 4, xxs: 2 }}
                cols={100}
                rowHeight={18}
                margin={[15, 15]}
                isDraggable={false}
                isResizable={false}
                onLayoutChange={this._handleLayoutChange}
            >
                {this.props.children}
            </ReactGridLayout>
        )
    }
}
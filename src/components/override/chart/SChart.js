
import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { ResponsiveContainer, ComposedChart, Line, Bar, XAxis, YAxis, Legend, Tooltip, ReferenceLine, PieChart, Pie, Cell, Area } from 'recharts'
import { toJS } from 'mobx';
import ContentsTemplate from 'components/template/ContentsTemplate';
export {
    SChart,
    SChartXAxis,
    SChartYAxis,
    SChartLine,
    SChartBar,
    SChartLegend,
    SChartTooltip,
    SReferenceLine,
    SPieChart,
    SPie,
    SCell,
    SArea,
}

/**
 * @id ** ID
 * @data ** 차트에 추가할 data Array
 * @margin 차트 margin. 기본값: { top: 10, bottom: 10, left: 10, right: 10 }
 */
class SChart extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        const { id, chartData, margin, anotherData, background, subTitle } = this.props;
        let isPie;
        React.Children.map(this.props.children, child => {
            if (child && child.props.chartType != undefined) {
                isPie = true;
            }
        });
        return (
            <React.Fragment>
                {subTitle ? (
                    <div className="chart_sub_tit">
                        {subTitle}
                    </div>)
                    : (null)
                }
                <ContentsTemplate id={id} overflow={'hidden'} shadow={false} background={background} noMargin={true} height={'100%'}>
                    {
                        (chartData && chartData.length > 0) || anotherData == true ?
                            (
                                <ResponsiveContainer width='100%' height='100%'>
                                    {isPie ?
                                        (this.props.children)
                                        :
                                        (<ComposedChart
                                            data={toJS(chartData)}
                                            margin={margin}
                                        >
                                            {this.props.children}
                                        </ComposedChart>)
                                    }
                                </ResponsiveContainer>
                            ) :
                            (
                                null
                            )
                    }
                </ContentsTemplate>
            </React.Fragment>
        )
    }
}
SChart.defaultProps = {
    chartData: [],
    margin: { top: 10, bottom: 10, left: 10, right: 10 },
    background: "#fff",
}

/**
 * [X축]
 * @dataKey ** X축에 Binding할 Property
 */
class SChartXAxis extends XAxis {
    static defaultProps = {
        ...XAxis.defaultProps,
    }
}

/**
 * [Y축]
 * @type 기본값: number
 * @domain 범위. 기본값: ['auto', 'auto']
 * @tickFormatter 천자리 단위 Formatter
 */
class SChartYAxis extends YAxis {
    static defaultProps = {
        ...YAxis.defaultProps,
        type: 'number',
        domain: ['auto', 'auto'],
        tickFormatter: (value) => `${String(value).replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,')}`,
    }
}

/**
 * @layout
 * @align
 * @verticalAlign
 */
class SChartLegend extends Legend {
    static defaultProps = {
        ...Legend.defaultProps,
        layout: "vertical",
        align: 'right',
        verticalAlign: 'middle',
    }
}

/**
 * [Line]
 * @dataKey ** 라인으로 표현할 Data의 property
 * @type
 * @strokeWidth 라인 두께. 기본값: 3
 * @stroke 라인 색상. 기본값: #8884d8
 */
class SChartLine extends Line {
    static defaultProps = {
        ...Line.defaultProps,
        type: 'monotone',
        strokeWidth: 3,
        stroke: "#8884d8",
    }
}

/**
 * [Bar]
 * @dataKey ** Bar로 표현할 Data의 property
 * @fill Bar 색상. 기본값: #82ca9d
 * @barSize Bar 두께. 기본값: 20
 * @maxBarSize Bar 최대두께. 기본값 30
 */
class SChartBar extends Bar {
    static defaultProps = {
        ...Bar.defaultProps,
        fill: "#82ca9d",
        barSize: 20,
        maxBarSize: 30,
    }
}

/**
 * [Tooltip]
 * @formatter
 */
class SChartTooltip extends Tooltip {
    static defaultProps = {
        ...Tooltip.defaultProps,
        formatter: (value) => `${String(value).replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,')}`,
    }
}

/**
 * [ReferenceLine]
 * 'x' 또는 'y'중에 하나 필수로 기입해야 합니다.
 * @x 표현할 축 명칭.
 * @y 표현할 domain 위치.
 * @stroke 선 색상. 기본값: red
 * @strokeDasharray 점선 간격. 기본값: 3 3
 * @label 표기할 명칭.
 */
class SReferenceLine extends ReferenceLine {
    static defaultProps = {
        ...ReferenceLine.defaultProps,
        stroke: 'red',
        strokeDasharray: '3 3',
        ifOverflow: "extendDomain",
    }
}

/**
 * [PieChart]
 */
class SPieChart extends PieChart {
    static defaultProps = {
        ...PieChart.defaultProps,
        chartType: "pieChart",
    }
}

/**
 * [Pie]
 */
class SPie extends Pie {
    static defaultProps = {
        ...Pie.defaultProps,
    }
}

/**
 * [Pie]
 */
class SCell extends Cell {
    static defaultProps = {
        ...Cell.defaultProps,
        // stroke: 'red',
        // strokeDasharray: '3 3',
        // ifOverflow: "extendDomain",
    }
}

/**
 * [Area]
 */
class SArea extends Area {
    static defaultProps = {
        ...Area.defaultProps,
    }
}
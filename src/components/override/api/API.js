import axios from "axios";
import commonStore from 'modules/pages/common/service/CommonStore';
import mainStore from 'modules/layouts/main/service/MainStore';
import ObjectUtility from 'utils/object/ObjectUtility';
import alert from 'components/override/alert/Alert';

const API_ROOT = `${WEBPACK_CONFIG_API_URL}`;
const API_QUARTZ = `${WEBPACK_CONFIG_API_QUARTZ}`;
// const isLocal = WEBPACK_CONFIG_API_URL.indexOf("10.15.36.143") >= 0 ? true : false;
const isLocal = WEBPACK_CONFIG_API_URL.indexOf("localhost") >= 0 ? true : false;

const request = axios.create({
    baseURL: API_ROOT,
    responseType: "json",
    headers: {
        'Content-Type': 'application/json',
        Pragma: 'no-cache',
        "Access-Control-Allow-Methods": "OPTIONS,POST,GET",
    }
});

request.defaults.timeout = 120000;

// request 요청할때 값을 추가 할 수 있다.
request.interceptors.request.use(function (config) {

    if (config.url.indexOf("home") < 0) {
        ObjectUtility.loadingLayer(true);
    }

    config.headers.common = {
        'Authorization': `Bearer ${commonStore.token || ""}`
        , 'EmpNo': `${commonStore.loginId || ""}`
        // , 'loginNm': `${commonStore.loginNm}`
        , 'Device-Type': 'WEB'
        , 'Locale': `${commonStore.locale || ""}`
        , 'PageInfo': `${mainStore.currentPage ? mainStore.currentPage.class + "/" + encodeURI(mainStore.currentPage.menuNm) : ""}`
    }

    return config;
}, function (error) {
    // Do something with request error
    return Promise.reject(error);
});

// response 받을 때 오류 처리등 중간 처리 해 줌.
request.interceptors.response.use(function (response) {
    ObjectUtility.loadingLayer(false);      

    // 토큰값 다시 설정
    try {
        if (response.headers.token != undefined && response.headers.token != null && response.headers.token != '') {
            commonStore.setToken(response.headers.token)
        }
    } catch (error) { }


    // 응답은 정상이나 토큰값이 없거나 유효시간이 지났을 경우
    if (response.data.code == 401) {
        window.location.href = '/auth/login';
        return response;
    }

    try {
        if (response.data.success == false) {
            console.log('서버에서 에러가 발생하였습니다.', response);
            console.log('ERROR: ', response.data.msg);
            alert.meg(response.data.msg);
        }
    }
    catch { }

    //console.log('',response);

    return response;
}, function (error) { // 요청 및 서버오류 공통 처리
    // Do something with response error
    // API 서버에 연결이 안될 경우
    if (error.message === "Network Error" || error.response === undefined) {
        alert.meg(`서버에 연결할 수 없습니다. \n 잠시 후 다시 시도해 주시기 바랍니다.`);
        ObjectUtility.loadingLayer(false);
        return Promise.reject(error);
    }

    const { response } = error;

    if (response.status == 401) {
        window.location.href = '/auth/sso';
        // window.location.href = '/auth/sso';
    }
    return Promise.reject(error);
});

/* 권한이 없을 경우 사용함 (토큰 적용 안함)*/
const auth = axios.create();
auth.defaults.baseURL = API_ROOT;
auth.defaults.responseType = "json";
auth.defaults.headers = {
    'Content-Type': 'application/json'
};

/* 파일 업로드 시 사용함 */
const file = axios.create({
    baseURL: API_ROOT,
    responseType: "json",
    // headers:{
    //   'Content-Type':  'multipart/form-data',
    //   Pragma: 'no-cache'
    // }
});

file.defaults.timeout = 1200000; // 30초

file.interceptors.request.use(function (config) {

    ObjectUtility.loadingLayer(true);

    config.headers.common = {
        'Authorization': `Bearer ${commonStore.token}`
        , 'EmpNo': `${commonStore.loginId}`
        // , 'loginNm': `${commonStore.loginNm}`
        , 'Device-Type': 'WEB'
        , 'Locale': `${commonStore.locale}`
    }

    return config;
}, function (error) {
    // Do something with request error
    return Promise.reject(error);
});

// response 받을 때 오류 처리등 중간 처리 해 줌.
file.interceptors.response.use(function (response) {

    ObjectUtility.loadingLayer(false);

    //console.log(response);
    return response;
}, function (error) { // 요청 및 서버오류 공통 처리
    ObjectUtility.loadingLayer(false);

    if (error.message === "Network Error" || error.response === undefined) {
        alert.meg(`서버에 연결할 수 없습니다. \n 잠시 후 다시 시도해 주시기 바랍니다.`);
        ObjectUtility.loadingLayer(false);
        return Promise.reject(error);
    }

    const { status } = error.response;
    alert.meg('오류가 발생하였습니다.');
    return Promise.reject(error);
});


const quartz = axios.create({
    baseURL: API_QUARTZ,
    responseType: "json",
    headers: {
        'Content-Type': 'application/json',
        Pragma: 'no-cache',
    }
});

quartz.defaults.timeout = 60000;

quartz.interceptors.request.use(function (config) {

    return config;
}, function (error) {
    // Do something with request error
    return Promise.reject(error);
});

// response 받을 때 오류 처리등 중간 처리 해 줌.
quartz.interceptors.response.use(function (response) {
    ObjectUtility.loadingLayer(false);

    try {
        if (response.data.success == false) {
            console.log('서버에서 에러가 발생하였습니다.', response);
            console.log('ERROR: ', response.data.msg);
            alert.meg(response.data.msg);
        }
    }
    catch { }

    //console.log('',response);

    return response;
}, function (error) { // 요청 및 서버오류 공통 처리
    // Do something with response error
    // API 서버에 연결이 안될 경우
    if (error.message === "Network Error" || error.response === undefined) {
        alert.meg(`서버에 연결할 수 없습니다. \n 잠시 후 다시 시도해 주시기 바랍니다.`);
        ObjectUtility.loadingLayer(false);
        return Promise.reject(error);
    }

    // const { response } = error;

    // if (response.status == 401) {
    //     window.location.href = '/auth/sso';
    //     // window.location.href = '/auth/sso';
    // }
    return Promise.reject(error);
});

export default {
    request,
    auth,
    file,
    isLocal,
    quartz,
}
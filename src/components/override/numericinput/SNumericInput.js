import React, { Component } from 'react'
import PropTypes from 'prop-types';
import NumberFormat from 'react-number-format';
import 'components/override/numericinput/style.css';
import { BaseConsumer } from 'utils/base/BaseComponent';
import { getValue } from 'utils/object/ContextManager';

//https://www.npmjs.com/package/react-number-format
/**
 * @id ** 동기화할 store 변수
 * @readOnly [default: false]
 * @onChange 변경 감지 이벤트. [parameter: 현재 선택된 값의 display, formatValue 등]
 * @isFocus 최초 로딩 시 키보드 포커스 여부
 * @width 가로길이 [default: 110]
 * @background 컨트롤 배경색
 * @digit 자릿수 제한
 */
export default class SNumericInput extends Component {

    constructor(props) {
        super(props);
        this.state = {
            value: ""
        }
    }

    componentDidMount() {
        const value = getValue(this.baseContext, this.props.id);
        this.setState({ value: value })
    }

    timeFormat = (val) => {
        let month = this.limit(val.substring(0, 2), '23');
        let date = this.limit(val.substring(2, 4), '59');

        return month + (date.length ? ':' + date : '');
    }

    limit = (val, max) => {
        if (val.length === 1 && val[0] > max[0]) {
            val = '0' + val;
        }

        if (val.length === 2) {
            // if (Number(val) === 0) {
            //     // val = '01';
            // } else
            if (val > max) {
                val = max;
            }
        }

        return val;
    }

    _handleChange = (values) => {
        const { onChange, id, formatType } = this.props;

        const data = { id: id, value: values.floatValue };

        this.setState({ value: values.floatValue });

        if (this.baseContext) {
            if (formatType == "time") {
                data.value = values.formattedValue;
            }

            this.baseContext._handleChange(data);
        }

        if (onChange) {
            onChange(values);
        }
    }

    handleFocus = (event) => {
        this.numericInput.select();
    }

    handleBlur = (event) => {
        if (this.props.onBlur) {
            this.props.onBlur(event);
        }
    }

    _handleIsAllowed = (values) => {
        if (!values.floatValue) {
            return true;
        }
        const { test } = this.props;

        if (!test) {
            return true;
        }
        return values.floatValue < Math.pow(10, test);
    }

    render() {

        const { id, thousandSeparator, decimalScale, prefix, format, mask, readOnly, width, titleWidth, formatType, background } = this.props;
        let css = "";
        if (readOnly) {
            css = "search_box w250 readOnly";
        } else {
            css = "search_box w250";
        }

        const value = getValue(this.baseContext, id);
        return (
            <React.Fragment>
                {!this.baseContext ?
                    <BaseConsumer>
                        {(context) => { this.baseContext = context; }}
                    </BaseConsumer>
                    :
                    (null)
                }


                <div className="numeric_input_item">
                    <NumberFormat
                        id={id}
                        className={css}
                        style={{
                            width: width,
                            textAlign: formatType == "time" ? 'center' : 'right',
                            background: background
                        }}
                        type={formatType == "time" ? '' : 'search'}
                        onValueChange={this._handleChange}
                        thousandSeparator={thousandSeparator}
                        value={value}
                        decimalScale={decimalScale}
                        fixedDecimalScale={true}
                        prefix={prefix}
                        format={formatType == "time" ? this.timeFormat : format}
                        mask={mask}
                        readOnly={readOnly}
                        getInputRef={(el) => this.numericInput = el}
                        onFocus={this.handleFocus}
                        onBlur={this.handleBlur}
                        isAllowed={this._handleIsAllowed}
                    />
                </div>

            </React.Fragment>
        )
    }
}

SNumericInput.propTypes = {
    decimalScale: PropTypes.number,
}

SNumericInput.defaultProps = {
    thousandSeparator: true,
    readOnly: false,
    decimalScale: 10,
    width: 110,
    // format: "#### #### #### ####",
    // mask: "_",
    digit: undefined,
}
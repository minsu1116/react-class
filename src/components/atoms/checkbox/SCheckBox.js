import React, { Component } from 'react';
import 'components/atoms/checkbox/style.css'
import { BaseConsumer } from 'utils/base/BaseComponent'
import { getValue } from 'utils/object/ContextManager.js'
import PropTypes from 'prop-types';
import { reaction } from 'mobx';

/**
 * @id ** 동기화할 store 변수
 * @title CheckBox Label.
 * @readOnly ReadOnly. [default: false]
 * @checkedValue true/false일 때 설정할 값. [default: Y,N]
 * @onChange 변경 감지 이벤트. [Parameter: 현재 선택된 value]
 */
class SCheckBox extends Component {

    constructor(props) {
        super(props);
        this.state = {
            value: false
        }
    }

    componentDidMount() {
        this._isMounted = true;
        const { id,dynamicData } = this.props;

        if (this.baseContext) {
            const value = getValue(this.baseContext, id) ? getValue(this.baseContext, id) : dynamicData;
            this.setValue(value);
        }

        const ids = id.split('.');
        reaction(
            () => ids.length == 2 ? this.baseContext[ids[0]][ids[1]] : this.baseContext[id],
            (value) => {
                if (this._isMounted) {
                    this.setValue(value);
                }
            }
        );
    }

    setValue = (value) => {
        const { id, checkedValue } = this.props;
        const values = checkedValue.split(',');
        // 값이 checkedValue에 없으면
        if (!value || value == "" || values.indexOf(value) < 0) {
            this.baseContext._handleChange({ "id": id, "value": values[1] });
            this.setState({ value: false });
            return;
        } else if (value == values[1]) {
            // 값이 false에 해당되면 값 설정
            this.setState({ value: false });
            return;
        } else if (value == values[0]) {
            // 값이 true에 해당되면 값 설정
            this.setState({ value: true });
            return;
        }
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    _handleChange = (event) => {
        const { readOnly, checkedValue, onChange } = this.props;
        if (readOnly) {
            return;
        }

        this.setState({ value: event.target.checked });

        const values = checkedValue.split(',');
        const value = event.target.checked ? values[0] : values[1];

        if (this.baseContext) {
            this.baseContext._handleChange({ "id": event.target.id, "value": value });
        }

        if (onChange) {
            onChange(value);
        }
    }

    render() {
        const { id, title } = this.props;
        const { value } = this.state;

        return (
            <React.Fragment>
                {!this.baseContext ?
                    <BaseConsumer>
                        {(context) => { this.baseContext = context; }}
                    </BaseConsumer>
                    :
                    (null)
                }
                <div className="check_item">
                    <div className="check_box_warp">
                        <div className="check_box">
                            <input
                                id={id}
                                type="checkbox"
                                checked={value}
                                onChange={this._handleChange}
                            />
                            <label htmlFor={id} className="item_lb" style={{ marginTop: "0px" }}>{title}</label>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

SCheckBox.propTypes = {
    id: PropTypes.string.isRequired,
    title: PropTypes.string,
    readOnly: PropTypes.bool,
    checkedValue: PropTypes.string,
    dynamicData: PropTypes.string,
}

// Default 값 설정
SCheckBox.defaultProps = {
    readOnly: false,
    checkedValue: "Y,N",
    dynamicData: ''
}
export default SCheckBox;
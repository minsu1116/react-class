
import React, { Component } from 'react'
import PropTypes from 'prop-types';

import ico_search from 'style/img/ico_search.svg';
import ico_del from 'style/img/ico_del.png';

/**
 * @onClick ** 버튼 클릭 이벤트
 * @type 버튼 타입 search|delete
 * @customImg 버튼에 바인딩할 이미지
 */
export default class SIconButton extends Component {
    render() {
        const { img, onClick, type } = this.props;
        let customImg;

        switch (type) {
            case "search":
                customImg = ico_search;
                break;
            case "delete":
                customImg = ico_del;
                break;
            default:
                customImg = ico_search;
                break;
        }

        return (
            <span className="btn_ico">
                <a onClick={onClick} style={{ cursor: "pointer" }}>
                    <img className="ico" src={img || customImg} alt="" />
                </a>
            </span>
        );
    }
}

SIconButton.propTypes = {
    onClick: PropTypes.func
}

SIconButton.defaultProps = {
    img: ico_search
}

import React, { Component } from 'react'
import 'components/atoms/button/style.css'
import { SConsumer } from 'utils/context/SContext'
import PropTypes from 'prop-types';
import { SModalConsumer } from 'utils/context/SModalContext';

/**
 * @buttonName ** 버튼이름
 * @type ** 버튼 권한 명칭
 * @onClick ** 버튼 클릭 이벤트
 * @visible 버튼 Visible 처리 [default: false]
 * @on Pressed 상태로 스타일 유지 조건 [default: false]
 */
class SButton extends Component {

    menuInfo = JSON.parse(window.sessionStorage.getItem('allSubMenuArr'));

    constructor(props) {
        super(props);
        this.state = {
            visible: true,
        }
    }

    componentDidMount() {
        this.setState({ visible: this.props.visible })
    }

    getButtonAuth = context => {
        try {
            const { type, buttonName } = this.props;

            if (type == "default") {
                return 'Y';
            }

            if (context.auth) {
                const auth = context.auth.find(x => x.btnId == type && x.formId == context.formId);
                if (auth) {
                    return "Y";
                }
                else {
                    return "N";
                }
            } else {
                return "N";
            }

        } catch (exception) {
            return 'N';
        }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        if (nextProps.visible !== prevState.visible) {
            return { visible: nextProps.visible };
        }
        return null;
    }

    render() {
        const { width, float, onClick, buttonName, type, on, customtype } = this.props;
        let classNm = "btn_blue";
        if (customtype == "tab") {
            classNm = "tab_header"
        } else {
            classNm = "btn_blue";
        }
        return (
            <SConsumer>
                {
                    (context) => {
                        return (
                            <SModalConsumer>
                                {
                                    (modalContext) => {
                                        const systemGrpYn = context ? context.systemGrpYn : 'N';
                                        const target = modalContext ? modalContext : (context ? context : null);
                                        if (this.state.visible == true &&
                                            (systemGrpYn == 'Y' || this.getButtonAuth(target) == 'Y')) {
                                            return (
                                                <li className="button_item">
                                                    <div
                                                        className={classNm}
                                                        id={type}
                                                        type="button"
                                                        value={buttonName}
                                                        title={buttonName}
                                                        onClick={onClick}
                                                        style={{
                                                            width: width,
                                                            float: float,
                                                            background: on ? '#7fb375' : undefined,
                                                            color: on ? 'white' : undefined,
                                                        }}
                                                    >
                                                        {buttonName}
                                                    </div>
                                                </li>
                                            )
                                        } else {
                                            return null; //<invisible id={type} title={buttonName} />;
                                        }
                                    }
                                }
                            </SModalConsumer>
                        )
                    }
                }
            </SConsumer>
        );
    }
}

export default SButton;

// Type 체크
SButton.propTypes = {
    type: PropTypes.string.isRequired,
    visible: PropTypes.bool,
    on: PropTypes.bool,
}

SButton.defaultProps = {
    visible: true,
    float: 'right',
    on: false,
}
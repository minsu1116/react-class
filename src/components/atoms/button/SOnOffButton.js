import React, { Component } from 'react'
import { BaseConsumer } from 'utils/base/BaseComponent'
import { getValue } from 'utils/object/ContextManager.js'
import { reaction } from 'mobx';

class SOnOffButton extends Component {

    constructor(props) {
        super(props);

        this.state = {
            value: false,
        }
    }

    componentDidMount() {
        this._isMounted = true;
        const { id, checkedValue } = this.props;
        const checkedValues = checkedValue.split(',');


        if (this.baseContext) {
            const storeValue = getValue(this.baseContext, id);

            if (!storeValue || storeValue == "" || checkedValues.indexOf(storeValue) < 0) {
                this.baseContext._handleChange({ "id": id, "value": checkedValues[1] });
            } else {
                const checkValue = storeValue == checkedValues[0] ? true : false;
                if (checkValue) {
                    this.setState({ value: checkValue });
                }
            }
        }

        const ids = id.split(".");

        reaction(
            () => ids.length == 2 ? this.baseContext[ids[0]][ids[1]] : this.baseContext[id],
            async (value) => {
                if (this._isMounted) {
                    const v = checkedValues[0] == value ? true : false;
                    this.setState({ value: v });
                }
            }
        );
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    _handleChange = (e) => {
        if (this.props.readOnly == true) return;

        const value = e.target.checked;
        this.setState({ value: value });

        const { id, checkedValue, onChange } = this.props;
        const values = checkedValue.split(',');
        const storeValue = value ? values[0] : values[1];
        if (this.baseContext) {
            this.baseContext._handleChange({ "id": id, "value": storeValue });
        }

        if (onChange) {
            onChange(storeValue);
        }
    }

    render() {
        const { title, id } = this.props;

        return (
            <React.Fragment>
                {!this.baseContext ?
                    <BaseConsumer>
                        {(context) => { this.baseContext = context; }}
                    </BaseConsumer>
                    :
                    (null)
                }
                <div className="search_item">
                    <div className="toggle_warp">
                        <label className="toggle_label">
                            <input id={id} type="checkbox" onChange={this._handleChange} checked={this.state.value} />
                            <span className="back">
                                <span className="toggle"></span>
                                <span className="label on">ON</span>
                                <span className="label off">OFF</span>
                            </span>
                        </label>
                    </div>
                </div>
                {/* <ul className='btn_onoff'>
                    <li style={{ lineHeight: '24px' }}>{title}</li>
                    <li style={{ cursor: "pointer" }} onClick={this.onClick} >
                        {this.state.onOff == 'on' ? (
                            <img src={button_img_on} alt="" />
                        ) : (
                                <img src={button_img_off} alt="" />
                            )
                        }
                    </li>
                </ul> */}
            </React.Fragment>
        );
    }
}

// Default 값 설정
SOnOffButton.defaultProps = {
    readOnly: false,
    checkedValue: "Y,N",
}

export default SOnOffButton;
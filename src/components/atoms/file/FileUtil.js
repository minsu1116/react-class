import API from 'components/override/api/API';
import ObjectUtility from 'utils/object/ObjectUtility'
import Alert from 'components/override/alert/Alert';

class FileUtil {

    URL = "/api/file";
    URL_DOC_LOG = "/api/qc/doc/dataread";
    _promiseAttachInfo = {};

    uploadConfig = {
        headers: {
            'Content-Type': 'multipart/form-data',
            Pragma: 'no-cache'
        }
    }

    /**
     * 파일 첨부 Dialog를 띄웁니다.
     * @param {string} onlyImage 이미지 파일만 첨부 가능. [default: false]
     * @return 선택한 파일정보
     */
    fileAttach = async (onlyImage = false) => {
        var input = document.createElement('input');
        input.type = 'file';
        if (onlyImage == true) {
            input.accept = 'image/gif, image/jpeg, image/png';
        }

        input.onchange = e => {
            var file = e.target.files[0];

            if (file) {
                try {
                    this._promiseAttachInfo.resolve(file);
                }
                catch (e) {
                    // IE에서 this._promiseAttachInfo의 값 할당이 onChange 실행 위치보다 뒤에 있어 에러가 발생함.
                    setTimeout(() => {
                        let check = false;
                        while (check == false) {
                            if (this._promiseAttachInfo.resolve) {
                                this._promiseAttachInfo.resolve(file);
                                check = true;
                            }
                        }
                    }, 0.1);
                }
            } else {
                this._promiseAttachInfo.reject();
            }
        }

        input.click();

        return new Promise((resolve, reject) => {
            this._promiseAttachInfo = { resolve, reject };
        });
    }

    /**
     * 파일 업로드.
     */
    fileUpload = async (file, fileId = null) => {

        let formData = new FormData();
        formData.append('file', file);
        let result;
        if (fileId) {
            formData.append('fileId', fileId);

            result = await API.file.post(`${this.URL}/modify`, formData, this.uploadConfig);
            if (API.isLocal) {
                console.log('#fileUpload', `${this.URL}/modify`, { params: formData }, { result: result });
            }
        } else {
            result = await API.file.post(`${this.URL}/upload`, formData, this.uploadConfig);
            if (API.isLocal) {
                console.log('#fileUpload', `${this.URL}/upload`, { params: formData }, { result: result });
            }
        }

        return result.data;
    }

    fileGroupUpload = async (files, fileGrpId) => {
        if (files.length <= 0) return;

        let saveFileGrpId = fileGrpId;

        for (let i = 0; i < files.length; i++) {

            let formData = new FormData();
            formData.append('file', files[i]);
            formData.append('fileGrpId', saveFileGrpId);

            const result = await API.file.post(`${this.URL}/groupupload`, formData, this.uploadConfig);
            if (API.isLocal) {
                console.log('#fileGroupUpload', `${this.URL}/groupupload`, { params: formData }, { result: result });
            }
            if (result.data.success) {
                if (!saveFileGrpId || saveFileGrpId == "") {
                    saveFileGrpId = result.data.data.fileGrpId;
                }
            }
        }
        return saveFileGrpId;
    }

    fileDownload = async (fileId, docId) => {
        const res = await this._download(fileId);
        let disposition = res.headers['content-disposition']
        let filename = '';

        try {
            filename = ObjectUtility.replaceAll(decodeURI(disposition.match(/fileName="(.*)"/)[1]), "+", " ");
            if (docId) {
                await this.createDocLog(docId);
            }
        } catch (error) {
            Alert.meg('다운로드 중 문제가 발생 하였습니다.');
            return;
        }

        if (navigator.appVersion.toString().indexOf('.NET') > 0) {
            window.navigator.msSaveBlob(new Blob([res.data]), filename);
        } else {
            const url = window.URL.createObjectURL(new Blob([res.data]));
            const link = document.createElement('a');
            link.href = url;
            link.setAttribute('download', filename);
            document.body.appendChild(link);
            link.click();
        }
    }

    createDocLog = async (docId) => {
        const params = {
            docId: docId,
            gubn: "D",
        }

        const result = await API.request.post(`${this.URL_DOC_LOG}/insertlog`, params);
        if (API.isLocal) {
            console.log('#createQCLog', `${this.URL_DOC_LOG}/insertlog`, { params: params }, { result: result });
        }

        return result.data.success;
    }

    /**
     * 파일 개별 삭제
     */
    fileDelete = async (fileId) => {
        if (!fileId) return;

        const result = await API.request.get(`${this.URL}/delete?fileId=${fileId}`);
        if (API.isLocal) {
            console.log('#fileDelete', `${this.URL}/delete`, { params: fileId }, { result: result });
        }

        return result.data.success;
    }

    /**
     * 파일 그룹 삭제
     */
    fileGroupDelete = async (fileGrpId) => {
        if (!fileGrpId) return;
        if (fileGrpId.length <= 0) return;

        const result = await API.request.get(`${this.URL}/groupdelete?fileGrpId=${fileGrpId}`);
        if (API.isLocal) {
            console.log('#fileGroupDelete', `${this.URL}/groupdelete`, { params: fileGrpId }, { result: result });
        }

        return result.data.success;
    }

    imageFileDownload = async (fileGrpId) => {
        const res = await this._download(fileGrpId);
        const blob = new Blob([res.data], { type: "image/png" });
        return blob;
    }

    _download = async (fileId) => {
        const config = {
            headers: {
                Pragma: 'no-cache',
            },
            responseType: 'blob',
        }

        const result = await API.file.get(`${this.URL}/download?fileId=${fileId}`, config);
        if (API.isLocal) {
            console.log('#fileDownload', `${this.URL}/download?fileId=${fileId}`, { params: { fileId } }, { result: result });
        }
        return result;
    }
}

const instance = new FileUtil();
export default instance;
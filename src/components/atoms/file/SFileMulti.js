import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { BaseConsumer } from 'utils/base/BaseComponent'
import { getValue } from 'utils/object/ContextManager.js'
import 'components/atoms/file/style.css'
import Dropzone from 'react-dropzone'
import API from 'components/override/api/API';
import ContentsMiddleTemplate from 'components/template/ContentsMiddleTemplate';
import SButton from '../button/SButton';
import ContentsTemplate from 'components/template/ContentsTemplate';
import SGrid from 'components/override/grid/SGrid';
import FileUtil from 'components/atoms/file/FileUtil';
import Alert from 'components/override/alert/Alert';
import ObjectUtility from 'utils/object/ObjectUtility'
import { reaction } from 'mobx';

/**
 * @fileGrpId ** FILE_GRP_ID가 지정되는 Store 변수명
 * @showMessage 저장 항목 없음 알림, 저장 성공 알림 메시지 노출 옵션. [default: false]
 * @useFileAttach 파일첨부 버튼 사용여부. [default: true]
 * @useFileDelete 파일삭제 버튼 사용여부. [default: true]
 * @useDownload 파일다운로드 버튼 사용여부. [default: true]
 */
export default class SFileMulti extends Component {

    constructor(props) {
        super(props);
        this.state = {
            fileName: "",
            length: 0,
        }
    }

    async componentDidMount() {
        this._isMounted = true;
        const { fileGrpId } = this.props;
        const ids = fileGrpId.split(".");

        reaction(
            () => ids.length == 2 ? this.baseContext[ids[0]][ids[1]] : this.baseContext[fileGrpId],
            async (value) => {
                if (this._isMounted) {
                    await this.getFileInfo(value);
                }
            }
        );

        setTimeout(async () => {
            if (fileGrpId) {
                const idValue = getValue(this.baseContext, fileGrpId);
                await this.getFileInfo(idValue);
            }
        }, 0.1);

        reaction(
            () => this.baseContext._fileSaveStart,
            async (value) => {
                if (this._isMounted) {
                    if (value == true) {
                        const result = await this.fileSave();
                        this.baseContext._promiseInfoForFileSave.resolve(result);
                        this.baseContext._fileSaveStart = false;
                    }
                }
            }
        );
    }

    getFileInfo = async (fileGrpId) => {
        if (fileGrpId) {
            const result = await API.request.get(`/api/file/getfileinfo?fileGrpId=${fileGrpId}`);
            if (result.data.success) {
                this.grid.setRowData(result.data.data);
            } else {
                this.grid.clear();
            }
            this.setState({ length: this.grid.length() });
        } else {
            if (this.grid) {
                this.grid.clear();
            }
        }
    }

    componentWillUnmount() {
        this._isMounted = false;
        if (this.grid) {
            this.grid.clear();
        }
    }

    fileSave = async () => {
        const { fileGrpId, showMessage } = this.props;

        const editItems = this.grid.getModifiedRows();
        const currFileGrpId = getValue(this.baseContext, fileGrpId);

        if (!editItems || editItems.length <= 0) {
            if (showMessage) {
                await Alert.meg(ObjectUtility.l10n("M02017", "저장할 파일이 없습니다."));
            }
            return currFileGrpId;
        }
        const fileInfoItems = [];
        editItems.map(item => {
            if (item.fileInfo) {
                fileInfoItems.push(item.fileInfo);
            }
        })

        const result = await FileUtil.fileGroupUpload(fileInfoItems, currFileGrpId);

        await this.getFileInfo(result);

        if (result) {
            if (showMessage) {
                await Alert.meg(ObjectUtility.l10n("M02128", "파일을 저장하였습니다."));
            }
        }

        if (this.baseContext) {
            this.baseContext._handleChange({ id: fileGrpId, value: result })
        }

        return result;
    }

    setGridApi = (grid) => {
        this.grid = grid;
        const columnDefs = [
            { name: "GROUP ID", field: "fileGroupId", width: 60, align: 'center', hide: true },
            { name: "FILE ID", field: "fileId", width: 70, align: 'center', key: true, hide: true },
        ];

        if (this.props.useDownload) {
            columnDefs.push({ word: "M00403", name: "파일명", field: "orgFileNm", width: 100, hide: true });
            columnDefs.push({ word: "M00403", name: "파일명", field: "fileId", width: 150, type: 'download', fileNm: "orgFileNm", cellRendererParams: { byFileId: true } });
        } else {
            columnDefs.push({ word: "M00403", name: "파일명", field: "orgFileNm", width: 120, });
        }

        columnDefs.push({ word: "M00360", name: "등록자", field: "inputEmpNm", width: 80, align: 'center' })
        columnDefs.push({ word: "M00405", name: "등록일", field: "inputDtm", width: 120, align: 'center', type: "dateTimeFull" })

        this.grid.setColumnDefs(columnDefs);
    }

    _handleDrop = (files) => {
        if (files) {
            files.map(x => {
                this._addRow(x);
            })
        }
    }

    _handleFileAttach = async (event) => {
        const files = event.target.files;

        for (let i = 0; i < files.length; i++) {
            this._addRow(files[i]);
        }
    }

    _addRow = (result) => {
        if (result) {
            this.grid.addRow({ fileInfo: result, orgFileNm: result.name, });
        }
    }

    _handleFileDelete = async () => {

        const { allEditMode, onDeleteAfter } = this.props;

        const selectedRow = this.grid.getSelectedRow();
        if (selectedRow) {
            if (selectedRow.newRow) {
                this.grid.deleteRow(selectedRow);
                return;
            }
            
            if(allEditMode) {
                if (await Alert.confirm(ObjectUtility.l10n("M03345", "선택한 파일을 삭제하시겠습니까?"))) {
                    const result = await FileUtil.fileDelete(selectedRow.fileId);
                    if (result) {
                        const idValue = getValue(this.baseContext, this.props.fileGrpId);
                        await this.getFileInfo(idValue);
                        await Alert.meg(ObjectUtility.l10n("M03346", "파일을 삭제하였습니다."));
                        if (onDeleteAfter) {
                            onDeleteAfter(this.grid.length());
                        }
                    }
                }
            } else {
                await Alert.meg(ObjectUtility.l10n("M03347", "기존항목은 삭제 할 수 없습니다."));
                return;
            }
        } else {
            await Alert.meg(ObjectUtility.l10n("M03348", "삭제할 항목을 선택해주세요."));
            return;
        }
    }

    render() {
        const { fileGrpId, useFileAttach, useFileDelete, height, onOff } = this.props;
        return (
            <React.Fragment>
                {!this.baseContext ?
                    <BaseConsumer>
                        {(context) => { this.baseContext = context; }}
                    </BaseConsumer>
                    :
                    (null)
                }
                <ContentsTemplate shadow={false}>
                    <div className="file_multi_item basic">
                        <ContentsMiddleTemplate subTitle={ObjectUtility.l10n("M00359", "파일첨부")} expandTargetId={onOff ? `${fileGrpId}-FileContents` : undefined} length={onOff ? this.state.length : undefined}>
                            {useFileAttach ?
                                (
                                    <div className="file_item">
                                        <label className="file_title" htmlFor="file_add_txt">{ObjectUtility.l10n("M00359", "파일첨부")}</label>
                                        <input className="btn_file" type="file" id="file_add_txt" onChange={this._handleFileAttach} multiple />
                                    </div>
                                )
                                :
                                (null)
                            }
                            {useFileDelete ? <SButton buttonName={ObjectUtility.l10n("M00003", "삭제")} onClick={this._handleFileDelete} type={'delete'} /> : null}
                        </ContentsMiddleTemplate>
                        <ContentsTemplate id={`${fileGrpId}-FileContents`} shadow={false} height={height}>
                            <Dropzone onDrop={this._handleDrop}>
                                {({ getRootProps, getInputProps }) => (
                                    <div {...getRootProps({ className: 'dropzone basic' })}>
                                        <SGrid grid={'fileGrid'} gridApiCallBack={this.setGridApi} sizeToFit={false} editable={false} />
                                    </div>
                                )}
                            </Dropzone>
                        </ContentsTemplate>
                    </div>
                </ContentsTemplate>
            </React.Fragment>
        )
    }
}

SFileMulti.propTypes = {
    fileGrpId: PropTypes.string.isRequired,
    showMessage: PropTypes.bool,
    usefileAttach: PropTypes.bool,
    usefileDelete: PropTypes.bool,
    useDownload: PropTypes.bool,
    allEditMode: PropTypes.bool,
}

SFileMulti.defaultProps = {
    showMessage: false,
    useFileAttach: true,
    useFileDelete: true,
    allEditMode: true,
    useDownload: true,
    height: "110px",
    onOff: false,
}
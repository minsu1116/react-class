import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { BaseConsumer } from 'utils/base/BaseComponent'
import { getValue } from 'utils/object/ContextManager.js'
import 'components/atoms/file/style.css'
import Dropzone from 'react-dropzone'
import API from 'components/override/api/API';
import { reaction } from 'mobx';

/**
 * @id ** FILE_GRP_ID가 지정되는 Store 변수명
 * @fileNameId 파일 이름이 설정되어 있는 Store 변수명
 * @buttonName 파일첨부 버튼 이름. [default: "파일첨부"]
 * @visiblePath 파일 이름이 표현되는 inputBox의 visible 여부. [default: true]
 */
export default class SFile extends Component {

    constructor(props) {
        super(props);
        this.state = {
            fileName: "",
        }
    }

    async componentDidMount() {
        this._isMounted = true;
        const { id } = this.props;

        const ids = id.split('.');
        reaction(
            () => ids.length == 2 ? this.baseContext[ids[0]][ids[1]] : this.baseContext[id],
            (value) => {
                if (this._isMounted) {
                    this._init(value);
                }
            }
        );

        const idValue = getValue(this.baseContext, id);
        await this._init(idValue);
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    _init = async (value) => {
        if (value) {
            const result = await API.request.get(`/api/file/getfilename?fileGrpId=${value}`);
            if (result.data.success) {
                const fileName = result.data.data.orgFileNm;
                this.setState({ fileName: fileName });

                const { fileNameId } = this.props;
                if (fileNameId) {
                    this.baseContext._handleChange({ "id": fileNameId, "value": fileName });
                }
            }
        } else {
            this.setState({ fileName: "" });
        }
    }

    _handleChange = async (event) => {
        const file = event.target.files[0];
        this.setFileInfo(file);
    }

    _handleDrop = (files) => {
        this.setFileInfo(files[0]);
    }

    setFileInfo = (file) => {
        if (file) {
            const { id, fileNameId } = this.props;
            if (this.baseContext) {
                const targetId = id.replace(".", "");
                this.baseContext[`_${targetId}`] = file;
                if (fileNameId) {
                    this.baseContext._handleChange({ "id": fileNameId, "value": file.name });
                }
            }
            this.setState({ fileName: file.name });
        }
    }

    render() {
        const { buttonName, fileNameId, visiblePath } = this.props;
        const { fileName } = this.state;

        return (
            <React.Fragment>
                {!this.baseContext ?
                    <BaseConsumer>
                        {(context) => { this.baseContext = context; }}
                    </BaseConsumer>
                    :
                    (null)
                }

                <Dropzone onDrop={this._handleDrop}>
                    {({ getRootProps, getInputProps }) => (
                        <div {...getRootProps({ className: 'dropzone file_item' })}>
                            {visiblePath ?
                                <input className="search_box" type="search" value={fileName} readOnly={true} />
                                :
                                (null)
                            }
                            <label className="file_title" htmlFor="file_add_txt">{buttonName}</label>
                            <input className="btn_file" type="file" id="file_add_txt" onChange={this._handleChange} />
                        </div>
                    )}
                </Dropzone>
            </React.Fragment>
        )
    }
}

SFile.propTypes = {
    id: PropTypes.string.isRequired,
    fileNameId: PropTypes.string,
    buttonName: PropTypes.string,
    visiblePath: PropTypes.bool,
}

SFile.defaultProps = {
    buttonName: "파일첨부",
    visiblePath: true,
}
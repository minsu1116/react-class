import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { BaseConsumer } from 'utils/base/BaseComponent'
import { getValue } from 'utils/object/ContextManager.js'
import 'components/atoms/file/style.css'
import Dropzone from 'react-dropzone'
import FileUtil from 'components/atoms/file/FileUtil';
import { reaction } from 'mobx';

/**
 * @id ** FILE_GRP_ID가 지정되는 Store 변수명
 * @buttonName 파일첨부 버튼 이름. [default: "파일첨부"]
 */
export default class SImageFile extends Component {

    constructor(props) {
        super(props);
        this.state = {
            imageSrc: undefined,
        }
    }

    async componentDidMount() {
        this._isMounted = true;
        const { id } = this.props;

        if (id) {
            const value = getValue(this.baseContext, id);
            await this._initImageLoad(value);

            const ids = id.split('.');

            reaction(
                () => ids.length == 2 ? this.baseContext[ids[0]][ids[1]] : this.baseContext[id],
                (value) => {
                    if (this._isMounted == true) {
                        this._initImageLoad(value);
                    }
                }
            );
        }
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    _initImageLoad = async (fileGrpIdValue) => {
        if (fileGrpIdValue) {
            const blob = await FileUtil.imageFileDownload(fileGrpIdValue);

            if (blob) {
                const reader = new FileReader();
                reader.readAsDataURL(blob);
                reader.onload = () => {
                    this.setState({ imageSrc: reader.result });
                }
            } else {
                await Alert.meg(`해당 이미지 정보가 없습니다.<br/>fileGrpId: [${fileGrpIdValue}]`);
                this.setState({ imageSrc: undefined });
            }
        } else {
            this.setState({ imageSrc: undefined });
        }
    }

    _handleChange = async (event) => {
        const file = event.target.files[0];
        this.setFileInfo(file);
    }

    _handleDrop = (files) => {
        this.setFileInfo(files[0]);
    }

    setFileInfo = (file) => {
        if (file) {
            const { id } = this.props;
            if (this.baseContext) {
                const targetId = id.replace(".", "");
                this.baseContext[`_${targetId}`] = file;
            }
            var reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = () => {
                this.setState({ imageSrc: reader.result });
            };
        } else {
            this.setState({ imageSrc: undefined });
        }
    }

    render() {
        const { buttonName, imageWidth, imageHeight } = this.props;

        return (
            <React.Fragment>
                {!this.baseContext ?
                    <BaseConsumer>
                        {(context) => { this.baseContext = context; }}
                    </BaseConsumer>
                    :
                    (null)
                }

                <Dropzone onDrop={this._handleDrop} accept={'image/jpeg, image/png, image/gif'}>
                    {({ getRootProps, getInputProps }) => (
                        <div {...getRootProps({ className: 'dropzone file_item' })}>
                            <div className="file_img_wrap">
                                <img className="file_image_item" src={this.state.imageSrc} style={{ width: imageWidth, height: imageHeight }} />
                            </div>
                            <label className="file_title" labeltype="image" htmlFor="file_add_img">{buttonName}</label>
                            <input className="btn_file" type="file" id="file_add_img" accept="image/gif, image/jpeg, image/png" onChange={this._handleChange} />
                        </div>
                    )}
                </Dropzone>
            </React.Fragment>
        )
    }
}

SImageFile.propTypes = {
    id: PropTypes.string.isRequired,
    buttonName: PropTypes.string,
}

SImageFile.defaultProps = {
    buttonName: "파일첨부",
    // imageWidth: 100,
    // imageHeight: 100,
}

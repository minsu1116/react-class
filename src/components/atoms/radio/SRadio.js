import React, { Component } from 'react'
import PropTypes from 'prop-types';
import 'components/atoms/radio/style.css'
import { BaseConsumer } from 'utils/base/BaseComponent'
import { getValue } from 'utils/object/ContextManager.js'
import CommonStore from 'modules/pages/common/service/CommonStore';

/**
 * @id ** 동기화할 store 변수
 * @codeGroup ItemsSource로 설정할 공통코드
 * @optionGroup ItemsSource로 설정할 Array
 * @valueMember "값"에 해당하는 Property명. [default: cd]
 * @displayMember "명칭"에 해당하는 Property명. [default: cdNm]
 * @orientation 정렬 horizontal/vertical 중 선택. [default: horizontal]
 * @include 목록 중 포함시킬 코드. "," split text로 전달.
 * @exclude 목록 중 제외시킬 코드. "," split text로 전달.
 * @width 가로 길이. [default: 110]
 * @onChange 변경 감지 이벤트. [Parameter: 현재 선택한 값]
 */
export default class SRadio extends Component {

    constructor(props) {
        super(props);
        this.state = {
            optionList: [],
            originalList: [],
            codeGroup: "",
            selectedOption: "",
            css: "check_rao fl",
        }
    }

    async componentDidMount() {

        const { codeGroup, optionGroup, orientation } = this.props;

        if (codeGroup) {
            this.setState({ codeGroup: codeGroup });
        } else {
            this.setState({ originalList: optionGroup });
        }

        if (orientation == "vertical") {
            const css = "check_rao vertical";
            this.setState({ css });
        }
    }

    getSnapshotBeforeUpdate(prevProps, prevState) {
        const { codeGroup, include, exclude, valueMember } = this.props;

        if (codeGroup) {
            if (prevState.codeGroup != codeGroup) {
                let codeList = JSON.parse(JSON.stringify(CommonStore.codeObject[codeGroup]));

                let optionList = [];

                if (include) {
                    const keys = include.split(',');
                    keys.map(key => {
                        const code = codeList.find(x => x[valueMember] == key);
                        if (code) {
                            optionList.push(code);
                        }
                    });
                } else if (exclude) {
                    optionList = JSON.parse(JSON.stringify(codeList));
                    const keys = exclude.split(',');
                    keys.map(key => {
                        const idx = optionList.findIndex(x => x[valueMember] == key);
                        if (idx > -1) {
                            optionList.splice(idx, 1)
                        }
                    });

                } else {
                    optionList = JSON.parse(JSON.stringify(codeList));
                }

                return { optionList: optionList, codeGroup: codeGroup }
            }
        } else {
            const { optionGroup } = this.props;

            if (prevState.originalList != optionGroup) {

                let codeList = JSON.parse(JSON.stringify(optionGroup));

                let optionList = [];

                if (include) {
                    const keys = include.split(',');
                    keys.map(key => {
                        const code = codeList.find(x => x[valueMember] == key);
                        if (code) {
                            optionList.push(code);
                        }
                    });
                } else if (exclude) {
                    optionList = JSON.parse(JSON.stringify(codeList));
                    const keys = exclude.split(',');
                    keys.map(key => {
                        const idx = optionList.findIndex(x => x[valueMember] == key);
                        if (idx > -1) {
                            optionList.splice(idx, 1)
                        }
                    });
                } else {
                    optionList = JSON.parse(JSON.stringify(codeList));
                }

                return { optionList: optionList, originalList: optionGroup };
            }
        }

        return null;
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (snapshot) {
            if (snapshot.optionList) {
                this.setState({ optionList: snapshot.optionList });
            }

            if (snapshot.codeGroup) {
                this.setState({ codeGroup: snapshot.codeGroup });
            }

            if (snapshot.originalList) {
                this.setState({ originalList: snapshot.originalList });
            }
        }
    }

    _handleChange = (event) => {
        this._setSelectedOption(event.target.value);
    }

    _handleClick = (event) => {
        this._setSelectedOption(event.currentTarget.id);
    }

    _setSelectedOption = (value) => {
        this.setState({ selectedOption: value });

        const { id, onChange } = this.props;

        if (this.baseContext) {
            this.baseContext._handleChange({ "id": id, "value": value });
        }

        if (onChange) {
            onChange(value);
        }
    }

    render() {
        const { id, valueMember, displayMember, width } = this.props;

        const { optionList, css } = this.state;

        const value = getValue(this.baseContext, id);

        const radioList =
        <div className="fl" style={{ width: { width } }}>
                {(optionList && optionList.map.length > 0) && optionList.map(item => {
                    const optionValue = item[valueMember];
                    const optionDisplay = item[displayMember];

                    return (
                        <div className={css} key={optionValue}>
                            <input type="radio"
                                id={id}
                                value={optionValue}
                                checked={value === optionValue}
                                onChange={this._handleChange}
                            />
                            <label id={optionValue} onClick={this._handleClick}>{optionDisplay}</label>
                        </div>
                    )
                }
                )}
            </div>;
        return (
            <React.Fragment>
                {!this.baseContext ?
                    <BaseConsumer>
                        {(context) => { this.baseContext = context; }}
                    </BaseConsumer>
                    :
                    (null)
                }
                <div className="search_item">
                    {radioList}
                </div>
            </React.Fragment>
        );
    }
}

// Type 체크
SRadio.propTypes = {
    id: PropTypes.string.isRequired,
    titleWidth: PropTypes.number,
    width: PropTypes.number,
}

// Default 값 설정
SRadio.defaultProps = {
    id: 'radio',
    valueMember: 'cd',
    displayMember: 'cdNm',
    width: 110,
    orientation: "horizontal"
}
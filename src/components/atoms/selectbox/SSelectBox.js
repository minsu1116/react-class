
import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { BaseConsumer } from 'utils/base/BaseComponent'
import { getValue } from 'utils/object/ContextManager.js'
import CommonStore from 'modules/pages/common/service/CommonStore'
import 'components/atoms/selectbox/style.css'

/**
 * @id ** 동기화할 store 변수
 * @readOnly [default: false]
 * @codeGroup ItemsSource로 설정할 공통코드
 * @optionGroup ItemsSource로 설정할 Array
 * @valueMember "값"에 해당하는 Property명. [default: cd]
 * @displayMember "명칭"에 해당하는 Property명. [default: cdNm]
 * @notIn 목록 중 제외시킬 코드. "," split text로 전달.
 * @onChange 변경 감지 이벤트. [parameter: 현재 선택된 항목의 object]
 * @pk 키가 두 개 이상이면 React-Key를 만들기 위하여 전달. "," split text로 전달.
 * @width 가로길이 [default: 110]
 */
export default class SSelectBox extends Component {
    constructor(props) {
        super(props);
        this.state = {
            optionList: [],
            codeGroup: "",
            optionGroup: [],
            value: "",
        }
    }

    async componentDidMount() {
        this.setState({ codeGroup: this.props.codeGroup });
        // reaction(
        //     () => this.baseContext[this.props.id],
        //     (value) => {
        //         if (this.state.value != value) {
        //             this.setState({ value: value })
        //         }
        //     }
        // );
    }

    getSnapshotBeforeUpdate(prevProps, prevState) {
        if (this.props.codeGroup) {
            if (prevState.codeGroup != this.props.codeGroup) {
                const { addOption, valueMember, displayMember, notIn, codeGroup } = this.props;
                let codeList = JSON.parse(JSON.stringify(CommonStore.codeObject[codeGroup]));

                let optionList = [];
                if (notIn) {
                    optionList = JSON.parse(JSON.stringify(codeList));
                    const keys = notIn.split(',');
                    keys.map(key => {
                        const idx = optionList.findIndex(x => x[valueMember] == key);
                        if (idx > -1) {
                            optionList.splice(idx, 1)
                        }
                    })
                } else {
                    optionList = JSON.parse(JSON.stringify(codeList));
                }

                if (addOption != undefined && !optionList.some(x => x[valueMember] == "")) {
                    let all = {};
                    all[valueMember] = "";
                    all[displayMember] = addOption;

                    optionList.unshift(all);
                }

                this.setDefaultValue(optionList);

                return { optionList: optionList, codeGroup: this.props.codeGroup };
            }
        } else {
            const { addOption, valueMember, displayMember } = this.props;

            // addOption 뺀 나머지 리스트로만 비교
            let beforeList;
            if (addOption != undefined && prevState.optionList.some(x => x[valueMember] == "")) {
                const list = prevState.optionList;
                beforeList = list.slice(1, list.length);
            } else {
                beforeList = prevState.optionList;
            }

            if (JSON.stringify(beforeList) != JSON.stringify(this.props.optionGroup)) {
                let optionList = JSON.parse(JSON.stringify(this.props.optionGroup));

                if (addOption != undefined && !optionList.some(x => x[valueMember] == "")) {
                    let all = {};
                    all[valueMember] = "";
                    all[displayMember] = addOption;

                    optionList.unshift(all);
                }

                this.setDefaultValue(optionList);

                return { optionList };
            }
        }

        return null;
    }

    /**
     * 리스트에 현재 value의 값이 없으면 첫번째 항목으로 value 설정
     */
    setDefaultValue = (groupList) => {
        const { id, valueMember } = this.props;

        if (groupList.length <= 0) {
            if (this.baseContext) {
                this.baseContext._handleChange({ "id": id, "value": '' });
            }
            return;
        }

        const value = getValue(this.baseContext, id);

        let firstValue;
        const find = groupList.find(x => x[valueMember] == value);

        if (find == undefined) {
            firstValue = groupList[0][valueMember];

            if (this.baseContext) {
                this.baseContext._handleChange({ "id": id, "value": firstValue });
            }
        }
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (snapshot) {
            if (snapshot.optionList) {
                this.setState({ optionList: snapshot.optionList });
            }

            if (snapshot.codeGroup) {
                this.setState({ codeGroup: snapshot.codeGroup });
            }
        }
    }


    handleChange = (event) => {
        const { onChange, valueMember } = this.props;
        let data = { "id": event.target.id, "value": event.target.value };

        this.setState({ value: event.target.value })

        if (this.baseContext) {
            this.baseContext._handleChange({ "id": event.target.id, "value": event.target.value });
        }

        if (onChange) {
            const selectedItem = this.state.optionList.filter(x => eval(`x.${valueMember}`) == event.target.value);
            onChange(selectedItem[0]);
        }
    }

    getCodeGroup = async (codeGroup) => {
        await CommonStore.getCodeObject().then(codeObject => {
            let option = codeObject[codeGroup].concat();
            this.setList(option);
        });
    }

    setList = async (optionList) => {
        const { addOption, valueMember, displayMember } = this.props;

        if (addOption && !optionList.some(x => x[valueMember] == "")) {
            let all = {};
            all[valueMember] = "";
            all[displayMember] = addOption;

            optionList.unshift(all);
        }

        this.setState({ optionList });
    }

    render() {

        const { id, pk, valueMember, displayMember, width, marginLeft, marginRight, readOnly, marginTop, title } = this.props;
        const options = this.state.optionList;

        const value = getValue(this.baseContext, id);

        const selectOption =
            options && options.map(function (select) {
                const optionValue = select[valueMember];
                const optionDisplay = select[displayMember];
                let optionKey = select[valueMember];

                if (pk) {
                    const keys = pk.split(',');
                    optionKey = '';

                    keys.map(item => {
                        optionKey += select[item] + '_';
                    });
                }

                // if (notIn) {
                //     if (notIn.indexOf(optionValue) > -1) {
                //     } else {
                //         return <option key={optionKey} value={optionValue}>{optionDisplay}</option>
                //     }
                // } else {
                //     return <option key={optionKey} value={optionValue}>{optionDisplay}</option>
                // }
                return <option key={optionKey} value={optionValue}>{optionDisplay}</option>
            });

        let css = "";
        if (readOnly) {
            css = "select_box readOnly";
        } else {
            css = "select_box";
        }

        return (
            <React.Fragment>
                {!this.baseContext ?
                    <BaseConsumer>
                        {(context) => { this.baseContext = context; }}
                    </BaseConsumer>
                    :
                    (null)
                }

                <div className="select_item" style={{ marginLeft: `${marginLeft}px`, marginRight: `${marginRight}px`, marginTop: `${marginTop}px` }}>
                    {
                        title != "" ?
                            <label className="item_lb w100">{title}</label>
                            : null
                    }
                    <div className={css} style={{ width: `${width}px` }}>
                        <select
                            id={id}
                            value={value ? value : ""}
                            disabled={readOnly}
                            onChange={this.handleChange}
                            className="w250"
                            style={{ color: "#333" }}
                        >
                            {selectOption}
                        </select>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

SSelectBox.propTypes = {
    id: PropTypes.string.isRequired,
    pk: PropTypes.string,
    valueMember: PropTypes.string,
    displayMember: PropTypes.string,
    width: PropTypes.number,
    onChange: PropTypes.func,
    codeGroup: PropTypes.string,
    title: PropTypes.string
}

SSelectBox.defaultProps = {
    valueMember: 'cd', // value property
    displayMember: 'cdNm', // display text property
    optionGroup: [],

    width: 110,

    marginLeft: 0,
    marginRight: 0,
    marginTop: 0,
    addOption: undefined,
    title: "",
}
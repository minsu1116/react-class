
import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { getValue } from 'utils/object/ContextManager';
import { BaseConsumer } from 'utils/base/BaseComponent';
import 'components/atoms/input/style.css'

/**
 * @id ** input의 값을 Binding할 Store의 Field명.
 * @readOnly [default: false]
 * @isFocus 최초 로딩 시 키보드 포커스 여부
 * @width textarea 가로길이 [default: 100%]
 * @height textarea 세로길이 [default: 50]
 * @onChange 변경 감지 이벤트. [Parameter: 변경 event]
 */
export default class STextArea extends Component {

    constructor(props) {
        super(props);
        this.state = {
            value: ""
        }
    }

    componentDidMount() {
        const { isFocus } = this.props;
        if (isFocus) {
            this.textArea.focus();
        }

        const value = getValue(this.baseContext, this.props.id);
        this.setState({ value: value });
    }

    _handleChange = (event) => {
        this.setState({ value: event.target.value })

        if (this.baseContext) {
            this.baseContext._handleChange({ "id": event.target.id, "value": event.target.value });
        }

        const { onChange } = this.props;
        if (onChange) {
            onChange(event);
        }
    }


    render() {
        const { id, readOnly, width, height, resize } = this.props;
        let css = "";
        if (readOnly) {
            css = "textarea readOnly";
        } else {
            css = "textarea";
        }

        const value = getValue(this.baseContext, id);

        return (
            <React.Fragment>
                {!this.baseContext ?
                    <BaseConsumer>
                        {(context) => { this.baseContext = context; }}
                    </BaseConsumer>
                    :
                    (null)
                }
                <div className="input_item">
                    <textarea
                        className={css}
                        id={id}
                        style={{ width: width, height: height, resize: resize }}
                        value={value ? value : ""}
                        onChange={this._handleChange}
                        readOnly={readOnly}
                        ref={(input) => { this.textArea = input; }}
                    />
                </div>
            </React.Fragment>
        );
    }
}

STextArea.propTypes = {
    id: PropTypes.string.isRequired,
    isFocus: PropTypes.bool,
    width: PropTypes.oneOf(PropTypes.string, PropTypes.number),
    height: PropTypes.oneOf(PropTypes.string, PropTypes.number),
    resize: PropTypes.string,
}

STextArea.defaultProps = {
    width: '40em',
    height: 50,
    resize: 'auto'
}
import React, { useContext } from 'react';
import PropTypes from 'prop-types';
import { BaseConsumer } from 'utils/base/BaseComponent'
import { getValue } from 'utils/object/ContextManager.js'
import CommonStore from 'modules/pages/common/service/CommonStore';
import 'components/atoms/input/style.css'

/**
 * @id ** input의 값을 Binding할 Store의 Field명.
 * @readOnly [default: false]
 * @isFocus 최초 로딩 시 키보드 포커스 여부
 * @width 가로길이 [default: 110]
 * @codeGroup 공통코드의 명칭을 바인딩 옵션. 공통코드의 CD_GUBN 바인딩.
 * @codeGroupCd 공통코드의 값 바인딩 옵션. 공통코드의 CD 바인딩.
 * @onChange 변경 감지 이벤트. [Parameter: 변경 event]
 */
class SInput extends React.Component {

    optionList;
    constructor(props) {
        super(props);
        this.state = {
            value: "",
            codeGroup: undefined,
            codeGroupNm: "",
        }
    }

    componentDidMount() {
        const { isFocus, id, codeGroup } = this.props;
        const value = getValue(this.baseContext, id);

        this.setState({ value: value });

        if (isFocus) {
            this.nameInput.focus();
        }

        if (codeGroup) {
            this.optionList = CommonStore.codeObject[this.props.codeGroup];
            this.setState({ codeGroupNm: this.getCdNm() });
        }
    }

    getSnapshotBeforeUpdate(prevProps, prevState) {
        if (prevProps.codeGroupCd != this.props.codeGroupCd) {
            let codeGroupNm = this.getCdNm();
            return { codeGroupNm };
        }
        return null;
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (snapshot) {
            if (snapshot.codeGroupNm) {
                this.setState({ codeGroupNm: snapshot.codeGroupNm });
            }
        }
    }

    getCdNm = () => {
        const data = this.optionList.find(x => x.cd == this.props.codeGroupCd);
        if (data) {
            if (this.baseContext) {
                this.baseContext._handleChange({ "id": this.props.id, "value": data.cdNm });
            }
            return data.cdNm;
        } else {
            return "";
        }
    }

    _handleKeyDown = (event) => {
        if (this.props.onKeyDown) {
            this.props.onKeyDown(event);
        }

        if (event.key === 'Enter') {
            if (this.props.enter) {
                this.props.enter(event);
            }

            if (this.props.onEnterKeyDown) {
                this.props.onEnterKeyDown(event);
            }
        }
    }

    _handleBlur = (event) => {
        if (this.props.onBlur) {
            this.props.onBlur(event);
        }
    }

    _handleKeyUp = (event) => {
        if (this.props.onKeyUp) {
            this.props.onKeyUp(event);
        }
    }

    _handleClick = (event) => {
        if (this.props.isSelect) {
            this.nameInput.select();
        }

        if (this.props.onClick) {
            this.props.onClick(event);
        }
    }

    _handleChange = (event) => {
        const value = event.target.value;
        this.setState({ value: value })

        if (this.baseContext._handleChange) {
            this.baseContext._handleChange({ "id": event.target.id, "value": value });
        }

        const { onChange } = this.props;
        if (onChange) {
            onChange(event, value);
        }
    }

    render() {
        const { id, placeholderValue, readOnly, width, codeGroupCd, customtype, title } = this.props;
        let css = "";
        if (readOnly) {
            css = "search_box readOnly";
        } else {
            css = "search_box";
        }

        let value;
        if (codeGroupCd) {
            value = this.state.codeGroupNm;
        } else {
            value = getValue(this.baseContext, id);
        }

        return (
            <React.Fragment>
                {!this.baseContext ?
                    <BaseConsumer>
                        {(context) => { this.baseContext = context; }}
                    </BaseConsumer>
                    :
                    (null)
                }

                <div className="input_item">
                    {
                        title != "" ? 
                            <label className="item_lb w100">{title}</label> 
                        : null
                    }
                    <input
                        id={id}
                        key={`input-${id}`}
                        type="search"
                        className={css}
                        customtype={customtype}
                        onChange={this._handleChange}
                        onKeyDown={this._handleKeyDown}
                        onKeyUp={this._handleKeyUp}
                        onClick={this._handleClick}
                        onBlur={this._handleBlur}
                        value={value ? value : ""}
                        style={{ width: width }}
                        ref={(input) => { this.nameInput = input; }}
                        placeholder={placeholderValue}
                        readOnly={readOnly}
                    />
                </div>
            </React.Fragment>
        );
    }
}

export default SInput;

SInput.propTypes = {
    readOnly: PropTypes.bool,
    enter: PropTypes.func,
    title: PropTypes.string,
}

SInput.defaultProps = {
    readOnly: false,
    width: 110,
    title: "",
}
import React, { Component, createContext } from 'react';

const Context = createContext();

const { Provider, Consumer: SModalConsumer } = Context;

class SModalProvider extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Provider value={this.props}>
                {this.props.children}
            </Provider>
        )
    }
}

export {
    SModalProvider,
    SModalConsumer,
};
export {
    nonenumerable
}

function nonenumerable(target, property, descriptor) {
    descriptor.enumerable = false;
    return descriptor;
}
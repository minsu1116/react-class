Date.prototype.format = function (f) {

    if (!this.valueOf()) return " ";

    var weekKorName = ["일요일", "월요일", "화요일", "수요일", "목요일", "금요일", "토요일"];
    var weekKorShortName = ["일", "월", "화", "수", "목", "금", "토"];
    var weekEngName = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    var weekEngShortName = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
    var d = this;
    var h;

    return f.replace(/(yyyy|yy|MM|dd|KS|KL|ES|EL|HH|hh|mm|ss|a\/p)/gi, function ($1) {
        switch ($1) {

            case "yyyy": return d.getFullYear(); // 년 (4자리)

            case "yy": return (d.getFullYear() % 1000).zf(2); // 년 (2자리)

            case "MM": return (d.getMonth() + 1).zf(2); // 월 (2자리)

            case "dd": return d.getDate().zf(2); // 일 (2자리)

            case "KS": return weekKorShortName[d.getDay()]; // 요일 (짧은 한글)

            case "KL": return weekKorName[d.getDay()]; // 요일 (긴 한글)

            case "ES": return weekEngShortName[d.getDay()]; // 요일 (짧은 영어)

            case "EL": return weekEngName[d.getDay()]; // 요일 (긴 영어)

            case "HH": return d.getHours().zf(2); // 시간 (24시간 기준, 2자리)

            case "hh": return ((h = d.getHours() % 12) ? h : 12).zf(2); // 시간 (12시간 기준, 2자리)

            case "mm": return d.getMinutes().zf(2); // 분 (2자리)

            case "ss": return d.getSeconds().zf(2); // 초 (2자리)

            case "a/p": return d.getHours() < 12 ? "오전" : "오후"; // 오전/오후 구분

            default: return $1;
        }
    });
};

String.prototype.toDate = function (f) {
    if (this.length == 8) {
        return new Date(this.substring(0, 4), parseInt(this.substring(4, 6)) - 1, this.substring(6, 8));
    }
    return new Date(this.replace(" ", "T"));
}

String.prototype.string = function (len) { var s = '', i = 0; while (i++ < len) { s += this; } return s; };

String.prototype.zf = function (len) { return "0".string(len - this.length) + this; };

Number.prototype.zf = function (len) { return this.toString().zf(len); };

/**
 * 주어진 값 다음의 날짜 구하기(과거는 - 마이너스)
 * @param nextDateInt   날짜에 더하거나 빼야할 값
 * @return Date
 */
Date.prototype.getAddDays = function (nextDateInt) {

    var oneDate = 1000 * 3600 * 24; // 하루

    var nowDate;
    if (this == undefined) nowDate = new Date();
    else if (this.getTime != undefined) nowDate = this;

    return new Date(nowDate.getTime() + (oneDate * nextDateInt));
}

/**
 * 주어진 값 다음의 날짜 구하기(과거는 - 마이너스)
 * @param nextDateInt   날짜에 더하거나 빼야할 값
 * @return Date
 */
String.prototype.getAddDays = function (nextDateInt) {

    var oneDate = 1000 * 3600 * 24; // 하루

    const value = this.replace(/[^0-9]/g, '');

    var nowDate;
    if (this == undefined) nowDate = new Date();
    else if (value.length >= 8) nowDate = new Date(value.substring(0, 4), parseInt(value.substring(4, 6)) - 1, value.substring(6, 8));

    return new Date(nowDate.getTime() + (oneDate * nextDateInt));
}

String.format = function () {
    // The string containing the format items (e.g. "{0}")
    // will and always has to be the first argument.
    var theString = arguments[0];

    // start with the second argument (i = 1)
    for (var i = 1; i < arguments.length; i++) {
        // "gm" = RegEx options for Global search (more than one instance)
        // and for Multiline search
        var regEx = new RegExp("\\{" + (i - 1) + "\\}", "gm");
        theString = theString.replace(regEx, arguments[i]);
    }

    return theString;
}

String.isDateTime = function (d, format = "yyyy-MM-dd") {
    if (!d) return false;
    if (typeof d === 'object' && d instanceof Date) return true;
    if (typeof d === 'string' && d.toDate().isValid()) return true;

    const value = d.replace(/[^0-9]/g, '');
    let re;
    if (format.replace(/-/gi, "") == "yyyyMM") {
        re = /[0-9]{4}(0[1-9]|1[0-2])$/;
    } else if (format == "yyyy-MM-dd HH:mm") {
        re = /[0-9]{4}(0[1-9]|1[0-2])(0[1-9]|[1-2][0-9]|3[0-1])(0[0-9]|1[0-9]|2[0-3])([0-5][0-9])$/;
    } else if (format.replace(/:/gi, "") == "HHmm") {
        re = /(0[0-9]|1[0-9]|2[0-3])([0-5][0-9])/;
    } else {
        re = /[0-9]{4}(0[1-9]|1[0-2])(0[1-9]|[1-2][0-9]|3[0-1])$/;
    }
    return re.test(value);

    // var re = /[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1]) (2[0-3]|[01][0-9]):[0-5][0-9]/;
    //         yyyy -       MM      -       dd//           hh     :   mm  :   ss
}

Date.prototype.isValid = function () {
    // An invalid date object returns NaN for getTime() and NaN is the only
    // object not strictly equal to itself.
    return this.getTime() === this.getTime();
}
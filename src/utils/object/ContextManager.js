export {
    getValue
}

function getValue(context, id) {
    if (!id) return "";

    const ids = id.split('.');
    let value = "";

    if (context) {
        let index;
        const regex = /\[[0-9]*\]/;
        const result = id.match(regex);
        if (result) {
            ids[0] = ids[0].replace(result[0], "");
            index = result[0].replace(/[^0-9]/g, "");
        }

        if (ids.length == 2) {
            if (index) {
                value = context[ids[0]][index][ids[1]];
            } else {
                if (context[ids[0]]) {
                    value = context[ids[0]][ids[1]];
                }
            }
        } else if(ids.length == 3) {
            value = context[ids[0]][ids[1]][ids[2]] ? context[ids[0]][ids[1]][ids[2]] : '';
        }
        else {
            value = context[id];
        }
    }

    return value;
}
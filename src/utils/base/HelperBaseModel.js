export {
    cd,
    cdNm,
    search,
}

function cd(target, property, descriptor) {
    target._cd = property;
    return descriptor;
}

function cdNm(target, property, descriptor) {
    target._cdNm = property;
    return descriptor;
}

function search(target, property, descriptor) {
    target._search = property;
    return descriptor;
}
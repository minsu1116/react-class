import { action, toJS, observable } from "mobx";
import React from 'react'
import API from "components/override/api/API";
import Alert from "components/override/alert/Alert";
import { nonenumerable } from 'utils/object/Decorator.js';
import { locale } from 'utils/word/locale'
// import { BasePopupStore, BasePopupComponent } from "utils/base/BasePopupComponent";
import FileUtil from 'components/atoms/file/FileUtil';
import CommonStore from "modules/pages/common/service/CommonStore";
import { getValue } from "utils/object/ContextManager";
import GridUtil from 'components/override/grid/utils/GridUtil';
// import { User } from "modules/pages/system/pgm/user/User";

export {
    BaseStore,
    gridTrigger,
    gridAddPosition,
    // gridColumnType,
    model,
    name,
}

function model(target, property, descriptor) {
    if (!target._modelList) {
        target._modelList = [];
    }
    target._modelList[property] = "";
}

function name(name) {
    return (target, property, descriptor) => {
        if (!target._nameList) {
            target._nameList = [];
        }
        target._nameList.push({ id: property, name: name || property })
    }
}

const gridTrigger = {
    "editable": "editable",
    "onCellValueChanged": "onCellValueChanged",
    "rendererVisible": "cellRendererParams",
    "colSpan": "colSpan",
    "valueFormatter": "valueFormatter",
    "onCellClick": "onCellClick",
    "cellRendererParams": "cellRendererParams",
}

const gridAddPosition = {
    "lastIndex": "lastIndex",
    "firstIndex": "firstIndex",
    "selectedItemNextIndex": "selectedItemNextIndex",
}

// const gridColumnType = {

//     // 날짜 Format 설정
//     "dateTime": "dateTime",
//     "dateTimeFull": "dateTimeFull",

//     // 숫자 컬럼 설정
//     "number": "number",

//     // 체크박스 컬럼 설정
//     "checkbox": "checkbox",
//     "checkboxAll": "checkboxAll",

//     // 버튼 컬럼 설정
//     "button": "button",
//     "iconButton": "iconButton",

//     // CodeHelper
//     "codeHelper": "codeHelper",

//     // 날짜 Input 컬럼 설정
//     "datePicker": "datePicker",
//     "datePickerYYYYMM": "datePickerYYYYMM",
//     "datePickerHHmm": "datePickerHHmm",
//     "datePickerTime": "datePickerTime",

//     // textarea 컬럼 설정
//     "textarea": "textarea",

//     // 다운로드 컬럼 설정
//     "download": "download",
// }

class BaseStore {

    constructor() {
        this._modelList = Object.getPrototypeOf(this)._modelList || {};
        this._nameList = Object.getPrototypeOf(this)._nameList || {};
        this._setInitialState();
    }

    /**
     * [공통] 변수 초기값 저장
     */
    @nonenumerable _superInitialState;
    @nonenumerable _modelList = {};
    @nonenumerable _nameList = {};
    @nonenumerable _gridList = [];
    @nonenumerable __tabGridList = {};

    /**
     * [공통] 화면에서 선언되어 있는 <CodeHelper />의 팝업 핸들로 관련 변수
     */
    _codeHelpers = {};

    /**
     * [공통] Component Initialize, Loading 완료 여부
     */
    @observable @nonenumerable _isInitialize = false;
    @observable @nonenumerable _isLoading = false;
    @observable @nonenumerable _loadingLayer = true;

    /**
     * [공통] Loaded 관련 "SGrid" 로딩 여부 체크 Property
     */
    @nonenumerable _gridCnt = 0;
    @observable @nonenumerable _loadedGridCnt = 0;

    /**
     * [공통-Modal] showModal을 통하여 Modal 띄웠을 때 동기 처리 관리
     */
    @nonenumerable _promiseInfo = {};

    /**
     * [공통-파일저장] 파일 저장 시에 컨트롤에 신호를 주고 해당 작업 비동기로 완료
     */
    @observable @nonenumerable _fileSaveStart = false;
    @observable @nonenumerable _promiseInfoForFileSave = {};

    // [Modal] Modal 정보
    /**
     * [공통-Modal] Component에 전달할 showModal 정보
     */
    @observable @nonenumerable _modalManage;

    /**
     * [공통-Modal] 내 화면이 Modal일 때 종료하기 위한 핸들러
     */
    @nonenumerable _closeHandler;

    /**
     * [공통] MainTabChanged 이벤트가 일어났을 때 이벤트 발생 
     */
    @nonenumerable _isTabChange = false;
    @nonenumerable _parameter;

    /**
     * 해당 model에 필수요소 값(essential decorate)에 한하여 빈 값인지 체크합니다.
     * @modelField ** 검사할 객체 명칭
     * @showMessage 빈 값이 있으면 message를 띄움. 기본값 true
     */
    @nonenumerable
    validation = async (modelField, showMessage = true) => {
        const empty = [];
        const essential = this._modelList[modelField].filter(x => x.essential == true);
        const model = this[modelField];

        essential.map(item => {
            if (model[item.id] == null || model[item.id] == "") {
                empty.push(item);
            }
        });

        if (empty.length > 0 && showMessage) {
            let message = "";
            empty.map(item => {
                message += `[${this.l10n(item.name)}],`
            })
            message = message.substr(0, message.length - 1);
            await Alert.meg(this.l10n("M03343", "필수항목 값이 입력되지 않았습니다.") + `<br/>${message}`);
        }

        return empty.length == 0 ? true : false;
    }

    @nonenumerable
    targetValidation = async (targetName, fields, showMessage = true) => {
        let fieldArray = [];
        if (!Array.isArray(fields)) {
            fieldArray.push(fields);
        } else {
            fieldArray = fields;
        }

        const target = targetName == "this" ? this : this[targetName];
        const empty = [];

        fieldArray.map(f => {
            if (target[f] == null || target[f] == "") {
                empty.push(f);
            }
        });

        if (empty.length > 0 && showMessage) {
            const nameArray = targetName == "this" ? this._nameList : this._modelList[targetName];

            let message = "";
            empty.map(item => {
                const info = nameArray.find(x => x.id == item);
                if (info) {
                    message += `[${this.l10n(info.name)}],`
                } else {
                    message += `[${item}],`
                }
            })
            message = message.substr(0, message.length - 1);
            await Alert.meg(this.l10n("M03343", "필수항목 값이 입력되지 않았습니다.") + `<br/>${message}`);
        }

        return empty.length == 0 ? true : false;
    }

    propertyInit = (property) => {
        if (this[property]) {
            if (typeof (this[property]) == "object") {
                const intersectionKeys = this._intersection(this[key], this._superInitialState[property]);
                intersectionKeys.map(k => {
                    this[key][k] = this._superInitialState[property][k];
                })
            } else {
                this[property] = this._superInitialState[property];
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * 리포트 출력
     * @mrdName 리포트 출력 Name. "---.mrd"
     * @reportType 리포트 Type 
     * @param 데이터 조회 시 사용할 parameter. Object 형태로 보내주세요.
     */
    @nonenumerable
    showReport = async (mrdName, reportType, params = {}) => {
        const report = {
            mrdName: mrdName,
            reportType: reportType,
            locale: CommonStore.locale,
        }

        Object.assign(params, report);

        window.open('about:blank', "POPUPFORM", "location=false,width=750,height=700,resizable=1");

        var form = document.createElement('FORM');
        form.method = "POST";
        form.action = `${WEBPACK_CONFIG_API_URL}/report`;
        form.target = "POPUPFORM";

        const length = Object.keys(params).length;
        const keyArr = Object.keys(params);
        for (let i = 0; i < length; i++) {
            const key = keyArr[i];
            const value = params[key];

            var input = document.createElement("INPUT");
            input.name = key;
            input.type = "hidden";
            input.value = value;
            form.appendChild(input);
        }
        document.body.appendChild(form);

        form.submit();
    }

    /**
     * 서버 접근
     * @path 접근할 api주소
     * @params 전달할 RequestBody 정보
     */
    @nonenumerable
    execute = async (path, params) => {
        const result = await API.request.post(encodeURI(`/api${path}`), params);
        if (API.isLocal) {
            if (path.indexOf('/home') < 0) {
                console.log('#execute', `/api${path}`, { params: params }, { result: result });
            }
        }
        return result.data;
    }

    @nonenumerable
    save = async (path, params, showMessage = true) => {

        if (showMessage) {
            const confirmResult = await Alert.confirm(this.l10n("M01490", "저장하시겠습니까?"));
            if (!confirmResult) {
                return;
            }
        }

        const result = await API.request.post(encodeURI(`/api${path}`), params);
        if (API.isLocal) {
            console.log('#save', `/api${path}`, { params: params }, { result: result });
        }

        if (result.data.success) {
            if (showMessage) {
                await Alert.meg(this.l10n("M01060", "저장되었습니다"));
            }
            return result.data;
        } else {
            return;
        }
    }

    @nonenumerable
    delete = async (path, params) => {

        const confirmResult = await Alert.confirm(this.l10n("M01492", "삭제하시겠습니까?"));
        if (!confirmResult) {
            return;
        }

        const result = await API.request.post(encodeURI(`/api${path}`), params);
        if (API.isLocal) {
            console.log('#delete', `/api${path}`, { params: params }, { result: result });
        }

        if (result.data.success) {
            await Alert.meg(this.l10n("M01311", "삭제되었습니다"));
            return result.data;
        } else {
            return;
        }
    }

    @nonenumerable
    quartz = async (path, params) => {
        const result = await API.quartz.post(encodeURI(`/quartz${path}`), params);
        if (API.isLocal) {
            console.log('#quartz', `/quartz${path}`, { params: params }, { result: result });
        }
        return result.data;
    }

    /**
     * 원하는 Modal을 띄워줍니다.
     * @modalInfo /utils/base/ModalManage 객체 전달
     */
    @nonenumerable
    showModal = async (modalInfo) => {
        const { path, parameter } = modalInfo;
        const split = path.split('/');
        const className = split[split.length - 1];

        await import(`modules/pages${path}`).then(module => {
            const Component = module[className];
            const Store = module[className + "Store"];
            modalInfo.contents = <Component store={new Store()} _parameter={parameter} _closeHandler={this._closeModal} _isModal={true} />;
            modalInfo.isOpen = true;
            this._modalManage = modalInfo;
        })

        return new Promise((resolve, reject) => {
            this._promiseInfo = { resolve, reject };
        });
    }

    /**
     * 현재 내 화면이 Modal일 때 종료합니다.
     * @param 부모에게 넘겨줄 parameter
     */
    @nonenumerable
    close = (param = null) => {
        this._closeHandler(param);
    }

    /**
     * Helper Open
     */
    @nonenumerable
    showHelper = async (helper) => {
        helper.parameter = { helper: helper };
        const result = await this.showModal(helper);

        if (result) {
            return result;
        } else {
            return;
        }
    }

    /**
     * Componenet에 선언되어 있는 <CodeHelper /> 를 소스상에서 오픈하고 싶을 때 사용합니다.
     * @helperId ** 오픈하고자 하는 <CodeHelper helperId={"특정값"} /> 에서 "특정값"을 넘겨주세요.
     */
    @nonenumerable
    showCodeHelper = async (helperId) => {
        if (this._codeHelpers[helperId]) {
            const handler = this._codeHelpers[helperId];
            await handler();
        } else {
            console.warn("<CodeHelper />에 helperId props을 넘겨주세요.");
        }
    }

    // @nonenumerable
    // showPopup = async (modalInfo) => {

    //     // const path = "/system/std/code/Code";
    //     const path = "/base/BasePopupComponent";
    //     const split = path.split('/');
    //     const className = split[split.length - 1];
    //     // const className = "Code";

    //     // BasePopupStore.setParameter(modalInfo)
    //     console.log("SHOW POPUP", modalInfo);
    //     // console.log("document", document)

    //     const spec =  "toolbar=no, width=540, height=467, directories=no, status=no, scrollorbars=no, resizable=no";
    //     const popupWindow = window.open("", '', spec);
    //     // console.log("MYFORM", )

    //     if (popupWindow) {
    //         // popupWindow.document.createElement('div');
    //         console.log("popupWindow 1)", popupWindow);
    //         console.log("popupWindow 2)", popupWindow.document.body);
    //         console.log("popupWindow 3)", popupWindow.document.getElementById("root"));

    //         const Component = React.createElement(BasePopupComponent, modalInfo, null);
    //         // var d= popupWindow.document.createElement(child);
    //         popupWindow.document.body.appendChild(ReactDOM.render(<BasePopupComponent parameter={modalInfo}/>, popupWindow.document))
    //         console.log("POPUP>>>", popupWindow, popupWindow.document.getElementById("popup"));
    //         // d.id = "popup";//.classList.add("help-block");
    //         // console.log(">>>>>>>>", d, popupWindow.document.getElementById("popup"))
    //         // d.appendChild(child);A
    //         // popupWindow.document.getElementById("popup").appendChild(child);


    //         // const containerElement = popupWindow.document.createElement(child);
    //         // popupWindow.document.body.write(child);
    //         // console.log()
    //         // popupWindow.document.body.appendChild(child)

    //         // const child = BasePopupComponent;
    //         // console.log("IMPORT containerElement>>>", containerElement, popupWindow.document.body)

    //         // // popupWindow.document.write("<html><head><title>New Window Title!</title></head><body><h1 style='font-size:12pt;padding:5px;border:1px solid black;'>Hello</h1></body></html>");  

    //         // // const containerElement = popupWindow.document.getElementById("root"); //popupWindow.document.createElement('div');
    //         // // console.log("###", containerElement);

    //         // // // popupWindow.document.body.appendChild(containerElement);
    //         // // console.log("popupWindow 2)", popupWindow);

    //         // // const props = { className: "youngjae", onClick: () => { } };
    //         // // //const child = <User />        //<BasePopupComponent />// <div>Hello YJ</div>

    //         // // await import(`modules/pages${path}`).then(module => {
    //         // await import(`utils${path}`).then(module => {
    //         //     const Component = module[className];
    //         //     console.log("IMPORT", Component)

    //         //     // const containerElement = popupWindow.document.createElement(ReactDOM.render(<Component />, popupWindow.document));
    //         //     const containerElement = React.createElement(Component, modalInfo, null); //popupWindow.document.createElement(React.createElement(<Component />, modalInfo, null));
    //         //     console.log("IMPORT containerElement>>>", containerElement, popupWindow.document.body)
    //         //     popupWindow.document.body.appendChild(containerElement)

    //         //     // popupWindow.document.write(ReactDOM.render(<Component />, popupWindow.document))
    //         //     // modalInfo.contents = <Component _parameter={parameter} _closeHandler={this._closeModal} _isModal={true} />;
    //         //     // modalInfo.isOpen = true;
    //         //     // this._modalManage = modalInfo;
    //         // })

    //         // console.log("AAAAAAAAAAAAA", popupWindow.document.getElementsByTagName('div'));
    //     }


    //     // var myForm = document.getElementsByClassName('popup') ;
    //     // var url = "/popup";
    //     // window.open("", "popup",
    //     //     "toolbar=no, width=540, height=467, directories=no, status=no,    scrollorbars=no, resizable=no");
    //     //     console.log("myForm", myForm);
    //     // // myForm.action = url;
    //     // myForm.method = "post";
    //     // myForm.target = "popup";
    //     // myForm.testVal = 'test';
    //     // myForm.submit();

    //     // console.log("SHOW POPUP", myForm)
    // }

    @nonenumerable
    setBind = (target, newValue) => {
        if (!newValue) {
            return;
        }
        const newValueKeys = Object.keys(newValue);
        newValueKeys.map(key => {
            target[key] = newValue[key];
        })
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////
    // 'SGrid'의 컬럼 설정

    /** 
     * [SGrid] 컬럼 Option 추가
     * @columnDefs ** 컬럼 정의 집합.
     * @field ** 적용할 field. 단일 적용: "field", 다중 적용: ["field1", "field2"], 전체 적용: "ALL"
     * @trigger ** 적용할 Option.
     * @func ** Method Handler 또는 적용 값.
     * ***********************
     * #Trigger 종류: BaseStore의 "gridTrigger" import하여 "gridTrigger.valueFormatter" 처럼 사용하시면 됩니다.
     * @valueFormatter 셀의 값을 커스텀.
     * @editable 조건에 따라 수정여부를 조정.
     * @onCellValueChanged 셀의 값이 변경될 때 이벤트 핸들러.
     * @colSpan Cell 병합.
     * @onCellClick 셀을 클릭했을 때 이벤트 핸들러.
     */
    @nonenumerable
    setOption = (columnDefs, field, trigger, func) => {
        if (!columnDefs || columnDefs.length <= 0) return;

        let target = [];
        if (!Array.isArray(field)) {
            target.push(field);
        } else {
            target = field;
        }

        if (field == "ALL") {
            target = columnDefs.map(x => { return x.field });
        }

        target.map(field => {
            this._setOption(columnDefs, field, trigger, func);
        })
    }

    @nonenumerable
    _setOption = (columnDefs, field, trigger, func) => {
        for (let i = 0; i < columnDefs.length; i++) {
            const col = columnDefs[i];
            // console.log(i, col, col.children)
            if (col.children) {
                this._setOption(col.children, field, trigger, func);
            }

            if (col.field == field) {
                col[trigger] = func;
                break;
            }
        }
    }

    /** 
     * [SGrid] Row 병합
     * @columnDefs ** 컬럼 정의 집합.
     * @field ** 적용할 field. 단일 적용: "field", 다중 적용: ["field1", "field2"], 전체 적용: "ALL"
     * @refColumn 병합 시 참조할 columnField. 단일 적용: "field", 다중 적용: ["field1", "field2"]
     */
    @nonenumerable
    setRowSpanning = (columnDefs, field, refColumn = null) => {
        if (!columnDefs || columnDefs.length <= 0) return;

        let refTarget = undefined;
        if (refColumn) {
            refTarget = [];
            if (!Array.isArray(refColumn)) {
                refTarget.push(refColumn);
            } else {
                refTarget = refColumn;
            }
        }

        const func = function (params) {
            return GridUtil.rowSpanning(params, refColumn ? false : true, refTarget);
        }

        this.setOption(columnDefs, field, "cellRenderer", "rowSpanCellRender")
        this.setOption(columnDefs, field, "rowSpan", func);
        this.setOption(columnDefs, field, "valueFormatter", undefined);
    }

    /**
     * [SGrid] 합계라인 추가
     * @columnDefs ** 컬럼 정의 집합.
     * @field ** 적용할 field. 단일 적용: "field", 다중 적용: ["field1", "field2"]
     */
    @nonenumerable
    setSummary = (columnDefs, field) => {
        if (!columnDefs || columnDefs.length <= 0) return;

        let target = [];
        if (!Array.isArray(field)) {
            target.push(field);
        } else {
            target = field;
        }

        const firstColumnField = columnDefs.find(x => x.hide == undefined || x.hide == false);
        const pinned = [];
        target.map(field => {
            if (firstColumnField) {
                pinned[firstColumnField.field] = this.l10n("M03344", "합계");
            }
            pinned[field] = 0;
        })
        columnDefs[0].pinnedBottomRowData = [pinned];
    }

    /** 
     * [SGrid] Row 병합
     * @columnDefs ** 컬럼 정의 집합.
     * @func ** Method Handler.
     */
    @nonenumerable
    setRowStyle = (columnDefs, func) => {
        if (!columnDefs || columnDefs.length <= 0) return;
        columnDefs[0].rowStyle = func;
    }

    /** 
     * [SGrid] CellStyle 적용.
     * @columnDefs ** 컬럼 정의 집합.
     * @field ** 적용할 field. 단일 적용: "field", 다중 적용: ["field1", "field2"], 전체 적용: "ALL"
     * @func ** Method Handler.
     */
    @nonenumerable
    setCellStyle = (columnDefs, field, func) => {
        if (!columnDefs || columnDefs.length <= 0) return;
        if (!func) return;

        function cellStyle(params) {
            if (params.node.rowPinned != "bottom") {
                if (typeof func == "function") {
                    return func(params);
                } else {
                    return func;
                }
            }
        }

        this.setOption(columnDefs, field, "cellStyle", cellStyle);
    }

    /**
     * [SGrid] 추가된 Row면 수정가능, 기존 Row면 수정불가.
     * @columnDefs ** 컬럼 정의 집합.
     * @field ** 적용할 field. 단일 적용: "field", 다중 적용: ["field1", "field2"], 전체 적용: "ALL"
     */
    @nonenumerable
    setNewRowEditable = (columnDefs, field) => {
        if (!columnDefs || columnDefs.length <= 0) return;

        function keyEditable(params) {
            if (params.data.newRow) {
                return true;
            } else {
                return false;
            }
        }
        this.setOption(columnDefs, field, "editable", keyEditable);
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * SFile이나 SImageFile을 사용하여 파일을 저장할 때 사용합니다.
     * @file File Id가 저장되어 있는 field
     * 성공 시 저장한 fileId Return
     */
    fileSave = async (fileId, showMessage = false) => {
        const originValue = getValue(this, fileId);
        const targetId = fileId.replace(".", "");
        const file = getValue(this, `_${targetId}`);

        if (!file) {
            if (showMessage) {
                await Alert.meg(this.l10n("M02017", "저장할 파일이 없습니다."));
            }

            return originValue;
        }

        const result = await FileUtil.fileUpload(file, originValue);

        if (result.success) {
            return result.data.fileGrpId;
        } else {
            if (showMessage) {
                await Alert.meg(this.l10n("M02018", "파일 저장 시 에러가 발생하였습니다."));
            }
            return;
        }
    }

    fileGroupSave = async () => {
        this._fileSaveStart = true;

        if (document.getElementsByClassName("file_multi_item").length >= 1) {
            return new Promise((resolve, reject) => {
                this._promiseInfoForFileSave = { resolve, reject };
            });
        } else {
            console.warn("[ERROR] 파일 저장 컨트롤이 화면에 없습니다.");
            return;
        }
    }

    fileAttach = async () => {
        return await FileUtil.fileAttach();
    }

    fileDelete = async (fileId) => {
        return FileUtil.fileDelete(fileId);
    }

    fileGroupDelete = async (fileGrpId) => {
        return FileUtil.fileGroupDelete(fileGrpId);
    }

    /**
     * @fileId ** 다운로드 할 fileId. (fileGrpId 아닙니다.)
     * @docId "품질-자료" 다운로드 히스토리 관련. "품질-자료"가 아니라면 넘기지 마세요.
     */
    fileDownload = async (fileId, docId) => {
        return await FileUtil.fileDownload(fileId, docId);
    }

    /**
     * 
     * @param {List Object Key} key 
     * @param {asc, desc} order 
     * @returns 
     */
    comareValues = (key, order = 'asc') => {
        return function (a, b) {
            if (!a.hasOwnProperty(key) || !b.hasOwnProperty(key)) return 0;
            let comparison = a[key].localeCompare(b[key]);

            return (
                (order == 'desc') ? (comparison * -1) : comparison
            );
        };
    }


    //////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * (Auto) Component handleChange Event
     */
    @action.bound @nonenumerable
    _handleChange = (data) => {
        const { id, value } = data;

        if (!id) return;
        const o = id.split('.');

        let index;
        const regex = /\[[0-9]*\]/;
        const result = id.match(regex);
        if (result) {
            o[0] = o[0].replace(result[0], "");
            index = result[0].replace(/[^0-9]/g, "");
        }

        if (o.length == 2) {
            if (index) {
                this[o[0]][index][o[1]] = value;
            } else {
                this[o[0]][o[1]] = value;
            }
        } else {
            this[id] = value;
        }

        if (API.isLocal) {
            if (id.indexOf("_") != 0) {
                console.log('#change', data);
            }
        }
    }

    _intersection(o1, o2) {
        if(!o1 || !o2) {
            return;
        }
        return Object.keys(o1).filter({}.hasOwnProperty.bind(o2));
    }

    @action.bound
    _setParameter = (parameter) => {

        if (parameter) {
            const currentKeys = Object.keys(this._getVariable(this));
            const paramsKeys = Object.keys(parameter);

            currentKeys.map(key => {
                if (paramsKeys.some(x => x == key)) {
                    if (typeof (parameter[key]) == "object") {

                        if (this[key]) {
                            const intersectionKeys = this._intersection(this[key], parameter[key]);
                            if(intersectionKeys) {
                                intersectionKeys.map(k => {
                                    this[key][k] = parameter[key][k];
                                })
                            }
                        } else {
                            this[key] = parameter[key];
                        }

                    } else {
                        this[key] = parameter[key];
                    }
                }
            })
        }
    }

    /**
     * 다국어 설정
     */
    @nonenumerable
    l10n = (wordCd) => {
        return locale[wordCd] ? locale[wordCd] : wordCd;
    }

    /**
     * type을 넘겨주세요.
     * @comp 회사 리스트
     */
    @nonenumerable
    getCommonList = async (type, param = null) => {
        let commonList;
        switch (type) {
            case "comp":
                const basicParam = {
                    compCd: "",
                }
                const result = await this.execute(`/helper/select-box/comp`, param || basicParam);
                if (result.success) {
                    commonList = result.data;
                }
                break;
        }

        return commonList;
    }

    @nonenumerable
    _getVariable = (target = this) => {
        const result = {};
        const items = toJS(target);

        Object.keys(items).map(key => {
            const item = items[key];
            if (typeof item != "function" && key.substring(0, 1) != "_" && key != "grid") {
                result[key] = item;
            }
        });

        return result;
    }

    // @nonenumerable
    // _getReactionVariable = (target = this) => {
    //     const result = {};
    //     const items = toJS(this.$mobx.values);

    //     Object.keys(items).map(key => {
    //         const item = items[key];
    //         if (typeof item != "function" && key.substring(0, 1) != "_" && key != "grid") {
    //             result[key] = item;
    //         }
    //     });

    //     return result;
    // }

    // [공통] Component 로딩될 때 초기화
    @nonenumerable
    _initialize = async (parameter) => {
        // // "로그인 시" 초기화 되면 compCd 정보 없음
        // Object.keys(this._modelList).map(key => {
        //     if (!this[key].compCd) this[key].compCd = CommonStore.compCd;
        // });

        this._parameter = parameter;
        if (parameter) {
            this._setParameter(parameter)
        }
        // if (!this._isInitialize) {
        if (this.initialize) {
            await this.initialize(parameter);
        }

        this._isInitialize = true;
    }

    _loaded = () => {
        if (this._gridCnt == 0) {
            this._loadedGrid();
        }
    }

    @nonenumerable
    _loadedGrid = async (gridId) => {
        // if (!gridId) return;
        if (this._gridCnt == 0) {
            if (!this._isLoading) {
                this._isLoading = true;
                if (this.loaded) {
                    await this.loaded();
                }
            }

            if (this._isTabChange) {
                if (this.tabChangedLoaded) {
                    await this.tabChangedLoaded(this._parameter);
                    this.tabChangedLoaded = undefined;
                }
            }
        } else {
            this._loadedGridCnt++;
            this._gridList.push(gridId);

            // 1) Tab안에 Grid가 있을 경우 2) 그 Tab에 onChanged 이벤트가 있을 경우
            if (this.__tabGridList.gridList) {
                // => 그리드의 onReadyGrid 콜백함수 호출 후에 TabChange 이벤트 발생
                if (this.__tabGridList.gridList.length > 1) {
                    let result = true;
                    this.__tabGridList.gridList.map(x => {
                        if (!this._gridList.some(grid => grid == x)) {
                            result = false;
                        }
                    });
                    if (result) {
                        this.__tabGridList.onTabChange();
                        this.__tabGridList = {};
                    }
                } else {
                    if (this._gridList.some(x => x == this.__tabGridList.gridList[0])) {
                        this.__tabGridList.onTabChange();
                        this.__tabGridList = {};
                    }
                }
            }

            if (this._loadedGridCnt == this._gridCnt) {
                if (!this._isLoading) {
                    this._isLoading = true;
                    if (this.loaded) {
                        await this.loaded();
                    }
                }

                this._loadedGridCnt = 0;
                this._gridList = [];

                if (this._isTabChange) {
                    if (this.tabChangedLoaded) {
                        await this.tabChangedLoaded(this._parameter);
                        this.tabChangedLoaded = undefined;
                    }
                }

            }
        }

        this._loadingLayer = false;
    }

    @nonenumerable
    _finalize = () => {
        this._gridCnt = 0;
        this._loadedGridCnt = 0;
        this._gridList = [];

        if (this.finalize) {
            this.finalize();
        }
    }

    // [공통] 변수 초기값 저장
    @action.bound
    _setInitialState() {
        this._superInitialState = toJS(this);
        if (this._modelList) {
            Object.keys(this._modelList).map(property => {
                this._modelList[property] = this[property]._getProperty() || this[property]._varProperty;
            });
        }
    }

    // [MainStore] Modal 종료
    @nonenumerable
    _closeModal = (param) => {
        this._modalManage = null;
        this._promiseInfo.resolve(param);
    }

    @nonenumerable
    @action.bound
    reset = () => {
        if (!this._superInitialState) {
            this._superInitialState = {};
        }

        const initVariable = Object.keys(this._superInitialState);
        const variable = Object.keys(this);

        variable.forEach(key => {
            if (initVariable.some(x => x == key)) {
                if (typeof (this._superInitialState[key]) == "object") {
                    const intersectionKeys = this._intersection(this[key], this._superInitialState[key]);
                    if(intersectionKeys) {
                        if (intersectionKeys.length <= 0) {
                            this[key] = this._superInitialState[key];
                        } else {
                            intersectionKeys.map(k => {
                                if (k != "_varProperty") { //TODO IE랑 CHROME이 intersectionKeys 내용이 다름.. IE때문에 추가함
                                    this[key][k] = this._superInitialState[key][k];
                                }
                            })
                        }
                    }
                } else {
                    this[key] = this._superInitialState[key];
                }
            } else {
                if (typeof (this[key]) == 'function') {
                } else {
                    this[key] = undefined;
                }
            }
        })

        this._isInitialize = false;
        this._isLoading = false;
    }
}
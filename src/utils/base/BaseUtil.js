import API from "components/override/api/API";
import { nonenumerable } from "utils/object/Decorator";
import { locale } from 'utils/word/locale'

export default class BaseUtil {

    /**
     * 서버 접근
     * @path 접근할 api주소
     * @params 전달할 RequestBody 정보
     */
    @nonenumerable
    execute = async (path, params) => {
        const result = await API.request.post(encodeURI(`/api${path}`), params);
        if (API.isLocal) {
            if (path.indexOf('/home') < 0) {
                console.log('#execute', `/api${path}`, { params: params }, { result: result });
            }
        }
        return result.data;
    }

    /**
     * 다국어 설정
     */
    @nonenumerable
    l10n = (wordCd) => {
        return locale[wordCd] ? locale[wordCd] : wordCd;
    }
}
import { observable } from "mobx";

export default class ModalManage {

    @observable isOpen = false;
    contents;

    id = "";
    parameter = {};
    width = 800;
    title = "LS_Template";
    className = "pop_panel_body";
}

import React, { Component, createContext } from 'react';
import SModal from 'components/override/modal/SModal'
import { locale } from 'utils/word/locale'
import ReactDOM from 'react-dom'
import { when, _allowStateChangesInsideComputed } from 'mobx';

const Context = createContext();

const { Provider, Consumer: BaseConsumer } = Context;

export {
    BaseComponent,
    BaseConsumer,
    // BaseContext,
}


class BaseComponent extends Component {

    girdCnt = undefined;
    constructor(props) {
        super(props);
        this.store = this.props.store;
        this.state = {
            loaded: false,
        }
    }

    async componentDidMount() {
        const { _parameter, _closeHandler, _isTabChange } = this.props;
        const { _isInitialize, _initialize, _handleChange } = this.store;

        const { sComponentDidMount } = this;
        _handleChange({ id: "_isTabChange", value: _isTabChange });
        _handleChange({ id: "_parameter", value: _parameter });

        // _handleChange({ id: "_loadingLayer", value: true });

        this._isMounted = true;

        if (!_isInitialize) {
            if (_initialize) {
                await _initialize(_parameter);
            }
        }

        if (_closeHandler) {
            // Modal일 경우 종료를 위하여 closeHandler 전달
            _handleChange({ id: "_closeHandler", value: _closeHandler });
        }

        if (sComponentDidMount) {
            await sComponentDidMount();
        }

        // this.forceUpdate(); //기본값 설정

        if (this._isMounted) {
            this.setState({ loaded: true });
        }

        if (this._isMounted) {
            try {
                const thisNode = ReactDOM.findDOMNode(this);
                if (thisNode) {
                    const gridCnt = thisNode.getElementsByClassName('ag-root-wrapper').length;
                    _handleChange({ id: "_gridCnt", value: gridCnt });
                    this.gridCnt = gridCnt;
                }
            } catch (exception) {
            }
        }

        await this.store._loaded();

        const labels = document.querySelectorAll("label");
        const ths = document.querySelectorAll("th");

        for (let i = 0; i < labels.length; i++) {
            labels[i].title = labels[i].innerText;
        }
        for (let i = 0; i < ths.length; i++) {
            ths[i].title = ths[i].innerText;
        }
    }

    l10n = (wordCd) => {
        return locale[wordCd] ? locale[wordCd] : wordCd;
    }

    render() {
        const { loaded } = this.state;
        const { _isModal } = this.props;

        if (loaded) {
            this._reaction();
            const inheritance = this.sRender();

            return (
                <div className={_isModal ? "" : "basic"}>
                    <Provider value={this.store}>
                        {inheritance}
                        {
                            this.store._modalManage && this.child != true ?
                                <SModal
                                    id={this.store._modalManage.id}
                                    contentLabel={this.store._modalManage.title}
                                    isOpen={this.store._modalManage.isOpen}
                                    width={this.store._modalManage.width}
                                    height={this.store._modalManage.height}
                                    onRequestClose={this.store._closeModal}
                                    customClassName={this.store._modalManage.className}
                                // onAfterOpen={this.modalOpen}
                                >
                                    <div id={this.store._modalManage.id}>
                                        {this.store._modalManage.contents}
                                    </div>
                                </SModal>
                                :
                                null
                        }
                    </Provider>
                </div>
            )
        } else {
            return (
                <div className='basic' style={{
                    zIndex: 2001
                    , backgroundColor: 'white'
                    , overflow: 'hidden'
                    , opacity: 0.5
                }} />
            )
        }
    }

    _reaction() {

        if (this.store) {
            const variable = this.store._getVariable();

            Object.keys(variable).map((key, index) => {
                if (variable[key] instanceof Object) {
                    const var2 = this.store._getVariable(variable[key]);
                    Object.keys(var2).map((key2, index) => {
                        this.store[key][key2]
                    })
                }
                this.store[key];
            });
        }
    }

    // /**
    //  * Component 초기 생성단계가 끝났을 때 호출됩니다.
    //  */
    // componentDidMount() {
    //     const { sComponentDidMount } = this;
    //     if (sComponentDidMount) {
    //         sComponentDidMount();
    //     }
    // }

    // /**
    //  * props 로 받아온 값을 state 로 동기화 하는 작업을 해줘야 하는 경우에 사용됩니다.
    //  */
    // static getDerivedStateFromProps(nextProps, prevState) {
    //     const { sGetDerivedStateFromProps } = this;
    //     if (sGetDerivedStateFromProps) {
    //         sGetDerivedStateFromProps(nextProps, prevState);
    //     }
    // }

    // /**
    //  * 컴포넌트를 최적화하기 위하여 불필요한 경우에 재렌더링을 방지하기 위하여 사용됩니다.
    //  * return false일 때 업데이트를 하지 않습니다.
    //  */
    // shouldComponentUpdate(nextProps, nextState) {
    //     const { sShouldComponentUpdate } = this;
    //     if (sShouldComponentUpdate) {
    //         return sShouldComponentUpdate(nextProps, nextState);
    //     } else {
    //         return true;
    //     }
    // }

    /**
     * DOM 업데이트가 일어나기 직전의 시점입니다.
     * 여기서 반환 하는 값은 componentDidMount 에서 snapshot 값으로 받아올 수 있습니다.
     * 해당 API를 사용할 때엔 componentDidUpdate도 같이 구현이 되어야 합니다.
     */
    getSnapshotBeforeUpdate(prevProps, prevState) {
        const { sGetSnapshotBeforeUpdate } = this;
        if (sGetSnapshotBeforeUpdate) {
            return sGetSnapshotBeforeUpdate(prevProps, prevState);
        } else {
            return null;
        }
    }

    /**
     * 이 API는 컴포넌트에서 render() 를 호출하고난 다음에 발생하게 됩니다.
     * 이 시점에선 this.props 와 this.state 가 바뀌어있습니다.
     * 그리고 파라미터를 통해 이전의 값인 prevProps 와 prevState 를 조회 할 수 있습니다.
     * 그리고, getSnapshotBeforeUpdate 에서 반환한 snapshot 값은 세번째 값으로 받아옵니다.
     */
     UNSAFE_componentDidUpdate(prevProps, prevState, snapshot) {
        const { sComponentDidUpdate } = this;
        if (sComponentDidUpdate) {
            sComponentDidUpdate(prevProps, prevState, snapshot);
        }
    }

    /**
     * Component가 해제될 때 호출됩니다.
     * 이벤트, setTimeout, 외부 라이브러리 인스턴스 제거합니다.
     */
    componentWillUnmount() {
        this._isMounted = false;

        const { sComponentWillUnmount } = this;
        const { _finalize } = this.store;
        if (sComponentWillUnmount) {
            sComponentWillUnmount();
        }

        if (this.props._isModal) {
            this.store.reset();
        }

        _finalize();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////

    // exception = ["label", "SButton", "SGrid", "th"];

    // makeElement = (target) => {
    //     if (target.type && this.exception.includes(target.type)) {
    //         return target;
    //     }

    //     if (target.type && target.type.name && this.exception.includes(target.type.name)) {
    //         return target;
    //     }

    //     const { store } = this.props;
    //     const id = target.props.id;
    //     const props = JSON.parse(JSON.stringify(target.props));
    //     props.value = store[id];// ? store[id] : store.storeBase[id];
    //     props.onChange = store._handleChange;
    //     props.key = `key-${id}`;

    //     return React.cloneElement(target, props);
    // }

    // makeContents = (target) => {

    //     const main = React.Children.toArray(target.props.children);
    //     let control;

    //     control = [];
    //     for (let i = 0; i < main.length; i++) {
    //         const a = [];
    //         this.makeChildren(main[i], a);
    //         const props = this.copyProps(main[i].props);
    //         props.children = a;
    //         control.push(React.cloneElement(main[i], props));
    //     }

    //     const props = this.copyProps(target.props);
    //     props.children = control;
    //     return React.cloneElement(target, props);
    // }

    // makeChildren = (target, newChildren2) => {
    //     const childrenArr = React.Children.toArray(target.props.children);

    //     for (let i = 0; i < childrenArr.length; i++) {
    //         let newChild;
    //         let props;
    //         if (this.isExsitChildren(childrenArr[i])) { // 자식이 있으면
    //             const newChildren = [];
    //             this.makeChildren(childrenArr[i], newChildren);
    //             newChild = newChildren;

    //             props = this.copyProps(childrenArr[i].props);
    //             props.children = newChild;
    //             props.key = `key-${i}`;
    //             newChildren2.push(React.cloneElement(childrenArr[i], props));

    //         } else { // 자식이 없으면
    //             newChild = this.makeElement(childrenArr[i]);
    //             newChildren2.push(newChild);
    //         }
    //     }
    //     return newChildren2;
    // }

    // isExsitChildren = (target) => {
    //     const arr = React.Children.toArray(target.props.children);
    //     if (typeof arr[0] == "string") {
    //         return false;
    //     }
    //     return arr.length > 0 ? true : false;
    // }

    // copyProps = (targetProps) => {
    //     const props = {};
    //     Object.keys(targetProps).map(key => {
    //         if (key != "children") {
    //             props[key] = targetProps[key];
    //         }
    //     })
    //     return props;
    // }
}
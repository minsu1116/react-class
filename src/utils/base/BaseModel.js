import { action, observable } from 'mobx';
import { nonenumerable } from 'utils/object/Decorator.js';
import CommonStore from 'modules/pages/common/service/CommonStore';

export {
    BaseModel,
    essential,
    name,
}

function essential(name) {
    return (target, property, descriptor) => {
        target._setProperty(property, name, true);
    }
}

function name(name) {
    return (target, property, descriptor) => {
        target._setProperty(property, name, false);
    }
}

class BaseModel {

    @observable compCd = CommonStore.compCd;

    ///////////////////////////////////////////////////////////////////////////////////////////////////

    @nonenumerable _varProperty;

    @action.bound @nonenumerable
    _setProperty = (property, name, essential) => {
        if (!this._varProperty) {
            this._varProperty = [];
        }
        this._varProperty.push({ name: name ? name : property, id: property, essential: essential });
    }

    @nonenumerable
    _getProperty = () => {
        // 크롬에서는 this._varProperty이 없음..
        return this._varProperty ? this._varProperty : Object.getPrototypeOf(this)._varProperty;
    }
}
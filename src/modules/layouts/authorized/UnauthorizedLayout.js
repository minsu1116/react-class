import React, {Component} from 'react'
import { Switch, Route, Redirect } from 'react-router-dom'

import LoginPage from 'modules/layouts/login/Login'
import Sso from 'modules/layouts/login/Sso'

class UnauthorizedLayout extends Component {
    
    constructor(props) {
        super(props);
    }
    
    render() {


        return(
            <div className="unauthorized-layout">
                <Switch>
                    <Route path="/auth/login/:target" component={Sso} />
                    <Route path="/auth/login" exact component={LoginPage} />
                    <Redirect to="/auth/login" />
                </Switch>
            </div>
 
        );
    }
}
export default UnauthorizedLayout
import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
import NoMatch from 'modules/layouts/match/NoMatch';

// Popup에 사용할 컨텐츠 import
// Param1: 실제 컨텐츠 경로
// Param2: 권한 관리에 적용된 Form URL
// export const Code = asyncComponent(() => import('modules/pages/system/code/Code'), '/system/code/Code');

class DirectPopupLayout extends Component {

    render() {
        return (
            <div className="unauthorized-layout" style={{ height: '100vh', width: '100vh' }}>
                <Switch>
                    {/* <Route path="/popup/code" exact component={Code} /> */}
                    <Route component={NoMatch} />
                </Switch>
            </div>
        );
    }
}
export default DirectPopupLayout
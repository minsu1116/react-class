
import React, { Component } from 'react'
import { inject } from 'mobx-react'

@inject(stores => ({
    mainStore: stores.mainStore,
    onMenuOpen: stores.mainStore.onMenuOpen,
    isMenuExpand: stores.mainStore.isMenuExpand,
    isMenuOverlap: stores.mainStore.isMenuOverlap,
    onOneDepthMenuClick: stores.mainStore.onOneDepthMenuClick,
    sideMenuList: stores.mainStore.sideMenuList,
}))
class SideNavMenu extends Component {

    UNSAFE_componentDidUpdate(nextProps) {
        if (this.props.isMenuExpand != nextProps.isMenuExpand) {
            this.twoDepthAllVisible(nextProps.isMenuExpand);
        }
    }

    // 소메뉴 전체 Visible 처리
    twoDepthAllVisible = (value) => {
        // const activeAll = document.querySelectorAll('.active');

        // for (let i = 0; i < activeAll.length; i++) {
        //     const classList = activeAll[i].classList;

        //     if (value) {
        //         classList.remove('allOff');
        //     } else {
        //         classList.add('allOff');
        //     }
        // }
    }

    oneDepthMenuClick = (event) => {

        const { isMenuExpand, isMenuOverlap, onOneDepthMenuClick } = this.props;

        const targetClassList = event.currentTarget.parentNode.classList;
        const onCheck = targetClassList.contains('off'); // onCheck true: 현재 닫힌 상태

        // 닫힌 상태에서 누를 경우
        if (!isMenuExpand && !isMenuOverlap) {
            this.twoDepthAllVisible(true);
            onOneDepthMenuClick();
            targetClassList.remove('off');
        } else {
            if (onCheck) {
                // 소메뉴 열기
                targetClassList.remove('off');
            } else {
                // 소메뉴 닫기
                targetClassList.add('off');
            }
        }
    }

    twoDepthMenuClick = (subMenu) => {

        console.log(subMenu, 'twoDepthMenuClick')
        const { isMenuExpand, onMenuOpen } = this.props;

        if (!isMenuExpand) {
            this.twoDepthAllVisible(false);
        }

        onMenuOpen(subMenu);

        // 메뉴 클릭 시 소메뉴 축소
        this.props.mainStore.handleChange({ id: "isLargeMenuExpand", value: false });
        this.props.mainStore.handleChange({ id: "isSmallMenuVisible", value: false });
    }

    render() {

        const { sideMenuList } = this.props;

        return (
            <React.Fragment>
                <ul className='left_menu'>
                    {
                        (sideMenuList && sideMenuList.length > 0) && sideMenuList.map((menu) => {

                            return (
                                <React.Fragment key={menu.menuId}>
                                    <li className='active'>
                                        <a className='has_ul' onClick={this.oneDepthMenuClick}>
                                            <i className={menu.iconNm ? menu.iconNm : 'icon-folder'} />
                                            <span title={menu.localeMenuNm}>{menu.localeMenuNm}</span>
                                            {/* <span>{menu.menuNm}</span> */}
                                        </a>
                                        <ul className='two_depth_menu'>
                                            {
                                                (menu.subMenu && menu.subMenu.length > 0) && menu.subMenu.map((subMenu) => {
                                                    return (
                                                        <li key={subMenu.menuId} onClick={() => this.twoDepthMenuClick(subMenu)}>
                                                            <a title={subMenu.localeMenuNm}>
                                                                {subMenu.localeMenuNm}
                                                                {/* {subMenu.menuNm} */}
                                                            </a>
                                                        </li>
                                                    )
                                                })
                                            }
                                        </ul>
                                    </li>
                                </React.Fragment>
                            );
                        })
                    }
                </ul>
            </React.Fragment>
        );
    }
}

export default SideNavMenu;
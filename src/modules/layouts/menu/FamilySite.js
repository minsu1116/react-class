import React, { Component } from 'react'
import { inject } from 'mobx-react';
import { toJS } from 'mobx';
@inject(stores => ({
    familySiteArr: stores.mainStore.familySiteArr
}))
class FamilySite extends Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event) {
        const value = event.target.value;
        if (value != '') {
            window.open(value);
        }
    }

    render() {
        const { familySiteArr } = this.props;
        let option = null;
        let selectOption = null;
        if (familySiteArr) {
            option = toJS(familySiteArr);
            selectOption = (option.map.length > 0) && option.map((select, index) =>
                <option key={index} value={select.cd}>{select.cdNm}</option>
            );
        }

        return (
            <React.Fragment>
                {/* <div className="lnb_shadow">그림자</div> */}
                <div className="familysite_select">
                    <select id="checkYn" onChange={this.handleChange}>
                        {selectOption}
                    </select>
                </div>
            </React.Fragment>
        );
    }
}

export default FamilySite;
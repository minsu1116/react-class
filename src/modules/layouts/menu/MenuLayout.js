import React from 'react'
import { NavLink, withRouter } from 'react-router-dom'
import { inject } from 'mobx-react'
import { locale } from 'utils/word/locale'
import SideNavMenu from 'modules/layouts/menu/SideNavMenu';
import logo_wing from 'style/img/ls_logo_wing.png';
import ico_plus from 'style/img/ico_plus.png';
import ico_logout from 'style/img/ico_logout.svg';

import help from 'style/img/help.svg';
import ico_lang from 'style/img/lang.png'
import SModal from 'components/override/modal/SModal';
import moment from 'moment-timezone';
import ReactTooltip from 'react-tooltip';

import service_icon from 'style/img/ico_service.png'
import common_icon from 'style/img/ico_common.png'
import system_icon from 'style/img/ico_system.png'

import ico_kr from 'style/img/flag/ico_KR.png';
import ico_en from 'style/img/flag/ico_EN.png';
import ico_ja from 'style/img/flag/ico_JA.png';
import ico_ch from 'style/img/flag/ico_CH.png';


@inject(stores => ({
    mainStore: stores.mainStore,
    getSideMenu: stores.mainStore.getSideMenu,
    sideMenuList: stores.mainStore.sideMenuList,
    isMenuExpand: stores.mainStore.isMenuExpand,
    isMenuOverlap: stores.mainStore.isMenuOverlap,
    logout: stores.loginStore.logout,
    loginNm: stores.commonStore.loginNm,
    deptNm: stores.commonStore.deptNm,
    loginId: stores.commonStore.loginId,
    compCd: stores.commonStore.compCd,
    empPhoto: stores.commonStore.empPhoto,
    onMenuExpandClick: stores.mainStore.onMenuExpandClick,
    topMenuList: stores.mainStore.topMenuList,
    setSideMenuList: stores.mainStore.setSideMenuList,
    isLargeMenuExpand: stores.mainStore.isLargeMenuExpand,
    isSmallMenuVisible : stores.mainStore.isSmallMenuVisible,
    helperInfo: stores.commonStore.helperInfo,
    familySite: stores.commonStore.familySite,
    handleSettingOpen: stores.mainStore.handleSettingOpen,
    handleHelpDeskOpen: stores.mainStore.handleHelpDeskOpen,
    _modalManage: stores.mainStore._modalManage,
    _closeModal: stores.mainStore._closeModal,
    handleLoginChange: stores.commonStore.handleLoginChange,
}))
class MenuLayout extends React.Component {

    activeTopMenuNm = "";
    dataPolling = undefined;
    locale = undefined;

    isFamilySiteOpen = false;

    constructor(props) {
        super(props);
        this.state = {
            timezone: '',
        }
    }

    componentDidMount() {
        this.setTimezone();
        this.dataPolling = setInterval(
            () => (this.setTimezone(), 6000)
        );
    }

    componentWillUnmount() {
        this.dataPolling = null;
    }

    setTimezone = () => {
        try {
            this.locale = JSON.parse(window.localStorage.getItem('codeObject')) ? JSON.parse(window.localStorage.getItem('codeObject'))["LANG_CD"].filter(x => x.cd == window.localStorage.getItem("locale")) : undefined;
            const currentTime = moment().tz(this.locale ? this.locale[0].desc2.toString() : "Asia/Seoul").format('YY-MM-DD HH:mm');
            this.setState({ timezone: currentTime });
        } catch (ex) {
            console.warn(ex);
        }
    }


    l10n = (wordCd) => {
        if (locale) {
            return locale[wordCd] ? locale[wordCd] : wordCd;
        }
    }

    logout = async () => {
        const { logout, history } = this.props;

        await logout();
        window.location.href = '/auth/login';
    }

    familySiteSatusChange = () => {
        if(this.isFamilySiteOpen) {
            this.isFamilySiteOpen = false;
        } else {
            this.isFamilySiteOpen = true;
        }
    }

    _handleTopMenuClick = (event, menu) => {
        this.activeTopMenuNm = menu.localeMenuNm;
        const currentNode = event.currentTarget;

        const activeNodes = currentNode.parentNode.getElementsByClassName('active');
        if (activeNodes && activeNodes.length > 0) {
            const beforeSelectedNode = currentNode.parentNode.getElementsByClassName('active')[0].classList;
            beforeSelectedNode.remove('active');
        }

        currentNode.classList.add('active');

        this.props.setSideMenuList(menu);

        // 대메뉴를 클릭했을 때: 소메뉴 보임
        this._mainStoreValueChange("isSmallMenuVisible", true);
    }

    /**
     * 로고클릭 했을 때: 대메뉴 확장, 소메뉴 숨김
     */
    _handleLogClick = (e) => {
        if (window.localStorage.getItem("Device") != "Mobile") {
            this._mainStoreValueChange("isLargeMenuExpand", true);
        }
        this._mainStoreValueChange("isSmallMenuVisible", false);
        e.stopPropagation();
        this.props.mainStore.handleChange({ id: "tabActiveIndex", value: 0 });
    }

    /**
     * 메뉴 모두 접는 아이콘 클릭했을 때: 대메뉴 축소, 소메뉴 숨김 
     */
    _handleCollapseClick = () => {
        if (window.localStorage.getItem("Device") != "Mobile") {
            this._mainStoreValueChange("isLargeMenuExpand", false);
            this._mainStoreValueChange("isSmallMenuVisible", false);
        }
    }

    _mainStoreValueChange = (id, value) => {
        if (id) {
            const { mainStore } = this.props;
            mainStore.handleChange({ id: id, value: value });
        }
    }
    _handleFamilySite = (e) => {
        const url = e.desc1;

        if (url) {
            window.open(url, '_blank');
        }
    }

    render() {
        const { isMenuExpand, isMenuOverlap, loginNm, deptNm, empPhoto, onMenuExpandClick, topMenuList, isLargeMenuExpand, isSmallMenuVisible, helperInfo, familySite, _modalManage, _closeModal} = this.props;
        const { timezone } = this.state;

        let largeMenuClassName = "";
        let smallMenuClassName = "";
        let familySiteClassName = "";
        let logoTit = "";
        if (isLargeMenuExpand && window.innerWidth > 1023) {
            largeMenuClassName = "top_left top_left_expand"; //TODO: 메뉴 작업 후 top_left 빼기
            logoTit = "PABAL";
        } else {
            largeMenuClassName = "top_left top_left_contract"; //TODO: 메뉴 작업 후 top_left 빼기
            logoTit = "P";
            this._mainStoreValueChange("isLargeMenuExpand", false);
        }

        if (isSmallMenuVisible) {
            smallMenuClassName = "left_toolbar left_toolbar_visible"; //TODO: 메뉴 작업 후 left_toolbar 빼기
        } else {
            smallMenuClassName = "left_toolbar left_toolbar_collapse"; //TODO: 메뉴 작업 후 left_toolbar 빼기
        }

        if (this.isFamilySiteOpen && isLargeMenuExpand) {
            familySiteClassName = "family_site expand"
        } else {
            familySiteClassName = "family_site closed"
        }

        let image = "";

        if (empPhoto == undefined || empPhoto == '') {
            image = '';
        } else {
            image = empPhoto;
        }
    
        //TODO: 메뉴 작업 후 아래 내용 삭제
        let toolbarClassName = (isMenuExpand ? 'left_toolbar' : 'left_toolbar left_toolbar_close');
        let topLogoClassName = (isMenuExpand ? 'top_logo' : 'top_logo top_logo_close');
        let profileClassName = (isMenuExpand ? 'personInfoPhoto ' : 'personInfoPhoto profile_close'); //TODO: 메뉴 작업 후 close 분기 삭제 필요
        let topMenuClassName = (isMenuExpand ? 'top_menu' : 'top_menu top_menu_close'); //TODO: 메뉴 작업 후 close 분기 삭제 필요

        if (isMenuOverlap) {
            toolbarClassName = 'left_toolbar overlap';
            topLogoClassName = 'top_logo overlap';
        }

        //TODO: 임시
        toolbarClassName += " " + smallMenuClassName;

        return (
            <React.Fragment>
                {/* 대메뉴 영역 */}
                <div className={largeMenuClassName}>
                    <div className={topLogoClassName} onClick={this._handleLogClick}>
                        <img className={"wing"} src={logo_wing}/>
                        <NavLink to={"/app"}>
                            <div className='top_text'>
                                <span className="logo_tit">{logoTit}</span>
                            </div>
                        </NavLink>
                    </div>
                    <div className={profileClassName}>
                        <span className='per50'>
                                {/* <span className="pro_img">
                                    {
                                        image == '' ? <img className='profilePhoto' src={profile_m} style={{filter:'hue-rotate(10deg)'}} /> :
                                            <img className='profilePhoto' src={`data:image/jpeg;base64,${image}`} />
                                    }
                                </span> */}
                            <ul className='txtlist'>
                                {
                                    isLargeMenuExpand ?
                                        <li>
                                            <span style={{ fontSize: "15px", paddingRight: '6px', color: '#fff', opacity: 0.8 }}>{timezone}</span>
                                            <img src={this.locale ? this.locale[0].cd == 'KR' ? ico_kr : this.locale == 'EN' ? ico_en : this.locale == 'JA' ? ico_ja : ico_ch : ico_kr} alt={this.locale ? this.locale[0].desc2 : "Asia/Seoul"} style={{ position: 'absolute', width: '16px', height: '16px', marginTop: '3px' }} />
                                        </li>
                                        :
                                        null
                                }
                                <li className='name'>
                                    <div className="user_info login_nm">{loginNm ? decodeURIComponent(loginNm) : ''}</div>
                                    <div className="user_info dept_nm"><span>{deptNm}</span></div>
                                    <div><span className='logout' onClick={this.logout}>{"LOGOUT"}<img src={ico_logout} /></span></div>
                                 </li>
                            </ul>
                            <p className='noti set_user'>
                                <img src={ico_lang} onClick={this.props.handleSettingOpen} style={{width: '28px', height: '28px'}} />
                            </p>
                        </span>
                    </div>
                    {/* 소메뉴 영역 */}
                    <ul className={`${topMenuClassName}`}>
                        {
                            topMenuList.map((item, index) => {
                                let css = 'top_menu_item';
                                if (index == 0) {
                                    if (this.activeTopMenuNm == "") {
                                        this.activeTopMenuNm = item.localeMenuNm;
                                    }
                                    css = css + ' active';
                                }
                                return (
                                    <li className={css} key={`topmenu-${index}`} onClick={(event) => this._handleTopMenuClick(event, item)}>
                                        {/* <div className={`menu menu_${item.iconNm}`} style={{BackgroudImage:`url('src/style/img/ico_service.png')`}}></div> */}
                                        <span className="menu_name">
                                            <div className={"menu_icon"}>
                                                <img className={`menu menu_${item.iconNm}`} src={item.iconNm == 'service_icon' ? service_icon : item.iconNm  == 'common_icon' ? common_icon : system_icon} />
                                            </div>
                                            <div className={"menu_title"}>{item.localeMenuNm}</div>
                                        </span>
                                    </li>
                                )
                            })
                        }
                        {
                            isLargeMenuExpand == true ?
                                <React.Fragment>
                                    <div className="family_site">
                                        <div className={familySiteClassName}>
                                            {
                                                <ul className='family_group'>
                                                        {familySite.map((x, i) => {
                                                            if(x.desc1 == 'label') {
                                                                return <li key={`family-site-${i}`} className={"family_label"}>{x.cdNm}</li>
                                                            } else {
                                                                return <li key={`family-site-${i}`} className={"family_list"} onClick={() => this._handleFamilySite(x)} ><span>{x.cdNm}</span></li>
                                                            }
                                                        })
                                                    }
                                                </ul>   
                                            }
                                        </div>
                                        <div className="family_main" onClick={this.familySiteSatusChange}>
                                            <span className="family_site_tit">Family Site</span>
                                            {
                                                !this.isFamilySiteOpen ?
                                                    <img className="family_site_img plus" src={ico_plus} />
                                                    : <img className="family_site_img close" src={ico_plus} />
                                            }
                                        </div>
                                    </div>
                                </React.Fragment>
                                :
                                null
                        }
                        <div className='help_wrap' >
                            <div className="help_open" data-tip='' data-for={`helper`}>
                                    <ReactTooltip backgroundColor={"white"} textColor={'black'} borderColor={'green'} border={true} id={`helper`}>
                                        업무별 담당자 상세정보 
                                    </ReactTooltip>
                                <img src={help} onClick={this.props.handleHelpDeskOpen}/>
                            </div>
                            <div className="helper">{helperInfo ? helperInfo.cdNm : this.l10n("M03325", "담당자: 한민수(경영정보)..")}</div>
                            <div className="helper_num">{helperInfo ? helperInfo.desc1 : this.l10n("M03324", "연락처: 8856")}</div>
                        </div>
                    </ul>
                </div>

                {/* 대메뉴-담당자 영역 */}
                <div className={toolbarClassName} style={{ left: isLargeMenuExpand == false ? "65px" : "225px" }}>
                    <div className='top_left_toolbar'>
                        {/* icon-left-open-big /  icon-right-open-big */}
                            <a className="con_btn"><i className="icon-left-open-big" onClick={this._handleCollapseClick} style={{ cursor: "pointer" }}></i></a>
                        {this.activeTopMenuNm}
                    </div>
                    <SideNavMenu sideMenu={this.props.sideMenuList} />
                </div>

                {/* 설정 모달 */}
                {
                    _modalManage ?
                        <SModal
                            id={_modalManage.id}
                            contentLabel={_modalManage.title}
                            isOpen={_modalManage.isOpen}
                            width={_modalManage.width}
                            onRequestClose={_closeModal}
                        >
                            <div id={_modalManage.id}>
                                {_modalManage.contents}
                            </div>
                        </SModal>
                        :
                        null
                }
            </React.Fragment>
        );
    }
}
export default withRouter(MenuLayout)

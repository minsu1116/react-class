import React, { Component } from 'react';
import { inject } from 'mobx-react';
import { NavLink, withRouter } from 'react-router-dom';
import logo from 'style/img/logo.png';
import SClock from 'components/override/clock/SClock';

@inject(stores => ({
    isMenuExpand: stores.mainStore.isMenuExpand,
    isMenuOverlap: stores.mainStore.isMenuOverlap,
    onMenuExpandClick: stores.mainStore.onMenuExpandClick,
    logout: stores.loginStore.logout,
    loginNm: stores.commonStore.loginNm,
    loginId: stores.commonStore.loginId,
    topMenuList: stores.mainStore.topMenuList,
    setSideMenuList: stores.mainStore.setSideMenuList,
}))
class HeaderLayout extends Component {

    logout = async () => {
        const { logout, history } = this.props;

        await logout();
        window.location.href = '/auth/login';
    }

    _handleTopMenuClick = (event, menu) => {
        const currentNode = event.currentTarget;

        const beforeSelectedNode = currentNode.parentNode.getElementsByClassName('active')[0].classList;
        beforeSelectedNode.remove('active');

        currentNode.classList.add('active');

        this.props.setSideMenuList(menu);
    }

    render() {
        const { isMenuExpand, isMenuOverlap, onMenuExpandClick, loginNm, loginId, topMenuList } = this.props;

        let topLogoClassName = (isMenuExpand ? 'top_logo' : 'top_logo_close');
        const title = "ITSM";

        if (isMenuOverlap) {
            topLogoClassName = 'top_logo overlap'
        }

        return (
            <div id='header'>
                <div className={topLogoClassName}>
                    <NavLink to={"/app"}><img src={logo} style={{ textAlign: "center" }} />
                        <div className='top_text eng'>
                            {title}
                            <span className="eng"></span>
                        </div>
                    </NavLink>
                    
                </div>

                <div className='top_left'>
                    <a>
                        <i className='icon-menu' onClick={onMenuExpandClick} />
                    </a>
                    <ul className='top_menu'>
                        {
                            topMenuList.map((item, index) => {
                                let css = 'top_menu_item';
                                if (index == 0) css = css + ' active';
                                return (
                                    <li className={css} key={`topmenu-${index}`} onClick={(event) => this._handleTopMenuClick(event, item)}>
                                        <div className={`menu_${item.iconNm}`} style={{ width: '20px', height: '20px', float: 'left', marginRight: '10px' }}></div>
                                        {item.localeMenuNm}
                                    </li>
                                )
                            })
                        }
                    </ul>
                </div>

                <div className='top_right'>
                    {/* <li>
                        <a>
                            <i className='icon-logout' onClick={this.logout} style={{ top: '10px' }} />
                        </a>
                    </li> */}
                </div>
            </div>
        )
    }
}

export default HeaderLayout;
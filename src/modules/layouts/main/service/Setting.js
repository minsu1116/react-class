import React from 'react';
import { observable} from 'mobx';
import { observer } from 'mobx-react';
import { BaseComponent } from 'utils/base/BaseComponent'
import { BaseStore } from 'utils/base/BaseStore'
import SRadio from 'components/atoms/radio/SRadio';
import ContentsMiddleTemplate from 'components/template/ContentsMiddleTemplate';
import SButton from 'components/atoms/button/SButton';
import SettingUtil from 'modules/layouts/main/service/SettingUtil';

export { Setting, SettingStore }

class SettingStore extends BaseStore {
    @observable homeType = window.localStorage.getItem("homeType");

    @observable langCd;
    @observable userInfo = {};
    @observable initLangCd;

    loaded = async () => {
        // const result = await SettingUtil.getUserLocale();
        // if (result) {
        //     this.userInfo = result;
        //     this.langCd = this.userInfo.langCd;
        //     this.initLangCd = this.langCd;
        // } else {
        //     this.langCd = "KR";
        // }
    }

    handleSave = async () => {
        const result = {};
        if (window.localStorage.getItem("homeType") != this.homeType) {
            result.homeType = this.homeType;
            window.localStorage.setItem("homeType", this.homeType);
        }

        if (this.initLangCd != this.langCd) {
            this.userInfo.langCd = this.langCd;
            const langResult = await SettingUtil.saveUserLocale(this.userInfo);
            if (langResult) {
                result.langCd = this.langCd;
            }
        }

        this.close(result);
    }
}

@observer
class Setting extends BaseComponent {

    sRender() {
        return (
            <React.Fragment>
                <ContentsMiddleTemplate>
                    <SButton buttonName={this.l10n("M00004", "저장")} type={"default"} onClick={this.store.handleSave} />
                </ContentsMiddleTemplate>
                <table id={'table'} style={{ width: "100%" }} >
                    <tbody>
                        <tr>
                            <th>{this.l10n("M03098", "HOME 설정")}</th>
                            <td>
                                <SRadio id={"homeType"} codeGroup={"HOME_TYPE"} />
                            </td>
                        </tr>
                        <tr>
                            <th>{this.l10n("M02455", "다국어설정")}</th>
                            <td>
                                <SRadio id={"langCd"} codeGroup={"LANG_CD"} />
                            </td>
                        </tr>
                    </tbody>
                </table>
            </React.Fragment>
        )
    }
}
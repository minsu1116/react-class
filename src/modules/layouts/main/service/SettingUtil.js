import CommonStore from 'modules/pages/common/service/CommonStore';
import BaseUtil from "utils/base/BaseUtil";
import { toJS } from 'mobx';

class SettingUtil extends BaseUtil {

    /**
     * 다국어 관련 사용자 정보 가져오기
     */
    getUserLocale = async () => {
        const params = {
            empNo: CommonStore.loginId,
        };

        const result = await this.execute(`/setting/user/locale/get`, params);

        if (result.success) {
            if (result.data && result.data.length > 0) {
                return result.data[0];
            } else {
                return;
            }
        } else {
            return;
        }
    }

    saveUserLocale = async (userInfo) => {
        const result = await this.execute(`/word/user/locale/save`, toJS(userInfo));
        if (result.success) {
            return userInfo.langCd;
        } else {
            return;
        }
    }
}


const instance = new SettingUtil();
export default instance;
import React, { Component } from 'react';
import { observable, action, toJS } from 'mobx';
import { SProvider } from 'utils/context/SContext'
import MenuRepository from 'modules/layouts/main/service/MenuRepository';
import VersionSuperviseRepository from 'modules/layouts/main/service/VersionSuperviseRepository';
import NoticeRepository from 'modules/layouts/main/service/NoticeRepository';
import ModalManage from 'utils/base/ModalManage';
import { nonenumerable } from 'utils/object/Decorator.js';
import MenuModel from 'modules/layouts/main/model/MenuModel';
import ObjectUtility from 'utils/object/ObjectUtility';
import RootStore from 'modules/pages/storeIndex'
import CommonStore from 'modules/pages/common/service/CommonStore';
import Alert from 'components/override/alert/Alert';
import API from 'components/override/api/API';
import { BaseStore } from 'utils/base/BaseStore';
import { locale } from 'utils/word/locale'
import { UserSetting, UserSettingStore } from 'modules/layouts/main/service/UserSetting';
import { SysCategoryHelper, SysCategoryHelperStore } from 'modules/pages/common/codehelper/sysCategory/SysCategoryHelper';
import { NoticeSaveP, NoticeSavePStore } from 'modules/pages/service/notice/popup/NoticeSaveP';

import BaseUtil from 'utils/base/BaseUtil';

class MainStore extends BaseUtil {

    @observable isMenuExpand = true;

    @observable isLargeMenuExpand = true;
    @observable isSmallMenuVisible = false;

    @observable isMenuOverlap = false;

    @observable tabsInfo = [];
    @observable tabActiveIndex = 0;

    @nonenumerable _promiseInfo = {};

    @observable topMenuList = []; //JSON.parse(window.sessionStorage.getItem('topMenuList'));
    @observable sideMenuList = []; //JSON.parse(window.sessionStorage.getItem('sideMenuList'));
    @observable allSubMenuArr = JSON.parse(window.sessionStorage.getItem('allSubMenuArr'));
    @observable noticeObj = '';

    
    @observable siteArray = [];
    @observable isMenu = false;
    menuNameMapByUrl = JSON.parse(window.sessionStorage.getItem('menuNameMapByUrl'));

    @observable familySiteArr = JSON.parse(window.sessionStorage.getItem('familySite'));

    @observable profileImage = '';
    currentPage = "";

    @observable _modalManage;

    // 사용자가 선택한 메뉴 기록
    menuLog = [];

    // 업데이트 시 사용자 알림
    getDataPolling = null;
    currentVersion = "";

    async loaded() {
        this.root = root;
        await CommonStore.getCodeObject();
        await this.addHome();
        // this.currentVersion = await this.getLastVersion();
        // this.setUpdateTimer();
        await this.getNotice();
    }

    setUpdateTimer() {
        let timer = CommonStore.codeObject["VER_CHK_TIME"][0].cdNm != undefined ? CommonStore.codeObject["VER_CHK_TIME"][0].cdNm : 240000;

        this.getDataPolling = setInterval(async () => {
            try {
                await this.updateCheck();
            }
            catch (exception) {
                console.error('Update Exception', exception);
            }
        }, timer);
    }

    async getNotice() {
        const result = await NoticeRepository.getNotice();
        if(result.data.data && result.data.data.contents) {
            const modal = new ModalManage();
            modal.id = "notice";
            modal.title = this.l10n("T00134", "공지사항");
            modal.width = 1024;
            modal.isOpen = true;
            modal.parameter = {masterP : result.data.data , readOnlyYn : "Y" }
            
            modal.contents = <NoticeSaveP store={new NoticeSavePStore()} _closeHandler={this._closeModal} isModal={true} _parameter={modal.parameter} />
            this._modalManage = modal;
        }
    }

    async getLastVersion() {
        const result = await VersionSuperviseRepository.getLastVer();

        if (result.data.success == true) {
            return result.data.data.version == undefined ? 0 : result.data.data.version;
        }
    }

    @action.bound
    async updateCheck() {
        const lastVersion = await this.getLastVersion();

        if (lastVersion != this.currentVersion) {
            clearInterval(this.getDataPolling);
            this.getDataPolling = null;
            const element = document.querySelector('#updateLayer');
            element.style.display = 'block';
        }
    }

    @action.bound
    handleChange(data) {
        if (this[data.id] != data.value) {
            this[data.id] = data.value;
        }
    }

    @action.bound
    async getSideMenu() {
        const params = {
            userId: CommonStore.loginId
        }
        const data = await this.execute(`/pass/login/menu`, params);
        
        let tempMenuList = [];
        
        if(data.data) {
            // 1 Depth
            data.data.forEach(element => {
                // 2 Depth
                if(element.subMenu) {
                    let tempSubMenu = [];
                    element.subMenu.forEach(element => {
                        
                        if(element.subMenu && element.subMenu.length > 0) {
                            tempSubMenu.push(element);
                        }
                    });
                    element.subMenu = tempSubMenu;
                }

                if(element.subMenu && element.subMenu.length > 0) {
                    tempMenuList.push(element);
                }
            });
        }
        
        this.setLocale(tempMenuList);
        this.topMenuList = tempMenuList;
        if (this.topMenuList.length > 0) {
            this.sideMenuList = this.topMenuList[0].subMenu;
        }
    }

    setLocale = (menu) => {
        menu.forEach(x => {
            if (x.subMenu) {
                x.localeMenuNm = locale[x.wordCd] || x.wordCd;
                this.setLocale(x.subMenu);
            } else {
                x.localeMenuNm = locale[x.wordCd] || x.wordCd;
            }
        })
    }

    setSideMenuList = (selectedMenu) => {
        this.sideMenuList = selectedMenu.subMenu;
    }

    @action.bound
    getProfileImage = async () => {
        const { data, status } = await MenuRepository.getProfileImage();
        if (data.success == true) {
            this.profileImage = data.data.empPhotoSrc;
        } else {
            Alert.meg(data.msg);
        }

    }
    @action clearMenu() {
        this.allSubMenuArr = undefined;
        this.sideMenuList = undefined;
        this.menuNameMapByUrl = undefined;
        window.sessionStorage.removeItem('menuNameMapByUrl');
        window.sessionStorage.removeItem('allSubMenuArr');
        window.sessionStorage.removeItem('sideMenuList');
    }

    @action setAllSubMenuArr(allSubMenuArr) {
        this.allSubMenuArr = allSubMenuArr;
    }

    @action setSideMenuList(sideMenuList) {
        this.sideMenuList = sideMenuList;
    }

    @action setMenuNameMapByUrl(menuNameMapByUrl) {
        window.sessionStorage.setItem('menuNameMapByUrl', JSON.stringify(menuNameMapByUrl));
    }

    @action setSubMenuSesstion(allSubMenuArr) {
        window.sessionStorage.setItem('allSubMenuArr', JSON.stringify(allSubMenuArr));
    }

    @action setMenuSesstion(sideMenuList) {
        window.sessionStorage.setItem('sideMenuList', JSON.stringify(sideMenuList));
    }

    @action setFamilySiteSesstion(familySite) {
        window.sessionStorage.setItem('familySite', JSON.stringify(familySite));
    }

    @action.bound
    async getFamilySite() {
        const codeGroup = [{ "code": "SYS020", "allAdd": false }, { "code": "SYS021", "allAdd": false }];

        commonStore.getCodeObjectSelectBox(codeGroup).then(codeObject => {
            let siteArr = [];
            for (let i = 0; i < codeGroup.length; i++) {
                const element = codeGroup[i];
                const arr = eval(`codeObject['${element.code}']`);

                for (let j = 0; j < arr.length; j++) {
                    const data2 = arr[j];
                    siteArr.push({ cd: data2.desc2, cdNm: `${data2.desc1}` });
                }

            }
            this.familySiteArr = siteArr.concat();
            this.setFamilySiteSesstion(this.familySiteArr);

        })
    }

    @action.bound
    getMenuNameByUrl(url) {
        return this.menuNameMapByUrl[ObjectUtility.replaceAll(url, "/app", "")];
    }

    @action.bound
    setIsMenu(flag) {
        this.isMenu = flag;
    }

    @action.bound
    getButtonAuth(path) {
        const length = this.allSubMenuArr.length;

        for (let i = 0; i < length; i++) {
            const subMenu = this.allSubMenuArr[i];
            if (path.indexOf(subMenu.formUrl) > 0) {
                return subMenu;
            }
        }
    }

    @action.bound
    onMenuOpen(subMenu) {
        this.isMenuOverlap = false;
        this.addMenu(subMenu);
    }

    @action.bound
    addMenu = async (selectedMenu, parameter = null, isTabChange = false, addIndex = null) => {
        
        const url = selectedMenu.url;
        const formId = selectedMenu.formId;
        const menuNm = selectedMenu.menuNm;

        let auth;
        if (selectedMenu.btnList) {
            auth = JSON.parse(selectedMenu.btnList).buttons;
        }

        const index = this.tabsInfo.findIndex(x => x.contentUrl == url);
        console.log(url, menuNm);
        this.currentPage = { menuNm: menuNm, class: selectedMenu.formClass };

        ObjectUtility.loadingLayer(true);

        if (index < 0) {
            const newMenu = {
                title: selectedMenu.localeMenuNm,
                formClass: selectedMenu.formClass,
                id: selectedMenu.menuId,
                contentUrl: url,
                content: await this.createMenu(selectedMenu, auth, parameter, isTabChange),
                auth: auth,
                formId: formId,
            }

            const existTabInfo = toJS(this.tabsInfo);
            if (addIndex == null) {
                addIndex = this.tabsInfo.length;
            }

            existTabInfo.splice(addIndex, 0, newMenu);

            this.tabsInfo = existTabInfo;
            this.tabActiveIndex = addIndex;

            this.addMenuLog(selectedMenu);
        } else {
            if (isTabChange) {
                // 종료 후 다시 닫기                
                const closeMenu = this.tabsInfo.find((x, i) => i == index);
                RootStore.reloadStore(closeMenu.formClass);
                this.tabsInfo = this.tabsInfo.filter((x, i) => i != index);

                await this.addMenu(selectedMenu, parameter, isTabChange, index);
            } else {
                this.tabActiveIndex = index;
            }
        }
        ObjectUtility.loadingLayer(false);
    }

    async createMenu(selectedMenu, auth, parameter, isTabChange) {
        const { formClass, url, formId, menuNm, compList, systemGrpYn } = selectedMenu;

        // TODO: chunk File 나누기 테스트중
        const url2 = "pages" + url;
        return await import(`modules/${url2}`).then(module => {
            const Component = module[formClass];
            const Store = module[formClass + "Store"];
            return (
                <SProvider menuPath={url} formId={formId} menuNm={menuNm} companyAuth={compList} systemGrpYn={systemGrpYn} auth={auth}>
                    <div id={formId} className='basic'>
                        <Component store={new Store()} _parameter={parameter} _isTabChange={isTabChange} />
                    </div>
                </SProvider>
            )
        }).catch(function (err) {
        });
    }

    @action.bound
    handleTabChange(index) {
        this.tabActiveIndex = index;
    }

    @action.bound
    handleEdit({ type, index }) {
        if (type == 'delete') {
            const closeMenu = this.tabsInfo.find((x, i) => i == index);

            this.tabsInfo = this.tabsInfo.filter((x, i) => i != index);
        }
        let activeIndex = 0;

        if (index - 1 >= 0) {
            activeIndex = index - 1;
        } else {
            activeIndex = 0;
        }

        this.handleTabChange(activeIndex);
    }

    @action.bound
    onMenuExpandClick = () => {
        this.isMenuExpand = !this.isMenuExpand;
        this.isMenuOverlap = false;
    }

    onOneDepthMenuClick = () => {
        this.isMenuOverlap = true;
    }

    tabChange = async (formId, params = null) => {
        const findedMenu = this.findMenu(this.topMenuList, formId);

        if (!findedMenu) {
            await Alert.meg("해당 메뉴에 권한이 없습니다.");
            return;
        }

        await this.addMenu(findedMenu, params, true);
    }

    findMenu = (menus, formId) => {
        for (let i = 0; i < menus.length; i++) {
            const topMenu = menus[i];
            if (topMenu.subMenu == null) {
                if (topMenu.formId == formId) {
                    return topMenu;
                }
            } else {
                const result = this.findMenu(topMenu.subMenu, formId);
                if (result) {
                    return result;
                }
            }
        }
    }

    closeAllMenu = async (msg) => {
        if (this.tabsInfo.length <= 1) return;

        if (msg == true) {
            if (!await Alert.confirm("모든 메뉴를 삭제하시겠습니까?")) {
                return;
            }
        }
        this.tabsInfo = toJS(this.tabsInfo.filter((x, i) => i == 0));
    }

    addMenuLog = (selectedMenu) => {
        const log = { empNo: CommonStore.loginId, url: selectedMenu.url, menuNm: selectedMenu.menuNm };
        this.menuLog.push(log);

        // 현재 메뉴가 10개가 넘으면 로그 저장 후 초기화
        if (this.menuLog.length >= 10) {
            try {
                // TODO: 메뉴 로그 저장 서비스 구현
                const result = {}; // TODO: 예시> this.exceute(~~~);
                result.success = true; //TODO: 삭제
                if (result.success) {
                    this.menuLog = [];
                    window.localStorage.removeItem("menuLog");
                } else {
                    window.localStorage.setItem("menuLog", JSON.stringify(this.menuLog));
                }
            } catch (ex) {
                console.warn("메뉴 로그 쌓는 도중 에러 발생", ex);
            }

        } else {
            window.localStorage.setItem("menuLog", JSON.stringify(this.menuLog));
        }
    }

    addHome = async () => {
        let url = '/home/Home';
        let homeResult;
        let homeType = "";

        if (url) {
            homeResult = {
                title: "HOME",
                menuNm: "HOME",
                localeMenuNm: "HOME",
                formClass: 'Home',
                menuId: null,
                url: url,
                formId: null,
                auth: "",
                // systemGrpYn: "Y",
            };

            await this.addMenu(homeResult, null, false);
        }
    }

    handleSettingOpen = async () => {
        const modal = new ModalManage();
        modal.id = "setting";
        modal.title = this.l10n("M01602", "설정");
        modal.width = 500;
        modal.isOpen = true;
        modal.contents = <UserSetting store={new UserSettingStore()} _closeHandler={this._userSettingClose} isModal={true} />
        this._modalManage = modal;
    }

    handleHelpDeskOpen = async () => {
        const modal = new ModalManage();
        modal.id = "help";
        modal.title = this.l10n("T00141", "담당자 상세정보");
        modal.width = 1000;
        modal.height = 800;
        modal.isOpen = true;
        modal.parameter = {editorMode : false}
        modal.contents = <SysCategoryHelper store={new SysCategoryHelperStore()}  _closeHandler={this._closeModal} isModal={true} _parameter={modal.parameter} />
        this._modalManage = modal;
    }

    _closeModal = async () => {
        this._modalManage = null;
    }

    _settingClose = async (result) => {
        this._modalManage = null;
        if (result) {
            if (result.langCd) {
                await Alert.meg(this.l10n("M02456", "재로그인 시 적용됩니다."));
            }

            if (result.homeType) {
                const homeTypeInfo = CommonStore.getCdType("HOME_TYPE");
                const url = homeTypeInfo[result.homeType].desc1;

                const split = url.split("/");
                const newHome = {
                    title: "HOME",
                    menuNm: "HOME",
                    localeMenuNm: "HOME",
                    formClass: split[split.length - 1],
                    menuId: null,
                    url: url,
                    formId: null,
                    auth: "",
                    // systemGrpYn: "Y",
                };

                this.tabsInfo = this.tabsInfo.filter((x, i) => i != 0);
                await this.addMenu(newHome, null, false, 0);

                // HOME 화면이 포커싱 되어있을 때 재렌더링이 안되서 아래와 같이 index 조절하여 visible 처리
                this.tabActiveIndex = undefined;
                this.tabActiveIndex = 0;
            }
        }
    }

    _userSettingClose = async (result) => {
        this._modalManage = null;
        if (result) {
            await Alert.meg(this.l10n("M02456", "재로그인 시 적용됩니다."));
            // await RootStore.loginStore.login(undefined, CommonStore.loginId, "!@#$%QWERT");
            // window.location.href = '/app';
        }
    }

    refreshHome = async () => {
        await this._settingClose({ homeType: window.localStorage.getItem("homeType") });
    }
}

export default new MainStore();
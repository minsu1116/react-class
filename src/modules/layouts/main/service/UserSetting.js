import React from 'react';
import { observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import { BaseComponent } from 'utils/base/BaseComponent'
import { BaseStore, model } from 'utils/base/BaseStore'
import CommonStore from 'modules/pages/common/service/CommonStore';
import SRadio from 'components/atoms/radio/SRadio';
import ContentsMiddleTemplate from 'components/template/ContentsMiddleTemplate';
import SButton from 'components/atoms/button/SButton';
import Alert from 'components/override/alert/Alert';
import SettingUtil from 'modules/layouts/main/service/SettingUtil';

export { UserSetting, UserSettingStore }

class UserSettingStore extends BaseStore {
    @observable langCd;
    @observable userInfo = {};

    loaded = async () => {
        // const result = await SettingUtil.getUserLocale();
        // if (result) {
        //     this.userInfo = result;
        //     this.langCd = this.userInfo.langCd;
        // } else {
        //     this.langCd = "KR";
        // }
    }

    handleSave = async () => {
        this.userInfo.langCd = this.langCd;
        const result = await SettingUtil.saveUserLocale(this.userInfo);
        this.close(result);
    }
}

@observer
class UserSetting extends BaseComponent {

    sRender() {
        return (
            <React.Fragment>
                <ContentsMiddleTemplate>
                    <SButton buttonName={this.l10n("M00004", "저장")} type={"default"} onClick={this.store.handleSave} />
                </ContentsMiddleTemplate>
                <table id={'table'} style={{ width: "100%" }} >
                    <tbody>
                        <tr>
                            <th>{this.l10n("M02455", "다국어설정")}</th>
                            <td>
                                <SRadio id={"langCd"} codeGroup={"LANG_CD"} />
                            </td>
                        </tr>
                    </tbody>
                </table>
            </React.Fragment>
        )
    }
}
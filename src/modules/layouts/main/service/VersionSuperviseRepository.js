import API from "components/override/api/API";

class VersionSuperviseRepository {

    URL = "/api/ver";

    getLastVer() {
        return API.request.post(encodeURI(`${this.URL}/get/lastver`))
    }

    // getVer(device) {
    //     return API.request.get(encodeURI(`${this.URL}/getver?device=${device}`))
    // }

    // saveVersion(param) {
    //     return API.request.post(encodeURI(`${this.URL}/saveversion`), param);
    // }

}
export default new VersionSuperviseRepository();
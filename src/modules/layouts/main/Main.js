import React, { Component } from 'react';
import SMainTab from 'components/override/tab/SMainTab'
import { inject } from 'mobx-react'

@inject(stores => ({
    tabsInfo: stores.mainStore.tabsInfo,
    tabActiveIndex: stores.mainStore.tabActiveIndex,
    handleEdit: stores.mainStore.handleEdit,
    handleTabChange: stores.mainStore.handleTabChange,
    onMenuExpandClick: stores.mainStore.onMenuExpandClick,
}))
class Main extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { tabsInfo, tabActiveIndex, handleEdit, handleTabChange, onMenuExpandClick } = this.props;
        const agent = navigator.userAgent.toLowerCase(); 
        let ieChk = false;
        if( (navigator.appName == 'Netscape' && navigator.userAgent.search('Trident') != -1) || (agent.indexOf("msie") != -1) ) {
            ieChk = true;
        }
        return (
            <React.Fragment>
                <div className='main_wrap' id='mainWrap'>       
                    {
                        ieChk ?
                            <div style={{ left: '27%', position: 'absolute', paddingTop: '5px', opacity: 0.3 }}>
                                <span style={{ fontSize: '20px', color: 'red', fontWeight: 600 }}>Internet Explore에서는 디자인이 깨질 수 있습니다.</span>
                            </div> :
                            null
                    }
                    <SMainTab tabs={tabsInfo}
                        onTabEdit={handleEdit}
                        onTabChange={handleTabChange}
                        activeIndex={tabActiveIndex}
                    />
                </div>
            </React.Fragment>
        )
    }
}

export default Main;
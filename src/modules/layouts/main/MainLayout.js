import React, { Component } from 'react';
import { inject } from 'mobx-react';
import MenuLayout from 'modules/layouts/menu/MenuLayout';
import Main from 'modules/layouts/main/Main';

@inject(stores => ({
    mainStore: stores.mainStore,
    commonStore: stores.commonStore,
}))
class MainLayout extends Component {

    constructor(props) {
        super(props);

        const { commonStore, match } = this.props;

        if (!commonStore.loginId) {
            window.location.href = '/auth/login';
        }

        if (match.path.includes('/sr-req')) {
            commonStore.setPopupYn('Y');
            const workCategory = match.url.split("_")[1];
            if (workCategory) {
                commonStore.setWorkCategory(workCategory);
            }
        } else if (match.path.includes('/sr/')) {
            commonStore.setPopupYn('Y');
            if (match.params.target) {
                commonStore.setWorkCategory_sr(match.params.target);
            }
        }
    }

    async componentDidMount() {
        const { commonStore, mainStore, match } = this.props;

        if (match.path.includes('/sr-req') || match.path.includes('/sr/')) {
            window.location.href = '/app';
        }

        const mainWrapClick = function (e) {
            mainStore.handleChange({ id: "isSmallMenuVisible", value: false });
        }

        const mainWrap = document.getElementById("mainWrap");
        if (mainWrap) {
            mainWrap.addEventListener("click", mainWrapClick);
        }

        if (commonStore.loginId) {
            await commonStore.commonUserInfo();
        } else {
            // window.location.href = '/auth/sso';
            window.location.href = '/auth/login';
        }

        await commonStore.getCodeObject();

        // 최초 로딩 시에 무조건 Menu 가져오기
        await commonStore.getUserWord();
        await mainStore.getSideMenu();

        if (!commonStore.compList) {
            await commonStore.getCompList();
        }

        // if (!commonStore.deptList) {
        //     await commonStore.getDeptList();
        // }

        await mainStore.loaded();

        const ele = document.getElementById('ipl-progress-indicator');
        if (ele) {
            ele.classList.add('available');
            ele.outerHTML = '';
        }
    }

    handleVerUpdate = () => {
        window.location.href = '/';
    }

    render() {
        return (
            <div id='wrapper'>
                <div id="layerOverlap" style={{
                    zIndex: 999999
                    , position: 'absolute'
                    , width: '100%'
                    , height: '100%'
                    , backgroundColor: '#ffffff'
                    , overflow: 'hidden'
                    , opacity: 0
                    , display: 'none'
                }} />

                {/* <HeaderLayout /> */}

                <div id='container'>
                    <MenuLayout />
                    <Main />
                </div>
            </div>
        )
    }
}

export default MainLayout;

import React, { Component } from 'react';
import { inject } from 'mobx-react';
// import loading from 'style/img/loading.gif';

@inject(stores => ({
    getSSO: stores.loginStore.getSSO
}))
class Sso extends Component {

    constructor(props) {
        super(props);
    }


    async componentDidMount() {
        const { getSSO, match } = this.props;
        await getSSO(match?.params?.target, this);
    }

    render() {

        return (
            <div>
                <div className="wrap_auto_img">
                    {/* <img src={loading} /> */}
                </div>
                <div className="wrap_auto_txt">
                    자동 로그인 진행중입니다.
                </div>
            </div>
        );
    }
}

export default Sso;
import API from "components/override/api/API";

class LoginRepository {

    URL_AUTH = "/auth";
    URL = "/api";

    loginUserCheck(params) {
        const param = JSON.stringify(params);
        return API.request.post(`${this.URL_AUTH}/login`, param);
    }

    getLoginUserInfo(params) {
        return API.request.post(`${this.URL}/pass/login/userinfo`, params);
    }

    login(params) {
        return API.request.post(`${this.URL}/pass/login`, params);
    }

    logout(params) {
        return API.request.post(`${this.URL}/pass/logout`, params);
    }
    getPhoto(params) {
        return API.request.post(`${this.URL}/pass/getphoto`, params);
    }

    getSSO(params) {
        return API.request.post(`${this.URL}/pass/sso/get`, params);            
    }

    getSSOUserInfo(params) {
        return API.request.get(`${this.URL}/pass/sso/userinfo?ticket=${params}`);            
    }
}
export default new LoginRepository();
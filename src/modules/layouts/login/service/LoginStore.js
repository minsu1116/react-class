import { observable, action } from 'mobx';

import LoginRepository from "modules/layouts/login/service/LoginRepository"
import CommonStore from 'modules/pages/common/service/CommonStore';
import Alert from 'components/override/alert/Alert';
class LoginStore {

    constructor(root) {
        this.root = root;
    }

    @observable loginId = '';
    @observable loginPass = '';
    @observable langCd = "KR";

    isSSO = true;

    @action.bound
    handleChange(event) {
        this[event.target.id] = event.target.value;
    }

    @action.bound
    async userLogin(params) {
        if (!this.loginId) {
            Alert.meg('ID/PW 를 입력해 주세요.');
            return;
        }

        if (await this.login(params, this.loginId, this.loginPass) == false) {
            this.loginPass = "";
            Alert.meg('사용자 정보를 다시 입력해 주세요.');
        } else {
            params?.props?.history?.push("/app");
        }
    }

    @action.bound
    login = async (params, id, pw) => {
        const loginData = {
            bempNo: id,
            bpassword: pw
        };

        const result = await LoginRepository.login(loginData);

        if (result.data.success) {
            CommonStore.setLocale(result.data.data.langCd != undefined ? result.data.data.langCd : "KR");
            await this.loginAfter(result.data.data);
            this.getCode();
            // this.getPhoto(loginData);
        } else {
            return false;
        }
    }

    getCode() {
        // 바로 사용하는 것이 아니기 때문에 동기처리
        CommonStore.requestGetCodes();
    }

    async getPhoto(param) {
        const result = await LoginRepository.getPhoto(param);
        CommonStore.setEmpPhto(result.data.data == undefined ? '' : result.data.data.empPhoto);
    }

    loginAfter(loginInfo) {
        CommonStore.setToken(loginInfo.accessToken); // 로컬스토리지에 로그인 정보 저장
        CommonStore.setLoginId(loginInfo.bempNo); // 2020-04-01 [TB_USER_MT] Key값으로 변경 bempNo=> buserId
        // CommonStore.setLoginNm(encodeURIComponent(loginInfo.pempNm)); // TODO 임시 주석
        CommonStore.setLoginNm(loginInfo.buserNm);
        CommonStore.setLocale(loginInfo.langCd)
        CommonStore.setDeptNm(loginInfo.bdeptNm)
        CommonStore.setCompCd(loginInfo.bcompCd);
        CommonStore.setPcAdminAuth(loginInfo.bpcAdminAuth);
    }

    @action.bound
    async logout() {
        this.isSSO = false;

        const loginData = {
            bempNo: CommonStore.bempNo
        };

        const data = await LoginRepository.logout(loginData);
        CommonStore.clearLocalStorage();
        return Promise.resolve();
    }

    @action.bound
    async getSSO(params, prop) {

        const param = {
            loginKey : params ?? ""
        }

        const result = await LoginRepository.getSSO(param);

        if (result.data.success) {
            CommonStore.setLocale(result.data.data.langCd != undefined ? result.data.data.langCd : "KR");
            await this.loginAfter(result.data.data);
            this.getCode();
            // this.getPhoto(loginData);
            prop.props.history.push("/app");
        } else {
            prop.props.history.push("/auth/login");
        }
    }
}

export default new LoginStore();
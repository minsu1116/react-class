import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';
import { observable } from 'mobx';
import main_login from 'style/img/main_login.png';
import backVedio from 'style/img/login_background1.mp4';
import backImg from 'style/img/ls_video.png';
import logo from 'style/img/logo_en.png';
import { Mobile, Tablet, PC } from "modules/MediaQuery.tsx"

@inject(stores => ({
    userLogin: stores.loginStore.userLogin,
    handleChange: stores.loginStore.handleChange,
    loginId: stores.loginStore.loginId,
    loginPass: stores.loginStore.loginPass
}))

@observer
class Login extends Component {

    @observable DeviceMode = window.localStorage.getItem("Device");

    constructor(props) {
        super(props);
        this.onKeyDown = this.onKeyDown.bind(this);
    }

    componentDidMount() {
        this.refs.input1.addEventListener('keydown', this.onKeyDown);
        this.refs.input2.addEventListener('keydown', this.onKeyDown);

        const ele = document.getElementById('ipl-progress-indicator');
        if (ele) {
            ele.classList.add('available');
            ele.outerHTML = '';
        }

    }

    componentWillUnmount() {
        // this.refs.input1.addEventListener('keydown', this.onKeyDown);
        // this.refs.input2.addEventListener('keydown', this.onKeyDown);
    }

    onKeyDown(event) {
        if (event.keyCode == 13) {
            event.stopPropagation();

            const { userLogin } = this.props;
            userLogin(this);
        }
    }

    render() {
        const { userLogin, handleChange, loginId, loginPass } = this.props;

        return (
            <React.Fragment>
                <PC>
                    <video className="loginVideo" autoPlay loop muted poster={backImg}>
                        <source src={backVedio} type="video/mp4" />
                    </video>
                    <div className="loginbx">
                        <img className="sys_logo" src={main_login} alt="" />
                        <form className="form-signin">
                            <label className="IdText" >
                                <input id="loginId" ref="input1" onChange={handleChange}  type="text" placeholder="ID" value={loginId} />
                            </label>
                            <label className="PassText">
                                <input id="loginPass" ref="input2" onChange={handleChange}  type="password" placeholder="Password" value={loginPass} />
                            </label>
                            <p className="loginbtn" onClick={() => userLogin(this)}><a>Login</a></p>
                        </form>
                        <img src={logo} className="m_b_logo" ></img>
                        <div style={{ fontWeight: 1000, fontStyle: 'normal', fontVariant: 'unicase', fontSize: '15px', color: '#c53d3d', marginTop: '2px', filter: 'drop-shadow(2px 4px 6px black)' }}>
                            COPYRIGHTⓒ 2021 BY LS전선. ALL RIGHTS RESERVED.
                        </div>
                    </div>
                </PC>
                <Tablet>
                    <video className="loginVideo" autoPlay loop muted poster={backImg}>
                        <source src={backVedio} type="video/mp4" />
                    </video>
                    <div className="loginbx">
                        <img className="sys_logo" src={main_login} alt="" />
                        <form className="form-signin">
                            <label className="IdText" >
                                <input id="loginId" ref="input1" onChange={handleChange}  type="text" placeholder="ID" value={loginId} />
                            </label>
                            <label className="PassText">
                                <input id="loginPass" ref="input2" onChange={handleChange}  type="password" placeholder="Password" value={loginPass} />
                            </label>
                            <p className="loginbtn" onClick={() => userLogin(this)}><a>Login</a></p>
                        </form>
                        <img src={logo} className="m_b_logo" ></img>
                        <div style={{ fontWeight: 1000, fontStyle: 'normal', fontVariant: 'unicase', fontSize: '15px', color: '#c53d3d', marginTop: '2px', filter: 'drop-shadow(2px 4px 6px black)' }}>
                            COPYRIGHTⓒ 2021 BY LS전선. ALL RIGHTS RESERVED.
                        </div>
                    </div>
                </Tablet>
                <Mobile>
                    <video className="loginVideo" autoPlay loop muted poster={backImg}>
                        <source src={backVedio} type="video/mp4" />
                    </video>
                    <div className="loginbx">
                        <img className="sys_logo" src={main_login} alt="" />
                        <form className="form-signin">
                            <label className="IdText" >
                                <input id="loginId" ref="input1" onChange={handleChange}  type="text" placeholder="ID" value={loginId} />
                            </label>
                            <label className="PassText">
                                <input id="loginPass" ref="input2" onChange={handleChange}  type="password" placeholder="Password" value={loginPass} />
                            </label>
                            <p className="loginbtn" onClick={() => userLogin(this)}><a>Login</a></p>
                        </form>
                        <img src={logo} className="m_b_logo" ></img>
                        <div style={{ fontWeight: 1000, fontStyle: 'normal', fontVariant: 'unicase', fontSize: '3.5vw', color: '#c53d3d', marginTop: '2px', filter: 'drop-shadow(2px 4px 6px black)' }}>
                            COPYRIGHTⓒ 2021 BY LS전선. ALL RIGHTS RESERVED.
                        </div>
                    </div>
                </Mobile>
            </React.Fragment>
        )
    }
}

export default Login;
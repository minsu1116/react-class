import  React from 'react';
import { useMediaQuery } from 'react-responsive';


const Mobile: React.FC = ({children}) => {
    const isMobile = useMediaQuery({
        query: '(max-width: 412px)',    
    });

    if(window.localStorage.getItem("Device")){
      window.localStorage.removeItem("Device");
    }
    getDevice();
    return <React.Fragment>{isMobile && children}</React.Fragment>;
}

const Tablet: React.FC = ({ children }) => {
  const isTablet = useMediaQuery({
    query: '(min-width: 413px) and (max-width: 1023px)',
  });
  if(window.localStorage.getItem("Device")){
    window.localStorage.removeItem("Device");
  }
  getDevice();
  return <React.Fragment>{isTablet && children}</React.Fragment>;
};

const PC: React.FC = ({ children }) => {
  const isPc = useMediaQuery({ query: '(min-width: 1024px) and (max-width: 12220px)'});
  if(window.localStorage.getItem("Device")){
    window.localStorage.removeItem("Device");
  }
  getDevice();
  return <React.Fragment>{isPc && children}</React.Fragment>;
};

function getDevice() {
    const ua = navigator.userAgent;
    if (/(tablet|ipad|playbook|silk)|(android(?!.*mobi))/i.test(ua)) {
      window.localStorage.setItem('Device', 'Tablet');
    }
    if (
      /Mobile|iP(hone|od)|Android|BlackBerry|IEMobile|Kindle|Silk-Accelerated|(hpw|web)OS|Opera M(obi|ini)/.test(
        ua
      )
    ) {
      window.localStorage.setItem('Device', 'Mobile');
    }
    return window.localStorage.setItem('Device', 'PC');
}

export { Mobile, Tablet, PC};
import { BaseModel } from "utils/base/BaseModel";
import { observable } from "mobx";
import moment from "moment";

export default class SysMngModel extends BaseModel {
    
    @observable sysNm;
    @observable useYn = '';
}
import React from 'react';
import { BaseStore, gridTrigger, model } from "utils/base/BaseStore";
import { observer } from "mobx-react";
import { BaseComponent } from "utils/base/BaseComponent";
import { observable, toJS } from 'mobx';

import SearchTemplate from 'components/template/SearchTemplate';
import ContentsTemplate from 'components/template/ContentsTemplate';
import ContentsMiddleTemplate from 'components/template/ContentsMiddleTemplate';
import Alert from 'components/override/alert/Alert';
import SButton from 'components/atoms/button/SButton';
import SGrid from 'components/override/grid/SGrid';
import SInput from 'components/atoms/input/SInput';
import ModalManage from 'utils/base/ModalManage';
import SysMngModel from 'modules/pages/work/sysCategoryMng/SysMngModel';
import CategoryMngModel from 'modules/pages/work/sysCategoryMng/CategoryMngModel';

export { SysCategoryMng, SysCategoryMngStore }

class SysCategoryMngStore extends BaseStore {

    gubnTypeMap = new Map();
    dtlGubnCdNmMap = new Map();
    optionGroup = [];

    @observable @model sysMaster = new SysMngModel();
    @observable @model categoryMaster = new CategoryMngModel();

    @observable currWorkNm = "";
    @observable currWorkDtlNm = "";

    loaded = async () => {
        await this.handleSearchClick();
    }

    setSysGridApi = async (grid) => {
        this.sysGrid = grid;

        const columnDefs = [
            { word: "M03350", name: "분류", field: "sysType", width: 110, codeGroup: "SYS_CATEGORY_GROUP", editable: true, essential: true, addOption: this.l10n("M00233", "선택"), default: "" }
            , { word: "M03352", name: "시스템명", field: "sysNm", width: 110, editable: true, essential: true }
            , { word: "M00399", name: "사용여부", field: "useYn", width: 80, align: 'center', codeGroup: "USE_YN", editable: true, default: "Y", essential: true }
            , { name: "시스템ID", field: "sysId", hide: true }
        ];

        this.sysGrid.setColumnDefs(columnDefs);
    }

    setCategoryMtGridApi = async (grid) => {
        this.categoryMtGrid = grid;

        const columnDefs = [
            { word: "M03350", name: "분류", field: "categoryLargeId", width: 95, codeGroup: "CATEGORY_GROUP", editable: true, addOption: this.l10n("M00233", "선택"), default: "", essential: true }
            , { word: "M03351", name: "업무명", field: "categoryNm", width: 150, editable: true, essential: true }
            , { word: "M03398", name: "승인절차 여부", field: "apprYn", codeGroup: "USE_YN", align: 'center', addOption: this.l10n("M00233", "선택"), default: "", width: 100, editable: true, essential: true }
            , { word: "M03359", name: "하위구분여부", field: "dtlYn", codeGroup: "USE_YN", align: 'center', width: 110, editable: true, addOption: this.l10n("M00233", "선택"), default: "", editable: true, essential: true }
            , { word: "M03360", name: "구분타입", field: "dtlGubnType", width: 100, editable: true }
            , { word: "M00685", name: "코드그룹", field: "dtlGubnCd", width: 150, editable: true }
            , { word: "M03376", name: "코드그룹명", field: "dtlGubnCdNm", width: 120, editable: true }
            , { word: "M03377", name: "만료기간여부", field: "expiredYn", width: 100, align: 'center', codeGroup: "USE_YN", editable: true, default: "Y", essential: true }
            , { word: "M03434", name: "업무설명", field: "categoryDesc", width: 80, editable: false, type: 'button', buttonName: this.l10n("M00001", "등록"), onButtonClick: this.handleCategoryDescAdd }
            , { word: "M03436", name: "초기컨텐츠", field: "contentsDesc", width: 80, editable: false, type: 'button', buttonName: this.l10n("M00001", "등록"), onButtonClick: this.handleContentsDescAdd }
            , { word: "M00399", name: "사용여부", field: "useYn", width: 80, align: 'center', codeGroup: "USE_YN", editable: true, default: "Y", essential: true }


            , { name: "시스템ID", field: "sysId", hide: true }
            , { name: "업무ID", field: "categoryId", hide: true }

        ];

        // function dtlEditable(params) {
        //     if (params.data.dtlYn == "N") {
        //         params.data.dtlGubn = '';
        //         return false;
        //     } else {
        //         params.data.dtlGubn = '09';
        //         return true;
        //     }
        // }
        // this.setOption(columnDefs, "dtlGubn", gridTrigger.editable, dtlEditable);

        this.categoryMtGrid.setColumnDefs(columnDefs);
    }

    initCategoryMtGrid = () => {
        const columnDefs = [
            { word: "M00685", name: "코드그룹", field: "dtlGubnCd", optionGroup: this.optionGroup, width: 95, editable: true, essential: true }
            , { word: "M03376", name: "코드그룹명", field: "dtlGubnCdNm", width: 95, essential: true }
            , { word: "M03360", name: "구분타입", field: "dtlGubnType", width: 100, codeGroup: "DTL_GUBN", addOption: this.l10n("M03378", "자동변경"), default: '' }
            , { word: "M00066", name: "코드", field: "categoryDtlCd", width: 70, editable: true, essential: true }
            , { word: "M01212", name: "코드명", field: "categoryDtlCdNm", width: 100, editable: true, essential: true }
            , { word: "M00282", name: "정렬순서", field: "sort", width: 70, essential: true, type: 'number', editable: true, default: 1}
            , { word: "M00399", name: "사용여부", field: "useYn", width: 80, align: 'center', codeGroup: "USE_YN", editable: true, default: "Y", essential: true }


            , { name: "업무ID", field: "categoryId", hide: true }
            , { name: "업무상세ID", field: "categoryDtlId", hide: true }
            , { name: "업무설명", field: "categoryDesc", hide: true }
            , { name: "업무파일ID", field: "categoryFileGrpId", hide: true }
            , { name: "초기컨텐츠", field: "contentsDesc", hide: true }
        ];

        this.setOption(columnDefs, ["dtlGubnCd"], gridTrigger.onCellValueChanged, this.codeGroupValueChanged);
        this.categoryDtlGrid.setColumnDefs(columnDefs);
    }

    setCategoryDtlGridApi = async (grid) => {
        this.categoryDtlGrid = grid;

        const columnDefs = [
            { word: "M00685", name: "코드그룹", field: "dtlGubnCd", optionGroup: this.optionGroup, width: 100, editable: true, essential: true }
            , { word: "M03376", name: "코드그룹명", field: "dtlGubnCdNm", width: 100, essential: true }
            , { word: "M03360", name: "구분타입", field: "dtlGubnType", width: 100, codeGroup: "DTL_GUBN", addOption: this.l10n("M03378", "자동변경"), default: '' }
            , { word: "M00066", name: "코드", field: "categoryDtlCd", width: 80, editable: true, essential: true }
            , { word: "M01212", name: "코드명", field: "categoryDtlCdNm", width: 100, editable: true, essential: true }
            , { word: "M00282", name: "정렬순서", field: "sort", width: 80, essential: true, type: 'number', editable: true }
            , { word: "M00399", name: "사용여부", field: "useYn", width: 80, align: 'center', codeGroup: "USE_YN", editable: true, default: "Y", essential: true }

            , { name: "업무ID", field: "categoryId", hide: true }
            , { name: "업무상세ID", field: "categoryDtlId", hide: true }
        ];

        this.categoryDtlGrid.setColumnDefs(columnDefs);
    }

    //시스템 마스터 조회
    handleSearchClick = async () => {
        const param = toJS(this.sysMaster);
        await this.sysGrid.search(`/work/sys-category-mng/system-mt/get`, param);
        this.sysGrid.setFocusByRowIndex(0);
    }

    //시스템 마스터 저장
    handleSaveSys = async () => {
        if (await this.sysGrid.validation()) {
            const param = toJS(this.sysMaster);
            await this.sysGrid.saveEditAll(`/work/sys-category-mng/system-mt/save`, param);
        }
    }

    //업무 마스터 저장
    handleSaveCategoryMt = async () => {
        if (await this.categoryMtGrid.validation()) {

            let selectedRow = this.sysGrid.getSelectedRow();
            this.sysMaster.sysId = selectedRow.sysId;
            let param = toJS(this.sysMaster);
            await this.categoryMtGrid.saveEditAll(`/work/sys-category-mng/category-mt/save`, param);

            selectedRow = this.categoryMtGrid.getSelectedRow();
            param = toJS(selectedRow);
            await this.categoryDtlGrid.search(`/work/sys-category-mng/category-dtl/get`, param);
            this.changeCategoryDtlColumns();
            this.categoryDtlGrid.setFocusByRowIndex(0);
        }
    }

    //시스템 추가
    handleAddSys = async () => {
        this.sysGrid.addRow();
    }

    //업무 추가
    handleAddCategoryMt = async () => {
        const selectedRow = this.sysGrid.getSelectedRow();

        if (selectedRow) {
            this.categoryMtGrid.addRow({ sysId: selectedRow.sysId, dtlGubn: '' });
        } else {
            // await this.sysGrid.search(`/work/mng/system-mt/get`, param);
        }
    }

    changeCategoryDtlColumns = () => {
        const selectedRow = this.categoryMtGrid.getSelectedRow();

        const splitDtlGubnCd = selectedRow.dtlGubnCd.split(',')
        const splitDtlGubnCdNm = selectedRow.dtlGubnCdNm.split(',')
        const splitDtlGubnType = selectedRow.dtlGubnType.split(',')

        for (let index = 0; index < splitDtlGubnCd.length; index++) {
            this.gubnTypeMap.set(splitDtlGubnCd[index], splitDtlGubnType[index]);
            this.dtlGubnCdNmMap.set(splitDtlGubnCd[index], splitDtlGubnCdNm[index]);
        }

        const selectGroup = selectedRow.dtlGubnCd.split(',');
        this.optionGroup = [];

        if (selectGroup && selectGroup.length > 1) {
            const initGroup = {
                cd: '',
                cdNm: this.l10n("M00233", "선택")
            }

            this.optionGroup.push(initGroup);
        }

        selectGroup.forEach(element => {
            const temp = {
                cd: element
                , cdNm: element
            }

            this.optionGroup.push(temp);
        });

        this.initCategoryMtGrid();
    }

    //업무 분류 추가
    handleAddCategoryDtl = async () => {
        const selectedRow = this.categoryMtGrid.getSelectedRow();

        if (!selectedRow) {
            await Alert.meg(this.l10n("M03361", "업무를 먼저 지정해 주십시오."));
            return;
        }

        const splitDtlGubnCd = selectedRow.dtlGubnCd.split(',')
        const splitDtlGubnCdNm = selectedRow.dtlGubnCdNm.split(',')
        const splitDtlGubnType = selectedRow.dtlGubnType.split(',')

        for (let index = 0; index < splitDtlGubnCd.length; index++) {
            this.gubnTypeMap.set(splitDtlGubnCd[index], splitDtlGubnType[index]);
            this.dtlGubnCdNmMap.set(splitDtlGubnCd[index], splitDtlGubnCdNm[index]);
        }

        if (!selectedRow) {
            await Alert.meg(this.l10n("M03361", "업무를 먼저 지정해 주십시오."));
            return;
        }

        if (!selectedRow.categoryId) {
            await Alert.meg(this.l10n("M03363", "업무를 먼저 저장해 주십시오."));
            return;
        }

        if (selectedRow.dtlYn == 'Y' && selectedRow.dtlGubnType && selectedRow.dtlGubnCd && selectedRow.dtlGubnCdNm) {
            const selectGroup = selectedRow.dtlGubnCd.split(',');
            this.optionGroup = [];


            if (selectGroup && selectGroup.length > 1) {
                const initGroup = {
                    cd: '',
                    cdNm: this.l10n("M00233", "선택")
                }

                this.optionGroup.push(initGroup);
            }

            selectGroup.forEach(element => {
                const temp = {
                    cd: element
                    , cdNm: element
                }

                this.optionGroup.push(temp);
            });

            this.initCategoryMtGrid();
            this.categoryDtlGrid.addRow({ categoryId: selectedRow.categoryId, useYn: 'Y', dtlGubnType: selectGroup.length == 1 ? splitDtlGubnType[0] : '', dtlGubnCdNm: selectGroup.length == 1 ? splitDtlGubnCdNm[0] : this.l10n("M03378", "자동변경") });
        } else {
            await Alert.meg(this.l10n("M03362", "해당 업무의 하위업무여부를 'Y'로 변경 후 진행하십시오."));
            return;
        }
    }

    codeGroupValueChanged = async(param) => {
        param.data.dtlGubnType = this.gubnTypeMap.get(param.data.dtlGubnCd);
        param.data.dtlGubnCdNm = this.dtlGubnCdNmMap.get(param.data.dtlGubnCd);
        if(param.data.dtlGubnType == 'I' || param.data.dtlGubnType == 'NI') {
            if(this.categoryDtlGrid.getRows().filter(x => x.dtlGubnCd == param.data.dtlGubnCd).length > 1) {
                await Alert.meg(this.l10n("M03481", "해당 코드 그룹은 항목에 있습니다."));
                this.categoryDtlGrid.deleteRow(this.categoryDtlGrid.getSelectedRow());
                return;
            }

            param.data.categoryDtlCd = param.data.dtlGubnCd
            param.data.categoryDtlCdNm = param.data.categoryDtlCd
        }
    }

    // 시스템 마스터 Grid row 변경시
    handleSysSelectCahnge = async () => {
        const selectedRow = this.sysGrid.getSelectedRow();

        if (selectedRow) {
            this.currWorkNm = selectedRow.sysNm + ' ';

            if (selectedRow.sysId) {
                const param = toJS(selectedRow);
                await this.categoryMtGrid.search(`/work/sys-category-mng/category-mt/get`, param);
                // 업무 조회
                this.categoryMtGrid.setFocusByRowIndex(0);
                const selectedCategoryRow = this.categoryMtGrid.getSelectedRow();

                if (!selectedCategoryRow) {
                    this.currWorkDtlNm = '';
                    this.categoryDtlGrid.setRowData([]);
                }
                return;
            }

        }

        this.currWorkNm = '';
        this.currWorkDtlNm = '';
        this.categoryMtGrid.setRowData([]);
        this.categoryDtlGrid.setRowData([]);
    }

    // 업무 마스터 Grid row 변경시
    handleCategorySelectCahnge = async () => {
        const selectedRow = this.categoryMtGrid.getSelectedRow();
        this.categoryMaster = selectedRow;

        if (selectedRow) {
            if (selectedRow.categoryId) {
                this.changeCategoryDtlColumns();
                this.currWorkDtlNm = selectedRow.categoryNm + ' ';

                // 업무 상세분류 조회
                const param = toJS(selectedRow);
                await this.categoryDtlGrid.search(`/work/sys-category-mng/category-dtl/get`, param);
                this.categoryDtlGrid.setFocusByRowIndex(0);
            }
        } else {
            this.currWorkDtlNm = '';
            this.categoryDtlGrid.setRowData([]);
        }
    }

    // 시스템 삭제
    handleDeleteSys = async () => {
        const deleteRow = this.sysGrid.getSelectedRow();
        const param = toJS(this.sysMaster);

        if (!deleteRow.newRow) {
            const result = await Alert.confirm(this.l10n("M03365", "삭제하시겠습니까?"));
            if (!result) {
                return;
            }
        }

        await this.sysGrid.deleteSelectedItem(`/work/sys-category-mng/system-mt/delete`, param, true, false);
    }

    // 업무 삭제
    handleDeleteCategoryMt = async () => {
        const deleteRow = this.categoryMtGrid.getSelectedRow();
        const param = toJS(this.sysMaster);

        if (!deleteRow.newRow) {
            const result = await Alert.confirm(this.l10n("M03364", "삭제하시겠습니까?"));
            if (!result) {
                return;
            }
        }

        await this.categoryMtGrid.deleteSelectedItem(`/work/sys-category-mng/category-mt/delete`, param, true, false);
    }

    // 업무 상세 삭제
    handleDeleteCategoryDtl = async () => {
        const param = toJS(this.categoryMaster);
        await this.categoryDtlGrid.deleteSelectedItem(`/work/sys-category-mng/category-dtl/delete`, param);
    }

    //업무 상세 저장
    handleSaveCategoryDtl = async () => {
        if (await this.categoryDtlGrid.validation()) {
            const param = toJS(this.categoryMaster);
            await this.categoryDtlGrid.saveEditAll(`/work/sys-category-mng/category-dtl/save`, param);
        }
    }

    handleCategoryDescAdd = async () => {
        const selectedObj = this.categoryMtGrid.getSelectedRow();
        selectedObj.contentType = '01';

        const modal = new ModalManage();
        modal.id = "work_sys_category_mng_p";
        modal.path = "/work/sysCategoryMng/popup/SysCategoryContentsP";
        modal.title = this.l10n("M03439"); //"업무 설명 등록"
        modal.parameter = { param: selectedObj };
        modal.width = 1133;

        const result = await this.showModal(modal);

        if (result || result == '') {
            selectedObj.categoryDesc = result;
        }
    }

    handleContentsDescAdd = async () => {
        const selectedObj = this.categoryMtGrid.getSelectedRow();
        selectedObj.contentType = '02';

        const modal = new ModalManage();
        modal.id = "work_sys_category_mng_p";
        modal.path = "/work/sysCategoryMng/popup/SysCategoryContentsP";
        modal.title = this.l10n("M03438"); //"초기컨텐츠 등록"
        modal.parameter = { masterP: selectedObj };
        modal.width = 1133;

        const result = await this.showModal(modal);

        if (result || result == '') {
            selectedObj.contentsDesc = result;
        }
    }
}

@observer
class SysCategoryMng extends BaseComponent {
    sRender() {
        return (
            <React.Fragment>
                <div className="ratio_h40 shadow">
                    <SearchTemplate>
                        <div className='search_item_btn'>
                            <SButton buttonName={this.l10n("M00011", "조회")} onClick={this.store.handleSearchClick} type={'sysSearch'} />
                        </div>
                        <SInput id={"sysMaster.sysNm"} isFocus={true} enter={this.store.handleSearchClick} title={this.l10n("M03352", "시스템명")} />
                    </SearchTemplate>
                    <ContentsMiddleTemplate subTitle={this.l10n("M03437", "시스템목록")}>
                        <SButton buttonName={this.l10n("M00983", "추가")} onClick={this.store.handleAddSys} type={'sysAdd'} />
                        <SButton buttonName={this.l10n("M00004", "저장")} onClick={this.store.handleSaveSys} type={'sysSave'} />
                        <SButton buttonName={this.l10n("M00003", "삭제")} onClick={this.store.handleDeleteSys} type={'sysDelete'} />
                    </ContentsMiddleTemplate>
                    <ContentsTemplate id={"sysContents"} shadow={true}>
                        <SGrid grid={'sysGrid'} gridApiCallBack={this.store.setSysGridApi} onSelectionChanged={this.store.handleSysSelectCahnge} editable={false} />
                    </ContentsTemplate>
                </div>
                <div className="ratio_h60 shadow">
                    <div className="ratio_w65 ratio_h100 shadow">
                        <ContentsMiddleTemplate subTitle={this.store.currWorkNm + this.l10n("M03358", "업무 목록")} >
                            <SButton buttonName={this.l10n("M00983", "추가")} onClick={this.store.handleAddCategoryMt} type={'categoryMtAdd'} />
                            <SButton buttonName={this.l10n("M00004", "저장")} onClick={this.store.handleSaveCategoryMt} type={'categoryMtSave'} />
                            <SButton buttonName={this.l10n("M00003", "삭제")} onClick={this.store.handleDeleteCategoryMt} type={'categoryMtDelete'} />
                        </ContentsMiddleTemplate>
                        <ContentsTemplate id={"categoryMtContents"} shadow={true}>
                            <SGrid grid={'categoryMtGrid'} gridApiCallBack={this.store.setCategoryMtGridApi} onSelectionChanged={this.store.handleCategorySelectCahnge} editable={false} />
                        </ContentsTemplate>
                    </div>
                    <div className="ratio_w35 ratio_h100 shadow">
                        <ContentsMiddleTemplate subTitle={this.store.currWorkDtlNm + this.l10n("M03357", "상세분류")}>
                            <SButton buttonName={this.l10n("M00983", "추가")} onClick={this.store.handleAddCategoryDtl} type={'categoryDtlAdd'} />
                            <SButton buttonName={this.l10n("M00004", "저장")} onClick={this.store.handleSaveCategoryDtl} type={'categoryDtlSave'} />
                            <SButton buttonName={this.l10n("M00003", "삭제")} onClick={this.store.handleDeleteCategoryDtl} type={'categoryDtlDelete'} />
                        </ContentsMiddleTemplate>
                        <ContentsTemplate id={"categoryDtlContents"} shadow={true}>
                            <SGrid grid={'categoryDtlGrid'} gridApiCallBack={this.store.setCategoryDtlGridApi} editable={false} />
                        </ContentsTemplate>
                    </div>
                </div>
            </React.Fragment>
        )
    }
}
import React from 'react';
import { BaseStore, model } from "utils/base/BaseStore";
import { observer } from "mobx-react";
import { BaseComponent } from "utils/base/BaseComponent";
import { observable, toJS } from "mobx";
import ContentsMiddleTemplate from 'components/template/ContentsMiddleTemplate';
import ContentsTemplate from 'components/template/ContentsTemplate';

import SButton from 'components/atoms/button/SButton';
import Alert from 'components/override/alert/Alert';
import SEditor from 'components/override/editor/SEditor'

import SysCategoryContentsPModel from 'modules/pages/work/sysCategoryMng/popup/SysCategoryContentsPModel';

export { SysCategoryContentsP, SysCategoryContentsPStore }

class SysCategoryContentsPStore extends BaseStore {

    @observable @model masterP = new SysCategoryContentsPModel();

    initialize = async (params) => {
        this.setBind(this.masterP, params.masterP);

        if (this.masterP.contentType) {
            if(this.masterP.contentType == '01')  {
                this.masterP.contents = this.masterP.categoryDesc;
            } else {          
                this.masterP.contents = this.masterP.contentsDesc;
            }
        }
    }

    //버튼 선택
    handleContentsSaveClick = async () => {
        if(this.masterP.contentType == '01')  { 
            this.masterP.categoryDesc = this.masterP.contents;
        } else {
            this.masterP.contentsDesc = this.masterP.contents;
        }
    
        const params = toJS(this.masterP);
        const result = await this.save(`/work/sys-category-mng/category-mt/contents/save`, params);
        if (result) {
            this.close(this.masterP.contents);
        }
    }

    handleContentsDeleteClick = async () => {

        this.masterP.contents = '';
        if(this.masterP.contentType == '01')  { 
            this.masterP.categoryDesc = this.masterP.contents;
        } else {
            this.masterP.contentsDesc = this.masterP.contents;
        }
    
        const params = toJS(this.masterP);
        const result = await this.delete(`/work/sys-category-mng/category-mt/contents/save`, params);
        if (result) {
            this.close('');
        }
    }

}

@observer
export default class SysCategoryContentsP extends BaseComponent {

    sRender() {
        return (
            <div className="basic">
                <React.Fragment>
                    <ContentsMiddleTemplate>
                        <SButton buttonName={this.l10n("M00003", "삭제")} onClick={this.store.handleContentsDeleteClick} type={'delete'} />
                        <SButton buttonName={this.l10n("M00004", "저장")} onClick={this.store.handleContentsSaveClick} type={'save'} />
                    </ContentsMiddleTemplate>
                    <ContentsTemplate height={250} isShadow={true}>
                        <SEditor id={"masterP.contents"}/>
                    </ContentsTemplate>
                </React.Fragment>
            </div>
        )
    }

}
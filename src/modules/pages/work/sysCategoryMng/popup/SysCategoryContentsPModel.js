import { BaseModel } from "utils/base/BaseModel";
import { observable } from "mobx";

export default class SysCategoryContentsPModel extends BaseModel {    
    @observable contents;
    @observable categoryId;
    @observable contentType;
}
import React from 'react';
import { BaseStore, gridTrigger, model } from "utils/base/BaseStore";
import { inject, observer } from "mobx-react";
import { BaseComponent } from "utils/base/BaseComponent";
import SearchTemplate from 'components/template/SearchTemplate';
import ContentsTemplate from 'components/template/ContentsTemplate';
import ContentsMiddleTemplate from 'components/template/ContentsMiddleTemplate';
import Alert from 'components/override/alert/Alert';
import SInput from 'components/atoms/input/SInput';
import SButton from 'components/atoms/button/SButton';
import SGrid from 'components/override/grid/SGrid';
import WorkMngModel from 'modules/pages/work/workMng/WorkMngModel';
import CommonStore from 'modules/pages/common/service/CommonStore';
import { observable, toJS } from 'mobx';

export { WorkMng, WorkMngStore }
class WorkMngStore extends BaseStore {
    
    @observable @model workMaster = new WorkMngModel();
    @observable currWorkNm = "";

    loaded = async () => {
        await this.handleSearchClick();
    }

    setWorkGridApi = async (grid) => {
        this.workGrid = grid;

        const columnDefs = [
              { word: "M03352", name: "시스템명", field: "sysNm", width: 110, align: 'center'}
            , { word: "M03350", name: "분류", field: "categoryLargeId", codeGroup: "CATEGORY_GROUP", width: 110, align: 'center'}
            , { word: "M03351", name: "업무명", field: "categoryNm", width: 250}
                   
            , { name: "시스템ID", field: "sysId", hide: true }
            , { name: "업무ID", field: "categoryId", hide: true }
        ];
 
        this.setRowSpanning(columnDefs, "sysNm", "sysNm");
        this.workGrid.setColumnDefs(columnDefs);
    }

    setWorkMngMtGridApi = async (grid) => {
        this.workMngMtGrid = grid;

        const columnDefs = [
              { word: "M02453", name: "회사명", field: "compNm", width: 100, type: 'codeHelper', business: 'compMt', onHelperResult: (result) => {result.grid = 'workMng'; this.handleCompHelperResult(result, ["compNm"])}, default: 'LS전선'}
            , { word: "M03443", name: "담당 사업장", field: "locCd", width: 100, align: 'center', codeGroup: "LOC_CD", editable: true, addOption: this.l10n("M03444", "LS전선"), default:  '' }
            , { word: "M03368", name: "업무 담당자", field: "mngEmpNm", width: 100, align: 'center', essential: true,
                type: "codeHelper", business: "user", helperParams: { _code: "userNm" }, onHelperResult: (result) => {result.grid = 'workMng'; this.handleUserHelperResult(result, ["empNo"])} 
              }                    
            , { word: "M00621", name: "부서", field: "deptNm", width: 150}
            , { word: "M03367", name: "담당자(정) 여부", field: "headYn", width: 80, align: 'center', codeGroup: "HEAD_YN", editable: true, default: "Y", essential: true, addOption: this.l10n("M00233", "선택"), default:  ''}
            , { word: "M00399", name: "사용여부", field: "useYn", width: 80, align: 'center', codeGroup: "USE_YN", editable: true, default: "Y", essential: true }            

            , { name: "", field: "compCd", width: 0, hide: true, default: 'A002'}
            , { name: "", field: "mngEmpNo", width: 0, hide: true}
            , { name: "", field: "categoryId", hide: true }
            , { name: "", field: "mngId", hide: true }
        ];


        this.setOption(columnDefs, "headYn", gridTrigger.onCellValueChanged, this.headValueChanged);
        this.workMngMtGrid.setColumnDefs(columnDefs);
    }

    headValueChanged = async (param) => {
        const validationList = this.workMngMtGrid.getRows().filter(x => x.compCd == param.data.compCd && x.headYn  == 'Y' && x.locCd == param.data.locCd)

        if(validationList.length > 1) {
            param.data.headYn = 'N';
            this.workMngMtGrid.updateRowData(this.workMngMtGrid.getRows());
            await Alert.meg(this.l10n("M03384", "회사별 주(정)담당자는 한명입니다.</br> [부]로 선택이 되었습니다."));
            return;
        }
    }

    setApprMngMtGridApi = async (grid) => {
        this.apprMngMtGrid = grid;

        const columnDefs = [
            { word: "M02453", name: "회사명", field: "compNm", width: 100, type: 'codeHelper', business: 'compMt', essential: true, onHelperResult: (result) => { result.grid = 'apprMng'; this.handleCompHelperResult(result, ["compNm"]) }, default: 'LS전선' }
            , {
                word: "M03333", name: "승인자", field: "apprEmpNm", width: 80, align: 'center', essential: true,
                type: "codeHelper", business: "user", helperParams: { _code: "userNm" }, onHelperResult: (result) => { result.grid = 'apprMng'; this.handleUserHelperResult(result, ["empNo"]) }
            }
            , { word: "M00621", name: "부서", field: "deptNm", width: 150 }
            , { word: "M03372", name: "승인타입", field: "apprType", width: 150, align: 'center', codeGroup: "APPR_TYPE", editable: true, addOption: this.l10n("M00233", "선택"), default: '', essential: true }
            , { word: "M03371", name: "승인순번", field: "sort", width: 80, align: 'center', editable: true, essential: true, type: 'number' },
            , { word: "M00399", name: "사용여부", field: "useYn", width: 80, align: 'center', codeGroup: "USE_YN", editable: true, default: "Y" }
            
            
            , { word: "M03385", name: "자등승인여부", field: "autoApprYn", width: 90, align: 'center', codeGroup: "USE_YN", editable: true, default: "N", hide: true }
            , { name: "", field: "compCd", width: 0, hide: true, default: 'A002' }
            , { name: "", field: "apprEmpNo", width: 0, hide: true }
            , { name: "", field: "categoryId", hide: true }
            , { name: "", field: "apprId", hide: true }
        ];

        this.setOption(columnDefs, "apprType", gridTrigger.onCellValueChanged, this.apprTypeValueChanged);
        this.apprMngMtGrid.setColumnDefs(columnDefs);
    }

    apprTypeValueChanged = async (param) => {
        const validationList = this.apprMngMtGrid.getRows().filter(x => x.compCd == param.data.compCd && x.apprType  == param.data.apprType)

        if(validationList.length > 1) {
            param.data.apprType = '';
            await Alert.meg(this.l10n("M03386", "해당 회사에 같은 승인타입이 이미 존재합니다."));
            return;
        }
    
    }

    handleUserHelperResult = async (result) => {
        let data = {};
        
        data["deptNm"] = result ? result.deptNm : null;
        data["rankNm"] = result ? result.rankNm : null;

        switch(result.grid) {
            case 'workMng':
                data["mngEmpNo"] = result ? result.empNo : null;
                data["mngEmpNm"] = result ? result.userNm : null;
                this.workMngMtGrid.setDataByRowIndex(data);
            default: 
                data["apprEmpNo"] = result ? result.empNo : null;
                data["apprEmpNm"] = result ? result.userNm : null;
                this.apprMngMtGrid.setDataByRowIndex(data);
        }

        this.validationCheck(result);
    }

    handleCompHelperResult = async (result) => {
        let data = {};
        data["compCd"] = result ? result.compCd : null;
        data["compNm"] = result ? result.compNm : null;
        
        switch(result.grid) {
            case 'workMng':
                this.workMngMtGrid.setDataByRowIndex(data);
            default: 
                this.apprMngMtGrid.setDataByRowIndex(data);
        }

        this.validationCheck(result);
    }

    validationCheck = async (param) => {
        let targetObj  = undefined;
        let filterList = undefined;
        switch(param.grid) {
            case 'workMng':
                targetObj  = this.workMngMtGrid.getSelectedRow();
                filterList = this.workMngMtGrid.getRows().filter(x => x.mngEmpNo == targetObj.mngEmpNo && targetObj.compCd == x.compCd && x.locCd == targetObj.locCd);

                if(filterList.length > 1) {
                    await Alert.meg(this.l10n("M03380", "해당 회사에 같은 업무담당자가 이미 등록 되어 있습니다."));
                    let data = {};
                    data["compCd"]   = null;
                    data["compNm"]   = null;
                    data["deptNm"]   = null;
                    data["locCd"]    = '';
                    data["rankNm"]   = null;
                    data["mngEmpNo"] = null;
                    data["mngEmpNm"] = null;
                    this.workMngMtGrid.setDataByRowIndex(data);
                    return;
                }

            default: 
                targetObj  = this.apprMngMtGrid.getSelectedRow();
                filterList = this.apprMngMtGrid.getRows().filter(x => x.apprEmpNo == targetObj.apprEmpNo && targetObj.compCd == x.compCd);

                if (filterList.length > 1) {
                    await Alert.meg(this.l10n("M03381", "해당 회사에 같은 승인자가 이미 등록 되어 있습니다."));
                    let data = {};
                    data["compCd"]    = null;
                    data["compNm"]    = null;
                    data["deptNm"]    = null;
                    data["rankNm"]    = null;
                    data["apprEmpNo"] = null;
                    data["apprEmpNm"] = null;
                    this.apprMngMtGrid.setDataByRowIndex(data);
                    return;
                }
        }
    }

    //업무 마스터 조회
    handleSearchClick = async () => {
        const param = toJS(this.workMaster);
        await this.workGrid.search(`/work/sys-category-mng/category-mt/get`, param);
        this.workGrid.setFocusByRowIndex(0);
    }

    // IT 담당자 추가
    handleAddWorkMngMt = async () => {
        const selectedRow = this.workGrid.getSelectedRow();

        if (selectedRow) {
            this.workMngMtGrid.addRow({ categoryId: selectedRow.categoryId});
        } else {
            await Alert.meg(this.l10n("M03361", "업무를 먼저 지정해 주십시오."));
            return;
        }
    }

    // 시스템 마스터 Grid row 변경시
    handleWorkSelectCahnge = async () => {
        const selectedRow = this.workGrid.getSelectedRow();

        if (selectedRow) {
            this.currWorkNm = selectedRow.categoryNm + ' ';

            if (selectedRow.categoryId) {
                this.workMaster.categoryId = selectedRow.categoryId;
                const param = toJS(selectedRow);
                await this.workMngMtGrid.search(`/work/work-appr-mng/work-mng-mt/get`, param);
                await this.apprMngMtGrid.search(`/work/work-appr-mng/work-appr-mt/get`, param);
                this.apprMngMtGrid.setFocusByRowIndex(0);
                this.workMngMtGrid.setFocusByRowIndex(0);
            } else {
                this.apprMngMtGrid.setRowData([]);
                this.workMngMtGrid.setRowData([]);
            }
        } else {
            this.currWorkNm = '';
            this.apprMngMtGrid.setRowData([]);
            this.workMngMtGrid.setRowData([]);
        }
    }

    // 승인자 추가
    handleAddApprMngMt = async () => {
        const selectedRow = this.workGrid.getSelectedRow();

        if (selectedRow) {
            
            if(selectedRow.apprYn != 'Y') {
                await Alert.meg(String.format(this.l10n("M03412", "{0}업무는 승인여부가 'N' 입니다. </br> 업무등록 메뉴에서 변경해십시오."), selectedRow.categoryNm));
                return;
            }

            this.apprMngMtGrid.addRow({ categoryId: selectedRow.categoryId, apprType: '', sort: ''});
        } else {
            await Alert.meg(this.l10n("M03361", "업무를 먼저 지정해 주십시오."));
            return;
        }
    }

    // 승인자 저장
    handleSaveApprMngMt = async () => {
        if (await this.apprMngMtGrid.validation()) {
            const param = toJS(this.workMaster);
            await this.apprMngMtGrid.saveEditAll(`/work/work-appr-mng/work-appr-mt/save`, param);
        }
    }

    // 승인자 조회
    handleSearchApprMngMt = async () => {
        if (await this.apprMngMtGrid.validation()) {
            const param = toJS(this.workMaster);
            await this.apprMngMtGrid.search(`/work/work-appr-mng/work-appr-mt/get`, param);
        }
    }

    // 승인자 삭제
    handleDeleteApprMngMt = async () => {
        const param = toJS(this.workMaster);
        await this.apprMngMtGrid.deleteSelectedItem(`/work/work-appr-mng/work-appr-mt/delete`, param);
    }

    // 업무 담당자 저장
    handleSaveWorkMngMt = async () => {

        const validationList = this.workMngMtGrid.getRows();
        const val1 = validationList.filter(x => x.locCd == '' && x.compCd == CommonStore.userInfo.bcompCd);
        const val2 = validationList.filter(x => x.locCd != '' && x.compCd == CommonStore.userInfo.bcompCd);

        if(val1.length > 0 && val2.length > 0) {
            await Alert.meg(this.l10n("M03453", "사업장별 담당자와 전사 공통 담당자가 둘 다 있습니다. 사업장 별 업무 담당자 또는 전사 공통으로 담당자 1명을 지정합시오."));
            return;
        }


        if (await this.workMngMtGrid.validation()) {
            const param = toJS(this.workMaster);
            await this.workMngMtGrid.saveEditAll(`/work/work-appr-mng/work-mng-mt/save`, param);
        }
    }

    // 업무 담당자 조회
    handleSearchWorkMngMt = async () => {
        const param = toJS(this.workMaster);
        await this.workMngMtGrid.search(`/work/work-appr-mng/work-mng-mt/get`, param);
    }

    // 업무 담당자 삭제
    handleDeleteWorkMngMt = async () => {
        const param = toJS(this.workMaster);
        await this.workMngMtGrid.deleteSelectedItem(`/work/work-appr-mng/work-mng-mt/delete`, param);
    }

}

@observer
class WorkMng extends BaseComponent {
    sRender() {
        return (
            <React.Fragment>
                <div className="ratio_w40 ratio_h100 shadow">
                    <SearchTemplate>
                        <div className='search_item_btn'>
                            <SButton buttonName={this.l10n("M00011", "조회")} onClick={this.store.handleSearchClick} type={'sysSearch'} />
                        </div>
                        <SInput id={"workMaster.sysNm"} isFocus={true} enter={this.store.handleSearchClick} title={this.l10n("M03352", "시스템명")} />
                        <SInput id={"workMaster.categoryNm"} isFocus={true} enter={this.store.handleSearchClick} title={this.l10n("M03351", "업무명")} />
                    </SearchTemplate>
                    <ContentsMiddleTemplate subTitle={this.l10n("M03358", "업무 목록")}/>
                    <ContentsTemplate id={"sysContents"} shadow={true}>
                        <SGrid grid={'workGrid'} gridApiCallBack={this.store.setWorkGridApi} onSelectionChanged={this.store.handleWorkSelectCahnge} editable={false} />
                    </ContentsTemplate>
                </div>
                <div className="ratio_w60 ratio_h100 shadow">
                    <div className="ratio_h40 ratio_w100 shadow">
                        <ContentsMiddleTemplate subTitle={this.store.currWorkNm + this.l10n("M03369", "담당자 목록")} >
                            <SButton buttonName={this.l10n("M00983", "추가")} onClick={this.store.handleAddWorkMngMt} type={'workMngMtAdd'} />
                            <SButton buttonName={this.l10n("M00004", "저장")} onClick={this.store.handleSaveWorkMngMt} type={'workMngMtSave'} />
                            <SButton buttonName={this.l10n("M00003", "삭제")} onClick={this.store.handleDeleteWorkMngMt} type={'workMngMtDelete'} />
                        </ContentsMiddleTemplate>
                        <ContentsTemplate id={"workMngMtContents"} shadow={true}>
                            <SGrid grid={'workMngMtGrid'} gridApiCallBack={this.store.setWorkMngMtGridApi} editable={false} />
                        </ContentsTemplate>
                    </div>
                    <div className="ratio_h60 ratio_w100 shadow">
                        <ContentsMiddleTemplate subTitle={this.store.currWorkNm + this.l10n("M03370", "승인자 목록")}>
                            <SButton buttonName={this.l10n("M00983", "추가")} onClick={this.store.handleAddApprMngMt} type={'apprMngMtAdd'} />
                            <SButton buttonName={this.l10n("M00004", "저장")} onClick={this.store.handleSaveApprMngMt} type={'apprMngMtSave'} />
                            <SButton buttonName={this.l10n("M00003", "삭제")} onClick={this.store.handleDeleteApprMngMt} type={'apprMngMtDelete'} />
                        </ContentsMiddleTemplate>
                        <ContentsTemplate id={"apprMngContents"} shadow={true}>
                            <SGrid grid={'apprMngMtGrid'} gridApiCallBack={this.store.setApprMngMtGridApi} editable={false} />
                        </ContentsTemplate>
                    </div>
                </div>
            </React.Fragment>
        )
    }
}
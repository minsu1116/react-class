import React from 'react';
import { observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import SearchTemplate from 'components/template/SearchTemplate';
import ContentsMiddleTemplate from 'components/template/ContentsMiddleTemplate';
import ContentsTemplate from 'components/template/ContentsTemplate';
import SGrid from 'components/override/grid/SGrid';
import SInput from 'components/atoms/input/SInput';
import SButton from 'components/atoms/button/SButton';
import SSelectBox from 'components/atoms/selectbox/SSelectBox';
import { BaseComponent } from 'utils/base/BaseComponent';
import { BaseStore, model } from 'utils/base/BaseStore';
import LangModel from 'modules/pages/system/master/lang/LangModel';
import CommonStore from 'modules/pages/common/service/CommonStore';

export { Lang, LangStore }

class LangStore extends BaseStore {

    @observable @model master = new LangModel();

    setGridApi = async (grid) => {
        this.wordGrid = grid;

        const columnDefs = [
            { field: "check", type: 'checkboxAll' },
            { word: "M01511", name: "메세지구분", field: "wordType", width: 100, align: 'center', codeGroup: "WORD_GUBN", default: "M" },
            { word: "M01005", name: "다국어코드", field: "wordCd", width: 100, align: 'center', editable: false },
        ];
        const codes = CommonStore.codeObject["LANG_CD"];
        for (let i = 0; i < codes.length; i++) {
            let wordCd = codes[i].wordCd;
            let name = codes[i].cdNm;
            let field = codes[i].cd.toLowerCase();
            columnDefs.push({ word: wordCd, name: name, field: field, width: 300, type: 'textarea' });
        }

        this.setNewRowEditable(columnDefs, "wordType");
        this.wordGrid.setColumnDefs(columnDefs);
    }

    handleSearchClick = async () => {
        const params = toJS(this.master);
        await this.wordGrid.search(`/system/master/lang/get`, params);
    }

    handleAddClick = async () => {
        this.wordGrid.addRow({ wordType: this.master.wordType, wordCd: "", plnt: CommonStore.plnt, bpCd: CommonStore.bpCd }, "firstIndex");
    }

    handleSaveClick = async () => {
        if (await this.wordGrid.validation(false)) {
            const searchdata = toJS(this.master);
            this.wordGrid.saveEditAll(`/system/master/lang/save`, searchdata);
        }
    }

    handleDeleteClick = async () => {
        const searchdata = toJS(this.master);
        await this.wordGrid.deleteCheckedItems('/system/master/lang/delete', searchdata);
    }
}

@observer
class Lang extends BaseComponent {
    sRender() {
        return (
            <React.Fragment>
                <SearchTemplate>
                    <div className='search_item_btn'>
                        <SButton buttonName={this.l10n("M00011", "조회")} onClick={this.store.handleSearchClick} type={'search'} />
                    </div>

                    <SSelectBox id={"master.wordType"} codeGroup={"WORD_GUBN"} title={this.l10n("M01511", "메세지구분")}/>
                    <SInput id={"master.wordCd"} enter={this.store.handleSearchClick} title={this.l10n("M01005", "다국어코드")}/>
                    <SInput id={"master.wordNm"} enter={this.store.handleSearchClick} title={this.l10n("M00574", "명")} />
                </SearchTemplate>

                <ContentsMiddleTemplate>
                    <SButton buttonName={this.l10n("M00983", "추가")} onClick={this.store.handleAddClick} type={'add'} />
                    <SButton buttonName={this.l10n("M00004", "저장")} onClick={this.store.handleSaveClick} type={'save'} />
                    <SButton buttonName={this.l10n("M00003", "삭제")} onClick={this.store.handleDeleteClick} type={'delete'} />
                </ContentsMiddleTemplate>

                <ContentsTemplate>
                    <SGrid grid={'wordGrid'} gridApiCallBack={this.store.setGridApi} editType={"single"} />
                </ContentsTemplate>
            </React.Fragment>
        )
    }
}
import { observable } from 'mobx';
import { BaseModel } from 'utils/base/BaseModel'

export default class LangModel extends BaseModel {
    @observable wordCd = "";
    @observable wordNm = "";
    @observable langCd = "";
    @observable wordType = "M";
}
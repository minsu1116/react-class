import { observable } from 'mobx';
import { BaseModel } from 'utils/base/BaseModel'

export default class DeptModel extends BaseModel {

    @observable useYn; 
    @observable compCd;
    @observable deptCd;
}
import { observable } from 'mobx';
import { BaseModel, essential } from "utils/base/BaseModel";
import CommonStore from 'modules/pages/common/service/CommonStore';

export default class UserSavePModel extends BaseModel {

    @observable @essential("M02445", "회사코드") compCd;
    @observable compNm;
    @observable langCd;
    @observable @essential("M00673", "사용자 ID") userId;
    @observable empNo; 
    @observable userNm;
    @observable password = "";
    @observable @essential("M00621", "부서") deptCd;
    @observable deptNm;
    @observable @essential("M00420", "이메일") email;
    @observable officeNo;
    @observable mobileNo;
    @observable useYn;
    @observable rankNm;
    @observable hrYn;
}
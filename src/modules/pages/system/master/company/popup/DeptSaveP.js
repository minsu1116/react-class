import React from 'react';
import { inject, observer } from 'mobx-react';
import { BaseComponent } from 'utils/base/BaseComponent'
import { observable, toJS } from 'mobx';
import { BaseStore, model } from 'utils/base/BaseStore';

import SSelectBox from 'components/atoms/selectbox/SSelectBox';
import SButton from 'components/atoms/button/SButton';
import SInput from 'components/atoms/input/SInput';
import Alert from 'components/override/alert/Alert';
import DeptSavePModel from 'modules/pages/system/master/company/popup/DeptSavePModel';

export { DeptSaveP, DeptSavePStore }

class DeptSavePStore extends BaseStore {

    @observable @model saveDept = new DeptSavePModel();
    @observable readOnly = false;
    @observable compList;
    @observable selectedDeptList;
    deptList;


    loaded = async () => {
        await this.handleSelectionChanged();
    }

    handleSaveClick = async () => {
        const params = toJS(this.saveDept);
        const result = await this.save(`/system/master/dept/save`, params);

        this.close(result);
        
    }

    handleSelectionChanged = async() => {
        this.selectedDeptList = [];
        const parentDeptCd = this.saveDept.parentDeptCd;

        const param = {
             compCd : this.saveDept.compCd
            ,deptCd : this.saveDept.compCd
        }
        const params = toJS(param);
        const data = await this.execute(`/system/master/dept/get`, params);

        this.deptList = data.data;
        if(this.deptList.length > 0) {
            this.deptList.forEach(element => {
                if(element.compCd == this.saveDept.compCd) {
                    this.selectedDeptList.push(element);
                }
            });
            this.saveDept.parentDeptCd = (parentDeptCd == undefined ? this.selectedDeptList[0].deptCd : parentDeptCd);
        }
    }
}

@observer
export default class DeptSaveP extends BaseComponent {

    sRender() {
        return (
            <React.Fragment>
                <table id={'table'} style={{ width: "100%" }}>
                    <tbody>
                        <tr>
                            <th className="essential">{this.l10n("M02445", "회사")}</th>
                            <td colSpan="3">
                                <SSelectBox id={"saveDept.compCd"} optionGroup={this.store.compList} valueMember={"compCd"} displayMember={"compNm"} onChange={this.store.handleSelectionChanged}  />
                            </td>
                        </tr>
                        <tr>
                            <th className="essential">{this.l10n("M00986", "부서코드")}</th>
                            <td>
                                <SInput id={"saveDept.deptCd"} />
                            </td>
                            <th className="essential">{this.l10n("M00677", "부서명")}</th>
                            <td>
                                <SInput id={"saveDept.deptNm"} />
                            </td>
                        </tr>
                        <tr>
                            <th className="essential">{this.l10n("M02451", "상위부서")}</th>
                            <td colSpan="3">
                                <SSelectBox id={"saveDept.parentDeptCd"} optionGroup={this.store.selectedDeptList} valueMember={"deptCd"} displayMember={"deptNm"}/>
                            </td>
                        </tr>
                        <tr>
                        <th className="essential">{this.l10n("M02448", "부서타입")}</th>
                            <td>
                                <SSelectBox id={"saveDept.deptType"} codeGroup={"DEPT_TYPE"} />
                            </td>
                            <th className="essential">{this.l10n("M00399", "사용여부")}</th>
                            <td>
                                <SSelectBox id={"saveDept.useYn"} codeGroup={"USE_YN"} />
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div style ={{ paddingTop:5 }}>
                    <SButton buttonName={this.l10n("M00004", "저장")} onClick={this.store.handleSaveClick} type={'save'} />
                </div>
            </React.Fragment>
        )
    }
}
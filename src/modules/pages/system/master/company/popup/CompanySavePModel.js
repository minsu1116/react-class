import { BaseModel, essential } from "utils/base/BaseModel";
import { observable } from "mobx";

export default class CompanySavePModel extends BaseModel {
    @observable @essential("M02445", "회사코드") compCd;
    @observable compNm;
    @observable remark;
    @observable useYn;
    @observable address;
    
}
import { observable } from 'mobx';
import { BaseModel, essential } from "utils/base/BaseModel";
import CommonStore from 'modules/pages/common/service/CommonStore';

export default class DeptSavePModel extends BaseModel {


    @observable @essential("M02445", "회사코드") compCd;
    @observable @essential("M00621", "부서") deptCd;
    @observable @essential("M00621", "부서") deptNm;
    @observable @essential("M02451", "상위부서") parentDeptCd;
    @observable useYn;
    @observable @essential("M02452", "부서타입") deptType;
}
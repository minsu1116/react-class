import React from 'react';
import { BaseStore, model } from "utils/base/BaseStore";
import { inject, observer } from "mobx-react";
import { BaseComponent } from "utils/base/BaseComponent";
import { observable, toJS } from "mobx";

import SInput from "components/atoms/input/SInput";
import CompanySavePModel from "modules/pages/system/master/company/popup/CompanySavePModel";
import SSelectBox from 'components/atoms/selectbox/SSelectBox';
import STextArea from 'components/atoms/input/STextArea';

import SButton from 'components/atoms/button/SButton';


export { CompanySaveP, CompanySavePStore }

class CompanySavePStore extends BaseStore {

    @observable @model masterP = new CompanySavePModel();

    //프로그램 저장
    handleSaveCompany = async () => {

        const param = toJS(this.masterP);

        if (await this.validation("masterP")) {
            const result = await this.execute(`/system/master/company/create`, param);
            if (result.success) {
                this.close(result);
            }
        }
    }

}

@observer
export default class CompanySaveP extends BaseComponent {

    sRender() {

        return (
            <div className="basic">
                <React.Fragment>
                    <table id={'table'} style={{ width: "100%" }}>
                        <tbody>
                            <tr>
                                <th className="essential">{this.l10n("M02445", "회사코드")}</th>
                                <td>
                                    <SInput id={"masterP.compCd"}/>
                                </td>
                            </tr>
                            <tr>
                                <th>{this.l10n("M02437", "회사명")}</th>
                                <td>
                                    <SInput id={"masterP.compNm"} contentWidth={260} />
                                </td>
                            </tr>
                            <tr>
                                <th>{this.l10n("M00421", "주소")}</th>
                                <td>
                                    <SInput id={"masterP.address"} contentWidth={260} />
                                </td>
                            </tr>
                            <tr>
                                <th>{this.l10n("M00094", "비고")}</th>
                                <td>
                                    <STextArea id={"masterP.remark"} contentWidth={260} />
                                </td>
                            </tr>
                            <tr>
                                <th>{this.l10n("M00399", "사용여부")}</th>
                                <td>
                                    <SSelectBox
                                        id={"masterP.useYn"}
                                        codeGroup={"USE_YN"}
                                    />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div>
                        <SButton buttonName={this.l10n("M00004", "저장")} onClick={this.store.handleSaveCompany} type={'save'} />
                    </div>
                </React.Fragment>
            </div>
        )
    }
}
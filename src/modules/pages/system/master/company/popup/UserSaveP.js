import React from 'react';
import { inject, observer } from 'mobx-react';
import { BaseComponent } from 'utils/base/BaseComponent'
import { observable, toJS } from 'mobx';
import { BaseStore, model } from 'utils/base/BaseStore';

import SSelectBox from 'components/atoms/selectbox/SSelectBox';
import SButton from 'components/atoms/button/SButton';
import SInput from 'components/atoms/input/SInput';
import Alert from 'components/override/alert/Alert';
import UserSavePModel from 'modules/pages/system/master/company/popup/UserSavePModel';

export { UserSaveP, UserSavePStore }

class UserSavePStore extends BaseStore {

    @observable @model saveUser = new UserSavePModel();
    @observable readOnly = false;
    @observable compList;
    @observable selectedDeptList;
    deptList;


    loaded = async () => {
        this.handleSelectionChanged();
        this.saveUser.password = "";
    }

    handleSaveClick = async () => {
        const params = toJS(this.saveUser);
        if (await this.validation("saveUser")) {
            const result = await this.save(`/system/master/user/save`, params);

            if(result.success) {
                this.close(result);
            }
        }
    }

    handleSelectionChanged = async() => {
        this.selectedDeptList = [];
        const deptCd = this.saveUser.deptCd
        this.deptList.forEach(element => {
            if(element.compCd == this.saveUser.compCd) {
                this.selectedDeptList.push(element);
            }
        });

        this.saveUser.deptCd = (deptCd == undefined ? this.selectedDeptList[0].deptCd : deptCd);
    }
}

@observer
export default class UserSaveP extends BaseComponent {

    sRender() {
        return (
            <React.Fragment>
                <table id={'table'} style={{ width: "100%" }}>
                    <tbody>
                        <tr>
                            <th className="essential">{this.l10n("M02445", "회사")}</th>
                            <td>
                                <SSelectBox id={"saveUser.compCd"} optionGroup={this.store.compList} valueMember={"compCd"} displayMember={"compNm"} onChange={this.store.handleSelectionChanged}  />
                            </td>
                            <th className="essential">{this.l10n("M00673", "사용자ID")}</th>
                            <td>
                                <SInput id={"saveUser.userId"} />
                            </td>
                        </tr>
                        <tr>
                            <th className="essential">{this.l10n("M00675", "사용자이름")}</th>
                            <td>
                                <SInput id={"saveUser.userNm"}  />
                            </td>
                            <th className="essential">{this.l10n("M00674", "사번")}</th>
                            <td>
                                <SInput id={"saveUser.empNo"} />
                            </td>
                        </tr>
                        <tr>
                            <th>{this.l10n("M00676", "비밀번호")}</th>
                            <td>
                                <SInput id={"saveUser.password"} />
                            </td>
                            <th >{this.l10n("M02450", "직책")}</th>
                            <td>
                                <SInput id={"saveUser.rankNm"} />
                            </td>
                            
                        </tr>
                        <tr>
                            <th className="essential">{this.l10n("M00621", "부서코드")}</th>
                            <td>
                                <SSelectBox id={"saveUser.deptCd"} optionGroup={this.store.selectedDeptList} valueMember={"deptCd"} displayMember={"deptNm"}  />
                            </td>
                            <th className="essential">{this.l10n("M00420", "이메일")}</th>
                            <td>
                                <SInput id={"saveUser.email"} />
                            </td>
                        </tr>
                        <tr>
                            <th>{this.l10n("M00679", "전화번호")}</th>
                            <td>
                                <SInput id={"saveUser.officeNo"} />
                            </td>
                            <th>{this.l10n("M00433", "연락처(모바일)")}</th>
                            <td>
                                <SInput id={"saveUser.mobileNo"} />
                            </td>
                        </tr>
                        <tr>
                        <th className="essential">{this.l10n("M00982", "hr여부")}</th>
                            <td>
                                <SSelectBox id={"saveUser.hrYn"} codeGroup={"USE_YN"} />
                            </td>
                            <th className="essential">{this.l10n("M00399", "사용여부")}</th>
                            <td>
                                <SSelectBox id={"saveUser.useYn"} codeGroup={"USE_YN"} />
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div style ={{ paddingTop:5 }}>
                    <SButton buttonName={this.l10n("M00004", "저장")} onClick={this.store.handleSaveClick} type={'save'} />
                </div>
            </React.Fragment>
        )
    }
}
import { observable } from 'mobx';
import { BaseModel } from 'utils/base/BaseModel'

export default class UserModel extends BaseModel {

    @observable epmNo;
    @observable useYn; 
    @observable compCd;
    @observable deptCd;
}
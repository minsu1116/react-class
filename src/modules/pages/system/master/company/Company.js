import React from 'react';
import { BaseStore, model } from "utils/base/BaseStore";
import { inject, observer } from "mobx-react";
import { BaseComponent } from "utils/base/BaseComponent";
import { STab, STabPanel } from 'components/override/tab/STab';

import SearchTemplate from 'components/template/SearchTemplate';
import ContentsTemplate from 'components/template/ContentsTemplate';
import SButton from 'components/atoms/button/SButton';
import SInput from 'components/atoms/input/SInput';
import SGrid from 'components/override/grid/SGrid';
import STree from 'components/override/tree/STree';
import Alert from 'components/override/alert/Alert';

import ContentsMiddleTemplate from 'components/template/ContentsMiddleTemplate';
import CompanyModel from 'modules/pages/system/master/company/CompanyModel';
import UserModel from 'modules/pages/system/master/company/UserModel';
import { observable, toJS } from 'mobx';
import ModalManage from 'utils/base/ModalManage';
import DeptModel from 'modules/pages/system/master/company/DeptModel';


export { Company, CompanyStore }

class CompanyStore extends BaseStore {

    /* ----------------------------------- 공통 영역 -------------------------------------- */

    @observable @model master = new CompanyModel();
    @observable @model detail = new CompanyModel();

    @observable @model userModel = new UserModel();
    @observable @model deptModel = new DeptModel();

    @observable treeData = [];
    @observable selectedTreeKey;
    @observable expandedTreeKeys;

    @observable compList = [];
    @observable deptList = [];


    loaded = async () => {
        await this.handleSearch();
        await this.handleSearchClick("init");
    }

    handleTreeSelect = async (key, info) => {

        let deptCheck = true;

        if (this.detail.compCd == info.compCd) {
            deptCheck = false;
        }
        this.detail.compCd = info.compCd;
        this.detail.deptCd = info.deptCd;
        this.userModel.compCd = info.compCd;
        this.userModel.deptCd = info.deptCd;
        const deptParam = toJS(this.detail);
        const userParam = toJS(this.userModel);

        if (key.length == 0) {
            return;
        } else

        await this.userGrid.search(`/system/master/user/get`, userParam);
        await this.deptGrid.search(`/system/master/dept/get`, deptParam);
    }

    // 회사 트리 가져오기
    handleSearch = async () => {
        const params = toJS(this.master);
        const data = await this.execute(`/system/master/dept/get`, params);
        
        if(data.data) {
            data.data.forEach(element => {
                if(element.parentDeptCd =='Top') {
                    element.parentDeptCd = element.detpCd;
                }
            });
            this.treeData = data.data.filter(x => x.deptNm != "LS");
        }
        
        this.deptList = data.data.filter(x => x.deptNm != "LS");
    }


    /* ----------------------------------- 회사 영역 -------------------------------------- */

    onTabChange = async () => {
        await this.handleSearch();
        await this.handleSearchClick();
    }

    setCompanyGridApi = async (grid) => {
        this.comGrid = grid;

        const columnDefs = [
            { name: "", field: "check", width: 32, type: 'checkbox' },
            , { word: "M02445", name: "회사코드", field: "compCd", width: 100, key: true }
            , { word: "M02437", name: "회사명", field: "compNm", width: 100 }
            , { word: "M00421", name: "주소", field: "address", width: 100 }
            , { word: "M00094", name: "비고", field: "remark", width: 150 }
            , { word: "M00399", name: "사용여부", field: "useYn", width: 80, align: 'center' }
        ];

        this.comGrid.setColumnDefs(columnDefs);
    }


    handleSearchClick = async (init) => {
        const param = toJS(this.master);
        const result = await this.execute(`/system/master/company/get`, param);
        if (result.success) {
            this.compList = toJS(result.data);
            if (init != "init") {
                if (result.data.length > 0) {
                    this.comGrid.setRowData(result.data);
                } else {
                    this.comGrid.setRowData([]);
                }
            }
        } else {
            if (init != "init") {
                this.comGrid.setRowData([]);
            }
        }
    }


    handleDelCom = async () => {
        const param = toJS(this.master);
        await this.comGrid.deleteCheckedItems('/system/master/company/delete', param);
    }

    modalOpenCompany = async (params) => {
        const modal = new ModalManage();
        modal.id = "system_master_company_save_p";
        modal.path = "/system/master/company/popup/CompanySaveP";
        modal.title = this.l10n("T00123"); //회사 등록/수정

        if (params == "edit") {

            if (!this.comGrid.getSelectedRow()) {
                await Alert.meg(this.l10n("M01622", "선택된 항목이 없습니다."));
                return;
            }

            modal.parameter = { masterP: this.comGrid.getSelectedRow() }
        }
        modal.height = 300;
        modal.width = 500;

        const result = await this.showModal(modal);

        if (result) {
            await this.handleSearchClick();
        }
    }

    handleAllChanged = async () => {
        const param = this.comGrid.getSelectedRow();
        param.deptCd = param.compCd;
        const params = toJS(param);
        await this.userGrid.search(`/system/master/user/get`, params);
        await this.deptGrid.search(`/system/master/dept/get`, params);
    }
    /* ----------------------------------- 부서 영역 -------------------------------------- */

    setDeptGridApi = async (grid) => {
        this.deptGrid = grid;

        const columnDefs = [
            { name: "", field: "check", width: 32, type: 'checkbox' },
            , { word: "M02445", name: "회사코드", field: "compCd", width: 100, align: 'center' }
            , { word: "M02437", name: "회사명", field: "compNm", width: 120, align: 'center' }
            , { word: "M00677", name: "부서명", field: "deptNm", width: 120 }
            , { word: "M00986", name: "부서코드", field: "deptCd", width: 100 }
            , { word: "M00999", name: "상위코드", field: "parentDeptCd", width: 100 }
            , { word: "M02448", name: "부서타입", field: "divCd", width: 100 }
            , { word: "M02449", name: "부서타입명", field: "divNm", width: 100 }
            , { word: "M00399", name: "사용여부", field: "useYn", width: 80, align: 'center' }
        ];

        this.deptGrid.setColumnDefs(columnDefs);

        await this.handleSearch(true);
    }

    deptModalOpen = async (params) => {
        const modal = new ModalManage();
        modal.id = "system_master_company_dept_save_p";
        modal.path = "/system/master/company/popup/DeptSaveP";
        modal.title = this.l10n("T00124"); //등록 : 수정

        if (params == "edit") {
            if (!this.deptGrid.getSelectedRow()) {
                await Alert.meg(this.l10n("M01622", "선택된 항목이 없습니다."));
                return;
            }

            modal.parameter = { saveDept: this.deptGrid.getSelectedRow(), compList: toJS(this.compList) }
        } else {
            modal.parameter = { compList: toJS(this.compList) }
        }
        modal.height = 315;
        modal.width = 600;

        const result = await this.showModal(modal);

        if (result) {
            const deptParam = toJS(this.detail);
            await this.deptGrid.search(`/system/master/dept/get`, deptParam);
        }
    }

    handleDelDept = async () => {
        const deptParam = toJS(this.detail);
        await this.deptGrid.deleteCheckedItems('/system/master/dept/delete', deptParam);

    }

    /* ----------------------------------- 유저 영역 -------------------------------------- */

    setUserGridApi = async (grid) => {
        this.userGrid = grid;

        const columnDefs = [
            { name: "", field: "check", width: 32, type: 'checkbox' }
            , { word: 'M02453', name: "회사", field: "compNm", width: 120 }
            , { word: 'M00673', name: "사용자ID", field: "userId", width: 90, key: true }
            , { word: 'M00674', name: "사번", field: "empNo", width: 90 }
            , { word: 'M00979', name: "이름", field: "userNm", width: 80 }
            , { word: 'M02450', name: "직책", field: "dutNm", width: 80 }
            , { word: 'M00420', name: "이메일", field: "email", width: 150 }
            , { word: 'M00679', name: "전화번호", field: "officeNo", width: 120 }
            , { word: 'M00433', name: "연락처(모바일)", field: "mobileNo", width: 120 }
            , { word: 'M00980', name: "언어", field: "langCd", width: 70, getCdNm: 'LANG_CD' }
            , { word: 'M00399', name: "사용여부", field: "useYn", width: 80, align: 'center' }
            , { word: 'M00360', name: "등록자", field: "inputEmpNm", width: 80 }
            , { word: 'M00405', name: "등록일", field: "inputDtm", width: 150, align: 'center', type:'dateTimeFull' }
        ];

        this.userGrid.setColumnDefs(columnDefs);
    }

    userModalOpen = async (params) => {
        const modal = new ModalManage();
        modal.id = "system_master_company_user_save_p";
        modal.path = "/system/master/company/popup/UserSaveP";
        modal.title = this.l10n("T00125"); //등록 : 수정

        if (params == "edit") {
            if (!this.userGrid.getSelectedRow()) {
                await Alert.meg(this.l10n("M01622", "선택된 항목이 없습니다."));
                return;
            }
            modal.parameter = { saveUser: this.userGrid.getSelectedRow(), compList: toJS(this.compList), deptList: toJS(this.deptList) }
        } else {
            modal.parameter = { compList: toJS(this.compList), deptList: toJS(this.deptList) }
        }
        modal.height = 315;
        modal.width = 600;

        const result = await this.showModal(modal);

        if (result) {
            const userParam = toJS(this.userModel);
            await this.userGrid.search(`/system/master/user/get`, userParam);
        }
    }

    handleDelUser = async () => {
        await this.userGrid.deleteCheckedItems('/system/master/user/delete');

        const userParam = toJS(this.userModel);
        await this.userGrid.search(`/system/master/user/get`, userParam);
    }

}

@observer
class Company extends BaseComponent {
    sRender() {
        return (
            <div className="basic">
                <React.Fragment>
                    <ContentsTemplate id={"content"}>
                        <div className='ratio_w25 ratio_h100'>
                            <STab id={"detailTabIndex"} onTabChange={this.store.onTabChange}>
                                <STabPanel header={this.l10n("M02447", "회사 목록")}>
                                    <STree
                                        treeData={this.store.treeData}
                                        keyMember={"deptCd"}
                                        pIdMember={"parentDeptCd"}
                                        titleMember={"deptNm"}
                                        onSelect={this.store.handleTreeSelect}
                                        selectedKeyId={"selectedTreeKey"}
                                        expandedKeysId={"expandedTreeKeys"}
                                        border={true}
                                    />
                                </STabPanel>
                                <STabPanel header={this.l10n("M02446", "회사 CRUD")} >
                                    <ContentsTemplate id={"contents"} shadow={true}>
                                        <SearchTemplate shadow={false}>
                                            <div className='search_item_btn'>
                                                <SButton buttonName={this.l10n("M00011", "조회")} onClick={this.store.handleSearchClick} type={'search'} />
                                            </div>
                                            <SInput id={"master.compCd"} enter={this.store.handleSearchClick} width={90} title={this.l10n("M02444", "회사명/코드")} />
                                        </SearchTemplate>
                                        <ContentsMiddleTemplate subTitle={this.l10n("T00123", "회사등록/수정")}>
                                            <SButton buttonName={this.l10n("M00983", "추가")} onClick={() => this.store.modalOpenCompany("add")} type={'comAdd'} />
                                            <SButton buttonName={this.l10n("M00002", "수정")} onClick={() => this.store.modalOpenCompany("edit")} type={'comEdit'} />
                                            <SButton buttonName={this.l10n("M00003", "삭제")} onClick={this.store.handleDelCom} type={'comDelete'} />
                                        </ContentsMiddleTemplate>
                                        <ContentsTemplate id={"menuContents"}>
                                            <SGrid grid={'comGrid'} gridApiCallBack={this.store.setCompanyGridApi} onSelectionChanged={this.store.handleAllChanged} rowDoubleClick={() => this.store.modalOpenCompany("edit")} editable={false} rowData={this.store.compList} />
                                        </ContentsTemplate>
                                    </ContentsTemplate>
                                </STabPanel>
                            </STab>
                        </div>
                        <div className='ratio_w75 ratio_h100'>
                            <div className="ratio_h35 shadow">
                                <ContentsMiddleTemplate subTitle={this.l10n("M00621", "부서")}>
                                    <SButton buttonName={this.l10n("M00983", "추가")} onClick={() => this.store.deptModalOpen("add")} type={'deptAdd'} />
                                    <SButton buttonName={this.l10n("M00002", "수정")} onClick={() => this.store.deptModalOpen("edit")} type={'deptEdit'} />
                                    <SButton buttonName={this.l10n("M00003", "삭제")} onClick={this.store.handleDelDept} type={'deptDelete'} />
                                </ContentsMiddleTemplate>
                                <ContentsTemplate id={"menuContents"}>
                                    <SGrid grid={'deptGrid'} gridApiCallBack={this.store.setDeptGridApi} rowDoubleClick={() => this.store.deptModalOpen("edit")} editable={false} />
                                </ContentsTemplate>
                            </div>
                            <div className="ratio_h65 shadow">
                                <ContentsMiddleTemplate subTitle={this.l10n("M00318", "사용자")}>
                                    <SButton buttonName={this.l10n("M00983", "추가")} onClick={() => this.store.userModalOpen("add")} type={'userAdd'} />
                                    <SButton buttonName={this.l10n("M00002", "수정")} onClick={() => this.store.userModalOpen("edit")} type={'userEdit'} />
                                    <SButton buttonName={this.l10n("M00003", "삭제")} onClick={this.store.handleDelUser} type={'userDelete'} />
                                </ContentsMiddleTemplate>
                                <ContentsTemplate id={"menuContents"}>
                                    <SGrid grid={'userGrid'} gridApiCallBack={this.store.setUserGridApi} rowDoubleClick={() => this.store.userModalOpen("edit")} editable={false} />
                                </ContentsTemplate>
                            </div>
                        </div>
                    </ContentsTemplate>
                </React.Fragment>
            </div>
        )
    }
}
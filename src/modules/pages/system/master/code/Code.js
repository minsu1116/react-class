import React from 'react';
import { observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import SearchTemplate from 'components/template/SearchTemplate';
import SInput from 'components/atoms/input/SInput';
import ContentsMiddleTemplate from 'components/template/ContentsMiddleTemplate';
import ContentsTemplate from 'components/template/ContentsTemplate';
import SGrid from 'components/override/grid/SGrid';
import SButton from 'components/atoms/button/SButton';
import { BaseComponent } from 'utils/base/BaseComponent'
import Alert from 'components/override/alert/Alert'
import { BaseStore, model } from 'utils/base/BaseStore'
import CodeModel from 'modules/pages/system/master/code/CodeModel';
import CommonStore from 'modules/pages/common/service/CommonStore';
import SSelectBox from 'components/atoms/selectbox/SSelectBox';

export { Code, CodeStore }

class CodeStore extends BaseStore {
    @observable @model search = new CodeModel();

    setMasterGridApi = async (grid) => {
        this.masterGrid = grid;

        const compList = [
            { cd: "LS", cdNm: this.l10n("M02454", "공용") },
            { cd: CommonStore.compCd, cdNm: "LS전선" }
        ];

        const columnDefs = [
            { word: "M00281", name: "사업장", field: "compCd", width: 100, align: 'center', editable: true, default: "LS", optionGroup: compList },
            { word: "M00685", name: "코드그룹", field: "cdGrp", width: 80, align: 'center', codeGroup: "CD_GROUP", essential: true },
            { word: "M00066", name: "코드", field: "cdType", width: 120, align: 'center', key: true },
            { word: "M01212", name: "코드명", field: "cdTypeNm", width: 200, essential: true },
            { word: "M01005", name: "다국어코드", field: "wordCd", width: 100, type: 'codeHelper', business: 'word' },
            { word: "M00399", name: "사용여부", field: "useYn", width: 80, align: 'center', default: 'Y', codeGroup: "USE_YN" },
        ];
        this.setNewRowEditable(columnDefs, "cdType");
        this.masterGrid.setColumnDefs(columnDefs);
    }

    setDetailGridApi = async (grid2) => {
        this.detailGrid = grid2;
        const columnDefs = [
            { word: "M00281", name: "사업장", field: "compCd", width: 100, align: 'center', hide: true, default: CommonStore.compCd },
            { word: "M00066", name: "코드", field: "cdType", width: 120, align: 'center', key: true, hide: true },
            { word: "M01182", name: "상세코드", field: "cd", width: 120, align: 'center', key: true },
            { word: "M01212", name: "코드명", field: "cdNm", width: 200, essential: true },
            { word: "M01005", name: "다국어코드", field: "wordCd", width: 100, type: 'codeHelper', business: 'word' },
            { word: "M01219", name: "Desc1", field: "desc1", width: 100 },
            { word: "M01220", name: "Desc2", field: "desc2", width: 100 },
            { word: "M01221", name: "Desc3", field: "desc3", width: 100 },
            { word: "M01222", name: "Desc4", field: "desc4", width: 100 },
            { word: "M01223", name: "Desc5", field: "desc5", width: 100 },
            { word: "M01041", name: "순서", field: "sort", width: 70, type: "number" },
            { word: "M00399", name: "사용여부", field: "useYn", align: 'center', width: 80, default: 'Y', codeGroup: "USE_YN" },
        ];
        this.setNewRowEditable(columnDefs, "cd");
        this.detailGrid.setColumnDefs(columnDefs);
    }

    handleSearchClick = async () => {
        const params = toJS(this.search);
        await this.masterGrid.search(`/system/master/code/get`, params, true);
    }

    handleMasterRowSelect = async () => {
        const params = this.masterGrid.getSelectedRow();
        if (params) {
            await this.detailGrid.search(`/system/master/code/detail/get`, params);
        }
        else {
            this.detailGrid.clear();
        }
    }

    handleMasterAddClick = () => {
        this.masterGrid.addRow();
    }

    handleMasterSaveClick = async () => {
        if (await this.masterGrid.validation()) {
            const params = toJS(this.search);
            await this.masterGrid.saveEditAll(`/system/master/code/save`, params);
        }
    }

    handleMasterDeleteClick = async () => {
        const params = toJS(this.search);
        await this.masterGrid.deleteSelectedItem(`/system/master/code/delete`, params);
    }

    handleDetailAddClick = () => {
        const selectedRow = this.masterGrid.getSelectedRow();
        if (!selectedRow) {
            Alert.meg(this.l10n("M01622", "선택된 항목이 없습니다."));
            return;
        }
        if (!selectedRow.cdType) {
            Alert.meg(this.l10n("M01254", "상위코드를 먼저 저장해주세요."));
            return;
        }
        const codemt = {
            compCd: CommonStore.compCd,
            cdType: selectedRow.cdType,
            sort: this.detailGrid.length() + 1,
        };
        this.detailGrid.addRow(codemt, "lastIndex");
    }

    handleDetailSaveClick = async () => {
        if (await this.detailGrid.validation()) {
            const selectedRow = this.masterGrid.getSelectedRow();
            if (!selectedRow) {
                await Alert.meg(this.l10n("M01622", "선택된 항목이 없습니다."));
                return;
            }
            const params = {
                compCd: CommonStore.compCd,
                cdType: selectedRow.cdType,
            };
            await this.detailGrid.saveEditAll(`/system/master/code/detail/save`, params);
        }
    }

    handleDetailDeleteClick = async () => {
        const selectedRow = this.masterGrid.getSelectedRow();
        if (!selectedRow) {
            await Alert.meg(this.l10n("M01622", "선택된 항목이 없습니다."));
            return;
        }
        const params = {
            compCd: CommonStore.compCd,
            cdType: selectedRow.cdType,
        };
        await this.detailGrid.deleteSelectedItem(`/system/master/code/detail/delete`, params);
    }
}

@observer
class Code extends BaseComponent {
    sRender() {
        return (
            <React.Fragment>
                <div className="ratio_h50">
                    <SearchTemplate>
                        <div className='search_item_btn'>
                            <SButton buttonName={this.l10n("M00011", "조회")} onClick={this.store.handleSearchClick} type={'search'} />
                        </div>
                        <SSelectBox id={"search.cdGrp"} codeGroup={"CD_GROUP"} addOption={this.l10n("M00322", "전체")} title={this.l10n("M00685", "코드그룹")}/>
                        <SInput id={"search.cdType"} enter={this.store.handleSearchClick} title={this.l10n("M01009", "코드/명")}/>
                    </SearchTemplate>

                    <ContentsTemplate shadow={true}>
                        <ContentsMiddleTemplate>
                            <SButton buttonName={this.l10n("M00983", "추가")} onClick={this.store.handleMasterAddClick} type={'add'} />
                            <SButton buttonName={this.l10n("M00004", "저장")} onClick={this.store.handleMasterSaveClick} type={'save'} />
                            <SButton buttonName={this.l10n("M00003", "삭제")} onClick={this.store.handleMasterDeleteClick} type={'delete'} />
                        </ContentsMiddleTemplate>

                        <ContentsTemplate>
                            <SGrid grid={'masterGrid'} gridApiCallBack={this.store.setMasterGridApi} onSelectionChanged={this.store.handleMasterRowSelect} editable={true} />
                        </ContentsTemplate>
                    </ContentsTemplate>
                </div>

                <div className="ratio_h50 shadow">
                    <ContentsMiddleTemplate>
                        <SButton buttonName={this.l10n("M00983", "추가")} onClick={this.store.handleDetailAddClick} type={'add'} />
                        <SButton buttonName={this.l10n("M00004", "저장")} onClick={this.store.handleDetailSaveClick} type={'save'} />
                        <SButton buttonName={this.l10n("M00003", "삭제")} onClick={this.store.handleDetailDeleteClick} type={'delete'} />
                    </ContentsMiddleTemplate>

                    <ContentsTemplate>
                        <SGrid grid={'detailGrid'} gridApiCallBack={this.store.setDetailGridApi} editable={true} />
                    </ContentsTemplate>
                </div>
            </React.Fragment>
        )
    }
}
import { observable } from 'mobx';
import { BaseModel, essential, name } from 'utils/base/BaseModel'

export default class CodeModel extends BaseModel {
    @observable cdGrp = "";
    @observable cdType = "";
    useYn = "";
}
import React from 'react';
import { observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import { BaseComponent } from 'utils/base/BaseComponent'
import { BaseStore, model } from 'utils/base/BaseStore'

import ContentsTemplate from 'components/template/ContentsTemplate';
import ContentsMiddleTemplate from 'components/template/ContentsMiddleTemplate';
import SearchTemplate from 'components/template/SearchTemplate';
import SGrid from 'components/override/grid/SGrid';
import GridUtil from 'components/override/grid/utils/GridUtil'
import SButton from 'components/atoms/button/SButton';
import SInput from 'components/atoms/input/SInput';
import Alert from 'components/override/alert/Alert';

import SCSysMngLegacyBatchQuartzModel from 'modules/pages/system/schedule/quartz/SCSysMngLegacyBatchQuartzModel';
import SSelectBox from 'components/atoms/selectbox/SSelectBox';

export { SCSysMngLegacyBatchQuartz, SCSysMngLegacyBatchQuartzStore }

class SCSysMngLegacyBatchQuartzStore extends BaseStore {

    @observable @model master = new SCSysMngLegacyBatchQuartzModel();

    loaded = async () => {
        await this.handleSearch();
    }

    setGridApi = async (grid) => {
        this.SGrid1 = grid;

        const columnDefs = [
            { name: "Job 이름", field: "jobName", width: 100, align: "center" }
            , { name: "Job 그룹", field: "jobGroup", width: 100, align: "center" }
            , { name: "Job Class 이름", field: "jobClassName", width: 450, key: true }
            , { name: "Cron Expression", field: "cronExpression", width: 150, editable: true }
            , { name: "Trigger 상태", field: "triggerState", width: 80, align: "center", editable: true }
            , { name: "Time Zone", field: "timeZoneId", width: 100, align: "center" }
            , { name: "Trigger 이름", field: "triggerName", width: 100, align: "center" }
            , { name: "Trigger 그룹", field: "triggerGroup", width: 100, align: "center" }
            , { name: "비고", field: "description", width: 100 }
            , { name: "다음 시작 시간", field: "nextFireTimeConvert", width: 130, align: "center", type: "dateTimeFull" }
            , { name: "이전 시작 시간", field: "prevFireTimeConvert", width: 130, align: "center", type: "dateTimeFull" }
            , { name: "생성시간", field: "startTimeConvert", width: 130, align: "center", type: "dateTimeFull" }
        ];

        // this.setNewRowEditable(columnDefs, ["jobGroup", "jobClassName", "description"]);
        // this.setCellStyle(columnDefs, ["jobGroup", "jobClassName","cronExpression","description"], this.cellStyle);
        this.SGrid1.setColumnDefs(columnDefs);
    }

    cellStyle = (params) => {
        const { field } = params.colDef;
        const { data } = params;
        if (field == "jobGroup" && !data.jobName) { return { background: "#FFFFE0" } }
        if (field == "jobClassName" && !data.jobName) { return { background: "#FFFFE0" } }
        if (field == "description" && !data.jobName) { return { background: "#FFFFE0" } }
        if (field == "cronExpression") { return { background: "#FFFFE0" } }
    }

    // 등록된 schedule 목록 조회
    handleSearch = async () => {
        const params = this.master;
        console.log("!!! list params : ", params);
        const result = await this.quartz(`/job/list`, { param: params });
        if (result.success) {
            this.SGrid1.setRowData(result.data);
        }
    }

    // Job Row 추가
    handleAdd = () => {
        this.SGrid1.addRow(null, "firstIndex");
    }

    // Job 저장
    handleSave = async () => {

        if (await this.SGrid1.validation()) {

            let result;
            const saveRow = GridUtil.getGridSaveArray(this.SGrid1.gridApi);

            if (!saveRow || saveRow.length <= 0) {
                await Alert.meg(this.l10n("M01323", "저장할 정보가 없습니다."));
                return;
            }

            const confirmResult = await Alert.confirm(this.l10n("M01490", "저장하시겠습니까?"));
            if (!confirmResult) {
                return;
            }

            if (saveRow && saveRow.length > 0) {
                for (let i = 0; i < saveRow.length; i++) {
                    if (saveRow[i].newRow) {
                        // Job 등록 및 실행
                        const params = saveRow[i];
                        console.log("!!! add params : ", params);
                        result = await this.quartz(`/job/add`, { param: params });
                    }
                    else {
                        // 등록된 Job cron 변경
                        const params = saveRow[i];
                        console.log("!!! cron params : ", params);
                        if (!params.triggerName || !params.triggerGroup) {
                            await Alert.meg(this.l10n("M03417", "Trigger 이름/그룹 값이 없습니다."));
                            return;
                        }
                        result = await this.quartz(`/job/cron`, { param: params });
                    }
                }

                if (result.success) {
                    this.handleSearch();
                }
            }
            else {
                await Alert.meg(this.l10n("M01323", "저장할 정보가 없습니다."));
            }
        }
    }

    // Job 삭제
    handleDel = async () => {
        const selectedRow = this.SGrid1.getSelectedRow();
        if (!selectedRow) {
            await Alert.meg(this.l10n("M01622", "선택된 항목이 없습니다."));
            return;
        }

        const confirmResult = await Alert.confirm(this.l10n("M01492", "삭제하시겠습니까?"));
        if (!confirmResult) {
            return;
        }
        if (await this.SGrid1.validation()) {
            const params = selectedRow;
            console.log("!!! delete params : ", params);
            const result = await this.quartz(`/job/delete`, { param: params });
            if (result.success) {
                this.handleSearch();
            }
        }
    }

    // 등록된 Job 중지
    handlePause = async () => {
        const selectedRow = this.SGrid1.getSelectedRow();
        if (!selectedRow) {
            await Alert.meg(this.l10n("M03480", "선택된 항목이 없습니다."));
            return;
        }
        if (selectedRow.triggerState == "PAUSED") {
            await Alert.meg(this.l10n("M03479", "중지된 Job 입니다."));
            return;
        }
        const confirmResult = await Alert.confirm(this.l10n("M03480", "중지하시겠습니까?"));
        if (!confirmResult) {
            return;
        }

        if (await this.SGrid1.validation()) {
            const params = selectedRow;
            console.log("!!! pause params : ", params);
            const result = await this.quartz(`/job/pause`, { param: params });
            if (result.success) {
                this.handleSearch();
            }
        }
    }

    // 중지된 Job 재시작
    handleResume = async () => {
        const selectedRow = this.SGrid1.getSelectedRow();
        if (!selectedRow) {
            await Alert.meg(this.l10n("M03477", "선택된 항목이 없습니다."));
            return;
        }
        if (selectedRow.triggerState != "PAUSED") {
            await Alert.meg(this.l10n("M03476", "중지된 Job 이 아닙니다"));
            return;
        }

        const confirmResult = await Alert.confirm(this.l10n("M03478", "재시작 하시겠습니까?"));
        if (!confirmResult) {
            return;
        }

        if (await this.SGrid1.validation()) {
            const params = selectedRow;
            console.log("!!! resume params : ", params);
            const result = await this.quartz(`/job/resume`, { param: params });
            if (result.success) {
                this.handleSearch();
            }
        }
    }

    handleTest = async () => {
        const selectedRow = this.SGrid1.getSelectedRow();
        const params = selectedRow;
        console.log("!!! resume params : ", params);
        const result = await this.quartz(`/job/test`, { param: params });
        if (result.success) {
            this.handleSearch();
        }
    }
}

@observer
class SCSysMngLegacyBatchQuartz extends BaseComponent {

    sRender() {
        return (
            <React.Fragment>
                <SearchTemplate>
                    <div className='search_item_btn'>
                        <SButton buttonName={this.l10n("M00011", "조회")} onClick={this.store.handleSearch} type={'search'} />
                        <SButton buttonName={"테스트"} onClick={this.store.handleTest} type={'test'} />
                    </div>
                    <div className='search_line'>
                        <label className="item_lb w90">{this.l10n("M03470", "Job 이름")}</label>
                        <SInput id={"master.jobName"} enter={this.store.handleSearch} width={244} />

                    </div> 
                </SearchTemplate>
                <ContentsMiddleTemplate subTitle={this.l10n("M03482", "배치 정보")}>
                    <SButton buttonName={this.l10n("M00983", "추가")} onClick={this.store.handleAdd} type={'add'} />
                    <SButton buttonName={this.l10n("M00004", "저장")} onClick={this.store.handleSave} type={'save'} />
                    <SButton buttonName={this.l10n("M03469", "중지")} onClick={this.store.handlePause} type={'pause'} />
                    <SButton buttonName={this.l10n("M03468", "재실행")} onClick={this.store.handleResume} type={'resume'} />
                    <SButton buttonName={this.l10n("M00003", "삭제")} onClick={this.store.handleDel} type={'delete'} />
                </ContentsMiddleTemplate>
                <ContentsTemplate shadow={true}>
                    <SGrid grid={"SGrid1"} gridApiCallBack={this.store.setGridApi} editable={false} />
                </ContentsTemplate>
            </React.Fragment>
        )
    }
}
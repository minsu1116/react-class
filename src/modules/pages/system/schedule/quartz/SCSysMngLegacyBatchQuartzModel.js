import { observable } from 'mobx';
import { BaseModel, essential, name } from 'utils/base/BaseModel'
import moment from 'moment';

export default class SCSysMngLegacyBatchQuartzModel extends BaseModel {

    @observable jobName;
    @observable @name("M03371","Job 그룹") jobGroup;
    @observable @name("M03372","Job Class 이름") jobClassName;
    @observable @name("M03373","Cron Expression") triggerName;
    @observable triggerGroup;
    @observable repeatInterval;
    @observable timesTriggered;
    @observable cronExpression;
    @observable timeZoneId;
    @observable triggerState;
    @observable description;
    @observable nextFireTime;
    @observable prevFireTime;
    @observable startTime;
    
    @observable nextFireTimeConvert;
    @observable prevFireTimeConvert;
    @observable startTimeConvert;

    @observable jobDescription;
}
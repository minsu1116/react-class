import React from 'react';
import { observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import SInput from 'components/atoms/input/SInput';
import ContentsTemplate from 'components/template/ContentsTemplate';
import SGrid from 'components/override/grid/SGrid';
import SButton from 'components/atoms/button/SButton';
import { BaseComponent } from 'utils/base/BaseComponent'
import Alert from 'components/override/alert/Alert'
import { BaseStore, gridTrigger, model } from 'utils/base/BaseStore'
import ModalManage from 'utils/base/ModalManage';
import ContentsMiddleTemplate from 'components/template/ContentsMiddleTemplate';
import UserHelperModel from 'modules/pages/common/codehelper/user/UserHelperModel';
import SysMngAuthModel from 'modules/pages/system/mng/auth/SysMngAuthModel';
import STree from 'components/override/tree/STree';

export { SysMngAuth, SysMngAuthStore }

class SysMngAuthStore extends BaseStore {

    @observable @model master = new SysMngAuthModel();
    @observable @model detail = new SysMngAuthModel();
    @observable treeData = [];
    @observable expandedTreeKeys = [];
    @observable checkedTreeKeys = [];
    // @observable ylist = [];
    
    loaded = async() => {
        await this.hendleSearch();
    }

    //그리드 SET
    setAuthGridApi = async (grid) => {
        this.authGrid = grid;

        const columnDefs = [
            { word: "M01025", name: "권한ID", field: "grpId", width: 100, key: true, essential: true }
            , { word: "M01026", name: "권한명", field: "grpNm", width: 120, essential: true }
            , { word: "M00517", name: "설명", field: "grpDesc", width: 200 }

        ];

        this.authGrid.setColumnDefs(columnDefs);
    }

    // setDeptGridApi = async (grid) => {
    //     this.deptGrid = grid;

    //     const columnDefs = [
    //         { word: "M00233", name: "선택", field: "authYn", width: 30, align: 'center', type: 'checkbox', editable: false }
    //         , { word: "", name: "부서", field: "deptCd", width: 100, align: 'center', editable: false }
    //         , { word: "", name: "부서명", field: "deptNm", width: 150, editable: false }

    //     ];

    //     this.deptGrid.setColumnDefs(columnDefs);
    // }

    setUserGridApi = async (grid) => {
        this.userGrid = grid;

        const columnDefs = [
            { name: "", field: "check", type: 'checkboxAll', editable: false }
            , { word: "M01029", name: "계정명", field: "userId", width: 90, align: 'center', editable: false, key: true }
            , { word: "M00979", name: "이름", field: "userNm", width: 100, editable: false }
        ];

        this.userGrid.setColumnDefs(columnDefs);
    }

    setMenuGridApi = async (grid) => {
        this.menuGrid = grid;

        const columnDefs = [
            { word: "M00233", name: "선택", field: "authYn", width: 50, align: 'center', type: 'checkbox', editable: false }
            , { word: "M00997", name: "메뉴ID ", field: "menuId", width: 80, align: 'center', editable: false }
            , { word: "M00669", name: "메뉴명", field: "menuNm", width: 500, editable: false }
            , { word: "M00666", name: "버튼명", field: "btnList", width: 250, editable: false }
            , { word: "M01030", name: "권한버튼명", field: "btnAuthList", width: 250, editable: false }//권한등록(M01032)
            , { word: "M01031", name: "버튼권한", field: "grpMenuId", width: 100, editable: false, type: 'button', buttonName: this.l10n("M01032", "권한등록"), onButtonClick: this.handleAddAuthBtn }

        ];

        //메뉴ID 없는건 상위 메뉴ID와 같기 떄문에 check박스는 맨위 메뉴만 나옴
        function authYnVisible(params) {
            if (params.data.menuId == "") {
                return { visible: false };
            } else {
                return { visible: true };
            }
        }

        //grpMenuId가 0이면 해당 메뉴 권한이 없기때문에 버튼X
        function authBtnVisible(params) {
            if (params.data.grpMenuId == 0) {
                return { visible: false };
            } else {
                return { visible: true };
            }
        }

        this.setOption(columnDefs, "authYn", gridTrigger.rendererVisible, authYnVisible);
        this.setOption(columnDefs, "grpMenuId", gridTrigger.rendererVisible, authBtnVisible);

        this.menuGrid.setColumnDefs(columnDefs);

    }

    /* ----------------------------------- 권한 영역 --------------------------------- */

    //권한 조회
    hendleSearch = async () => {
        const param = toJS(this.master);
        await this.authGrid.search('/system/mng/auth/getauth', param, true);
    }

    //권한 추가
    handleAddAuth = () => {
        this.authGrid.addRow();
    }

    //권한 저장
    handleSaveAuth = async () => {
        // const param = toJS(this.master);
        const param = { grpId: this.authGrid.getSelectedRow().grpId };
        if (await this.authGrid.validation()) {
            await this.authGrid.saveEditAll(`/system/mng/auth/saveauth`, param);
        }
    }

    //권한 삭제
    handleDelAuth = async () => {
        const param = toJS(this.master);
        await this.authGrid.deleteSelectedItem(`/system/mng/auth/deleteauth`, param);
    }

    //권한 선택
    handleAeuthSelect = async () => {
        const param = this.authGrid.getSelectedRow();
        //excute 로 다 불러와서 setRow 할거임
        if (param) {
            //행추가는 검색 X
            if (param.newRow) {
                //GRID 초기화 (권한 GRID 제외)
                this.userGrid.clear();
                this.menuGrid.clear();
                return;
            }

            const authData = await this.execute('/system/mng/auth/getauthall', param);

            let authList = "";

            //deptList
            this.treeData = authData.data[0];
            authData.data[0].forEach(e => {
                if(e.authYn == "Y") {
                    authList += e.deptCd + ",";
                    // ylist.push(e.deptCd);
                }
            })
            authList = authList.slice(0, authList.length -1);
            const authCheck = authList.split(",");
            
            this.expandedTreeKeys = [this.treeData[0].deptCd];
            this.checkedTreeKeys = authCheck;

            //userList
            this.userGrid.setRowData(authData.data[1]);
            //menuList
            this.menuGrid.setRowData(authData.data[2]);

        }
    }

    /* ------------------------------------- END ---------------------------------------- */

    /* ---------------------------------- 부서 영역 ------------------------------------ */

    // 부서 저장
    hendleSaveDept = async () => {
        const param1 = [];
        const param2 = this.authGrid.getSelectedRow();

        this.treeData.forEach(e => {
            if(e.edit) {
                param1.push(e)
            }
        })

        if(param1.length > 0) {
            const params = JSON.stringify({param1: param1, param2: param2});
            const result = await this.save(`/system/mng/auth/saveauthdept`, params);
            
        } else {
            await Alert.meg("저장할 데이터가 없습니다.");
            return;
        }

    }

    handleTreeCheck = async(keys, info) => {
        const params = this.treeData;
        this.childrenCheck(params, info.node, info.checked)
    }

    childrenCheck = (data, info, checked) => {
        
        const children = data.filter(x => x["parentDeptCd"] == info.key);
        const childrenYn = data.some(x => x["parentDeptCd"] == info.key);
        const yn = checked ? "Y" : "N";
        
        //자식이 있으면
        if(children.length > 0) {
            children.map(x => {
                const temp = { key: x["deptCd"], authYn: yn, edit: true };
                const child = Object.assign(temp, child);
                
                if(childrenYn) {
                    this.childrenCheck(data, child, checked);
                }
                //자식과 부모 edit -> true
                this.treeData.map(e => {
                    if(e.deptCd == info.key || e.deptCd == x.deptCd ) {
                        e.authYn = yn;
                        e.edit = true
                    }
                })
            })

        } else {
            this.treeData.map(e => {
                if(e.deptCd == info.deptCd) {
                    e.authYn = yn;
                    e.edit = true
                }
            })
        }

    }

    /* ------------------------------------- END ---------------------------------------- */

    

    /* ---------------------------------- 유저 영역 ------------------------------------ */

    //유저 헬퍼
    handleAddUser = async () => {

        const userDate = this.userGrid.getRows();
        userDate.map( x=> {
            x.empNo = x.userId;
        })

        const userHelper = new UserHelperModel();
        userHelper.multi = true;
        // userHelper.compCd = this.authGrid.getSelectedRow().compCd;
        userHelper.excludeData = userDate;


        const result = await this.showHelper(userHelper);

        if (result) {
            result.map(item => { item.newRow = true });
            const param = this.authGrid.getSelectedRow();
            const params = JSON.stringify({ param1: result, param2: param });

            const result2 = await this.save(`/system/mng/auth/saveauthuser`, params);
            if(result2) {
                this.userGrid.setRowData(result2.data);
            }
        }
    }

    //유저 삭제
    handleDelUser = async () => {
        const param = this.authGrid.getSelectedRow();
        await this.userGrid.deleteCheckedItems(`/system/mng/auth/saveauthuser`, param);
    }

    /* ------------------------------------- END ---------------------------------------- */

    /* ---------------------------------- 메뉴 영역 ------------------------------------ */

    //메뉴 조회
    handleSearchMenu = async () => {
        const param = toJS(this.master);
        if (this.authGrid.getSelectedRow()) {
            param.grpId = this.authGrid.getSelectedRow().grpId;
            param.compCd = this.authGrid.getSelectedRow().compCd;
        }
        await this.menuGrid.search('/system/mng/auth/getauthmenu', param);
    }

    //메뉴 저장
    handleSaveMenu = async () => {
        const param = toJS(this.master);
        if (this.authGrid.getSelectedRow()) {
            param.grpId = this.authGrid.getSelectedRow().grpId;
            param.compCd = this.authGrid.getSelectedRow().compCd;
            await this.menuGrid.saveEditAll(`/system/mng/auth/saveauthmenu`, param);
        } else {
            await Alert.meg("선택된 권한이 없습니다."); //삭제될 수도 있음
            return;
        }
    }

    //메뉴 버튼권한 추가
    handleAddAuthBtn = async () => {
        this.modalOpenAuthBtn({ masterP: this.menuGrid.getSelectedRow() });
    }

    //메뉴 버튼권한 팝업
    modalOpenAuthBtn = async (params) => {

        const modal = new ModalManage();
        modal.id = "system_mng_auth_btn_p";
        modal.path = "/system/mng/auth/popup/SysMngAuthBtnP";
        modal.title = this.l10n("M01033"); //"버튼권한 등록"
        modal.parameter = params;
        modal.width = 400;

        const result = await this.showModal(modal);

        if (result) {

            //코드 수정 될 수 있음
            //저장후 조회 값
            const param = this.authGrid.getSelectedRow();
            const params = JSON.stringify({ param1: result, param2: param });

            //버튼 저장후 포커스 현상유지를 위해 인덱스 값 선언
            const index = this.menuGrid.gridApi.getSelectedNodes()[0].rowIndex;

            //저장후 조회시 포커스가 첫행으로 가지 않기 위해 false
            await this.menuGrid.search(`/system/mng/auth/saveauthbtn`, params, false);

            //저장후 포커스 현상유지
            this.menuGrid.setFocusByRowIndex(index);

        }

    }

    /* ------------------------------------- END ---------------------------------------- */
}

@observer
class SysMngAuth extends BaseComponent {

    sRender() {
        
        return (
            <div className="basic">

                <React.Fragment>
                    <ContentsTemplate id={"contents"}>

                        <div className="ratio_h30 ratio_w100">
                            <div className="ratio_w33 ratio_h100 shadow">
                                <ContentsMiddleTemplate subTitle={this.l10n("M01034", "권한")}>
                                    <SButton buttonName={this.l10n("M00983", "추가")} onClick={this.store.handleAddAuth} type={'add'} />
                                    {/* <SButton buttonName={'복사'} onClick={this.store.handleCopyAuth} type={'default'} /> */}
                                    <SButton buttonName={this.l10n("M00004", "저장")} onClick={this.store.handleSaveAuth} type={'save'} />
                                    <SButton buttonName={this.l10n("M00003", "삭제")} onClick={this.store.handleDelAuth} type={'dlelte'} />
                                </ContentsMiddleTemplate>
                                <ContentsTemplate id={"authContents"}>
                                    <div className="panel_body" style={{ height: '100%' }}>
                                        <SGrid grid={'authGrid'} gridApiCallBack={this.store.setAuthGridApi} onSelectionChanged={this.store.handleAeuthSelect} />
                                    </div>
                                </ContentsTemplate>
                            </div>
                            <div className="ratio_w33 ratio_h100">
                                <div className="ratio_w100 ratio_h100 shadow">
                                    <ContentsTemplate noMargin={true}>
                                        <ContentsMiddleTemplate subTitle={"부서"}>
                                            <SButton buttonName={this.l10n("M00004", "저장")} onClick={this.store.hendleSaveDept} type={'save'} />
                                        </ContentsMiddleTemplate>
                                        <ContentsTemplate>
                                            <STree
                                                treeData={this.store.treeData}
                                                keyMember={"deptCd"}
                                                pIdMember={"parentDeptCd"}
                                                titleMember={"deptNm"}
                                                onCheck={this.store.handleTreeCheck}
                                                // checkedKeysId={"defaultCheckedKeys"}
                                                // onSelect={this.store.handleTreeSelect}
                                                checkable={true}
                                                // selectedKeyId={"selectedTreeKey"}
                                                expandedKeysId={"expandedTreeKeys"}
                                                checkedKeysId={"checkedTreeKeys"}
                                                border={true}
                                            />
                                        </ContentsTemplate>
                                    </ContentsTemplate>
                                </div>
                            </div>
                            <div className="ratio_w33 ratio_h100 shadow">
                                <ContentsMiddleTemplate subTitle={this.l10n("M00318", "사용자")}>
                                    <SButton buttonName={this.l10n("M00983", "추가")} onClick={this.store.handleAddUser} type={'add'} />
                                    <SButton buttonName={this.l10n("M00003", "삭제")} onClick={this.store.handleDelUser} type={'delete'} />
                                </ContentsMiddleTemplate>
                                <ContentsTemplate id={"menuContentsC"}>
                                    <div className="panel_body" style={{ height: '100%' }}>
                                        <SGrid grid={'itemGrid'} gridApiCallBack={this.store.setUserGridApi} suppressRowClickSelection={true} />
                                    </div>
                                </ContentsTemplate>
                            </div>
                        </div>
                        <div className="ratio_h70 ratio_w100 shadow">
                            <ContentsMiddleTemplate subTitle={this.l10n("M00668", "메뉴")}>
                                <ul style={{ float: 'right' }}>
                                    <SButton buttonName={this.l10n("M00011", "조회")} onClick={this.store.handleSearchMenu} type={'search'} />
                                    <SButton buttonName={this.l10n("M00004", "저장")} onClick={this.store.handleSaveMenu} type={'save'} />
                                </ul>
                                <ul style={{ float: 'right' }}>
                                    <SInput id={"master.menuId"} enter={this.store.handleSearchMenu} placeholderValue={this.l10n("M01035", "메뉴명/ID")} />
                                </ul>
                            </ContentsMiddleTemplate>
                            <ContentsTemplate id={"menuContents"}>
                                <div className="panel_body" style={{ height: '100%' }}>
                                    <SGrid grid={'menuGrid'} gridApiCallBack={this.store.setMenuGridApi} />
                                </div>
                            </ContentsTemplate>
                        </div>

                    </ContentsTemplate>
                </React.Fragment>
            </div>
        )
    }
}
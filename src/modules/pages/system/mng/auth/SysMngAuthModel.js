import { observable } from 'mobx';
import { BaseModel, essential } from "utils/base/BaseModel";

export default class AuthModel extends BaseModel {
    @observable @essential("권한ID") grpId = "";
    @observable @essential("권한명") grpNm = "";
    @observable menuId = "";
    @observable formId = "";
    @observable grpMenuId = 0;
}
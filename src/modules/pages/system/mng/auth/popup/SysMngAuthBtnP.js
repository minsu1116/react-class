import React from 'react';
import { BaseStore, model } from "utils/base/BaseStore";
import { observer } from "mobx-react";
import { BaseComponent } from "utils/base/BaseComponent";
import { observable, toJS } from "mobx";
import ContentsMiddleTemplate from 'components/template/ContentsMiddleTemplate';
import ContentsTemplate from 'components/template/ContentsTemplate';
import SGrid from 'components/override/grid/SGrid';
import SButton from 'components/atoms/button/SButton';
import Alert from 'components/override/alert/Alert';
import SysMngAuthModel from 'modules/pages/system/mng/auth/SysMngAuthModel';

export { SysMngAuthBtnP, SysMngAuthBtnPStore }

class SysMngAuthBtnPStore extends BaseStore {

    @observable @model masterP = new SysMngAuthModel();

    //그리드 SET
    setAuthBtnGridApi = async (grid) => {
        this.authBtnGrid = grid;

        const columnDefs = [
            { word: "", name: "", field: "authYn", width: 50, align: 'center', type: "checkboxAll", editable: false }
            , { word: "M00665", name: "버튼ID", field: "btnId", width: 100, editable: false }
            , { word: "M00666", name: "버튼명", field: "btnNm", width: 150, editable: false }

        ];

        this.authBtnGrid.setColumnDefs(columnDefs);
        this.getAuthBtn();
    }

    //버튼 조회
    getAuthBtn = async () => {
        const param = toJS(this.masterP);
        await this.authBtnGrid.search('/system/mng/auth/getauthbtn', param);
    }

    //버튼 선택
    handleChoiceClick = async () => {
        
        const choices = this.authBtnGrid.getRows();
        
        if (!choices || choices.length <= 0) {
            await Alert.meg("저장할 항목이 없습니다.");
            return false;
        }

        this.close(choices);
    }

}

@observer
export default class SysMngAuthBtnP extends BaseComponent {

    sRender() {
        return (
            <div className="basic">
                <React.Fragment>

                    <ContentsMiddleTemplate>
                        <SButton buttonName={this.l10n("M00004", "저장")} onClick={this.store.handleChoiceClick} type={'save'} />
                    </ContentsMiddleTemplate>

                    <ContentsTemplate id={"authBtnContents"} height={"250px"}>
                        <SGrid grid={'authBtnGrid'} gridApiCallBack={this.store.setAuthBtnGridApi} editable={false} />
                    </ContentsTemplate>
                </React.Fragment>
            </div>
        )
    }

}
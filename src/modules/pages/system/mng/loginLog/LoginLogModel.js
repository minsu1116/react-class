import { BaseModel } from "utils/base/BaseModel";
import { observable } from "mobx";
import moment from "moment";

export default class loginLogModel extends BaseModel {
    @observable empNo = '';
    @observable empNm;
    @observable searchDateFrom = moment().add(-7,'days').format("YYYYMMDD");
    @observable searchDateTo = moment().format("YYYYMMDD");
}
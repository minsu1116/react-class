import React from 'react';
import { BaseStore, model } from "utils/base/BaseStore";
import { inject, observer } from "mobx-react";
import { BaseComponent } from "utils/base/BaseComponent";

import SearchTemplate from 'components/template/SearchTemplate';
import ContentsTemplate from 'components/template/ContentsTemplate';

import SDatePicker from 'components/override/datepicker/SDatePicker';
import CodeHelper from 'modules/pages/common/codehelper/CodeHelper';
import SInput from 'components/atoms/input/SInput';
import SButton from 'components/atoms/button/SButton';
import SGrid from 'components/override/grid/SGrid';


import LoginLogModel from 'modules/pages/system/mng/loginLog/LoginLogModel';
import { observable, toJS } from 'mobx';


export { LoginLog, LoginLogStore }

class LoginLogStore extends BaseStore {

    @observable @model master = new LoginLogModel();

    setLoginLogGridApi = async (grid) => {
        this.logGrid = grid;

        const columnDefs = [
              { word: "M02437", name: "회사명", field: "compNm", width: 110}
            , { word: "M00677", name: "부서명", field: "deptNm", width: 110}
            , { word: "M00674", name: "사번", field: "empNo", width: 100, align: 'center', hide: true}
            , { word: "M00979", name: "이름", field: "empNm", width: 80, align: 'center'}
            , { word: "M02438", name: "직급", field: "rankNm", width: 80, align: 'center'}
            , { word: "M00679", name: "전화번호", field: "officeNo", width: 100, align: 'center'}
            , { word: "IP", field: "ip", width: 120, align: 'center'}
            , { word: "M02439", name: "성공여부", field: "successYn", width: 80, align: 'center'}
            , { word: "M00572", name: "요청일", field: "requestDtm", width: 150, type:"dateTimeFull" }
        ];

        this.logGrid.setColumnDefs(columnDefs);
    }

    //프로그램 조회
    handleSearchClick = async () => {
        const param = toJS(this.master);
        await this.logGrid.search(`/system/mng/log/login/getloginlog`, param);

        
    }
}

@observer
class LoginLog extends BaseComponent {
    sRender() {
        return (
            <div className="basic">
                <React.Fragment>
                    <SearchTemplate>
                        <div className='search_item_btn'>
                            <SButton buttonName={this.l10n("M00011", "조회")} onClick={this.store.handleSearchClick} type={'search'} />
                        </div>
                        <SDatePicker id={"master.searchDateFrom"} title={this.l10n("M02440", "로그인 일자")} />
                        <SDatePicker id={"master.searchDateTo"} title={"~"} />
                        <CodeHelper helperCode={"master.empNo"} business={"user"} mainStore={this.store} title={this.l10n("M01191", "사번/이름")} codeVisible={false}/>
                    </SearchTemplate>
                    
                    <ContentsTemplate id={"loginLogContents"} shadow={true}>
                        <SGrid grid={'logGrid'} gridApiCallBack={this.store.setLoginLogGridApi} editable={false} />
                    </ContentsTemplate>
                </React.Fragment>
            </div>
        )
    }
    
}
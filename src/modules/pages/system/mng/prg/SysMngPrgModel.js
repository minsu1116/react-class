import { observable } from 'mobx';
import { BaseModel, essential } from "utils/base/BaseModel";

export default class PrgModel extends BaseModel {

    @observable @essential("M00988", "프로그램ID") formId = "";
    @observable @essential("M00989", "프로그램명") formNm = "";
    @observable formType = "";
    @observable btnNo = 0;
    

}
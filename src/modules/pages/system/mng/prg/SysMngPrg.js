import React from 'react';
import { observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import SearchTemplate from 'components/template/SearchTemplate';
import SInput from 'components/atoms/input/SInput';
import ContentsTemplate from 'components/template/ContentsTemplate';
import SGrid from 'components/override/grid/SGrid';
import SButton from 'components/atoms/button/SButton';
import SSelectBox from 'components/atoms/selectbox/SSelectBox'
import { BaseComponent } from 'utils/base/BaseComponent'
import Alert from 'components/override/alert/Alert';
import { BaseStore, model } from 'utils/base/BaseStore'
import ContentsMiddleTemplate from 'components/template/ContentsMiddleTemplate';
import SysMngPrgModel from 'modules/pages/system/mng/prg/SysMngPrgModel';

export { SysMngPrg, SysMngPrgStore }

class SysMngPrgStore extends BaseStore {

    @observable @model master = new SysMngPrgModel();
    @observable formType = [];

    initialize = () => {
        this.formType = [{ cd: "WEB", cdNm: "WEB" }]
    }

    setGridApi = async (grid) => {
        this.grid = grid;

        const columnDefs = [
            { word: "M00988", name: "프로그램ID", field: "formId", width: 250, key: true, essential: true }
            , { word: "M00989", name: "프로그램명", field: "formNm", width: 250, essential: true }
            , { word: "M00990", name: "프로그램 설명", field: "formDesc", width: 200 }
            , { word: "M00991", name: "프로그램 주소", field: "formUrl", width: 250, essential: true }
            , { word: "M00992", name: "프로그램 타입", field: "formType", width: 90, align: 'center', default: "WEB", optionGroup: this.formType, valueMember: "cd", displayMember: "cdNm" }
            , { word: "M00993", name: "프로그램 클래스", field: "formClass", width: 200, essential: true }
        ];

        this.grid.setColumnDefs(columnDefs);
    }

    setGridApi2 = async (grid) => {
        this.grid2 = grid;

        const columnDefs = [
            { word: "M00988", name: "프로그램ID", field: "formId", width: 250, editable: false, essential: true }
            , { word: "M00665", name: "버튼ID", field: "btnId", width: 150, align: 'center', essential: true }
            , { word: "M00666", name: "버튼명", field: "btnNm", width: 200, essential: true }
            , { word: "M00094", name: "비고", field: "remark", width: 200 }
        ];

        this.grid2.setColumnDefs(columnDefs);
    }

    //프로그램 조회
    handleSearchClick = async () => {
        const param = toJS(this.master);
        await this.grid.search(`/system/mng/prg/getprg`, param, true);
    }

    //프로그램 추가
    programAddClick = () => {
        this.grid.addRow();
    }

    //프로그램 저장
    programSaveClick = async () => {
        const param = toJS(this.master);

        if (await this.grid.validation()) {
            await this.grid.saveEditAll(`/system/mng/prg/saveprg`, param);
        }
    }

    //프로그램 삭제
    programDelClick = async () => {
        const param = toJS(this.master);
        await this.grid.deleteSelectedItem(`/system/mng/prg/deleteprg`, param);
    }

    //버튼 조회
    getBtn = async () => {
        if (this.grid.getSelectedRow()) {
            const param = this.grid.getSelectedRow();
            await this.grid2.search(`/system/mng/prg/getprgbtn`, param);
        } else {
            this.grid2.clear();
        }
    }

    //버튼 추가
    btnAddClick = async () => {
        const param = this.grid.getSelectedRow();

        if (!param) {
            await Alert.meg(this.l10n("M01338", "선택된 정보가 없습니다."));
            return;
        }

        if (param.newRow) {
            await Alert.meg("프로그램 정보를 먼저 저장해 주세요");
            return;
        }

        const formId = param.formId;

        this.grid2.addRow({ formId: formId });
    }

    //버튼 저장
    btnSaveClick = async () => {
        const param = this.grid.getSelectedRow();

        if (param) {

            if (param.newRow) {
                await Alert.meg("프로그램 정보를 먼저 저장해 주세요");
                return;
            }

            await this.grid2.saveEditAll(`/system/mng/prg/saveprgbtn`, param);
        } else {
            await Alert.meg(this.l10n("M01338", "선택된 정보가 없습니다."));
            return;
        }
    }

    //버튼 삭제
    btnDelClick = async () => {
        const param = this.grid.getSelectedRow();

        if (param) {
            if (param.newRow) {
                await Alert.meg("프로그램 정보를 먼저 저장해 주세요");
                return;
            }

            await this.grid2.deleteSelectedItem(`/system/mng/prg/deleteprgbtn`, param);
        } else {
            await Alert.meg(this.l10n("M01338", "선택된 정보가 없습니다."));
            return;
        }
    }
}

@observer
class SysMngPrg extends BaseComponent {

    sRender() {
        return (
            <div className="basic">
                <React.Fragment>
                    <SearchTemplate>
                        <div className='search_item_btn'>
                            <SButton buttonName={this.l10n("M00011", "조회")} onClick={this.store.handleSearchClick} type={'search'} />
                        </div>
                        <SInput id={"master.formId"} enter={this.store.handleSearchClick} title={this.l10n("M00994", "프로그램ID/명")} />
                        <SSelectBox id={"master.formType"} optionGroup={this.store.formType} valueMember={"cd"} displayMember={"cdNm"} title={this.l10n("M00992", "프로그램 타입")} />
                    </SearchTemplate>

                    <ContentsTemplate>
                        <div className="ratio_h50 shadow">
                            <ContentsMiddleTemplate subTitle={this.l10n("M00995", "프로그램 정보")}>
                                <SButton buttonName={this.l10n("M00983", "추가")} onClick={this.store.programAddClick} type={'add'} />
                                <SButton buttonName={this.l10n("M00004", "저장")} onClick={this.store.programSaveClick} type={'save'} />
                                <SButton buttonName={this.l10n("M00003", "삭제")} onClick={this.store.programDelClick} type={'delete'} />
                            </ContentsMiddleTemplate>
                            <ContentsTemplate>
                                <SGrid grid={'grid'} gridApiCallBack={this.store.setGridApi} onSelectionChanged={this.store.getBtn} />
                            </ContentsTemplate>
                        </div>

                        <div className="ratio_h50 shadow">
                            <ContentsMiddleTemplate subTitle={this.l10n("M00996", "버튼 정보")}>
                                <SButton buttonName={this.l10n("M00983", "추가")} onClick={this.store.btnAddClick} type={'add'} />
                                <SButton buttonName={this.l10n("M00004", "저장")} onClick={this.store.btnSaveClick} type={'save'} />
                                <SButton buttonName={this.l10n("M00003", "삭제")} onClick={this.store.btnDelClick} type={'delete'} />
                            </ContentsMiddleTemplate>
                            <ContentsTemplate>
                                <SGrid grid={'grid2'} gridApiCallBack={this.store.setGridApi2} />
                            </ContentsTemplate>
                        </div>
                    </ContentsTemplate>
                </React.Fragment>
            </div>
        )
    }
}
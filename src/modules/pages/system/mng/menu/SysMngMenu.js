import React from 'react';
import { observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import SearchTemplate from 'components/template/SearchTemplate';
import SInput from 'components/atoms/input/SInput';
import ContentsTemplate from 'components/template/ContentsTemplate';
import SGrid from 'components/override/grid/SGrid';
import SButton from 'components/atoms/button/SButton';
import { BaseComponent } from 'utils/base/BaseComponent'
import Alert from 'components/override/alert/Alert'
import { BaseStore, model } from 'utils/base/BaseStore'
import ModalManage from 'utils/base/ModalManage';
import ContentsMiddleTemplate from 'components/template/ContentsMiddleTemplate';
import STree from 'components/override/tree/STree';
import SysMngMenuModel from 'modules/pages/system/mng/menu/SysMngMenuModel';

export { SysMngMenu, SysMngMenuStore }

class SysMngMenuStore extends BaseStore {

    @observable @model master = new SysMngMenuModel();
    @observable @model detail = new SysMngMenuModel();
    @observable treeData = [];
    @observable selectedTreeKey;
    @observable expandedTreeKeys;

    loaded = async () => {
        this.handleSearch();
    }

    setGridApi = async (grid) => {
        this.grid = grid;

        const columnDefs = [
            { word: "M00997", name: "메뉴ID", field: "menuId", width: 80, align: 'center', editable: false }
            , { word: "M00669", name: "메뉴명", field: "menuNm", width: 200, essential: true }
            , { word: "M01005", name: "다국어코드", field: "wordCd", width: 80, type: 'codeHelper', business: 'word', helperParams: { wordType: 'T' } }
            , { word: "M00999", name: "상위ID", field: "parentMenuId", width: 80, editable: false, essential: true }
            , { word: "M00988", name: "프로그램ID", field: "formId", width: 250, editable: false, type: "iconButton", buttonIcon: "icon-search", onButtonClick: this.buttonClick, suppressKeyboardEvent: this.keybordEvent }
            , { word: "M01000", name: "파라미터", field: "param", width: 80, align: 'center' }
            , { word: "M00282", name: "정렬순서", field: "sort", width: 80, align: 'center' }
            , { word: "M00670", name: "아이콘", field: "iconNm", width: 100, align: 'center' }
            , { word: "M01001", name: "하위여부", field: "childYn", width: 80, align: 'center', default: 'N', codeGroup: "USE_YN" }
            , { word: "M01002", name: "메뉴여부", field: "displayYn", width: 80, align: 'center', default: 'Y', codeGroup: "USE_YN" }
            , { word: "M00094", name: "비고", field: "menuDesc", width: 150, align: 'center' }
        ];

        this.grid.setColumnDefs(columnDefs);
    }

    keybordEvent = (params) => {
        if(params.event.which == 46 || params.event.which == 8) {
            this.grid.setDataByRowIndex({ formId: "" });
        }
    }

    setGridApi2 = async (grid) => {
        this.grid2 = grid;

        const columnDefs = [
            { word: "M00988", name: "프로그램ID", field: "formId", width: 200 }
            , { word: "M00989", name: "프로그램명", field: "formNm", width: 200 }
            , { word: "M01003", name: "메인여부", field: "mainYn", width: 80, align: 'center', cellRenderer: 'rowCellCheckbox' }
        ];

        this.grid2.setColumnDefs(columnDefs);
    }

    //메뉴 트리 데이터
    handleSearch = async (reSearch) => {

        if (reSearch != true) {
            reSearch = false;
        }

        const params = toJS(this.master);
        const data = await this.execute(`/system/mng/menu/getmenutree`, params);
        this.treeData = data.data;
        let menuIdList = [];
        data.data.forEach(x => {
            if (x.childYn == "Y") {
                menuIdList.push(x.menuId);
            }
        })

        if (!reSearch) {
            this.grid.clear();
            this.grid2.clear();
            this.selectedTreeKey = "0";
            this.expandedTreeKeys = menuIdList;
        }

    }

    //메뉴 트리 선택
    handleTreeSelect = async (key) => {

        const param = toJS(this.detail);
        param.menuId = key;

        //메뉴 추가시 선택한 트리 메뉴ID 필요
        this.detail.menuId = key;

        //트리 선택 여부
        if (key.length == 0) {
            this.grid.clear();
            this.grid2.clear();
            return;
        } else this.grid2.clear();

        await this.grid.search(`/system/mng/menu/getmenutree`, param);
    }

    //메뉴 프로그램 팝업 호출
    buttonClick = () => {
        this.modalOpenProgram("menu");
    }

    //메뉴 추가
    handleAddMenu = async () => {
        const param = toJS(this.detail);

        if (param.menuId == undefined || param.menuId == "") {
            await Alert.meg(this.l10n("M01338", "선택된 정보가 없습니다."));
            return;
        }

        let max = this.grid.max("sort");
        if (max == undefined) {
            max = 0;
        }

        this.grid.addRow({ parentMenuId: param.menuId, sort: max + 1 });
    }

    //메뉴 저장
    handleSaveMenu = async () => {
        const param = toJS(this.detail);

        if (await this.grid.validation()) {
            const result = await this.grid.saveEditAll(`/system/mng/menu/savemenu`, param);
            if (result) {
                //트리 재조회
                this.handleSearch(true);
            }
        }


    }

    //메뉴 삭제
    handleDelMenu = async () => {
        const param = toJS(this.detail); //트리선택할때 메뉴ID가 담겨있음

        //하위 메뉴가 있는지 확인
        const menuRow = this.grid.getSelectedRow();

        // if(childCheck) {
        //     if(childCheck.childCnt > 0) {
        //         // const test = await Alert.confirm("하위 메뉴가 존재합니다. 삭제하시겠습니까??");
        //         await Alert.meg("하위 메뉴가 존재합니다.");
        //     }
        // }

        const result = await this.grid.deleteSelectedItem(`/system/mng/menu/deletemenu`, param);

        if (result) {
            if (menuRow.newRow) return;
            //트리 재조회.
            this.handleSearch(true);
        }
    }

    //프로그램 조회
    handleMenuSelect = async () => {
        const param = this.grid.getSelectedRow();
        if (param) {
            //새로 행추가한 메뉴는 검색 되지 않도록
            if (param.newRow) {
                this.grid2.clear();
                return;
            }

            await this.grid2.search(`/system/mng/menu/getmenuprg`, param);
        } else {
            //프로그램 정보 초기화
            this.grid2.clear();
        }
    }

    //프로그램 추가
    handleAddProgram = async () => {

        const menuRow = this.grid.getSelectedRow();

        if (menuRow) {
            if (menuRow.newRow) {
                await Alert.meg(this.l10n("M02125", "메뉴를 먼저 저장해 주세요."));
                return;
            }
        } else {
            await Alert.meg(this.l10n("M01338", "선택된 정보가 없습니다."));
            return;
        }

        const programRows = this.grid2.getRows();
        this.modalOpenProgram("prg", { masterP: { programArr: programRows }, multi: true });
    }

    //프로그램 팝업
    modalOpenProgram = async (name, params) => {
        
        const modal = new ModalManage();
        modal.id = "system_mng_menu_prg_add_p";
        modal.path = "/system/mng/menu/popup/SysMngMenuPrgAddP";
        modal.title = this.l10n("M01004"); //프로그램 추가
        modal.parameter = params;
        modal.width = 500;

        const result = await this.showModal(modal);

        if (result) {
            if(name == "prg") {
                await this.grid2.gridApi.updateRowData({ add: result });
            } else if(name == "menu") {
                this.grid.setDataByRowIndex({ formId: result.formId });
            }
        }
    }

    //프로그램 저장
    handleSaveProgram = async () => {
        const param = toJS(this.detail);
        const menuRow = this.grid.getSelectedRow();

        if (menuRow) param.menuId = menuRow.menuId;

        //메인 여부 확인(1개만 체크되야함)
        const ynCheck = this.grid2.getRows();
        for (let i = 0; i < ynCheck.length; i++) {
            if (ynCheck[i].mainYn == "Y") {
                param.cnt++
                if (param.cnt > 1) {
                    await Alert.meg(this.l10n("M03366", "메인여부는 하나만 체크할 수 있습니다."));
                    return;
                }
            }
        }

        await this.grid2.saveEditAll(`/system/mng/menu/savemenuprg`, param);
    }

    //프로그램 삭제
    handleDelProgram = async () => {
        const param = toJS(this.detail);
        const menuRow = this.grid.getSelectedRow();

        if (menuRow) param.menuId = menuRow.menuId;

        await this.grid2.deleteSelectedItem(`/system/mng/menu/deletemenuprg`, param);
    }

}

@observer
class SysMngMenu extends BaseComponent {

    sRender() {
        return (
            <div className="basic">
                <React.Fragment>
                    <SearchTemplate>
                        <div className='search_item_btn'>
                            <SButton buttonName={this.l10n("M00011", "조회")} onClick={this.store.handleSearch} type={'search'} />
                        </div>
                        <SInput id={"master.menuNm"} enter={this.store.handleSearch} title={this.l10n("M00669", "메뉴명")} />
                    </SearchTemplate>

                    <ContentsTemplate id={"content"}>
                        <div className='ratio_w30 ratio_h100 shadow'>
                            <ContentsTemplate id={'menuTree'} noMargin={true}>
                                <STree
                                    treeData={this.store.treeData}
                                    keyMember={"menuId"}
                                    pIdMember={"parentMenuId"}
                                    titleMember={"menuNm"}
                                    onSelect={this.store.handleTreeSelect}
                                    selectedKeyId={"selectedTreeKey"}
                                    expandedKeysId={"expandedTreeKeys"}
                                    border={true}
                                />
                            </ContentsTemplate>
                        </div>

                        <div className='ratio_w70 ratio_h100'>
                            <div className="ratio_h50 shadow">
                                <ContentsMiddleTemplate subTitle={this.l10n("M00998", "메뉴 정보")}>
                                    <SButton buttonName={this.l10n("M00983", "추가")} onClick={this.store.handleAddMenu} type={'add'} />
                                    <SButton buttonName={this.l10n("M00004", "저장")} onClick={this.store.handleSaveMenu} type={'save'} />
                                    <SButton buttonName={this.l10n("M00003", "삭제")} onClick={this.store.handleDelMenu} type={'delete'} />
                                </ContentsMiddleTemplate>
                                <ContentsTemplate id={"menuContents"}>
                                    <div className="panel_body" style={{ height: '100%' }}>
                                        <SGrid grid={'grid'} gridApiCallBack={this.store.setGridApi} onSelectionChanged={this.store.handleMenuSelect} />
                                    </div>
                                </ContentsTemplate>
                            </div>

                            <div className="ratio_h50 shadow">
                                <ContentsMiddleTemplate subTitle={this.l10n("M00995", "프로그램 정보")}>
                                    <SButton buttonName={this.l10n("M00983", "추가")} onClick={this.store.handleAddProgram} type={'add'} />
                                    <SButton buttonName={this.l10n("M00004", "저장")} onClick={this.store.handleSaveProgram} type={'save'} />
                                    <SButton buttonName={this.l10n("M00003", "삭제")} onClick={this.store.handleDelProgram} type={'delete'} />
                                </ContentsMiddleTemplate>
                                <ContentsTemplate id={"programContents"}>
                                    <div className="panel_body" style={{ height: '100%' }}>
                                        <SGrid grid={'grid2'} gridApiCallBack={this.store.setGridApi2} editable={false} />
                                    </div>
                                </ContentsTemplate>
                            </div>
                        </div>
                    </ContentsTemplate>
                </React.Fragment>
            </div>
        )
    }
}
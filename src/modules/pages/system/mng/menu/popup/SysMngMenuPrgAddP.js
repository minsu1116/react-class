import React from 'react';
import { BaseStore, model } from "utils/base/BaseStore";
import { observer, inject } from "mobx-react";
import { BaseComponent } from "utils/base/BaseComponent";
import SearchTemplate from "components/template/SearchTemplate";
import SButton from "components/atoms/button/SButton";
import ContentsTemplate from "components/template/ContentsTemplate";
import SGrid from "components/override/grid/SGrid";
import SInput from 'components/atoms/input/SInput';
import { toJS, observable } from 'mobx';
import ContentsMiddleTemplate from 'components/template/ContentsMiddleTemplate';
import Alert from 'components/override/alert/Alert';
import SysMngMenuModel from 'modules/pages/system/mng/menu/SysMngMenuModel';


export { SysMngMenuPrgAddP, SysMngMenuPrgAddPStore }

class SysMngMenuPrgAddPStore extends BaseStore {

    @observable @model masterP = new SysMngMenuModel();
    @observable multi = false;

    setGridApi = async (grid) => {
        this.grid = grid;

        const columnDefs = [
            { name: "", field: "check",  type: 'checkboxAll', editable: false, hide: !this.multi }
            , { word: "M00988", name: "프로그램ID", field: "formId", width: 200, editable: false }
            , { word: "M00989", name: "프로그램명", field: "formNm", width: 200, editable: false }

        ];

        this.grid.setColumnDefs(columnDefs);

    }

    //프로그램 조회
    handleSearchClick = async () => {
        const rowData = [];
        const param = toJS(this.masterP);
        const data = await this.execute(`/system/mng/prg/getprg`, param);

        //조회시 메뉴관리 그리드 데이터는 제외
        data.data.forEach(item => {
            if (!param.programArr.some(x => x.formId == item.formId)) {
                rowData.push(item);
            }
        })

        this.grid.setRowData(rowData);

    }

    //프로그램 선택
    handleChoiceClick = async() => {
        if(this.multi) {
            const choices = this.grid.getCheckedRows();
            if(choices.length < 1) {
                await Alert.meg("선택된 항목이 없습니다.");
                return;
            }
            choices.map(item => { item.newRow = true, item.edit = true, item.mainYn = "N" });
            this.close(choices);

        } else {
            const choice = this.grid.getSelectedRow();
            if(choice) {
                this.close(choice);
            } else {
                await Alert.meg("선택된 항목이 없습니다.");
                return;
            }
            
        }

    }

}

@observer
export default class SysMngMenuPrgAddP extends BaseComponent {

    sRender() {
        return (
            <div className="basic">
                <React.Fragment>
                    <SearchTemplate>
                        <div className='search_item_btn'>
                            <SButton buttonName={this.l10n("M00011", "조회")} onClick={this.store.handleSearchClick} type={'search'} />
                        </div>
                        <SInput id={"masterP.formId"} enter={this.store.handleSearchClick} title={this.l10n("M00994", "프로그램ID/명")}/>
                    </SearchTemplate>

                    <ContentsMiddleTemplate subTitle={this.l10n("M00995", "프로그램 정보")}>
                        <SButton buttonName={this.l10n("M00233", "선택")} onClick={this.store.handleChoiceClick} type={'choice'} />
                    </ContentsMiddleTemplate>

                    <ContentsTemplate id={"contents"} height={"350px"}>
                        <SGrid grid={'grid'} gridApiCallBack={this.store.setGridApi} editable={false} rowDoubleClick={this.store.handleChoiceClick} />
                    </ContentsTemplate>
                </React.Fragment>
            </div>
        )
    }

}
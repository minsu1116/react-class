import { observable } from 'mobx';
import { BaseModel } from "utils/base/BaseModel";

export default class MenuModel extends BaseModel {

    @observable menuId = "";
    @observable menuNm = "";
    @observable programArr = [];
    @observable cnt = 0;

}
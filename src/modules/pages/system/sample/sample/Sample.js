import React from 'react';
import { observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import ContentsTemplate from 'components/template/ContentsTemplate';
import { BaseComponent } from 'utils/base/BaseComponent';
import { BaseStore, model } from 'utils/base/BaseStore';
import SearchTemplate from 'components/template/SearchTemplate';
import SButton from 'components/atoms/button/SButton';
import ModalManage from 'utils/base/ModalManage';
import SInput from 'components/atoms/input/SInput';
import API from 'components/override/api/API';
import Alert from 'components/override/alert/Alert';

export { Sample, SampleStore }

class SampleStore extends BaseStore {

    path = "";

    handleSR = async () => {
        const modal = new ModalManage();
        modal.id = "system_sample_sample";
        modal.path = "/system/sample/sample/popup/SampleSRP";
        modal.title = "서비스 요청 TEMPLATE";
        modal.width = 540;

        const result = await this.showModal(modal);

        if (result) {

        }
    }

    handleFileUpload = async () => {
        var fileInput = document.createElement('input');
        fileInput.id = "fileInput";
        fileInput.type = 'file';
        fileInput.multiple = true;

        fileInput.onchange = async (e) => {
            const files = e.target.files;

            if (!this.path) {
                await Alert.meg("경로 입력 필요");
                return;
            }

            if (files && files.length > 0) {
                for (let i = 0; i < files.length; i++) {
                    const file = files[i];

                    let formData = new FormData();
                    formData.append('file', file);
                    formData.append('path', this.path);

                    const result = await API.file.post(`/api/file/temp-save`, formData, {
                        headers: {
                            'Content-Type': 'multipart/form-data',
                            Pragma: 'no-cache'
                        }
                    });
                    console.log("RESULT", result);
                }
            }
        }

        fileInput.click();
    }
}

@observer
class Sample extends BaseComponent {
    sRender() {
        return (
            <React.Fragment>
                <SearchTemplate>
                    <div className='search_item_btn' >
                        {/* <SButton buttonName={"서비스요청 샘플"} onClick={this.store.handleSR} type={'default'} /> */}
                        <SButton buttonName={'파일업로드'} onClick={this.store.handleFileUpload} type={'default'} />
                    </div>
                    <SInput id={"path"} title={"샘플"} />

                </SearchTemplate>
                <ContentsTemplate>

                </ContentsTemplate>
            </React.Fragment>
        )
    }
}
import React from 'react';
import { observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import ContentsTemplate from 'components/template/ContentsTemplate';
import { BaseComponent } from 'utils/base/BaseComponent';
import { BaseStore, model } from 'utils/base/BaseStore';
import CommonStore from 'modules/pages/common/service/CommonStore';
import "modules/pages/system/sample/sample/popup/style.css"
import SSelectBox from 'components/atoms/selectbox/SSelectBox';
import CodeHelper from 'modules/pages/common/codehelper/CodeHelper';
import SInput from 'components/atoms/input/SInput';
import SDatePicker from 'components/override/datepicker/SDatePicker';
import SampleSRPModel from './SampleSRPModel';
import btn_left from 'style/img/btn_left.svg';
import btn_right from 'style/img/btn_right.svg';

export { SampleSRP, SampleSRPStore }

class SampleSRPStore extends BaseStore {

    @observable @model masterP = new SampleSRPModel();
    @observable srStatus = [];

    loaded = () => {
        const srProcStat = CommonStore.getCdType("SR_PROC_STAT");
        this.srStatus = Object.values(srProcStat);
    }
}

@observer
class SampleSRP extends BaseComponent {

    /**
     * 상세 상태 타이틀바를 누를 경우 상세 내용 보임/숨김 처리
     */
    handleProcItem = (e, stat) => {
        const detail = document.getElementById(`detail_${stat}`);
        if (detail) {
            if (!detail.style.display || detail.style.display == "block") {
                detail.style.display = "none";
            } else {
                detail.style.display = "block";
            }
        }
    }

    sRender() {
        return (
            <React.Fragment>

                {/* 해당 서비스요청(SR)의 단계 리스트 */}
                
                <div className="sr_proc_stat_item">
                    {this.store.srStatus.map((x, i) => {
                        return <div className={`sr_proc_stat sr_proc_stat_${x.cd} now_${x.cd} after_${x.cd} `} key={`stat_${i}`}>{x.cdNm}</div>
                    })}
                </div>
                


                {/* 선택한 단계의 상세 정보 */}
                <div className="proc_item">
                    <div className="proc_title" id="detail_title_100" onClick={(e) => this.handleProcItem(e, "100")}>{"요청 접수"}</div>
                    <div className="proc_detail" id="detail_100">
                        <table id={'table'}>
                            <tbody>
                                <tr>
                                    <th>{this.l10n("M02460", "요청일자")}</th>
                                    <td>
                                        <SDatePicker id={"masterP.reqDtm"} readOnly={true} width={80} />
                                    </td>
                                    <th className="essential">{this.l10n("M00584", "요청자")}</th>
                                    <td className='popupText'>
                                        <CodeHelper
                                            helperCode={"masterP.reqEmpNo"}
                                            helperCodeNm={"masterP.reqEmpNm"}
                                            mainStore={this.store}
                                            business={"user"}
                                            readOnly={CommonStore.isSystem ? false : true}
                                            onResult={this.store.handleUserHelperResult}
                                        />
                                    </td>
                                </tr>
                                <tr>
                                    <th className="essential">{this.l10n("M02475", "완료요구일")}</th>
                                    <td>
                                        <SDatePicker id={"masterP.endDemandDt"} />
                                    </td>
                                    <th className="essential">{this.l10n("M02456", "시스템")}</th>
                                    <td>
                                        <CodeHelper helperCode={"masterP.categoryMedium"}
                                            helperCodeNm={"masterP.categoryLarge"}
                                            mainStore={this.store}
                                            business={"sysCategory"}
                                            readOnly={this.store.masterP.srProcStat ? true : false}
                                            onResult={this.store.handleSysCategoryHelperResult}
                                        />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                {/* 선택한 단계의 상세 정보 */}
                <div className="proc_item">
                    <div className="proc_title" onClick={(e) => this.handleProcItem(e, "300")}>{"진행중"}</div>
                    <div className="proc_detail" id="detail_300">
                        <table id={'table'}>
                            <tbody>
                                <tr>
                                    <th>{this.l10n("M02460", "요청일자")}</th>
                                    <td>
                                        <SDatePicker id={"masterP.reqDtm"} readOnly={true} width={80} />
                                    </td>
                                    <th className="essential">{this.l10n("M00584", "요청자")}</th>
                                    <td className='popupText'>
                                        <CodeHelper
                                            helperCode={"masterP.reqEmpNo"}
                                            helperCodeNm={"masterP.reqEmpNm"}
                                            mainStore={this.store}
                                            business={"user"}
                                            readOnly={CommonStore.isSystem ? false : true}
                                            onResult={this.store.handleUserHelperResult}
                                        />
                                    </td>
                                </tr>
                                <tr>
                                    <th className="essential">{this.l10n("M02475", "완료요구일")}</th>
                                    <td>
                                        <SDatePicker id={"masterP.endDemandDt"} />
                                    </td>
                                    <th className="essential">{this.l10n("M02456", "시스템")}</th>
                                    <td>
                                        <CodeHelper helperCode={"masterP.categoryMedium"}
                                            helperCodeNm={"masterP.categoryLarge"}
                                            mainStore={this.store}
                                            business={"sysCategory"}
                                            readOnly={this.store.masterP.srProcStat ? true : false}
                                            onResult={this.store.handleSysCategoryHelperResult}
                                        />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

            </React.Fragment>
        )
    }
}
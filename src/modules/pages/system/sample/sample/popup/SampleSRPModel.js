import { BaseModel, essential } from "utils/base/BaseModel";
import { observable } from "mobx";

export default class SampleSRPModel extends BaseModel {

    @observable @essential("요청일자") reqDtm;
    @observable @essential("요청자") reqEmpNo;
    @observable @essential("완료요구일") endDemandDt;
    @observable @essential("시스템") sysCategoryId;
    @observable @essential("처리구분") srProcType;
    @observable @essential("제목") reqTitle;
    @observable @essential("내용") reqContents;
    @observable reqEmpNm;
    @observable categoryLarge;
    @observable categoryMedium;
    @observable reqCompCd;
    @observable reqDeptCd;
    @observable srProcDtlType;
}
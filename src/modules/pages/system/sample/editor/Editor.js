import React from 'react';
import { observable, toJS } from 'mobx';
import { inject, observer } from 'mobx-react';
import SearchTemplate from 'components/template/SearchTemplate';
import ContentsMiddleTemplate from 'components/template/ContentsMiddleTemplate';
import ContentsTemplate from 'components/template/ContentsTemplate';
import SGrid from 'components/override/grid/SGrid';
import SButton from 'components/atoms/button/SButton';
import { BaseComponent } from 'utils/base/BaseComponent';
import Alert from 'components/override/alert/Alert';
import { BaseStore, model } from 'utils/base/BaseStore';
import SEditor from 'components/override/editor/SEditor'
import SImageViewer from 'components/override/imageViewer/SImageViewer';
import SInput from 'components/atoms/input/SInput';
import CodeHelper from 'modules/pages/common/codehelper/CodeHelper';

export { Editor, EditorStore }

class EditorStore extends BaseStore {
    // @observable content = `<p><br></p><p><br></p><p><br></p><p>안녕하세용?</p><p><img id="37" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAI4AAABuCAYAAADxuSuKAAAOMUlEQVR4Ae2d34tdVxXH519QfOqzD775pH9Bg+CzVdLHIpI+KSJ9UEGlUnwQYkHERBA0TanF2BpKjEhNoRVKrDGSllaaam3FGk1DpW3SVuXI59rPzZo1+5y592TOnZk7a8Owzv6xfn/P2mfunTl7o6tWERgRgY0RPMVSEegKOAWCUREo4IwKWzEVcAoDoyJQwBkVtmIq4BQGRkWggDMqbMVUwCkMjIpAAWdU2IqpgFMYGBWBAs6osBVTAacwMCoCBZxRYSumAk5hYFQECjijwlZMBZzCwKgIFHBGha2YCjiFgVERKOCMClsxFXAKA6MiUMAZFbZiKuAUBkZFoIAzKmzFtFLgvPbKa91Pjz3cvfTc5e7Ck7/rHv3hz7o333hzz2Th6NFL3alTf95iz+XL/+puv/1Mc27L4j08cP78P7rbbnuwg95qWylwAAlgEThnHzrTvXvj3bkPOHTPPee7Gzf+Mx/biQvAACiG2rVr78x0Q3M76MBpxW/lwHnsxOmOygN49hJwWsHJADqo/VZs5sC59Mj93S++8snu3zfe6ri+cPKbs/6xQxvdjz71oe7qSxfnceOaMeb4efW3Z+dzYy64y++889ysjFJK/YlVwrveOcttLr/KwtnMIy9zsVHh7r3397P1jjNG9ZMHqk7WZNlWSvXntXff/ZvOaoZfi8qNMUCv/sqvHuzBhxMnXpzL1s/oC1sua23YhO1nz74651Nn9lGdyJ0BB7AAGgEAcCIg6D95/5GZruvX/j5bC6XFvs8wD333wS7+UF0WaQTBBMT1Omfgcx8+gMc4ThswZdA3GI5FCv/QPGuZN0mR1+s4n/XlvjxQE+72HOXEdVyjPyfeNSZZ3+lHsLKO+DDGnI0xYmfcW2ta9m8AGirI4/cdnlUbBEag0I/zgMtKI80VSaOWpQRGByIv46JdmgOIc8y1ANByXPnejUOgYG1OqAHXHqhJYw4/oMjP1Uxb5RX06HGu5Qdj6tB+aQsozklboGiNZV/Rme3ZAAgAhR/bdsCx+rheOlXFIanZcHVKce7IkaeawGs5Lh8BJ7He8Y5nmoNJP9rEdUyq67N8+ocP/3p+1/cl3Jsl3khZR7SxT05c0wJJa0zb5W3Fb4Mtih+3HhZn4AAUgeXzjduawneC9jnPOBWmryowboBbwY3z2c4cpDxvP66zSgkU7bMPD2PHj78wA1O0m+tYYZAb++qDklQrF/3MG9f2xS6uaYEkj7XktOK38ZO7PjJ/flEJIHEbguYKk7crH6rlvxVKIC3hXNsw3nGoQGE8blsEgkREXhMtvwluBUl9mSIPXbZoD/oEifPq1M48ri3wCQ55nINGncjA9tb8kC/GJPIZszznuPZCs13YMANOrh654kQh63QNGATRkF8GLidxiGe/zOWKs6jds62KB+TYDgpwos9D19zN8blkaO1+mxsNnPhso9MFnK2f06xjtSHfo4EjWIpWBJaJwPyT42WYam1FoIBTGBgVgQLOqLAVUwGnMDAqAgsB5/tnrnRfPP6XTQrefue/3afve7H7wB3PzH5YQ2OdY49fvPll2ibmNetcevl694mvvtBdeeO9pTwjPsRqijiRB3MSc/Xhuy522JsbY8zJ43yU4xh0NHBaYFKwhk4REHXsJUoscsAXsW8q4ABgbuoMZPoAfFeB04dEAnaQgDOUjEXAM8Wavpt6jK19eZ5XnLwA5W45UOZp0DjO9ce/8OwmdPcBx3IoP3cFa2nMffY7f+q+fepvc/nxLs68y1Qz72z1Rt7oT/SDcWyhfHPtusiL3fSZiy3bGv0keejRlrx15LgbA/miLq6jbGwgnsQRG3LrA46+YVP2j7mT567ObTZGTeBgbDSIfjQYg+jrVDawDzh5HTI01GArk77PDTiMPVBa7me5sY/8nBzn0R/9in4zTpDkNwbah4yhJKkDGv2M49vxZz/paxN2xRwpl/Hok+NQ+Pu2qr6cISvGjz4x2AKclgCDFo1QQBzzuiVDw+PdBsJNRASKcqQEwztUGp1xXYv22dlKWrRBvtaYevqSZ4K1NfopL7RlQ/Y1+4k9jHnnZ3mACRmtNhY45giZyCY2KwUOCvmxmRz6MUHOSzXW/jI06oh8raRFG+RrjSmHNa0kMc6PTVn2pdkGAafMVqKx59CXn+8+9vlnt2xHzLFNIbfVWvJcB08LdNl2czEDDp14V0THnYuBQFkWqAHQlhGOiV6c5M6JfbemKItr1xrQPE9fO5XnGsZbdyfz0c/c1z90a5djrO1L0nZ+aheUtfF5BJkREPgSK05MPH7lrQr7hmIU+aMd2rIdcPQNHRsABgYeBA06CtxSmPv509c23UEoikHUCAXHEs21zkCdQ/7XHvjrXGdMkPIijbzajD6b8/rgOJQx9UZ7sr0xEfoX7XIMmfE66uJaW9CV/Yxr0R+Bo1xtZc55cxL9wwZvimhn1MG1vMqFCsgcA9fgQ7aHOfXPt6qsrPr9ERhKUj/X1pmhCrB19fDIEJCHOcfNziqOKCv6/0/Bp4pDThF3tRVjKp1Tya2Kk7M5cT9uY24XE6ucRHwBZ5Kwrr/QAs7653gSDws4k4R1/YUWcNY/x5N4WMCZJKzrL3QQOPEDpp0MhR86xQ/ctpP/xOlzs/fpvH7l9dnLmXij1063V556uTv9uVPde9eX+4OsMXag61sf/HoH3Y9t3wAHoPAiJoGz6KtTlknKE9/41coSORY4f3jgQoedu90GgbPbxkX9AIeqw6vfAFAEjnNxfeuaV8n5RrA8f/WP/+we/szJ7vrVt/PUnur3AWfItykc2OBDKP5QB+rWwUfq9PnU0TGVM+d3KH4q6fcarumj+TsjtsLY6Ctz2Q/H3MriOwWjbF/BEgEX50kIP7EBpu999OhsS2FbyXe6VYO5uO3A98svPdY9c+zpOa+y2QbZDlmPbNbaAC1zzz/63JxPndkWdSoXGdv5qJ6doBs+x5B8vwVWMGMt4JBUk95aI/8QhU8ZrBOQPP+MbVSe1ptMAQtvOyWwrWbCcrUZ2roATU68sk2ySaWfqxm6GMvA+fGhH8yfs1prkCmY1BepL+gkFlO2GXCoBK0v7lqgyOtyf1FjM3D8BnfZSpP1ARJeI2dloRK1wBT5+pLBOHd2K1GMCYwoi+sWUPKaFihaYxm8fbZG+W7n+eWccc2tXu8Z4OiIv3GxZQGuZdqYisPWwbYS7/ys0y0p/sa1V4GzrysOCSfx/u1GTgT9XHHyGniH+PP6sc84gKJVUbJ8qgHAgdLgY1uxH9fvVMVpyUFvBHDUu/JnnLxVmXgfVKE+6+StKfdxRP6hxGfgyKNO9cXA9F2P/a3KB1WSkZtzPoRC8zq3Mtc430q48gEagJMH6rNSnnNcXmi2y+1y5b9VRaNWeQ2o4sPxKnWra+judc0qKcDJD8yr1L+MrpX9IVc0yucYK5JVpkX5Q6eD0vYVcFaVFMEiOHa72qzK72X0FHCWiVat3ZcR2DdfOezL6K6x0QWcNU7ulK4VcKaM7hrLLuCscXKndK2AM2V011j2SoETPxLv+15pN2Pd94p+3qzOGQeLvL5/N+3fTrfnT+zEy75XChy/gOOba4CTv73FoXxoxnbBWGSehMdDQVo8vGHcwzjy/EEHTit+KweOf4EHePYScFrByQA6qP1WbObA4fwGjw/ius7krDM5rbTxuCKuAdIMOHUm5/Yn8OXzqnL1ifP5Ds39yEty4gl9UU5cxzVbees8KeZMMrrs15mc6ZAzUJ8DSMAYbz3LDCVu0bOockJ5JuKAs3g3mrT4vIT8OpPzxluzk4LziXkziIc/lo4nAMc/43RdH+17OGa8BYgoh6TVmZw3j6iOsfG6dcRQayzfJK0br87kTEcmGuRMYzCtUlaYvE3Ay5jHLQJ8G9fxDE7kxr7roLFy0c+8cS368tYU55XXOj46jrXkoDf/tltncoaD5HOgYz8Ch3GC6TZVZ3K+H6mDckIeYLBqRJDkaysMYFm31tqqFvGxzuRcIEqU7zqTc3OgNupMzs0BsQdY+M3N7Wgdqw2+jq44BqpoRWCZCMw/OV6GqdZWBAo4hYFRESjgjApbMRVwCgOjIrAQcFr/dZn/T8p/ruP/pfzfKf6t9yC01r9BL+K3//Y8RZzIgzmJuep7Gwg+MCeP9kc5jkFHA6cFJgVr6BQBUcdeosQiB3wR+6YCDq+M4X/vobENnR2xMuD0IRFDDxJwhpIRk7bK676beoytfXmeV5y8AOVuOVDmadA4zrUHWRicPuCIavnjGymY4xVxdSbn5rhbyUg6cTYP5iLGkDFi71FF5kPaB5yY07xLMFdnchrB9ylBiYkgSSaCcRJFIN37GTORiBhKUlQFX07IIvwkOm45ggdZ/Ghr1MV49CnO9QFHW5CX7URWfC4yBlsqTqtaEKxsjAKiYV63ZDCn41YcqImg4uR3ECoPZyIP19EZ17Von52tpEcb5GuNqacveUN+ygtt2ZB9zX5iD2O5yiuvlXx1jgWOOUIO9hGblQIHhfzYTA79mCDnpRprfxkadUS+VtKiDfK1xpTDGmzLjXF+bMqyL802CDhlthKNPXUmZ/j1LybIwEqZ4y4zoI5HylysYM4x3ro7mW8l2ISb7GiXY/AyzrMEyY+NPne9d6m2289r4/MIa+tMzjtunjZnwk0uCSaZdSbn1gdZwOm2DKgEltUoApC13hQR4BGcXMurXKhboECPc1ybs2gP4+qfb1VZWfX7IzCUpH6urTOtrWjrqsVGYjVcjOPWVq3sVW4Z0Qexn1MVt9H9Fo+qODmbE/fjdu12MbHKScQXcCYJ6/oLLeCsf44n8bCAM0lY119oAWf9czyJhwWcScK6/kL/By+DVlVAg+FzAAAAAElFTkSuQmCC"></p>`;
    @observable content; // = `<p><br></p><p><br></p><p><br></p><p>안녕하세용?</p><p><img id="37" style="width: 207px; height: 188px;"></p>`;
    @observable input = `ㅏㅏㅏ<img id="60"><img id="61">`;

    @observable image = [
        { src: 'src/style/img/logo.png' },
        { src: 'src/style/img/top_menu_icon_prod_off.png' }
    ];

    handleSave = async () => {
        const params = {
            fileGrpId: 88,
            contents: this.content, //`<p>ㅜㅜ</p><p>ㅋ</p><p>ㅋ</p><p>ㅋ</p><p>ㅋ<img id="58" style="width: 1087px; height: 218px;"></p><p>ㅋ</p><p>ㅋ</p><p>ㅋ</p><p><img id="59" ></p><p><br></p><p>zkzk</p><p><br></p><p><br></p><p><br></p>`, //`<p>호ㅓ</p><p><br></p><p><br></p><p><img id="43"></p><p><br></p><p>jjljklj</p><p><br></p><p><img id="44" style="width: 206px; height: 180px;"></p><p><br></p><p>jhkhjkh</p>`//this.content//`<p><br></p><p><br></p><p><br></p><p>안녕하세용?</p><p><img id="37" style="width: 207px; height: 188px;"></p>`,
        }
        const result = await this.execute(`/system/master/code/get`, params);

        if (result.success) {
            console.log("handleSave", result.data);
        }
    }

    handleImport = async () => {

        const params = {
            // fileGrpId: 0,
            contents: this.input,
        }

        const result = await this.execute(`/system/master/code/detail/get`, params);

        if (result.success) {
            console.log("handleImport", result.data.contents);
            this.content = result.data.contents;
        }
    }
}

@observer
class Editor extends BaseComponent {
    sRender() {
        return (
            <React.Fragment>
                <SearchTemplate>
                    <div className='search_item_btn' >
                        <SButton buttonName={"저장"} onClick={this.store.handleSave} type={'default'} />
                        <SButton buttonName={"불러오기"} onClick={this.store.handleImport} type={'default'} />
                    </div>

                    <CodeHelper helperCode={"input"} business={"systemMt"} systemCategoryId={432} reviewType={"20"}/>
                    <SInput id="input" width={"300px"} />
                </SearchTemplate>
                <ContentsTemplate>
                    <SEditor id={"content"} height={"calc(100% - 52px)"} readOnly={true} />
                    <SImageViewer id={"image"} />
                </ContentsTemplate>
            </React.Fragment>
        )
    }
}
import React from 'react';
import { observable, toJS } from 'mobx';
import { inject, observer } from 'mobx-react';
import SearchTemplate from 'components/template/SearchTemplate';
import ContentsMiddleTemplate from 'components/template/ContentsMiddleTemplate';
import ContentsTemplate from 'components/template/ContentsTemplate';
import SGrid from 'components/override/grid/SGrid';
import SButton from 'components/atoms/button/SButton';
import { BaseComponent } from 'utils/base/BaseComponent';
import Alert from 'components/override/alert/Alert';
import { BaseStore, model } from 'utils/base/BaseStore';
import SEditor from 'components/override/editor/SEditor'
import SImageViewer from 'components/override/imageViewer/SImageViewer';
import SInput from 'components/atoms/input/SInput';
import CodeHelper from 'modules/pages/common/codehelper/CodeHelper';

export { NewEditor, NewEditorStore }

class NewEditorStore extends BaseStore {
    data = "";
}

@observer
class NewEditor extends BaseComponent {
    sRender() {
        return (
            <React.Fragment>
                <SEditor id={"data"} />
            </React.Fragment>
        )
    }
}
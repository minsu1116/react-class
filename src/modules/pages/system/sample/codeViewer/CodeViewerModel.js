import { BaseModel, essential } from "utils/base/BaseModel";
import { observable } from "mobx";
import moment from "moment";

export default class CodeViewerModel extends BaseModel {
    @observable @essential("입고일") date =  moment().format("YYYYMMDD");
    @observable @essential("업체") client = '';
    @observable @essential("상품") fruit  = '';
    @observable @essential("수량") inputCtn = 0;
    @observable remark = '';
}
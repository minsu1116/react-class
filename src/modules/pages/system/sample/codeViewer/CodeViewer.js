import React from 'react';
import { observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import { BaseComponent } from 'utils/base/BaseComponent';
import { BaseStore, model } from 'utils/base/BaseStore';
import SSelectBox from 'components/atoms/selectbox/SSelectBox';
import SRadio from 'components/atoms/radio/SRadio';
import SButton from 'components/atoms/button/SButton';
import STextArea from 'components/atoms/input/STextArea';
import SNumericInput from 'components/override/numericinput/SNumericInput';
import CodeViewerModel from "modules/pages/system/sample/codeViewer/CodeViewerModel";
import ContentsMiddleTemplate from 'components/template/ContentsMiddleTemplate';
import Alert from 'components/override/alert/Alert';
import ContentsTemplate from 'components/template/ContentsTemplate';
import SDatePicker from 'components/override/datepicker/SDatePicker';
import SGrid from 'components/override/grid/SGrid';
import { SChart, SChartXAxis, SChartYAxis, SChartTooltip, SChartLegend, SChartBar, SChartLine, SPie, SPieChart, SCell } from 'components/override/chart/SChart';
import { Label, Text } from 'recharts'

export { CodeViewer, CodeViewerStore }

class CodeViewerStore extends BaseStore {

    @observable @model master = new CodeViewerModel();
    @observable chartData = [];

    setGridApi = async (grid) => {
        this.masterGrid = grid;

        const columnDefs = [
            { name: "날짜", field: "date", width: 100, align: 'center', type: "dateTime", editable: false },
            { name: "업체", field: "client", width: 70, align: 'center', codeGroup: "CLIENT_GROUP", editable: false },
            { name: "과일", field: "fruit", width: 100, align: 'center', codeGroup: "FRUIT_GROUP", editable: false },
            { name: "수량", field: "inputCtn", width: 100, editable: false },
            { name: "비고", field: "remark", width: 250, type: "textarea", editable: false },
        ];

        this.setSummary(columnDefs, "inputCtn");
        this.masterGrid.setColumnDefs(columnDefs);
    }

    handleMasterRowSelect = () => {

    }

    handleMasterAddClick = async () => {

        if (await this.validation("master")) {
            if (this.master.remark.length > 10) {
                await Alert.meg("10자 이상을 넘었습니다.");
                return;
            }
            const param = {
                date: this.master.date,
                client: this.master.client,
                fruit: this.master.fruit,
                inputCtn: this.master.inputCtn,
                remark: this.master.remark,
            };
            this.masterGrid.addRow(param);

            if (this.chartData.find(x => x.date == this.master.date)) {
                let breakPonit = false;
                this.chartData.forEach(element => {
                    if (breakPonit) {
                        return;
                    }
                    if (element.date == this.master.date) {
                        breakPonit = true;
                        switch (this.master.fruit) {
                            case "apple":
                                element.apple += this.master.inputCtn;
                                break;
                            case "waterMelon":
                                element.waterMelon += this.master.inputCtn;
                                break;
                            case "berry":
                                element.berry += this.master.inputCtn
                        }
                    }
                });
            } else {
                const chartParam = {
                    date: this.master.date,
                    client: this.master.client,
                    apple: 0,
                    waterMelon: 0,
                    berry: 0,
                };

                switch (this.master.fruit) {
                    case "apple":
                        chartParam.apple += chartParam.apple + this.master.inputCtn;
                        break;
                    case "waterMelon":
                        chartParam.waterMelon += chartParam.waterMelon + this.master.inputCtn
                        break;
                    case "berry":
                        chartParam.berry += chartParam.berry + this.master.inputCtn
                }

                this.chartData.push(chartParam);
            }

            this.chartData = this.chartData.sort(this.comareValues('date'));
        }
        
        this.master = new CodeViewerModel();
    }

}

@observer
class CodeViewer extends BaseComponent {
    sRender() {
        const { chartData, handleChange} = this.store;
        return (
            <ContentsTemplate>
                <div className="ratio_h100 ratio_w100">
                    <div className="ratio_h30 ratio_w100">
                        <ContentsTemplate>
                            <table id={'table'} style={{ width: "100%" }} tabletype="th_w100">
                                <tbody>
                                    <tr>
                                        <th className="essential">{"날짜"}</th>
                                        <td>
                                            <SDatePicker id={"master.date"} />
                                        </td>
                                    </tr>
                                    <tr>
                                        <th className="essential">{"업체"}</th>
                                        <td>
                                            <SSelectBox id={"master.client"} codeGroup={"CLIENT_GROUP"} addOption={this.l10n("M00233", "선택")} />
                                        </td>
                                    </tr>
                                    <tr>
                                        <th className="essential">{"과일"}</th>
                                        <td>
                                            <SRadio id={"master.fruit"} codeGroup={"FRUIT_GROUP"} />
                                        </td>
                                    </tr>
                                    <tr>
                                        <th className="essential">{"수량"}</th>
                                        <td>
                                            <SNumericInput id={"master.inputCtn"} decimalScale={0} />
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>{"비고"}</th>
                                        <td>
                                            <STextArea id={"master.remark"} height={80} width={500}/>
                                        </td>
                                    </tr>

                                </tbody>
                            </table>
                        </ContentsTemplate>
                    </div>
                    <div className="ratio_h70 ratio_w100">
                        <div className="ratio_h40 ratio_w100">
                            <ContentsTemplate shadow={true}>
                                <ContentsMiddleTemplate subTitle={"일별 재고현황"}>
                                    <SButton buttonName={this.l10n("M00983", "추가")} onClick={this.store.handleMasterAddClick} type={'add'} />
                                </ContentsMiddleTemplate>
                                <ContentsTemplate>
                                    <SGrid grid={'masterGrid'} gridApiCallBack={this.store.setGridApi} onSelectionChanged={this.store.handleMasterRowSelect} editable={true} />
                                </ContentsTemplate>
                            </ContentsTemplate>
                        </div>
                        <div className="ratio_h60 ratio_w100">
                            <ContentsTemplate height={'100%'} id={"contents"} isShadow={true}>
                                <div className="ratio_w50 ratio_h100 shadow">
                                    <ContentsTemplate>
                                        <SChart chartData={chartData} background={"transparent"} subTitle={'Line Chart'}>
                                            <SChartXAxis dataKey="date" />
                                            <SChartYAxis domain={[0, 'dataMax']} />
                                            <SChartTooltip />
                                            <SChartLegend verticalAlign='bottom' align='center' layout='horizontal' />
                                            <SChartLine dataKey={"apple"} name={"사과"} stroke={"#0046e4d9"} />
                                            <SChartLine dataKey={"waterMelon"} name={"수박"} stroke={"#0d964185"} />
                                            <SChartLine dataKey={"berry"} name={"딸기"} stroke={"#e40000d9"} />
                                        </SChart>
                                    </ContentsTemplate>
                                </div>
                                <div className="ratio_w50 ratio_h100">
                                    <ContentsTemplate>
                                        <SChart chartData={chartData} background={"transparent"} subTitle={'Bar Chart'}>
                                            <SChartXAxis dataKey="date" />
                                            <SChartYAxis domain={[0, 'dataMax']} />
                                            <SChartTooltip />
                                            <SChartLegend verticalAlign='bottom' align='center' layout='horizontal' />
                                            <SChartBar dataKey={"apple"} name={"사과"} fill={"#0046e4d9"} />
                                            <SChartBar dataKey={"waterMelon"} name={"수박"} fill={"#0d964185"} />
                                            <SChartBar dataKey={"berry"} name={"딸기"} fill={"#e40000d9"} />
                                        </SChart>
                                    </ContentsTemplate>
                                </div>
                            </ContentsTemplate>
                        </div>
                    </div>
                </div>
            </ContentsTemplate>
        )
    }
}
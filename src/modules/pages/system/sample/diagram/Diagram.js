import React from 'react';
import { observable, toJS } from 'mobx';
import { inject, observer } from 'mobx-react';
import SearchTemplate from 'components/template/SearchTemplate';
import ContentsTemplate from 'components/template/ContentsTemplate';
import SButton from 'components/atoms/button/SButton';
import { BaseComponent } from 'utils/base/BaseComponent';
import { BaseStore, model } from 'utils/base/BaseStore';
// import SDiagram3 from 'components/override/diagram/SDiagram3';
// import SDiagram2 from 'components/override/diagram/SDiagram2';
// import SDiagram from 'components/override/diagram/SDiagram';

export { Diagram, DiagramStore }

class DiagramStore extends BaseStore {

    @observable diagramData = {
        offset: {
            x: 0,
            y: 0
        },
        nodes: {
            node1: {
                id: "node1",
                type: "output-only",
                position: {
                    x: 300,
                    y: 100
                },
                ports: {
                    port1: {
                        id: "port1",
                        type: "output",
                        properties: {
                            value: "yes"
                        }
                    },
                    port2: {
                        id: "port2",
                        type: "output",
                        properties: {
                            value: "no"
                        }
                    }
                }
            },
            node2: {
                id: "node2",
                type: "input-output",
                position: {
                    x: 300,
                    y: 300
                },
                ports: {
                    port1: {
                        id: "port1",
                        type: "input"
                    },
                    port2: {
                        id: "port2",
                        type: "output"
                    }
                }
            },
        },
        links: {
            link1: {
                id: "link1",
                from: {
                    nodeId: "node1",
                    portId: "port2"
                },
                to: {
                    nodeId: "node2",
                    portId: "port1"
                },
            },
        },
        selected: {},
        hovered: {}
    };

    handleAdd = () => {
        const data = this.diagramData;
        data.nodes.nodes3 = {
            id: "node3",
            type: "input-output",
            position: {
                x: 400,
                y: 400
            },
            ports: {
                port1: {
                    id: "port1",
                    type: "input"
                },
            }
        }

        this.diagramData = data;
        // console.log("BBB", this.diagramData);
    }
}

@observer
class Diagram extends BaseComponent {
    sRender() {
        return (
            <React.Fragment>
                <SearchTemplate>
                    <div className='search_item_btn' >
                        <SButton buttonName={"추가"} onClick={this.store.handleAdd} type={'default'} />
                    </div>
                </SearchTemplate>
                <ContentsTemplate>
                    {/* <SDiagram3 id={"diagramData"} /> */}
                    {/* <SDiagram /> */}
                </ContentsTemplate>
            </React.Fragment>
        )
    }
}
import React from 'react';
import { observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import { BaseComponent } from "utils/base/BaseComponent";
import { BaseStore, model } from 'utils/base/BaseStore'
import { STab, STabPanel } from 'components/override/tab/STab';

import CommonStore from 'modules/pages/common/service/CommonStore'
import ModalManage from 'utils/base/ModalManage';

import SrMngTodo from 'modules/pages/service/request/todo/popup/SrMngTodo';
import SrApprTodo from 'modules/pages/service/request/todo/popup/SrApprTodo';
import SrProcTodo from 'modules/pages/service/request/todo/popup/SrProcTodo';
import SrConfirmTodo from 'modules/pages/service/request/todo/popup/SrConfirmTodo';

export { SrMainTodo, SrMainTodoStore }
class SrMainTodoStore extends BaseStore {

    @observable gubn = '';
    @observable todoIndex = 0;
    srState = '110'

    initialize = async(param) => {
        if(param) {
            this.todoIndex = param.todoIndex;
            switch (param.todoIndex) {
                case 0:
                    this.gubn = '';
                    this.srState = '110';
                    break;
                case 1:
                    this.gubn = '';
                    this.srState = '200';
                    break;
                case 2:
                    this.srState = '300';
                    break;
                case 3:
                    this.srState = '400';
                    break;
                default:
                    break;
            }
        }
    }
    
    loaded = async () => {
        await this.onTabChange();
    }

    setTodoGridApi = async (grid) => {
        this.srTodoGrid = grid;
        await this.onTabChange();
    }

    handleSrCheck = async (gubn) => {

        const saveList = this.srTodoGrid.getCheckedRows();

        if (saveList && saveList.length > 0) {
            saveList.forEach(element => {
                element.apprGubn = gubn;
                element.apprEmpNo = CommonStore.userInfo.bempNo;
                element.apprDesc = '일괄처리';
                element.srState = '110';
            });


            const param = {
                srState: this.srState
            }

            const params = JSON.stringify({ param1: toJS(saveList), param2: toJS(param) });
            const result = await this.execute(`/service/request/todo/appr-todo/save`, params);

            if (result.data && result.data.length > 0) {
                this.srTodoGrid.setRowData(result.data);
            } else {
                this.srTodoGrid.setRowData([]);
            }

        }
    }


    handleSearchClick = async () => {
        switch (this.todoIndex) {
            case 0:
                this.gubn = this.gubn;
                this.srState = '110';
                break;
            case 1:
                this.gubn = this.mngGubn;
                this.srState = '200';
                break;
            case 2:
                this.srState = '300';
                break;
            case 3:
                this.srState = '400';
                break;
            default:
                break;
        }

        const param = {
            gubn: this.gubn
            , srState: this.srState
        }

        await this.srTodoGrid.search(`/service/request/todo/todo/get`, param);
    }


    handleRowDoubleClick = async () => {
        const selectedRow = this.srTodoGrid.getSelectedRow();

        const modal = new ModalManage();
        modal.id = "service_request_todo_sr_detail_main_p";
        modal.path = "/service/request/todo/popup/SrDetailMainP";

        // modal.path = "/service/request/reg/popup/SvcReqRegSaveP";
        modal.parameter = selectedRow;
        modal.width = 1133;
        modal.title = this.l10n("M03441", "서비스 요청상세")
        const result = await this.showModal(modal);

        const param = {
            gubn: this.gubn
            , srState: this.srState
        }

        if (result) {
            result.srId = selectedRow.srId;
            result.categoryId = selectedRow.categoryId;
            const saveList = [];
            let params = undefined;

            switch (result.srState) {
                case '110':
                    // 승인 처리
                    result.reviewGubn = result.gubn;
                    saveList.push(result);
                    params = JSON.stringify({ param1: saveList, param2: param });
                    const reviewList = await this.execute(`/service/request/todo/appr-todo/save`, params)

                    if (reviewList.data && reviewList.data.length > 0) {
                        this.srTodoGrid.setRowData(reviewList.data);
                    } else {
                        this.srTodoGrid.setRowData([]);
                    }

                    break;
                case '300':
                    // 접수
                    params = JSON.stringify({ param1: result, param2: param });
                    const mngList = await this.execute(`/service/request/todo/mng-todo/save`, params);

                    if (mngList.data && mngList.data.length > 0) {
                        this.srTodoGrid.setRowData(mngList.data);
                    } else {
                        this.srTodoGrid.setRowData([]);
                    }
                    break;
                case '400':
                    // 접수
                    params = JSON.stringify({ param1: result, param2: param });
                    const procList = await this.execute(`/service/request/todo/proc-todo/save`, params);

                    if (procList.data && procList.data.length > 0) {
                        this.srTodoGrid.setRowData(procList.data);
                    } else {
                        this.srTodoGrid.setRowData([]);
                    }
                break;
                case '900':
                case '310':
                    params = JSON.stringify({ param1: result, param2: param });
                    await this.execute(`/service/request/todo/complt-todo/save`, params);
                break;
                default:
                    break;
            }
        }
    }

    onTabChange = async() => {
    
        let columnDefs = [];
        this.srTodoGrid.setRowData([]);
        switch (this.todoIndex) {
            case 0:
                columnDefs = [
                    { field: "check", type: 'checkboxAll' },
                    { name: "ID", field: "srId", width: 50, align: 'right' },
                    { word: "M03330", name: "처리상태", field: "srState", width: 120, align: 'center', codeGroup: "SR_PROC_STAT" },
                    { word: "M03428", name: "업무구분", field: "categoryLargeId", width: 150, codeGroup: 'CATEGORY_GROUP', align: 'center' },
                    { word: "M03351", name: "업무명", field: "categoryNm", width: 150 },
                    { word: "M00348", name: "제목", field: "reqTitle", width: 200 },
                    { word: "M03327", name: "요청자", field: "reqEmpNm", width: 80, align: 'center' },
                    { word: "M00572", name: "요청일자", field: "reqDtm", width: 110, align: 'center', type: "dateTime" },
                ];

                this.srTodoGrid.setColumnDefs(columnDefs);
                break;
            case 1:
                columnDefs = [
                    { name: "ID", field: "srId", width: 50, align: 'right' },
                    { word: "M03330", name: "처리상태", field: "srState", width: 120, align: 'center', codeGroup: "SR_PROC_STAT" },
                    { word: "M03428", name: "업무구분", field: "categoryLargeId", width: 150, codeGroup: 'CATEGORY_GROUP', align: 'center' },
                    { word: "M03351", name: "업무명", field: "categoryNm", width: 150 },
                    { word: "M00348", name: "제목", field: "reqTitle", width: 200 },
                    { word: "M03327", name: "요청자", field: "reqEmpNm", width: 80, align: 'center' },
                    { word: "M00572", name: "요청일자", field: "reqDtm", width: 110, align: 'center', type: "dateTime" },
                ];

                this.srTodoGrid.setColumnDefs(columnDefs);
                break;
            case 2:
            case 3:
                columnDefs = [
                    { name: "ID", field: "srId", width: 50, align: 'right' },
                    { word: "M03330", name: "처리상태", field: "srState", width: 120, align: 'center', codeGroup: "SR_PROC_STAT" },
                    { word: "M03428", name: "업무구분", field: "categoryLargeId", width: 150, codeGroup: 'CATEGORY_GROUP', align: 'center' },
                    { word: "M03351", name: "업무명", field: "categoryNm", width: 150 },
                    { word: "M00348", name: "제목", field: "reqTitle", width: 200 },
                    { word: "M03327", name: "요청자", field: "reqEmpNm", width: 80, align: 'center' },
                    { word: "M00572", name: "요청일자", field: "reqDtm", width: 110, align: 'center', type: "dateTime" },
                ];
                this.srTodoGrid.setColumnDefs(columnDefs);
                break;
                
            default:
                break;
        }
        // 그리드 초기화
        await this.handleSearchClick();
    }
}

@observer
export default class SrMainTodo extends BaseComponent {

    sRender() {
        return (
            <React.Fragment>

                <STab id={"todoIndex"} onTabChange={this.store.onTabChange}>
                    <STabPanel header={this.l10n("M03406", "요청검토")}>
                        <SrApprTodo store={this.store} />
                    </STabPanel>
                    <STabPanel header={this.l10n("M03415", "요청접수")}>
                        <SrMngTodo store={this.store} />
                    </STabPanel>
                    <STabPanel header={this.l10n("M03416", "요청처리")}>
                        <SrProcTodo store={this.store} />
                    </STabPanel>
                    <STabPanel header={this.l10n("M03407", "완료확인")}>
                        <SrConfirmTodo store={this.store} />
                    </STabPanel>
                </STab>
            </React.Fragment>
        )
    }
}

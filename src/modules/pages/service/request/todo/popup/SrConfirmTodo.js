import React, { Component} from 'react'
import ObjectUtility from 'utils/object/ObjectUtility'

import ContentsMiddleTemplate from 'components/template/ContentsMiddleTemplate';
import SButton from 'components/atoms/button/SButton';
import ContentsTemplate from 'components/template/ContentsTemplate';
import SGrid from 'components/override/grid/SGrid';
/**
 * 서비스 처리 확인 목록
 */
export default class SrConfirmTodo extends Component {

    render() {
        const { store } = this.props;
        const { l10n } = ObjectUtility;
        
        return (
            <React.Fragment>
                <ContentsMiddleTemplate subTitle={l10n("M03452", "확인목록")}>
                    <SButton buttonName={l10n("M00011", "조회")} onClick={store.handleSearchClick} type={'default'} />
                </ContentsMiddleTemplate>
                <ContentsTemplate>
                    <SGrid grid={'srTodoGrid'} gridApiCallBack={store.setTodoGridApi} rowDoubleClick={store.handleRowDoubleClick} editable={false} />
                </ContentsTemplate>
            </React.Fragment>
        )
    }
}
import React, { Component, Fragment } from 'react';
import { observer } from 'mobx-react';
import SButton from 'components/atoms/button/SButton';
import ContentsMiddleTemplate from 'components/template/ContentsMiddleTemplate';
import SDateInput from 'components/override/datepicker/SDateInput'
import SInput from 'components/atoms/input/SInput';
import STextArea from 'components/atoms/input/STextArea';
import SFileMulti from 'components/atoms/file/SFileMulti';
import ContentsTemplate from 'components/template/ContentsTemplate';
import ObjectUtility from 'utils/object/ObjectUtility'
import CommonStore from 'modules/pages/common/service/CommonStore';
import SEditor from 'components/override/editor/SEditor'
import SSelectBox from 'components/atoms/selectbox/SSelectBox';

@observer
export default class SrProcP extends Component {
    render() {
        const { store } = this.props;
        const { l10n } = ObjectUtility;

        const currentEmpNo = CommonStore.userInfo.bempNo;
        
        let isMngEmp = false;

        if(store.masterP.srState > '400' ) {
            isMngEmp = store.masterP.procEmpModel ? store.masterP.procEmpModel.procEmpNo == currentEmpNo ? true :  false : false;
        } else {
            if(store.masterP.mngEmpList && store.masterP.mngEmpList.filter(x => x.mngEmpNo == currentEmpNo).length > 0) {
                isMngEmp = true;
            }

            if(store.masterP.mngEmpNo == currentEmpNo) {
                isMngEmp = true;
            }
        }

        const readOnlyMode = store.masterP.srState != '310' ? true : false;

        if(store.masterP.srState == '310') {
            store.masterP.procGubn = store.masterP.procEmpModel.procGubn;
            store.masterP.procContents = store.masterP.procEmpModel.procContents;
            store.masterP.procFileGrpId = store.masterP.procEmpModel.procFileGrpId ? store.masterP.procEmpModel.procFileGrpId : undefined;
            store.masterP.procEmpNm = store.masterP.procEmpModel.procEmpNm;
        }

        return (
            <React.Fragment>
                {
                    store.masterP.srState == '310' ?
                    <React.Fragment>
                        <ContentsMiddleTemplate subTitle={l10n("M03454", "반려내역")} />
                        <table id={'table'} style={{ width: "100%" }} tabletype="th_w100">
                            <tbody>
                                <tr>
                                    <th>{l10n("M03455", "반려자")}</th>
                                    <td style={{ width: '20px' }}>
                                        <SInput id={"masterP.reqEmpNm"} readOnly={true} width={150} />
                                    </td>
                                    <th>{l10n("M03457", "반려일시")}</th>
                                    <td>
                                        <SDateInput id={'masterP.returnDtm'} format={"yyyy-MM-dd HH:mm"} readOnly={true} />
                                    </td>
                                </tr>
                                <tr>
                                    <th>{l10n("M03456", "반려내용")}</th>
                                    <td colSpan="3">
                                        <STextArea id={"masterP.compltContents"} readOnly={true} />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </React.Fragment>
                    : null
                }
                {
                    (store.masterP.srState == '300' || store.masterP.srState == '310') && isMngEmp ?
                        <ContentsMiddleTemplate subTitle={l10n("M03448", "처리내역")}>
                            <SButton buttonName={l10n("M03417", "처리완료")} onClick={() => store.onClickProcResult()} type={'default'} />
                        </ContentsMiddleTemplate> : <ContentsMiddleTemplate subTitle={l10n("M03448", "처리내역")} />
                }
                {
                    store.masterP.srState > '310' && store.masterP.procEmpModel ?
                        <table id={'table'} style={{ width: "100%" }} tabletype="th_w100">
                            <tbody>
                                <tr>
                                    <th>{l10n("M01017", "처리자")}</th>
                                    <td style={{ width: '20px' }}>
                                        <SInput id={"masterP.procEmpModel.procEmpNm"} readOnly={true} width={150} />
                                    </td>
                                    <th className="essential">{l10n("M03331", "처리구분")}</th>
                                    <td>
                                        <SSelectBox id={"masterP.procEmpModel.procGubn"} codeGroup={"PROC_GUBN"} addOption={l10n("M00233", "선택")} readOnly={readOnlyMode} />
                                    </td>
                                </tr>
                                <tr>
                                    <th className="essential">{l10n("M03449", "처리내용")}</th>
                                    <td colSpan="3">
                                        <ContentsTemplate height={350}>
                                            <SEditor id={"masterP.procEmpModel.procContents"} readOnly={readOnlyMode}/>
                                        </ContentsTemplate>
                                    </td>
                                </tr>
                                <tr>
                                    <th>{l10n("M00359", "첨부파일")}</th>
                                    <td colSpan="3">
                                        <ContentsTemplate id={"contents"}>
                                            <SFileMulti fileGrpId={"masterP.procEmpModel.procFileGrpId"} useFileDelete={!readOnlyMode} useFileAttach={!readOnlyMode} height={'110px'} />
                                        </ContentsTemplate>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        :
                        <table id={'table'} style={{ width: "100%" }} tabletype="th_w100">
                            <tbody>
                                <tr>
                                    <th>{l10n("M01017", "처리자")}</th>
                                    <td style={{ width: '20px' }}>
                                        <SInput id={"masterP.procEmpNm"} readOnly={true} width={150} />
                                    </td>
                                    <th className="essential">{l10n("M03331", "처리구분")}</th>
                                    <td>
                                        <SSelectBox id={"masterP.procGubn"} codeGroup={"PROC_GUBN"} addOption={l10n("M00233", "선택")} />
                                    </td>
                                </tr>
                                <tr>
                                    <th className="essential">{l10n("M03449", "처리내용")}</th>
                                    <td colSpan="3">
                                        <ContentsTemplate height={350}>
                                            <SEditor id={"masterP.procContents"} />
                                        </ContentsTemplate>
                                    </td>
                                </tr>
                                <tr>
                                    <th>{l10n("M00359", "첨부파일")}</th>
                                    <td colSpan="3">
                                        <ContentsTemplate id={"contents"}>
                                            <SFileMulti fileGrpId={"masterP.procFileGrpId"} height={'110px'} />
                                        </ContentsTemplate>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                }
            </React.Fragment>
        )
    }
}

import React, { Component, Fragment } from 'react';
import { observer } from 'mobx-react';
import SButton from 'components/atoms/button/SButton';
import ContentsMiddleTemplate from 'components/template/ContentsMiddleTemplate';
import SDateInput from 'components/override/datepicker/SDateInput'
import SInput from 'components/atoms/input/SInput';
import STextArea from 'components/atoms/input/STextArea';
import SFileMulti from 'components/atoms/file/SFileMulti';
import ContentsTemplate from 'components/template/ContentsTemplate';
import ObjectUtility from 'utils/object/ObjectUtility'
import CommonStore from 'modules/pages/common/service/CommonStore';
import SEditor from 'components/override/editor/SEditor'
import SSelectBox from 'components/atoms/selectbox/SSelectBox';

import SrProcP from 'modules/pages/service/request/todo/popup/srDetail/SrProcP';

@observer
export default class SrConfirmP extends Component {
    render() {
        const { store } = this.props;
        const { l10n } = ObjectUtility;

        const currentEmpNo = CommonStore.userInfo.bempNo;
        

        const isMngEmp = store.mngEmpList ? store.mngEmpList.find(x => x.mngEmpNo == currentEmpNo) : undefined;

        return (
            <React.Fragment>
                {
                    store.masterP.srState == '400' ?
                        <ContentsMiddleTemplate subTitle={l10n("M03407", "처리확인")}>
                            <SButton buttonName={l10n("M03419", "반려")} onClick={() => store.onClickConfirmResult("REJECT")} type={'default'} />
                            <SButton buttonName={l10n("M00045", "완료")} onClick={() => store.onClickConfirmResult("APPROVE")} type={'default'} />
                        </ContentsMiddleTemplate> : <ContentsMiddleTemplate subTitle={l10n("M03407", "처리확인")} />
                }
                {
                    store.masterP.srState == '400' ?
                        <table id={'table'} style={{ width: "100%" }} tabletype="th_w100">
                            <tbody>
                                <tr>
                                    <th>{l10n("M03327", "요청자")}</th>
                                    <td>
                                        <SInput id={"masterP.reqEmpNm"} readOnly={true} width={150} />
                                    </td>
                                </tr>
                                <tr>
                                    <th>{l10n("M00094", "비고")}</th>
                                    <td>
                                        <STextArea id={"masterP.compltContents"} readOnly={false} />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        :
                        <table id={'table'} style={{ width: "100%" }} tabletype="th_w100">
                            <tbody>
                                <tr>
                                    <th>{l10n("M03327", "요청자")}</th>
                                    <td style={{width: '100px'}}>
                                        <SInput id={"masterP.reqEmpNm"} readOnly={true} width={150} />
                                    </td>
                                    <th>{l10n("M03458", "확인일시")}</th>
                                    <td>
                                        <SDateInput id={'masterP.returnDtm'} format={"yyyy-MM-dd HH:mm"} readOnly={true} />
                                    </td>
                                </tr>
                                <tr>
                                    <th>{l10n("M00094", "비고")}</th>
                                    <td colSpan="3">
                                        <STextArea id={"masterP.compltContents"} readOnly={true} />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                }
                <SrProcP store={store} />
            </React.Fragment>
        )
    }
}

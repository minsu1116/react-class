import React, { Component, Fragment } from 'react';
import { observer } from 'mobx-react';
import SButton from 'components/atoms/button/SButton';
import ContentsMiddleTemplate from 'components/template/ContentsMiddleTemplate';
import ObjectUtility from 'utils/object/ObjectUtility'
import STextArea from 'components/atoms/input/STextArea';

import ico_approve from 'style/img/ico_approve.png';
import ico_reject from 'style/img/ico_reject.png';
import SInput from 'components/atoms/input/SInput';
import CommonStore from 'modules/pages/common/service/CommonStore'
import ico_right_arrow from 'style/img/ico_right_arrow.svg';

import ReactTooltip from 'react-tooltip';

                                
@observer
export default class SrApprP extends Component {
    render() {
        const { store } = this.props;
        const { l10n } = ObjectUtility;
        
        let currApprEmpNo = undefined;
        let originApprEmpNo = undefined;
        if(store.masterP.srState == '110' && store.masterP.apprYn == 'Y') {
            currApprEmpNo = store.masterP.apprEmpNo ? store.masterP.apprEmpNo : store.masterP.apprEmpList.filter(x => !x.apprGubn)[0].apprEmpNo;
            originApprEmpNo = store.masterP.originApprEmpNo ? store.masterP.originApprEmpNo : store.masterP.apprEmpList.filter(x => !x.apprGubn)[0].originApprEmpNo;
            store.masterP.apprEmpNm = store.masterP.apprEmpNm ? store.masterP.apprEmpNm : store.masterP.apprEmpList.filter(x => !x.apprGubn)[0].apprEmpNm;
        }

        let gubn = '';

        if(store.masterP.srState > '110') {
            gubn = store.apprEmpList.filter(x => !x.apprGubn || x.apprGubn == 'REJECT').length
        }
    
        const apprEmpListRenter = [];
        if (store.apprEmpList.length > 0) {
            for (let index = 0; index < store.apprEmpList.length; index++) {
                const element = store.apprEmpList[index];

                apprEmpListRenter.push(
                    <React.Fragment key={'appr_' + element.apprEmpNo}>
                        <div data-tip='' data-for={`desc${index}`} className={!element.apprGubn ? (currApprEmpNo == element.apprEmpNo ? "appr_profile_p curr" : "appr_profile_p") : element.apprGubn == 'APPR' ? 'appr_profile_p appr' : 'appr_profile_p reject'}>
                            <ReactTooltip html={true}  backgroundColor={"white"} textColor={'black'} borderColor = {'green'} border = {true}  id={`desc${index}`}>
                                {
                                    `
                                    <div>
                                        <div>
                                            ${l10n("M03446", "승인 여부")} : ${element.apprGubn ? element.apprGubn == 'APPR' ?  l10n("M03418", "승인") : l10n("M03419", "반려") : l10n("M03445", "처리 대기")}
                                        </div>
                                        <div>
                                            ${l10n("M00094", "비고")} : ${element.apprDesc ? element.apprDesc : l10n("M03445", "처리 대기")}
                                        </div>
                                        <div>
                                            ${l10n("M01018", "처리 일시")} : ${element.apprDtm ? element.apprDtm : l10n("M03445", "처리 대기")}
                                        </div>
                                    </div>
                                    `
                                }
                            </ReactTooltip>
                            <div style={{marginTop: '2px'}}>
                                <div className="apprEmpNm" id={element.apprEmpNo}>
                                    {element.apprEmpNm}
                                </div>
                                <div className="apprDeptNm">
                                    {element.deptNm}
                                </div>
                            </div>
                            {(store.apprEmpList.length - 1) != index ? <img className="appr_arrow" src={ico_right_arrow} /> : null}
                        </div>
                    </React.Fragment>
                )
            }
        }

        return (
            <div className="basic">
                <React.Fragment>
                    {
                        store.masterP.srState == '110' && (currApprEmpNo == CommonStore.userInfo.bempNo || originApprEmpNo == CommonStore.userInfo.bempNo) ?
                        <ContentsMiddleTemplate subTitle={l10n("M03431", "승인내역")}>
                                <SButton buttonName={l10n("M03418", "승인")} onClick={() => store.onClickApprResult("APPR", currApprEmpNo)} type={'default'} />
                                <SButton buttonName={l10n("M03419", "반려")} onClick={() => store.onClickApprResult("REJECT", currApprEmpNo)} type={'default'} />
                            </ContentsMiddleTemplate> : 
                            (store.masterP.apprEmpList && store.masterP.apprEmpList.filter(x => !x.apprGubn).length == 0) || store.masterP.apprYn == 'N' ?
                                <ContentsMiddleTemplate subTitle={l10n("M03431", "승인내역")}>
                                    <img className={'reviewImg'} src={gubn == 0 ? ico_approve : ico_reject} />
                                </ContentsMiddleTemplate> :
                                null
                    }
                    <table id={'table'} style={{ width: "100%" }} tabletype="th_w100">
                        <tbody>
                        {
                            store.masterP.srState == '110' ?
                                <React.Fragment>
                                    <tr>
                                        <th>{l10n("M03430", "승인대기자")}</th>
                                        <td>
                                            <SInput id={"masterP.apprEmpNm"} readOnly={true} width={150} />
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>{l10n("M00094", "비고")}</th>
                                        <td colSpan="3">
                                            {
                                                store.masterP.apprGubn ?
                                                    <STextArea id={"masterP.apprDesc"} readOnly={true} /> :
                                                    <STextArea id={"desc"} readOnly={false} />
                                            }
                                        </td>
                                    </tr>
                                </React.Fragment>
                             :
                                null
                            }
                            {
                                apprEmpListRenter ?
                                    <tr>
                                        <th>{l10n("M03370", "승인자 목록")}</th>
                                        <td colSpan="3">
                                            {apprEmpListRenter}
                                        </td>
                                    </tr>
                                    : null
                            }
                        </tbody>
                    </table>
                </React.Fragment>
            </div>
        )
    }
}

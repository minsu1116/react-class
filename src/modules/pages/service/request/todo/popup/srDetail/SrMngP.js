import React, { Component, Fragment } from 'react';
import { observer } from 'mobx-react';
import SButton from 'components/atoms/button/SButton';
import ContentsMiddleTemplate from 'components/template/ContentsMiddleTemplate';
import SDateInput from 'components/override/datepicker/SDateInput'
import STextArea from 'components/atoms/input/STextArea';
import SInput from 'components/atoms/input/SInput';
import ObjectUtility from 'utils/object/ObjectUtility';
import CommonStore from 'modules/pages/common/service/CommonStore';
import ico_approve from 'style/img/ico_approve.png';
import ico_reject from 'style/img/ico_reject.png';

@observer
export default class SrMngP extends Component {
    render() {
        const { store } = this.props;
        const { l10n } = ObjectUtility;

        const currentEmpNo = CommonStore.userInfo.bempNo;
        store.mngEmpNm = CommonStore.userInfo.buserNm;

        // 사업장 별 담당자 리스트 
        const splitMngEmpList = store?.mngEmpList?.filter(x => x?.locCd != '');
        let isMngEmp = '';

        if (splitMngEmpList?.length > 1) {
            //요청자 사업장 코드
            const reqEmpLocCd = store?.masterP.locCd;
            isMngEmp = splitMngEmpList.find(x => x.mngEmpNo == currentEmpNo && x.locCd == reqEmpLocCd);
        } else {
            isMngEmp = store.mngEmpList ? store.mngEmpList.find(x => x.mngEmpNo == currentEmpNo) : undefined;
        }

        return (
            <React.Fragment>
                {
                    store.masterP.srState == '200' ?
                        <ContentsMiddleTemplate subTitle={l10n("M03425", "접수내역")}>
                            {
                                isMngEmp ?
                                    <React.Fragment>
                                        <SButton buttonName={l10n("M00044", "접수")} onClick={() => store.onClickMngResult("APPR")} type={'default'} />
                                        <SButton buttonName={l10n("M03424", "접수취소")} onClick={() => store.onClickMngResult("REJECT")} type={'default'} />
                                    </React.Fragment>
                                    : null
                            }

                        </ContentsMiddleTemplate> :
                        <ContentsMiddleTemplate subTitle={l10n("M03425", "접수내역")}>
                            <img className='reviewImg' src={store.masterP.acceptEmpModel.mngGubn == 'APPR' ? ico_approve : ico_reject} />
                        </ContentsMiddleTemplate>
                }

                {
                    store.masterP.srState >= '200' ?
                        <table id={'table'} style={{ width: "100%" }} tabletype="th_w100">
                            <tbody>
                                <tr>
                                    <th>{l10n("M00519", "접수자")}</th>
                                    <td>
                                        {
                                            store.masterP.acceptEmpModel ?
                                                <SInput id={"masterP.acceptEmpModel.mngEmpNm"} readOnly={true} width={150} /> :
                                                <SInput id={"masterP.mngEmpNm"} readOnly={true} width={150} />
                                        }
                                    </td>
                                    <th>{l10n("M00520", "접수일자")}</th>
                                    <td style={{ width: '69.5%' }}>
                                        {
                                            store.masterP.acceptEmpModel ?
                                                <SDateInput id={'masterP.acceptEmpModel.mngDtm'} format={"yyyy-MM-dd HH:mm"} readOnly={true} /> :
                                                <SDateInput id={'masterP.acceptDtm'} format={"yyyy-MM-dd HH:mm"} readOnly={true} />
                                        }

                                    </td>
                                </tr>
                                <tr>
                                    <th>{l10n("M00094", "비고")}</th>
                                    <td colSpan="3">
                                        {
                                            store.masterP.acceptEmpModel ?
                                                <STextArea id={"masterP.acceptEmpModel.mngDesc"} readOnly={true} /> :
                                                <STextArea id={"desc"} readOnly={false} />
                                        }
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        :
                        null
                }
            </React.Fragment>
        )
    }
}

import React from 'react';
import { observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import { BaseComponent } from "utils/base/BaseComponent";
import { BaseStore, model } from 'utils/base/BaseStore'

import CodeHelper from 'modules/pages/common/codehelper/CodeHelper';
import ContentsMiddleTemplate from 'components/template/ContentsMiddleTemplate';
import ContentsTemplate from 'components/template/ContentsTemplate';
import CommonStore from 'modules/pages/common/service/CommonStore'
import Alert from 'components/override/alert/Alert';

import SCheckBox from 'components/atoms/checkbox/SCheckBox';
import SDatePicker from 'components/override/datepicker/SDatePicker';
import SFileMulti from 'components/atoms/file/SFileMulti';
import SEditor from 'components/override/editor/SEditor'
import SInput from "components/atoms/input/SInput";
import SGrid from 'components/override/grid/SGrid';
import SSelectBox from 'components/atoms/selectbox/SSelectBox';
import SNumericInput from 'components/override/numericinput/SNumericInput';
// 이미지
import ico_sr_ing from 'style/img/sr/ico_sr_ing.png';
import ico_sr_chk from 'style/img/sr/ico_sr_chk.png';
import ico_sr_cpl from 'style/img/sr/ico_sr_cpl.png';
import ico_sr_mng from 'style/img/sr/ico_sr_mng.png';
import ico_right_arrow from 'style/img/ico_right_arrow.svg';
import ico_user from 'style/img/ico_user.svg';
import help from 'style/img/ico_exclamation.svg';
import ReactTooltip from 'react-tooltip';
import moment from 'moment';

import SrDetailMainPModel from "modules/pages/service/request/todo/popup/SrDetailMainPModel";

// 팝업 상세 화면
import SrMngP from 'modules/pages/service/request/todo/popup/srDetail/SrMngP';
import SrApprP from 'modules/pages/service/request/todo/popup/srDetail/SrApprP';
import SrProcP from 'modules/pages/service/request/todo/popup/srDetail/SrProcP';
import SrConfirmP from 'modules/pages/service/request/todo/popup/srDetail/SrConfirmP';


export { SrDetailMainPStore, SrDetailMainP }

class SrDetailMainPStore extends BaseStore {

    @observable gubn = '';
    @observable processImg = undefined;

    @observable @model masterP = new SrDetailMainPModel();
    @observable reqDetailOpen = true;
    @observable categoryDtlOpen = false;
    @observable apprYn = ''


    @observable renderChangeSrState = '';

    // 승인자
    @observable apprEmpList = [];

    // 대상자
    @observable reqEmpList = [];

    // 담당자
    @observable mngEmpList = [];

    @observable categoryDtlList = [];
    @observable expiredYn = false;
    @observable srState = undefined;
    @observable processImgList = undefined;

    srDataKeyList = [];
    dynamicDataKey = new Map();
    dtlObjMap = new Map();
    dtlLabelMap = new Map();
    jsonData = undefined;
    initMode = false;

    @observable isCanceledSr = false;

    initialize = async (param) => {
        if (!param) {
            this.isCanceledSr = true;
        } else {
            this.isCanceledSr = false;
            this.setBind(this.masterP, param);
            this.jsonData = this.masterP.srDtlData ? JSON.parse(this.masterP.srDtlData) : undefined;
            this.apprEmpList = this.masterP.apprEmpList;
            this.reqEmpList = this.masterP.reqEmpList;
            this.mngEmpList = this.masterP.mngEmpList;
            this.expiredYn = this.masterP.expiredYn;
            this.srState = this.masterP.srState;
            this.renderChangeSrState = this.masterP.srState;
            this.setSrImgRender(param.srState);
        }

    }

    setSrImgRender = (param) => {
        const srState = !this.masterP.rejectSrState ? this.masterP.srState : this.masterP.rejectSrState;
        this.processImgList = undefined;

        if (!this.initMode) {
            this.renderChangeSrState = !this.masterP.rejectSrState ? param : this.masterP.rejectSrState;
            this.initMode = true;
        } else {
            this.renderChangeSrState = param;
        }

        switch (srState) {
            case '110':
                this.processImgList =
                    <div className="srProcImg" key={`srProcess${srState}`}>
                        <div className={"active now"} key={`srChk${srState}`}>
                            <img src={ico_sr_chk} key={`srChk${srState}1`} />
                            <span key={`srChk${srState}2`}>{this.l10n("M03406", '요청검토')}</span>
                        </div>
                        <div className={"nonActive"} key={`srMng${srState}`}>
                            <img src={ico_sr_mng} key={`srMng${srState}1`} />
                            <span key={`srMng${srState}2`}>{this.l10n("M03415", '요청접수')}</span>
                        </div>
                        <div className="nonActive" key={`srIng${srState}`}>
                            <img src={ico_sr_ing} key={`srIng${srState}1`} />
                            <span key={`srIng${srState}2`}>{this.l10n("M03416", '요청처리')}</span>
                        </div>
                        <div className="nonActive" key={`srCpl${srState}`}>
                            <img src={ico_sr_cpl} key={`srCpl${srState}1`} />
                            <span key={`srCpl${srState}2`}>{this.l10n("M03417", '처리완료')}</span>
                        </div>
                    </div>
                    ;

                break;
            case '200':
                if (!this.masterP.acceptEmpModel && this.masterP.mngEmpList.filter(x => x.mngEmpNo == CommonStore.userInfo.bempNo).length > 0) {
                    this.masterP.mngEmpNm = CommonStore.userInfo.buserNm;
                    this.masterP.acceptDtm = moment().format("YYYY-MM-DD HH:mm");
                }
                this.processImgList =
                    <div className="srProcImg" key={`srProcess${srState}`}>
                        {
                            this.apprYn == 'Y' ?
                                <div className={param == '110' ? "active now" : "active"} key={`srChk${srState}`} onClick={() => this.setSrImgRender('110')}>
                                    <img src={ico_sr_chk} key={`srChk${srState}1`} />
                                    <span key={`srChk${srState}2`}>{this.l10n("M03406", '요청검토')}</span>
                                </div> :

                                null
                        }
                        <div className={param == '200' || param == '910' ? "active now" : "active"} key={`srMng${srState}`} onClick={() => this.setSrImgRender('200')}>
                            <img src={ico_sr_mng} key={`srMng${srState}1`} />
                            <span key={`srMng${srState}2`}>{this.l10n("M03415", '요청접수')}</span>
                        </div>
                        <div className="nonActive" key={`srIng${srState}`}>
                            <img src={ico_sr_ing} key={`srIng${srState}1`} />
                            <span key={`srIng${srState}2`}>{this.l10n("M03416", '요청처리')}</span>
                        </div>
                        <div className="nonActive" key={`srCpl${srState}`}>
                            <img src={ico_sr_cpl} key={`srCpl${srState}1`} />
                            <span key={`srCpl${srState}2`}>{this.l10n("M03417", '처리완료')}</span>
                        </div>
                    </div>
                    ;
                break;
            // case '210':
            //     this.processImgList =
            //     <div className="srProcImg" key={`srProcess${srState}`}>
            //         <div className={param == '110' ? "active now" : "active"} key={`srChk${srState}`} onClick={() => this.setSrImgRender('110')}>
            //             <img src={ico_sr_chk} key={`srChk${srState}1`}/>
            //             <span key={`srChk${srState}2`}>{this.l10n("M03406", '요청검토')}</span>
            //         </div>
            //         <div className={param == '200' ? "active now" : "active"} key={`srMng${srState}`} onClick={() => this.setSrImgRender('200')}>
            //             <img src={ico_sr_mng} key={`srMng${srState}1`}/>
            //             <span key={`srMng${srState}2`}>{this.l10n("M03415", '요청접수')}</span>
            //         </div>
            //         <div className="nonActive" key={`srIng${srState}`}>
            //             <img src={ico_sr_ing} key={`srIng${srState}1`}/>
            //             <span key={`srIng${srState}2`}>{this.l10n("M03416", '요청처리')}</span>
            //         </div>
            //         <div className="nonActive" key={`srCpl${srState}`}>
            //             <img src={ico_sr_cpl} key={`srCpl${srState}1`}/>
            //             <span key={`srCpl${srState}2`}>{this.l10n("M03417", '처리완료')}</span>
            //         </div>
            //     </div>
            //     ;
            //     break;
            case '300':
            case '310':
                if (!this.masterP.procEmpModel) {
                    this.masterP.procEmpNm = CommonStore.userInfo.buserNm;
                }
                this.processImgList =
                    <div className="srProcImg" key={`srProcess${srState}`}>
                        {
                            this.apprYn == 'Y' ?
                                <div className={param == '110' ? "active now" : "active"} key={`srChk${srState}`} onClick={() => this.setSrImgRender('110')}>
                                    <img src={ico_sr_chk} key={`srChk${srState}1`} />
                                    <span key={`srChk${srState}2`}>{this.l10n("M03406", '요청검토')}</span>
                                </div> :

                                null
                        }
                        <div className={param == '200' ? "active now" : "active"} key={`srMng${srState}`} onClick={() => this.setSrImgRender('200')}>
                            <img src={ico_sr_mng} key={`srMng${srState}1`} />
                            <span key={`srMng${srState}2`}>{this.l10n("M03415", '요청접수')}</span>
                        </div>
                        <div className={param == '300' || param == '310' ? "active now" : "active"} key={`srIng${srState}`} onClick={() => this.setSrImgRender('300')}>
                            <img src={ico_sr_ing} key={`srIng${srState}1`} />
                            <span key={`srIng${srState}2`}>{this.l10n("M03416", '요청처리')}</span>
                        </div>
                        <div className="nonActive" key={`srCpl${srState}`}>
                            <img src={ico_sr_cpl} key={`srCpl${srState}1`} />
                            <span key={`srCpl${srState}2`}>{this.l10n("M03417", '처리완료')}</span>
                        </div>
                    </div>
                    ;
                break;
            case '400':
                this.processImgList =
                    <div className="srProcImg" key={`srProcess${srState}`}>
                        {
                            this.apprYn == 'Y' ?
                                <div className={param == '110' ? "active now" : "active"} key={`srChk${srState}`} onClick={() => this.setSrImgRender('110')}>
                                    <img src={ico_sr_chk} key={`srChk${srState}1`} />
                                    <span key={`srChk${srState}2`}>{this.l10n("M03406", '요청검토')}</span>
                                </div> :

                                null
                        }
                        <div className={param == '200' ? "active now" : "active"} key={`srMng${srState}`} onClick={() => this.setSrImgRender('200')}>
                            <img src={ico_sr_mng} key={`srMng${srState}1`} />
                            <span key={`srMng${srState}2`}>{this.l10n("M03415", '요청접수')}</span>
                        </div>
                        <div className={param == '300' ? "active now" : "active"} key={`srIng${srState}`} onClick={() => this.setSrImgRender('300')}>
                            <img src={ico_sr_ing} key={`srIng${srState}1`} />
                            <span key={`srIng${srState}2`}>{this.l10n("M03416", '요청처리')}</span>
                        </div>
                        <div className={param == '400' ? "active now" : "active"} key={`srCpl${srState}`} onClick={() => this.setSrImgRender('400')}>
                            <img src={ico_sr_cpl} key={`srCpl${srState}1`} />
                            <span key={`srCpl${srState}2`}>{this.l10n("M03417", '처리완료')}</span>
                        </div>
                    </div>
                    ;
                break;
            case '900':
                this.processImgList =
                    <div className="srProcImg" key={`srProcess${srState}`}>
                        {
                            this.apprYn == 'Y' ?
                                <div className={param == '110' ? "active now" : "active"} key={`srChk${srState}`} onClick={() => this.setSrImgRender('110')}>
                                    <img src={ico_sr_chk} key={`srChk${srState}1`} />
                                    <span key={`srChk${srState}2`}>{this.l10n("M03406", '요청검토')}</span>
                                </div> :

                                null
                        }
                        <div className={param == '200' ? "active now" : "active"} key={`srMng${srState}`} onClick={() => this.setSrImgRender('200')}>
                            <img src={ico_sr_mng} key={`srMng${srState}1`} />
                            <span key={`srMng${srState}2`}>{this.l10n("M03415", '요청접수')}</span>
                        </div>
                        <div className={param == '300' ? "active now" : "active"} key={`srIng${srState}`} onClick={() => this.setSrImgRender('300')}>
                            <img src={ico_sr_ing} key={`srIng${srState}1`} />
                            <span key={`srIng${srState}2`}>{this.l10n("M03416", '요청처리')}</span>
                        </div>
                        <div className={param == '400' || param == '900' ? "active now" : "active"} key={`srCpl${srState}`} onClick={() => this.setSrImgRender('400')}>
                            <img src={ico_sr_cpl} key={`srCpl${srState}1`} />
                            <span key={`srCpl${srState}2`}>{this.l10n("M03417", '처리완료')}</span>
                        </div>
                    </div>
                    ;
                break;

            default:
                break;
        }
    }

    loaded = async () => {
        if (this.masterP.srId && this.masterP.srId != "") {
            // 업무 화면 구성
            await this.handleCategoryRender();

            // 검토자 Set
            if (this.masterP.srReviewEmpModel) {
                this.masterP.reviewEmpNo = this.masterP.srReviewEmpModel.reviewEmpNo;
                this.masterP.reviewEmpNm = this.masterP.srReviewEmpModel.reviewEmpNm;
            }

            // 대상자 Set
            if (this.masterP.reqEmpList.length > 0) {
                this.reqEmpList = this.masterP.reqEmpList;
                this.reqEmpGrid.setRowData(this.masterP.reqEmpList);
            }

            // 업무 상세 정보 바인딩
            if (this.masterP.srDtlData) {
                const jsonData = JSON.parse(this.masterP.srDtlData);
                Object.keys(jsonData).forEach(function (k) {
                    this[k] = jsonData[k];
                }, this);
            }
        }
    }

    setReqEmpGridApi = async (grid) => {
        this.reqEmpGrid = grid;

        const columnDefs = [
            { word: "M00674", name: "사번", field: "reqEmpNo", width: 90, align: 'center', key: true, hide: true },
            { word: "M00677", name: "부서명", field: "deptNm", width: 120 },
            { word: "M00979", name: "이름", field: "reqEmpNm", width: 90, align: 'center' },
            { word: "M00420", name: "이메일", field: "email", width: 150 },
            { word: "M00679", name: "전화번호", field: "officeNo", width: 90 },
            // { word: "M00433", name: "연락처(모바일)", field: "mobileNo", width: 135 },
        ];

        this.reqEmpGrid.setColumnDefs(columnDefs);
    }

    // 업무별 양식서 불러오기
    handleCategoryRender = async () => {
        const param = {
            useYn: 'Y',
            categoryId: this.masterP.categoryId
        }
        const params = toJS(param)
        const data = await this.execute(`/service/request/reg/category-render/get`, params);

        this.categoryDtlList = data.data.categoryMngList;

        if (!this.masterP.srId) {
            this.apprEmpList = data.data.apprMngList;
        }

        if (this.categoryDtlList.length > 0) {
            const splitObj = this.categoryDtlList[0];

            const cdList = splitObj.splitGubnCd.split(',');
            const cdNmList = splitObj.splitGubnCdNm.split(',');
            const typeList = splitObj.splitGubnType.split(',');

            for (let index = 0; index < cdList.length; index++) {
                const selectList = [];
                if (typeList[index] == 'S') {
                    this.setBind(String.toString(cdList[index]), '')
                    this.dynamicDataKey.set(cdList[index], 'S');
                    this.categoryDtlList.filter(x => x.dtlGubnCd == cdList[index]).forEach(element => {
                        const tempObj = {
                            cd: element.categoryDtlCd,
                            cdNm: element.categoryDtlCdNm
                        }

                        selectList.push(tempObj);
                    });
                } else {
                    this.dtlObjMap.set(cdList[index], this.categoryDtlList.filter(x => x.dtlGubnCd == cdList[index]))
                    this.categoryDtlList.filter(x => x.dtlGubnCd == cdList[index]).forEach(element => {
                        this.setBind(String.toString(element.categoryDtlCd), '');
                        this.dynamicDataKey.set(element.categoryDtlCd, 'C');
                    });
                }

                const element = {
                    'dtlGubnCd': cdList[index],
                    'dtlGubnCdNm': cdNmList[index],
                    'dtlGubnType': typeList[index],
                    'splitRow': typeList[index] == 'C' ? (this.categoryDtlList.filter(x => x.dtlGubnCd == cdList[index]).length / 2) : null,
                    'selectGroup': selectList
                }
                this.dtlLabelMap.set(cdList[index], element);
            }
            this.categoryDtlOpen = true;
        } else {
            this.categoryDtlOpen = false
        }
    }

    onClickApprResult = async (param, currApprEmpNo) => {
        const params = {
            apprGubn: param,
            apprEmpNo: currApprEmpNo,
            apprDesc: this.desc,
            srId: this.masterP.srId,
            categoryId: this.masterP.categoryId,
            srState: '110'
        }

        const result = await Alert.confirm(String.format(this.l10n("M03421", "{0} 하시겠습니까?"), param == 'APPR' ? this.l10n("M03418", "승인") : this.l10n("M03419", "반려")));
        if (!result) {
            return;
        }

        this.close(params);
    }

    // 요청 정보 상세페이지 열기
    onClickDetailOpen = () => {
        this.reqDetailOpen = !this.reqDetailOpen;
    }

    onClickMngResult = async (param) => {
        const params = {
            mngGubn: param,
            mngEmpNo: CommonStore.userInfo.bempNo,
            mngEmpNm: CommonStore.userInfo.buserNm,
            mngDesc: this.desc,
            srState: '300',

        }

        const result = await Alert.confirm(String.format(this.l10n("M03421", "{0} 하시겠습니까?"), param == 'APPR' ? this.l10n("M00044", "접수") : this.l10n("M03424", "접수취소")));
        if (!result) { return; }

        this.close(params)

    }

    onClickProcResult = async () => {

        if (!this.masterP.procGubn || !this.masterP.procContents) {
            await Alert.meg(this.l10n("M03343", "필수입력항목을 확인하여 주십시오."));
            return false;
        }

        const params = {
            srId: this.masterP.srId,
            procGubn: this.masterP.procGubn,
            procContents: this.masterP.procContents,
            procFileGrpId: undefined,
            srState: '400'
        }

        const result = await Alert.confirm(this.l10n("M03450", "처리를 완료하시겠습니까?"));
        if (!result) { return; }

        const fileGrpId = await this.fileGroupSave();
        if (fileGrpId != false || fileGrpId != "undefined") {
            params.procFileGrpId = fileGrpId;
        }

        this.close(params);
    }

    onClickConfirmResult = async (param) => {
        const params = {
            srId: this.masterP.srId,
            compltContents: this.masterP.compltContents,
            srState: param == "APPROVE" ? '900' : '310'
        }

        const result = await Alert.confirm(String.format(this.l10n("M03421", "{0} 하시겠습니까?"), param == 'APPROVE' ? this.l10n("M00045", "완료") : this.l10n("M03419", "반려")));
        if (!result) {
            return;
        }

        this.close(params);
    }

}

@observer
export default class SrDetailMainP extends BaseComponent {

    sRender() {
        const dynamicRenderList = [];
        if (this.store.categoryDtlOpen) {
            if (this.store.expiredYn) {
                dynamicRenderList.push(
                    <tr rowSpan='2' key={'expiredDtm'}><th className="essential">{this.l10n("M03397", "만료기간")}</th>
                        <td colSpan="3">
                            <SDatePicker id={'masterP.reqExpiredFromDtm'} format={'yyyy-MM-dd'} readOnly={true} />
                            <SDatePicker id={'masterP.reqExpiredToDtm'} format={'yyyy-MM-dd'} title={'~'} readOnly={true} />
                        </td>
                    </tr>
                )
            }

            let selectList = [];
            this.store.dtlLabelMap.forEach((v, k) => {
                /** 
                 * 업무별 상세 페이지 만들기
                 * v.dtlGubnType = { S : 셀렉트 박스, C : 체크 박스, I : INPUT 박스, NI : 숫자 INPUT 박스 -> 필요시 맞는 컴포넌트랑 매핑해주면 됨}
                 */
                switch (v.dtlGubnType) {
                    case 'S':
                        selectList.push(
                            <React.Fragment key={`${v.dtlGubnCd}`}>
                                <th className="essential">{v.dtlGubnCdNm}</th><td><SSelectBox id={`${k}`} optionGroup={v.selectGroup} valueMember={"cd"} displayMember={"cdNm"} readOnly={true} addOption={this.l10n("M00233", "선택")} /></td>
                            </React.Fragment>
                        );
                        if (selectList.length == 2) {
                            dynamicRenderList.push(<tr key={`${v.dtlGubnCd}`}>{selectList}</tr>)
                            selectList = [];
                        }
                        break;
                    case 'C':
                        const checkList = this.store.dtlObjMap.get(k);
                        if (checkList) {
                            const checkBoxList = [];
                            checkList.forEach(element => {
                                checkBoxList.push(<SCheckBox key={element.categoryDtlCd} dynamicData={this.store.jsonData ? this.store.jsonData[element.categoryDtlCd] : ''} id={element.categoryDtlCd} title={element.categoryDtlCdNm} readOnly={true} />)
                            });
                            dynamicRenderList.push(
                                <tr rowSpan='2' key={`${v.dtlGubnCd}`}><th className="essential">{v.dtlGubnCdNm}</th><td colSpan="3">
                                    {checkBoxList}
                                </td></tr>
                            )
                        }
                        break;
                    case 'I':
                        inputList.push(
                            <React.Fragment key={`${v.dtlGubnCd}`}>
                                <th className="essential">{v.dtlGubnCdNm}</th><td><SInput id={`${k}`} width={200} readOnly={true} /></td>
                            </React.Fragment>
                        );
                        if (inputList.length == 2) {
                            dynamicRenderList.push(<tr key={`${v.dtlGubnCd}`}>{inputList}</tr>)
                            inputList = [];
                        }
                        break;
                    case 'NI':
                        inputList.push(
                            <React.Fragment key={`${v.dtlGubnCd}`}>
                                <th className="essential">{v.dtlGubnCdNm}</th><td><SNumericInput id={`${k}`} width={200} readOnly={true} decimalScale={3} /></td>
                            </React.Fragment>
                        );
                        if (inputList.length == 2) {
                            dynamicRenderList.push(<tr key={`${v.dtlGubnCd}`}>{inputList}</tr>)
                            inputList = [];
                        }
                        break;
                }
            })

            if (selectList.length > 0) {
                dynamicRenderList.push(<tr key={`dynamic`}>{selectList}</tr>)
            }
        }

        const apprEmpListRenter = [];
        if (this.store.apprEmpList.length > 0) {
            for (let index = 0; index < this.store.apprEmpList.length; index++) {
                const element = this.store.apprEmpList[index];
                apprEmpListRenter.push(
                    <React.Fragment key={'appr_' + element.apprEmpNo}>
                        <div className="appr_profile">
                            <div>
                                <img className="apprIcon" src={ico_user} />
                                <div className="apprEmpNm" id={element.apprEmpNo}>
                                    {element.apprEmpNm}
                                </div>
                                <div className="apprDeptNm">
                                    {element.deptNm}
                                </div>
                            </div>
                            {(this.store.apprEmpList.length - 1) != index ? <img className="appr_arrow" src={ico_right_arrow} /> : null}
                        </div>
                    </React.Fragment>
                )
            }
        }

        return (
            <div className="basic">
                {
                    this.store.isCanceledSr ?
                        <div style={{ fontSize: '30px', fontWeight: 600, color: '#cd1616ba' }}>
                            {this.l10n("M03465", "해당 서비스는 요청자에 의해 취소되었습니다.")}
                        </div>
                        :
                        <React.Fragment>
                            {this.store.processImgList ? this.store.processImgList : null}

                            {/* 요청 검토 */}
                            {
                                this.store.renderChangeSrState == '110' && this.store.apprYn == 'Y' ?
                                    <SrApprP store={this.store} />
                                    : null
                            }

                            {/* 요청 접수 */}
                            {
                                this.store.renderChangeSrState == '200' ?
                                    <SrMngP store={this.store} />
                                    : null
                            }

                            {/* 요청 처리 */}
                            {
                                this.store.renderChangeSrState == '300' || this.store.renderChangeSrState == '310' ?
                                    <SrProcP store={this.store} />
                                    : null
                            }

                            {/* 처리 확인 */}
                            {
                                this.store.renderChangeSrState == '400' || this.store.renderChangeSrState == '900' ?
                                    <SrConfirmP store={this.store} />
                                    : null
                            }

                            <React.Fragment>
                                <ContentsMiddleTemplate subTitle={"요청내역"} />
                                <div>
                                    <table id={'table'} style={{ width: "100%" }}>
                                        <tbody>
                                            <tr>
                                                <th>{this.l10n("M00572", "요청일자")}</th>
                                                <td>
                                                    <SDatePicker id={"masterP.reqDtm"} readOnly={true} contentWidth={80} readOnly={true} />
                                                </td>
                                                <th className="essential">{this.l10n("M03327", "요청자")}</th>
                                                <td className='popupText'>
                                                    <CodeHelper helperCode={"masterP.reqEmpNo"} helperCodeNm={"masterP.reqEmpNm"} mainStore={this.store} business={"user"} readOnly={true} onResult={this.store.handleUserHelperResult} codeVisible={false} />
                                                </td>
                                            </tr>
                                            <tr>
                                                <th className="essential">{this.l10n("M03332", "완료요구일")}</th>
                                                <td>
                                                    <SDatePicker id={"masterP.endDemandDtm"} readOnly={true} />
                                                </td>
                                                <th className="essential">{this.l10n("M03328", "IT업무")}</th>
                                                {/* <th className="essential">{this.l10n("M03331", "처리구분")}</th> */}
                                                <td>
                                                    {
                                                        this.store.masterP.categoryDesc && this.store.masterP.categoryDesc != "" ?
                                                            <div className="cty_help" data-tip='' data-for={'categoryDesc'}>
                                                                <img src={help} />
                                                                <ReactTooltip html={true} backgroundColor={"white"} textColor={'black'} borderColor={'green'} border={true} id={'categoryDesc'}>
                                                                    {this.store.masterP.categoryDesc}
                                                                </ReactTooltip>
                                                            </div> :
                                                            null
                                                    }
                                                    <CodeHelper helperCode={"masterP.categoryId"} helperCodeNm={"masterP.categoryNm"} mainStore={this.store} business={"sysCategory"} onResult={this.store.handleSysCategoryHelperResult} codeVisible={false} codeNmWidth={220} readOnly={true} />
                                                </td>
                                            </tr>
                                            {
                                                dynamicRenderList ? dynamicRenderList
                                                    : null
                                            }
                                            <tr>
                                                <th className="essential">{this.l10n("M00348", "제목")}</th>
                                                <td colSpan="3">
                                                    <SInput id={"masterP.reqTitle"} width={500} readOnly={true} />
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div>
                                    <ContentsTemplate height={"auto"} isShadow={true}>
                                        <SEditor id={"masterP.reqContents"} readOnly={true} />
                                    </ContentsTemplate>
                                </div>
                                <div>
                                    {
                                        this.store.masterP.attachFileGrpId ?
                                            <React.Fragment>
                                                <div className="ratio_w65 ratio_h50">
                                                    <ContentsMiddleTemplate subTitle={this.l10n("M03394", "신청자 목록")} />
                                                    <ContentsTemplate height={110}>
                                                        <SGrid grid={'reqEmpGrid'} gridApiCallBack={this.store.setReqEmpGridApi} editable={false} />
                                                    </ContentsTemplate>
                                                </div>
                                                <div className="ratio_w35 ratio_h50">
                                                    <ContentsTemplate id={"contents"}>
                                                        <SFileMulti fileGrpId={"masterP.attachFileGrpId"} useFileDelete={false} useFileAttach={false} height={'110px'} />
                                                    </ContentsTemplate>
                                                </div>
                                            </React.Fragment>
                                            :
                                            <React.Fragment>
                                                <ContentsMiddleTemplate subTitle={this.l10n("M03394", "신청자 목록")} />
                                                <ContentsTemplate height={118}>
                                                    <SGrid grid={'reqEmpGrid'} gridApiCallBack={this.store.setReqEmpGridApi} editable={false} />
                                                </ContentsTemplate>
                                            </React.Fragment>
                                    }
                                </div>
                            </React.Fragment>
                        </React.Fragment>
                }
            </div>
        )
    }
}

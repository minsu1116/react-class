import { BaseModel, essential } from "utils/base/BaseModel";
import { observable } from "mobx";
import moment from 'moment';

export default class SrDetailMainPModel extends BaseModel {

    @observable @essential("요청일자") reqDtm;
    @observable @essential("요청자") reqEmpNo;
    @observable @essential("완료요구일") endDemandDtm;
    
    @observable @essential("업무ID") categoryId;
    @observable categoryNm;
    @observable srDtlData;
    
    @observable @essential("제목") reqTitle;
    @observable @essential("내용") reqContents;
    
    @observable @essential("요청승인자") reviewEmpNo;
    @observable @essential("대상자") reqEmpList = [];
    
    @observable reviewEmpNm;
    @observable reviewDtm;
    @observable apprYn;
    @observable attachFileGrpId;

    @observable acceptEmpModel = new Object();
    @observable procEmpModel = new Object();

    @observable srId;
    @observable srState;
    @observable parentSrId;

    
    @observable reqExpiredFromDtm = moment().format("YYYYMMDD");
    @observable reqExpiredToDtm = moment().add(30, 'days').format("YYYYMMDD");
    @observable useYn;
    @observable reqEmpNm;
    
    // 승인자 리스트
    apprEmpList = [];

    // 검토자
    srReviewEmpModel = {};

    fileContsId;

    popupMode = '';

}
import React from 'react';
import { observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import { BaseComponent } from 'utils/base/BaseComponent'
import { BaseStore } from 'utils/base/BaseStore'
import ModalManage from 'utils/base/ModalManage';

import SearchTemplate from 'components/template/SearchTemplate';
import ContentsTemplate from 'components/template/ContentsTemplate';
import ContentsMiddleTemplate from 'components/template/ContentsMiddleTemplate';
import SInput from 'components/atoms/input/SInput';
import SButton from 'components/atoms/button/SButton';

import SGrid from 'components/override/grid/SGrid';
import Alert from 'components/override/alert/Alert'
import SDatePicker from 'components/override/datepicker/SDatePicker';

import CommonStore from 'modules/pages/common/service/CommonStore'
import CodeHelper from 'modules/pages/common/codehelper/CodeHelper';
import SvcReqRegModel from 'modules/pages/service/request/reg/SvcReqRegModel'

export { SvcReqReg, SvcReqRegStore }

class SvcReqRegStore extends BaseStore {

    @observable master = new SvcReqRegModel();
    @observable popupMode = "";
    @observable reqEmpReadOnly = true;

    loaded = async () => {
        await this.handleSearchClick();
    }

    setGridApi = async (grid) => {
        this.srGrid = grid;

        const columnDefs = [
            { field: "check", type: 'checkboxAll' },
            { field: "srId", width: 0, hide: true },
            { word: "M03351", name: "업무명", field: "categoryNm", width: 180 },
            { word: "M00348", name: "제목", field: "reqTitle", width: 230 },
            { word: "M03330", name: "처리상태", field: "srState", width: 120, align: 'center', codeGroup: "SR_PROC_STAT", tooltipField: "srId" },
            { word: "M00572", name: "요청일자", field: "reqDtm", width: 110, align: 'center', type: "dateTime" },
            { word: "M03338", name: "처리대기자", field: "unprocEmpNm", width: 130, align: 'center' },
            { word: "M00522", name: "완료일자", field: "endDtm", width: 110, align: 'center', type: "dateTime" },
            { word: "M03327", name: "요청자", field: "reqEmpNm", width: 80, align: 'center' },
        ];

        const rowStyle = function (param) {
            switch (param.data.srState) {
                case "910": return { background: "#f5d6d6" }
                case "900": return { background: "#E4F5DF" }
            }
        }

        this.setRowStyle(columnDefs, rowStyle);
        this.srGrid.setColumnDefs(columnDefs);
    }

    handleSearchClick = async () => {
        const params = toJS(this.master);
        const result = await this.execute(`/service/request/reg/get`, params);
        let unprocEmpList = [];
        if (result.success) {
            result.data.forEach(element => {
                switch (element.srState) {
                    case '110':
                        unprocEmpList = element.apprEmpList.filter(x => x.apprGubn == null);
                        if (unprocEmpList && unprocEmpList.length > 0 && element.apprYn == 'Y') {
                            element.unprocEmpNm = unprocEmpList.reduce(function (prev, curr) {
                                return prev.sort < curr.sort ? prev : curr;
                            }).apprEmpNm;
                        } else {

                        }
                        break;
                    case '200':
                        unprocEmpList = element.mngEmpList.filter(x => x.headYn == 'Y');
                        element.unprocEmpNm = unprocEmpList[0].mngEmpNm;
                        break;
                    case '300':
                        element.unprocEmpNm = element.mngEmpNm;
                        break;
                    case '310':
                        element.unprocEmpNm = element.procEmpModel.procEmpNm;
                        break;
                    case '400':
                        element.unprocEmpNm = element.reqEmpNm;
                        break;
                    case '900':
                        element.unprocEmpNm = this.l10n("M00045", '완료');
                        break;
                }
            });

            this.srGrid.setRowData(result.data);
        }

    }

    handleAddClick = async () => {
        const params = {
        }
        this.popupMode = "I";
        this.modalOpenAdd(params);
    }

    handleCopyClick = async () => {
        const selectedRow = this.srGrid.getSelectedRow();

        if (!selectedRow) {
            await Alert.meg(this.l10n("M01012")); //항목을 선택해주세요.
            return;
        }

        if (selectedRow.reqEmpNo != CommonStore.userInfo.bempNo) {
            await Alert.meg(this.l10n("M03221")); //본인의 서비스 요청만 복사할 수 있습니다.
            return;
        }

        const params = {
            srId: selectedRow.srId,
        }
        this.popupMode = "C";
        this.modalOpenAdd(params);
    }

    handleRowDoubleClick = async () => {

        const selectedRow = this.srGrid.getSelectedRow();
        this.popupMode = "U";
        this.modalOpenAdd(selectedRow);
    }

    handleDeleteClick = async () => {
        const deleteParams = this.srGrid.getCheckedRows();

        if (!deleteParams || deleteParams.length <= 0) {
            await Alert.meg(this.l10n("M01446")); //삭제 할 항목을 체크하여 주십시오.
            return;
        }

        if (deleteParams && deleteParams.filter(x => x.srState >= '300').length > 0) {
            await Alert.meg(this.l10n("M03466")); //처리 이후부터는 삭제를 할 수 없습니다.
            return;
        }

        for (let i = 0; i < deleteParams.length; i++) {
            if (deleteParams[i].reqEmpNo != CommonStore.userInfo.bempNo) {
                await Alert.meg(this.l10n("M02578")); //요청자만 삭제 가능합니다.
                return;
            }
        }

        const params = toJS(this.master);
        //삭제
        await this.srGrid.deleteCheckedItems('/service/request/reg/delete', params);
    }

    modalOpenAdd = async (params) => {
        const modal = new ModalManage();
        modal.id = "service_request_reg_save_p";
        modal.path = "/service/request/reg/popup/SvcReqRegSaveP";
        params.popupMode = this.popupMode;

        if (this.popupMode == "I" || this.popupMode == "C") {
            modal.title = this.l10n("M03339"); //서비스요청 등록
            modal.parameter = params;
        } else if (this.popupMode == "U") {
            modal.parameter = { 'masterP': params };
            if (params.srState < "200" && (params.apprYn == 'Y' && params?.apprEmpList.filter(x => x.apprGubn == 'APPR' || x.apprGubn == 'REJECT').length == 0)) {
                modal.title = this.l10n("M03340"); //서비스요청 수정
            } else {
                modal.title = this.l10n("M03341"); //서비스요청 상세보기
                const selectedRow = this.srGrid.getSelectedRow();
                modal.id = "service_request_todo_sr_detail_main_p";
                modal.path = "/service/request/todo/popup/SrDetailMainP";

                modal.parameter = selectedRow;
                params.popupMode = this.popupMode;
            }
        }
        modal.width = 1133;
        const result = await this.showModal(modal);
        if (result) {
            await this.handleSearchClick();
        }
    }

    handleSysCategoryHelperResult = async (result) => {
        this.master.sysId = result ? result.sysId : "";
        this.master.sysCategoryId = result ? result.sysCategoryId : "";
        this.master.mgrEmpNo = result ? result.mgrEmpNo : "";
        this.master.mgrEmpNm = result ? result.mgrEmpNm : "";
    }
}

@observer
class SvcReqReg extends BaseComponent {

    sRender() {
        return (
            <React.Fragment>
                <SearchTemplate>
                    <div className='search_item_btn'>
                        <SButton buttonName={this.l10n("M00011", "조회")} onClick={this.store.handleSearchClick} type={'default'} />
                    </div>
                    <div className='search_line'>
                        <SDatePicker id={"master.reqFromDtm"} title={this.l10n("M03326", "요청기간")} />
                        <SDatePicker id={"master.reqToDtm"} title={'~'} />
                        <CodeHelper helperCode={"master.reqEmpNo"} codeNmWidth={120} helperCodeNm={"master.reqEmpNm"} mainStore={this.store} business={"user"} readOnly={this.store.reqEmpReadOnly} title={this.l10n("M03327", "요청자")} codeVisible={false} />
                    </div>
                    <div className='search_line'>
                        <SInput id={"master.reqTitle"} enter={this.store.handleSearchClick} width={244} title={this.l10n("M00348", "제목")} />
                        <CodeHelper helperCode={"master.categoryId"} codeNmWidth={120} helperCodeNm={"master.categoryNm"} mainStore={this.store} business={"sysCategory"} onResult={this.store.handleSysCategoryHelperResult} title={this.l10n("M03328", "IT업무")} codeVisible={false} />
                        {/* <STreeSelectBox id={"master.srProcStat"} codeGroup={"SR_PROC_STAT"} width={244} title={this.l10n("M03330", "처리상태")} />
                        <STreeSelectBox id={"master.srProcType"} codeGroup={"SR_PROC_TYPE"} width={244} title={this.l10n("M03331", "처리구분")} /> */}
                        {/* <label className="item_lb w90">{this.l10n("M02457", "처리상태")}</label>
                        <SSelectBox id={"master.srProcStat"} codeGroup={"SR_PROC_STAT"} addOption={this.l10n("M00322")} />
                        <label className="item_lb w90" style={{ width: "107px" }}>{this.l10n("M00617", "처리구분")}</label>
                        <SSelectBox id={"master.srProcType"} codeGroup={"SR_PROC_TYPE"} addOption={this.l10n("M00322")} /> */}
                    </div>
                </SearchTemplate>
                <ContentsMiddleTemplate>
                    <SButton buttonName={this.l10n("M00983", "추가")} onClick={this.store.handleAddClick} type={'add'} />
                    {/* <SButton buttonName={this.l10n("M03329", "복사")} onClick={this.store.handleCopyClick} type={'copy'} /> */}
                    <SButton buttonName={this.l10n("M00003", "삭제")} onClick={this.store.handleDeleteClick} type={'delete'} />
                </ContentsMiddleTemplate>
                <ContentsTemplate>
                    <SGrid grid={'srGrid'} gridApiCallBack={this.store.setGridApi} rowDoubleClick={this.store.handleRowDoubleClick} editable={false} />
                </ContentsTemplate>
            </React.Fragment>
        )
    }
}


import { BaseModel } from "utils/base/BaseModel";
import { observable } from "mobx";
import CommonStore from "modules/pages/common/service/CommonStore";

export default class SrUserSelectPModel extends BaseModel {

    @observable compCd = CommonStore.compCd;
    
    apprEmpList = [];

}
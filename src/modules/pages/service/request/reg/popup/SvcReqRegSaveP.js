import React from 'react';
import { observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import { BaseComponent } from "utils/base/BaseComponent";
import SInput from "components/atoms/input/SInput";
import SvcReqRegSavePModel from "modules/pages/service/request/reg/popup/SvcReqRegSavePModel";
import SSelectBox from 'components/atoms/selectbox/SSelectBox';
import SDatePicker from 'components/override/datepicker/SDatePicker';
import SButton from 'components/atoms/button/SButton';
import { BaseStore, model } from 'utils/base/BaseStore'
import ContentsMiddleTemplate from 'components/template/ContentsMiddleTemplate';
import CommonStore from 'modules/pages/common/service/CommonStore';
import CodeHelper from 'modules/pages/common/codehelper/CodeHelper';
import moment from 'moment';
import SGrid from 'components/override/grid/SGrid';
import SCheckBox from 'components/atoms/checkbox/SCheckBox';
import SEditor from 'components/override/editor/SEditor'
import ContentsTemplate from 'components/template/ContentsTemplate';
import SFileMulti from 'components/atoms/file/SFileMulti';
import Alert from 'components/override/alert/Alert'
import ModalManage from 'utils/base/ModalManage';
import help from 'style/img/ico_exclamation.svg';
import ReactTooltip from 'react-tooltip';
import SNumericInput from 'components/override/numericinput/SNumericInput';

import ico_right_arrow from 'style/img/ico_right_arrow.svg';
import ico_user_delete from 'style/img/btn_pop_close.png';


export { SvcReqRegSaveP, SvcReqRegSavePStore }

class SvcReqRegSavePStore extends BaseStore {

    @observable @model masterP = new SvcReqRegSavePModel();
    @observable @model argsParam = new SvcReqRegSavePModel();
    @observable popupMode = "";
    @observable saveBtnVisible = "";
    @observable approvalVisiable = "none";
    @observable categoryDtlOpen = false;
    @observable apprYn = ''

    @observable apprEmpList = [];
    @observable reqEmpList = [];
    @observable categoryDtlList = [];
    @observable expiredYn = false;

    reqEmpIsMe = true;
    readOnlyMode = false;

    srDataKeyList = [];
    dynamicDataKey = new Map();
    dtlObjMap = new Map();
    dtlLabelMap = new Map();
    jsonData = undefined;

    initialize = async (params) => {
        this.setBind(this.masterP, params);
        this.setBind(this.argsParam, params);
        this.jsonData = this.masterP.srDtlData ? JSON.parse(this.masterP.srDtlData) : undefined;

        if (this.masterP.srId && this.masterP.srId != "") { 
            this.reqEmpIsMe = CommonStore.userInfo.bempNo == this.masterP.reqEmpNo ? true : false;
            this.readOnlyMode = !this.reqEmpIsMe;
            this.apprEmpList = this.masterP.apprEmpList;
            this.apprYn = this.masterP.apprYn;
            this.expiredYn = this.masterP.expiredYn;
            this.reqEmpList  = this.masterP.reqEmpList;
        }

        this.popupMode = this.masterP.popupMode;
    }

    loaded = async () => {
        if (this.masterP.srId && this.masterP.srId != "") {
            // 업무 화면 구성
            await this.handleCategoryRender();

            // // 검토자 Set
            // if (this.masterP.srReviewEmpModel) {
            //     this.masterP.reviewEmpNo = this.masterP.srReviewEmpModel.reviewEmpNo;
            //     this.masterP.reviewEmpNm = this.masterP.srReviewEmpModel.reviewEmpNm;
            // }



            // 신청자 그리드 셋
            if(this.reqEmpList.length > 0) {
                this.reqEmpGrid.setRowData(this.reqEmpList);
            }

            // 업무 상세 정보 바인딩
            if (this.masterP.srDtlData) {
                const jsonData = JSON.parse(this.masterP.srDtlData);
                Object.keys(jsonData).forEach(function (k) {
                    this[k] = jsonData[k];
                }, this);
            }

        } else {
            await this.handleSet();
        }

    }

    handleSet = async () => {
        this.orgReqEmpNo = this.masterP.reqEmpNo ? this.masterP.reqEmpNo : CommonStore.userInfo.bempNo;
        if (this.popupMode == "I" || this.popupMode == "C") {

            // 승인자 테이블로 추가
            // const param = {};
            // const result = await this.execute(`/service/request/reg/reviewer/get`, param);
            // if (result.success) {
            //     const reviewerObj = result.data;
            //     this.masterP.reviewEmpNo = reviewerObj ? reviewerObj.reviewEmpNo : CommonStore.userInfo.bempNo;
            //     this.masterP.reviewEmpNm = reviewerObj ? reviewerObj.reviewEmpNm : CommonStore.userInfo.buserNm;
            // }

            this.masterP.reqDtm = moment().format('YYYYMMDD');
            this.masterP.endDemandDtm = moment().add(7, 'days').format("YYYYMMDD");
            this.masterP.reqCompCd = CommonStore.userInfo.bcompCd;
            this.masterP.reqDeptCd = CommonStore.userInfo.bdeptCd;
            this.masterP.reqEmpNo = CommonStore.userInfo.bempNo;
            this.masterP.reqEmpNm = CommonStore.loginNm;

            this.reqEmpList.push(
                {
                    reqEmpNo: CommonStore.userInfo.bempNo,
                    reqEmpNm: CommonStore.userInfo.buserNm,
                    deptNm: CommonStore.userInfo.bdeptNm,
                    email: CommonStore.userInfo.bemail,
                    officeNo: CommonStore.userInfo.bofficeNo,
                    mobileNo: CommonStore.userInfo.bmobileNo,
                }
            );

            this.reqEmpGrid.setRowData(this.reqEmpList);
        }

        if (this.popupMode == "U") {
            if (this.masterP.srProcStat > "200") { //접수 전에만 수정 가능
                this.saveBtnVisible = "none";
            }
            this.approvalVisiable = '';
        }
    }

    setReqEmpGridApi = async (grid) => {
        this.reqEmpGrid = grid;

        const columnDefs = [
            { name: "", field: "check", width: 32, type: 'checkbox' },
            // { word: "M00673", name: "사용자ID", field: "userId", width: 100, align: 'center' },
            { word: "M00677", name: "부서명", field: "deptNm", width: 120 },
            { word: "M00979", name: "이름", field: "reqEmpNm", width: 90, align: 'center' },
            { word: "M00420", name: "이메일", field: "email", width: 150 },
            { word: "M00679", name: "전화번호", field: "officeNo", width: 90 },
            // { word: "M00433", name: "연락처(모바일)", field: "mobileNo", width: 135 },
            { word: "M00674", name: "사번", field: "reqEmpNo", width: 90, align: 'center', key: true, hide: true },
        ];

        this.reqEmpGrid.setColumnDefs(columnDefs);
    }

    handleCategoryRender = async () => {
        const param = {
            useYn: 'Y',
            categoryId: this.masterP.categoryId
        };
        const params = toJS(param);
        const data = await this.execute(`/service/request/reg/category-render/get`, params);

        this.categoryDtlList = data.data.categoryMngList;
        
        if(!this.masterP.srId && this.apprYn == 'Y') {
            this.apprEmpList = data.data.apprMngList;
        }

        if (this.categoryDtlList.length > 0) {
            const splitObj = this.categoryDtlList[0];
            const cdList = splitObj.splitGubnCd.split(',');
            const cdNmList = splitObj.splitGubnCdNm.split(',');
            const typeList = splitObj.splitGubnType.split(',');

            for (let index = 0; index < cdList.length; index++) {
                const selectList = [];
                if (typeList[index] == 'S') {
                    this.setBind(String.toString(cdList[index]), '')
                    this.dynamicDataKey.set(cdList[index], 'S');
                    this.categoryDtlList.filter(x => x.dtlGubnCd == cdList[index]).forEach(element => {
                        const tempObj = {
                            cd: element.categoryDtlCd,
                            cdNm: element.categoryDtlCdNm
                        }

                        selectList.push(tempObj);
                    });
                } else {
                    this.dtlObjMap.set(cdList[index], this.categoryDtlList.filter(x => x.dtlGubnCd == cdList[index]))
                    this.categoryDtlList.filter(x => x.dtlGubnCd == cdList[index]).forEach(element => {
                        this.setBind(String.toString(element.categoryDtlCd), '');
                        this.dynamicDataKey.set(element.categoryDtlCd, 'C');
                    });
                }

                const element = {
                    'dtlGubnCd': cdList[index],
                    'dtlGubnCdNm': cdNmList[index],
                    'dtlGubnType': typeList[index],
                    'splitRow': typeList[index] == 'C' ? (this.categoryDtlList.filter(x => x.dtlGubnCd == cdList[index]).length / 2) : null,
                    'selectGroup': selectList
                };
                this.dtlLabelMap.set(cdList[index], element);
            }
            this.categoryDtlOpen = true;
        } else {
            this.categoryDtlOpen = false;
        }
    }

    //서비스요청 저장
    handleSaveSr = async () => {
        if (!this.reqEmpIsMe) {
            await Alert.meg(String.format(this.l10n("M03404", "{0}님 외에는 변경을 할 수 없습니다."), this.masterP.reqEmpNm));
            return;
        }

        this.masterP.reqEmpList = this.reqEmpList;

        this.masterP.apprYn = this.apprYn;

        if (this.apprYn == 'Y' && (this.apprEmpList && this.apprEmpList.length > 0)) {

            this.apprEmpList.forEach(element => {
                element.categoryId = this.masterP.categoryId;
            });

            this.masterP.apprEmpList = this.apprEmpList;
        }

        if(this.apprYn == 'Y' && (this.apprEmpList && this.apprEmpList.length == 0)) {            
            await Alert.meg(this.l10n("M03451", "승인이 필요한 업무 입니다.."));
            return;
        }

        if (this.categoryDtlOpen) {
            const jsonSrDtlData = {};
            let validationCheck = ''
            this.dynamicDataKey.forEach((value, key) => {
                jsonSrDtlData[key] = this[key];
                if (value == 'S') {
                    if (!this[key]) {
                        validationCheck += '[' + this.dtlLabelMap.get(key).dtlGubnCdNm + '], '
                    }
                }
            });

            this.dtlObjMap.forEach((v,k) => {
                const checkList = v;
                const CheckBoxvalidation = [];
                checkList.forEach(element => {
                    if(this[element.categoryDtlCd] == 'N') {
                        CheckBoxvalidation.push(this[element.categoryDtlCd]);
                    }
                });

                if(checkList.length == CheckBoxvalidation.length) {
                    validationCheck += '[' + checkList[0].dtlGubnCdNm + '], '
                }
                
            })

            if (validationCheck != '') {
                await Alert.meg(this.l10n("M03343", "필수항목 값이 입력되지 않았습니다.") + `<br/>${validationCheck.substring(0, validationCheck.length - 2)}`);
                return;
            }

            this.masterP.srDtlData = JSON.stringify(jsonSrDtlData);

        }

        if (await this.validation("masterP")) {
            // 파일 첨부
            const fileGrpId = await this.fileGroupSave();
            if (fileGrpId != false || fileGrpId != "undefined") {
                this.masterP.fileGrpId = fileGrpId;
            }

            // // 검토자 객체
            // this.masterP.srReviewEmpModel = {
            //     reviewEmpNo: this.masterP.reviewEmpNo
            //     , reviewEmpNm: this.masterP.reviewEmpNm
            // }

            const params = JSON.stringify({ param1: toJS(this.masterP), param2: toJS(this.argsParam) });
            const result = await this.execute(`/service/request/reg/save`, params);

            if (result.success) {
                this.close(result.data);
            }
        }

    }

    handleSysCategoryHelperResult = async (result) => {
        if (result) {
            this.srDataKeyList = [];
            this.dynamicDataKey = new Map();
            this.dtlObjMap = new Map();
            this.dtlLabelMap = new Map();
            this.jsonData = undefined;
            this.masterP.srDtlData = undefined;
            this.masterP.sysId = result.sysId ? result.sysId : "";
            this.masterP.categoryId = result.categoryId ? result.categoryId : "";
            this.masterP.mngEmpNo = result.mngEmpNo ? result.mngEmpNo : "";
            this.masterP.mngEmpNm = result.mngEmpNm ? result.mngEmpNm : "";
            this.masterP.categoryDesc = result.categoryDesc ? result.categoryDesc : "";
            this.masterP.reqContents = result.contentsDesc ? result.contentsDesc : "";
            this.masterP.apprYn = result.apprYn ? result.apprYn : 'N';            
            // 검토자 및 승인자 리스트 1개로 합치고, 무조건 1명 이상 승인
            // this.apprYn = 'Y';
            this.apprYn = result.apprYn ? result.apprYn : 'N';
            
            this.expiredYn = result.expiredYn ? result.expiredYn == 'Y' ? true : false : false;
            
            if(this.categoryDtlOpen) {
                this.categoryDtlOpen = !this.categoryDtlOpen;
            }
            
            if (this.masterP.categoryId) {
                this.handleCategoryRender();
            }

        }
    }

    handleUserHelperResult = async (result) => {
        this.masterP.reqCompCd = result ? result.compCd : "";
        this.masterP.reqDeptCd = result ? result.deptCd : "";
        this.masterP.reqEmpNo = result ? result.empNo : "";
        this.masterP.reqEmpNm = result ? result.userNm : "";

    }

    handleApprChange = async (param) => {

        if (!this.reqEmpIsMe) {
            await Alert.meg(String.format(this.l10n("M03404", "{0}님 외에는 변경을 할 수 없습니다."), this.masterP.reqEmpNm));
            return;
        }

        const modal = new ModalManage();
        const excludeData = [];
        this.apprEmpList.forEach(element => {
            const tempObj = {
                empNo: element.apprEmpNo
            };

            excludeData.push(tempObj);
        });

        modal.id = "common_user_helper";
        modal.path = "/common/codehelper/user/UserHelper";
        modal.title = "승인자 선택";
        modal.width = 800;
        modal.parameter = { excludeData: excludeData, compCdReadOnly: true };
        const result = await this.showModal(modal);

        if (result) {
            param.apprEmpNo = result.empNo;
            param.apprEmpNm = result.userNm;
            param.deptNm = result.deptNm;
            param.compCd = result.compCd;
            param.categortId = this.masterP.categortyId;
        }
    }

    handleApprEdit = async () => {

        if (!this.reqEmpIsMe) {
            await Alert.meg(String.format(this.l10n("M03404", "{0}님 외에는 변경을 할 수 없습니다."), this.masterP.reqEmpNm));
            return;
        }

        const modal = new ModalManage();
        modal.id = "service_request_reg_user_select_p";
        modal.path = "/service/request/reg/popup/SrUserSelectP";
        modal.title = this.l10n("M03396", "승인자 등록/수정");
        modal.parameter = { apprEmpList: this.apprEmpList };
        modal.width = 1024;

        const result = await this.showModal(modal);

        if (result) {
            const saveApprList = [];
            result.forEach(element => {
                if(!this.apprEmpList.filter(x => x.editYn =='N').find(x => x.apprEmpNo == element.apprEmpNo)) {
                    saveApprList.push(element);
                }
            });

            this.apprEmpList.filter(x => x.editYn =='N').forEach(element => {
                if(saveApprList.find(x => x.sort > element.sort)) {
                    const sort = Math.max.apply(Math, saveApprList.map(function(o) { return o.sort; }));
                    element.sort = element.sort + sort;
                }
            });

            this.apprEmpList = this.apprEmpList.filter(x => x.editYn =='N').concat(saveApprList).sort((a,b) => {
                return a.sort > b.sort ? 1: -1;
            });
        }
    }

    handleApprDelete = async (param) => {
        if (!this.reqEmpIsMe) {
            await Alert.meg(String.format(this.l10n("M03404", "{0}님 외에는 변경을 할 수 없습니다."), this.masterP.reqEmpNm));
            return;
        }

        const result = await Alert.confirm(param.apprEmpNm + this.l10n("M03393", "삭제하시겠습니까?"));
        if (result) {

            if(this.masterP.srId) {
                
                if(this.apprEmpList.length == 1 && this.apprYn == 'Y') {
                    await Alert.meg(this.l10n("M03403", "최소한 대상자 1명은 있어야합니다."));
                    return;
                }

                const params = {
                    srId : this.masterP.srId,
                    sort : param.sort
                };

                const result = await this.execute(`/service/request/reg/appr-emp/delete`, params);

                if(result.success) {
                    this.apprEmpList = this.apprEmpList.filter(x => x.apprEmpNo != param.apprEmpNo);    
                }

            } else {
                this.apprEmpList = this.apprEmpList.filter(x => x.apprEmpNo != param.apprEmpNo);
            }
        }
    }

    handleDeleteReqEmp = async () => {

        if (!this.reqEmpIsMe) {
            await Alert.meg(String.format(this.l10n("M03404", "{0}님 외에는 변경을 할 수 없습니다."), this.masterP.reqEmpNm));
            return;
        }

        const totalRows = this.reqEmpGrid.getRows();
        const validationCheckList = this.reqEmpGrid.getCheckedRows();

        if (totalRows.length != 0 && totalRows.length <= validationCheckList.length) {
            await Alert.meg(this.l10n("M03403", "최소한 대상자 1명은 있어야합니다."));
            return;
        }


        if(this.masterP.srId) {
            const param = {
                srId: this.masterP.srId
            }
            const params = toJS(param);

            this.reqEmpList =  this.reqEmpGrid.getRows().filter(x => x.check != 'Y');
            await this.reqEmpGrid.deleteCheckedItems('/service/request/reg/req-emp/delete', params);
            this.reqEmpGrid.setRowData(this.reqEmpList);
        } else {
            this.reqEmpGrid.deleteCheckedRows();
            this.reqEmpList = this.reqEmpGrid.getRows();
        }
    }


    handleAddReqEmp = async (param) => {

        if (!this.reqEmpIsMe) {
            await Alert.meg(String.format(this.l10n("M03404", "{0}님 외에는 변경을 할 수 없습니다."), this.masterP.reqEmpNm));
            return;
        }

        const modal = new ModalManage();
        const excludeData = [];
        this.reqEmpList.forEach(element => {
            const tempObj = {
                empNo: element.reqEmpNo
            }
            excludeData.push(tempObj);
        });

        modal.id = "common_user_helper";
        modal.path = "/common/codehelper/user/UserHelper";
        modal.title = this.l10n("M03395", "대상자 등록");
        modal.width = 800;
        modal.parameter = { excludeData: excludeData, compCdReadOnly: true, helper: { multi: true } };

        const result = await this.showModal(modal);

        if (result && result.length > 0) {

            result.forEach(element => {
                const reqObj = {
                    reqEmpNo: element.empNo,
                    reqEmpNm: element.userNm,
                    deptNm: element.deptNm,
                    email: element.email,
                    officeNo: element.officeNo,
                    mobileNo: element.mobileNo
                }

                this.reqEmpList.push(reqObj);
            });

            this.reqEmpGrid.setRowData(this.reqEmpList);
        }
    }
}

@observer
export default class SvcReqRegSaveP extends BaseComponent {

    sRender() {
        const dynamicRenderList = [];
        let selectList = [];
        let dynamicHeight = 190;
        if (this.store.expiredYn) {
            selectList.push(
                <React.Fragment key={'expiredDtm'}>
                    <th className="essential">{this.l10n("M03397", "만료기간")}</th>
                    <td>
                        <SDatePicker id={'masterP.reqExpiredFromDtm'} format={'yyyy-MM-dd'} readOnly={this.store.readOnlyMode} />
                        <SDatePicker id={'masterP.reqExpiredToDtm'} format={'yyyy-MM-dd'} title={'~'} readOnly={this.store.readOnlyMode} />
                    </td>
                </React.Fragment>
            )
        }
        if (this.store.categoryDtlOpen) {
            let inputList = [];
            this.store.dtlLabelMap.forEach((v, k) => {
                /** 
                 * 업무별 상세 페이지 만들기
                 * v.dtlGubnType = { S : 셀렉트 박스, C : 체크 박스 -> 필요시 맞는 컴포넌트랑 매핑해주면 됨}
                 */
                switch (v.dtlGubnType) {
                    case 'S':
                        selectList.push(
                            <React.Fragment key={`${v.dtlGubnCd}`}>
                                <th className="essential">{v.dtlGubnCdNm}</th><td><SSelectBox id={`${k}`} optionGroup={v.selectGroup} valueMember={"cd"} displayMember={"cdNm"} readOnly={this.store.readOnlyMode} addOption={this.l10n("M00233", "선택")} /></td>
                            </React.Fragment>
                        );
                        if(selectList.length == 2) {
                            dynamicRenderList.push(<tr key={`${v.dtlGubnCd}`}>{selectList}</tr>)
                            selectList = [];
                        }
                        break;
                    case 'C':
                        const checkList = this.store.dtlObjMap.get(k);
                        if (checkList) {
                            const checkBoxList = [];
                            checkList.forEach(element => {
                                checkBoxList.push(<SCheckBox key={element.categoryDtlCd} dynamicData={this.store.jsonData ? this.store.jsonData[element.categoryDtlCd] : ''} id={element.categoryDtlCd} title={element.categoryDtlCdNm} readOnly={this.store.readOnlyMode} />)
                            });
                            dynamicRenderList.push(
                                <tr rowSpan='2' key={`${v.dtlGubnCd}`}><th className="essential">{v.dtlGubnCdNm}</th><td colSpan="3">
                                    {checkBoxList}
                                </td></tr>
                            )
                        }
                        break;
                    case 'I':
                        inputList.push(
                            <React.Fragment key={`${v.dtlGubnCd}`}>
                                <th className="essential">{v.dtlGubnCdNm}</th><td><SInput id={`${k}`} width={200} readOnly={this.store.readOnlyMode}/></td>
                            </React.Fragment>
                        );
                        if(inputList.length == 2) {
                            dynamicRenderList.push(<tr key={`${v.dtlGubnCd}`}>{inputList}</tr>)
                            inputList = [];
                        }
                        break;
                    case 'NI':
                        inputList.push(
                            <React.Fragment key={`${v.dtlGubnCd}`}>
                                <th className="essential">{v.dtlGubnCdNm}</th><td><SNumericInput id={`${k}`} width={200} readOnly={this.store.readOnlyMode} decimalScale={3} /></td>
                            </React.Fragment>
                        );
                        if (inputList.length == 2) {
                            dynamicRenderList.push(<tr key={`${v.dtlGubnCd}`}>{inputList}</tr>)
                            inputList = [];
                        }
                        break;
                }
            })
            if(inputList.length > 0) {
                dynamicRenderList.push(<tr key={`dynamic`}>{inputList}</tr>)
            }
            
        } else {
            dynamicHeight = 'auto';
        }
        
        if(selectList.length > 0) {
            dynamicRenderList.push(<tr key={`dynamic`}>{selectList}</tr>)
        }

        const apprReqEmpListRender = [];
        const apprRecivEmpListRender = [];
        if (this.store.apprEmpList.length > 0) {
            if(this.store.apprEmpList.length > 0) {
                let count = 0;
                this.store.apprEmpList.forEach(element => {
                    apprReqEmpListRender.push(
                        <React.Fragment key={'appr_' + element.apprEmpNo}>
                            <div className="appr_profile_req">
                                {
                                    element.editYn == 'Y' ? 
                                    <>
                                        <img style={{cursor: 'pointer'}} className="apprDelete" src={ico_user_delete} onClick={() => this.store.handleApprDelete(element)} />
                                        <div style= {{position: 'inherit', margin: '-16px 15px 10px 20px', cursor: 'pointer'}} onClick={() => this.store.handleApprChange(element)}>
                                            <div className="apprEmpNm" id={element.apprEmpNo}>
                                                <span className='appr_desc'>요청부서</span>
                                                {element.apprEmpNm}
                                            </div>
                                            
                                            <div className="apprDeptNm">
                                                {element.deptNm}
                                            </div>
                                        </div>
                                    </>
                                     : 
                                     <>
                                        <div  style= {{position: 'inherit', margin: '0px 15px 10px 20px'} }>
                                            <div className="apprEmpNm" id={element.apprEmpNo}>
                                                <span className='appr_desc'>수신부서</span>
                                                {element.apprEmpNm}
                                            </div>
                                            <div className="apprDeptNm">
                                                {element.deptNm}
                                            </div>
                                        </div>
                                     </>
                                }
                                {(this.store.apprEmpList.length - 1) > count ? <img className="appr_arrow" src={ico_right_arrow} /> : null}
                            </div>
                        </React.Fragment>
                    )
                    count += 1;
                });
            }

            // if(this.store.apprEmpList.filter(x=> x.editYn=='N').length > 0) {
            //     let count = 0;
            //     this.store.apprEmpList.filter(x=> x.editYn=='N').forEach(element => {
            //         apprRecivEmpListRender.push(
            //             <React.Fragment key={'appr_' + element.apprEmpNo}>
            //                 <div className="appr_profile_receive">
            //                     <div  style= {{position: 'inherit', margin: '0px 15px 10px 20px'} }>
            //                         <div className="apprEmpNm" id={element.apprEmpNo}>
            //                             {element.apprEmpNm}
            //                         </div>
            //                         <div className="apprDeptNm">
            //                             {element.deptNm}
            //                         </div>
            //                     </div>
            //                     {(this.store.apprEmpList.filter(x=> x.editYn=='N').length - 1) > count ? <img className="appr_arrow" src={ico_right_arrow} /> : null}
            //                 </div>
            //             </React.Fragment>
            //         )
            //         count += 1;
            //     })
            // }
        }

        if(this.store.masterP.categoryId && (!this.store.apprEmpList || this.store.apprEmpList.filter(x => x.editYn =='Y').length == 0)) {
            apprReqEmpListRender.push(
                <div className="appr_edit" key={'appr_add'} onClick={this.store.handleApprEdit}>
                    <div className="apprEditTit">
                        {"승인자 편집"}
                    </div>
                </div>
            )
        }

        
        return (
            <div className="basic">
                <React.Fragment>
                    {this.store.apprYn == 'Y' && apprReqEmpListRender && apprReqEmpListRender.length > 0 ?
                        <div className='appr_contents'>
                            <div className='appr_req'>
                                <ContentsMiddleTemplate subTitle={this.l10n("M03370", "승인자 목록")}>
                                    <SButton buttonName={this.l10n("M00984", "등록/수정")} onClick={this.store.handleApprEdit} type={'default'} />
                                </ContentsMiddleTemplate>
                                {apprReqEmpListRender}
                            </div>
                        </div>
                        :
                        null
                    }
                    <ContentsMiddleTemplate subTitle={this.l10n("M03433", "서비스요청 상세정보")}>
                        <span style={{ display: this.store.saveBtnVisible }}>
                            <SButton buttonName={this.l10n("M00004", "저장")} onClick={this.store.handleSaveSr} type={'default'} />
                        </span>
                    </ContentsMiddleTemplate>
                    <div>
                        <table id={'table'} style={{ width: "100%" }}>
                            <tbody>
                                <tr>
                                    <th>{this.l10n("M00572", "요청일자")}</th>
                                    <td>
                                        <SDatePicker id={"masterP.reqDtm"} contentWidth={80} readOnly={this.store.readOnlyMode} />
                                    </td>
                                    <th className="essential">{this.l10n("M03327", "요청자")}</th>
                                    <td className='popupText'>
                                        <CodeHelper helperCode={"masterP.reqEmpNo"} helperCodeNm={"masterP.reqEmpNm"} mainStore={this.store} business={"user"} readOnly={true} onResult={this.store.handleUserHelperResult} codeVisible={false} />
                                    </td>
                                </tr>
                                <tr>
                                    <th className="essential">{this.l10n("M03332", "완료요구일")}</th>
                                    <td>
                                        <SDatePicker id={"masterP.endDemandDtm"} readOnly={this.store.readOnlyMode} />
                                    </td>
                                    <th className="essential">{this.l10n("M03328", "IT업무")}</th>
                                    {/* <th className="essential">{this.l10n("M03331", "처리구분")}</th> */}
                                    <td>
                                        {/* {
                                            this.store.masterP.categoryDesc && this.store.masterP.categoryDesc != "" ?
                                            <div className="cty_help" data-tip='' data-for={'categoryDesc'}>
                                                <img src={help}/>
                                                <ReactTooltip html={true}  backgroundColor={"white"} textColor={'black'} borderColor = {'green'} border = {true}  id={'categoryDesc'}>
                                                    {this.store.masterP.categoryDesc}
                                                </ReactTooltip>
                                            </div> :
                                            null
                                        } */}
                                        <CodeHelper helperCode={"masterP.categoryId"} helperCodeNm={"masterP.categoryNm"} mainStore={this.store} business={"sysCategory"} readOnly={this.store.masterP.srProcStat ? true : false} onResult={this.store.handleSysCategoryHelperResult} codeVisible={false} codeNmWidth={220} />
                                    </td>
                                </tr>
                                {
                                    dynamicRenderList.length > 0 ? dynamicRenderList
                                        : null
                                }
                                <tr>
                                    <th className="essential">{this.l10n("M00348", "제목")}</th>
                                    <td colSpan="3">
                                        <SInput id={"masterP.reqTitle"} width={500} readOnly={this.store.readOnlyMode} />
                                    </td>
                                </tr>
                                <tr>
                                    <th>{this.l10n("M03471", "요청내용")}</th>
                                    <td colSpan="3">
                                        <ContentsTemplate height={'auto'} isShadow={true}>
                                            <SEditor id={"masterP.reqContents"} readOnly={this.store.readOnlyMode} />
                                        </ContentsTemplate>
                                    </td>
                                </tr>
                                {
                                    this.store.masterP.categoryDesc && this.store.masterP.categoryDesc != "" ?
                                    <tr>
                                        <th>{this.l10n("M03487", "주의사항")}</th>
                                        <td colSpan="3">
                                            <ContentsTemplate height={'auto'} isShadow={true}>
                                                <SEditor id={"masterP.categoryDesc"} readOnly={true} />
                                            </ContentsTemplate>
                                        </td>
                                    </tr>
                                    :
                                    null
                                }
                            </tbody>
                        </table>
                        {/* <React.Fragment>
                             <ContentsMiddleTemplate subTitle={this.l10n("M03487", "주의사항")} >
                             </ContentsMiddleTemplate>
                             <ContentsTemplate height={'auth'} isShadow={true}>
                             <SEditor id={"masterP.categoryDesc"} readOnly={true} />
                             </ContentsTemplate>
                        </React.Fragment> */}
                    </div>
                    {/* <div>
                        <ContentsMiddleTemplate subTitle={this.l10n("M03401", "검토자 등록")} />
                        <table id={'table'} style={{ width: "100%", borderTop: '1px #7fb375 solid' }}>
                            <tbody>
                                <tr>
                                    <th className="essential">{this.l10n("M03400", "검토자")}</th>
                                    <td className='popupText'>
                                        <CodeHelper helperCode={"masterP.reviewEmpNo"} helperCodeNm={"masterP.reviewEmpNm"} mainStore={this.store} business={"user"} readOnly={this.store.popupMode == "U" ? true : false} />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div> */}
                    <div >
                        <div className= "ratio_w65 ratio_h50">
                                <React.Fragment>
                                    <ContentsMiddleTemplate subTitle={this.l10n("M03394", "대상자 목록")} >
                                        <SButton buttonName={this.l10n("M00983", "추가")} onClick={this.store.handleAddReqEmp} type={'default'} />
                                        <SButton buttonName={this.l10n("M00003", "삭제")} onClick={this.store.handleDeleteReqEmp} type={'default'} />
                                    </ContentsMiddleTemplate>
                                    <ContentsTemplate height={200}>
                                        <SGrid grid={'reqEmpGrid'} gridApiCallBack={this.store.setReqEmpGridApi} editable={false} />
                                    </ContentsTemplate>
                                </React.Fragment>
                        </div>
                        <div className= "ratio_w35 ratio_h50">
                            <ContentsTemplate id={"contents"}>
                                <SFileMulti fileGrpId={"masterP.attachFileGrpId"} useFileDelete={this.store.reqEmpIsMe} height={200} useFileAttach={this.store.reqEmpIsMe} />
                            </ContentsTemplate>
                        </div>
                    </div>
                    {/* {
                        this.store.masterP.categoryDesc && this.store.masterP.categoryDesc != "" ?
                            <div className="category_desc">
                                <ContentsMiddleTemplate subTitle={this.l10n("M03434", "업무 설명")} >
                                    <ContentsTemplate height={'auto'}>
                                        <SEditor id={"masterP.categoryDesc"} readOnly={true} />
                                    </ContentsTemplate>
                                </ContentsMiddleTemplate>
                            </div> :
                            null
                    } */}
                </React.Fragment>
            </div>
        )
    }
}

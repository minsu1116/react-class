import { BaseModel, essential } from "utils/base/BaseModel";
import { observable } from "mobx";
import moment from 'moment';

export default class SvcReqAuthReqSavePModel extends BaseModel {
    @observable startDtm = moment().format("YYYYMMDD");
    @observable expiredDtm = moment().add(1, 'month').format("YYYYMMDD");
    @observable newYn = 'Y';

    @observable sysId;
    @observable sysNm;
    @observable svrNm;
    @observable ip;

    @observable dbNm;
    @observable dmlYn;
    @observable ddlYn;
    @observable dclYn;
    @observable rename;
    @observable truncate;
    @observable select;
    @observable delete;
    @observable drop;
    @observable insert;
    @observable alter;
    @observable create;
    @observable grant;
    @observable revoke;
    @observable update;
    @observable reqAuthNm;
    @observable compNm;
}

import React from 'react';
import { observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import { BaseComponent } from "utils/base/BaseComponent";
import { BaseStore, gridTrigger, model } from 'utils/base/BaseStore';

import ContentsTemplate from 'components/template/ContentsTemplate';
import ContentsMiddleTemplate from 'components/template/ContentsMiddleTemplate';
import SInput from "components/atoms/input/SInput";
import SSelectBox from 'components/atoms/selectbox/SSelectBox';
import SButton from 'components/atoms/button/SButton';
import SGrid from 'components/override/grid/SGrid';
import Alert from 'components/override/alert/Alert';
import SDatePicker from 'components/override/datepicker/SDatePicker';

import SvcReqAuthReqSavePModel from "modules/pages/service/request/reg/popup/SvcReqAuthReqSavePModel";
import CodeHelper from 'modules/pages/common/codehelper/CodeHelper';
import SCheckBox from 'components/atoms/checkbox/SCheckBox';

export { SvcReqAuthReqSaveP, SvcReqAuthReqSavePStore }

class SvcReqAuthReqSavePStore extends BaseStore {
    @observable @model masterP = new SvcReqAuthReqSavePModel();
    
    @observable srProcDtlType;
    @observable popupMode;
    @observable readOnly;
    
    authReqList;
    authApprList;
    
    initialize = async (params) => {
        if (!this.masterP) this.masterP = new SvcReqAuthReqSavePModel();
        this.readOnly = this.popupMode == "R" ? true : false;
        this.authApprList = params.authApprList;
        this.authApprList.sort(function (a, b) { // 오름차순
            return parseInt(a.apprNo) < parseInt(b.apprNo) ? -1 : parseInt(a.apprNo) > parseInt(b.apprNo) ? 1 : 0;
        });

        this.authReqList = params.authReqList;
    }

    loaded = async () => { 
        if(this.authReqList){
            this.grid.setRowData(toJS(this.authReqList));
        }

        if(this.authApprList){
            this.apprGrid.setRowData(toJS(this.authApprList));
        }
    }

    handleUserHelperResult = (result) => {
        let data = {};
        data["empNo"] = result ? result.empNo : null;
        data["empNm"] = result ? result.userNm : null;
        data["deptCd"] = result ? result.deptCd : null;
        data["deptNm"] = result ? result.deptNm : null;
        data["jobNm"] = result ? result.jobNm : null;
        this.grid.setDataByRowIndex(data);
    }

    handleApprUserHelperResult = async (result) => {
        const authApprList = toJS(this.apprGrid.getRows());
        if(authApprList.find(x => x.empNo == result.empNo)) {
            await Alert.meg(this.l10n("M03460", "이미 승인자목록에 포함되어있습니다.."));
            result = null
        }

        let data = {};
        data["empNo"] = result ? result.empNo : null;
        data["empNm"] = result ? result.userNm : null;
        data["deptCd"] = result ? result.deptCd : null;
        data["deptNm"] = result ? result.deptNm : null;
        data["jobNm"] = result ? result.jobNm : null;
        this.apprGrid.setDataByRowIndex(data);
    }
    
    setGridApi = async (grid) => {
        this.grid = grid;
        let columnDefs = [];

        if (this.popupMode == "R") {
            columnDefs = [
                , { word: "M00979", name: "이름", field: "empNm", width: 80, align: 'center' }
                , { word: "M00677", name: "부서명", field: "deptNm", width: 120 }
                , { word: "M03442", name: "담당직무", field: "jobNm", width: 100 }
                , { field: "deptCd", hide: true }
                , { word: "M00674", name: "사번", field: "empNo", width: 100, align: 'center', hide: true }
            ];
        }
        else {
            columnDefs = [
                { word: "M00674", name: "사번", field: "empNo", width: 100, align: 'center', essential: true, hide: true}
                , {
                    word: "M00979", name: "이름", field: "empNm", width: 80, align: 'center',
                    type: "codeHelper", business: "user", helperParams: { _code: "userNm" }, onHelperResult: (result) => this.handleUserHelperResult(result)
                }
                , { word: "M00677", name: "부서명", field: "deptNm", width: 120 }
                , { word: "M03442", name: "담당직무", field: "jobNm", width: 100 }
                , { field: "deptCd", hide: true }
            ];
        }

        this.grid.setColumnDefs(columnDefs);
    }

    setApprGridApi = async (grid) => {
        this.apprGrid = grid;
        let columnDefs = [];

        if (this.popupMode == "R") {
            columnDefs = [
                { word: "M00674", name: "사번", field: "empNo", width: 100, align: 'center', hide: true}
                , { word: "M00979", name: "이름", field: "empNm", width: 80, align: 'center' }
                , { word: "M00677", name: "부서명", field: "deptNm", width: 150 }
                , { word: "M03442", name: "담당직무", field: "jobNm", width: 100 }
                , { word: "M03457", name: "결재순서", field: "apprNo", width: 80, type: "number", editable: false }
                , { word: "M00183", name: "구분", field: "apprGubn", width: 100, editable: false }
                , { field: "deptCd", hide: true }
            ];
        }
        else {
            columnDefs = [
                { word: "M00674", name: "사번", field: "empNo", width: 100, align: 'center', essential: true, hide: true}
                , {
                    word: "M00979", name: "이름", field: "empNm", width: 80, align: 'center',
                    type: "codeHelper", business: "user", helperParams: { _code: "userNm" }, onHelperResult: (result) => this.handleApprUserHelperResult(result)
                }
                , { word: "M00677", name: "부서명", field: "deptNm", width: 150 }
                , { word: "M03442", name: "담당직무", field: "jobNm", width: 100 }
                , { word: "M03457", name: "결재순서", field: "apprNo", width: 80, type: "number" }
                , { word: "M00183", name: "구분", field: "apprGubn", width: 100, editable: false }
                , { field: "deptCd", hide: true }
            ];
        }

        this.setOption(columnDefs, ["empNm"], gridTrigger.cellRendererParams, this.helperVisible);
        this.apprGrid.setColumnDefs(columnDefs);

    }

    helperVisible = (params) => {
        if(params.data.apprGubn == '사용자지정') {
            return { visible: true };
        } else {

            return { visible: false };
        }
    }

    handleSave = async () => {
        const authReqList = this.grid.getRows();
        const authApprList= toJS(this.apprGrid.getRows());
        const authReqInfo = toJS(this.masterP);

        if (authReqList.length == 0) {
            await Alert.meg(this.l10n("M03452", "권한 신청 대상자가 없습니다."));
            return;
        }

        if (await this.grid.validation() && await this.apprGrid.validation())  {
            this.close({ authReqInfo: authReqInfo, authReqList: authReqList, authApprList: authApprList});
        }
    }

    handleAdd = () => {
        this.grid.addRow(null, "firstIndex");
    }

    handleApprAdd = () => {
        if(this.apprGrid.getRows().filter(x => x.apprGubn == '사용자지정').length < 9) {
            this.apprGrid.addRow({ apprNo: this.apprGrid.getRows().filter(x => x.apprGubn == '사용자지정').length + 1, apprGubn: '사용자지정' });
            const rows = this.apprGrid.getRows();
            rows.sort(function (a, b) { // 오름차순
                return parseInt(a.apprNo) < parseInt(b.apprNo) ? -1 : parseInt(a.apprNo) > parseInt(b.apprNo) ? 1 : 0;
            });
            this.apprGrid.setRowData(rows);
        }
    }

    handleDelete = async () => {
        const selectedRow = this.grid.getSelectedRow();
        if (selectedRow) {
            this.grid.deleteRow(selectedRow);
        } else {
            await Alert.meg(this.l10n("M02418", "삭제할 항목을 선택해주세요."));
            return;
        }
    }

    handleApprDelete = async () => {
        const selectedRow = this.apprGrid.getSelectedRow();

        if (selectedRow) {

            if(selectedRow.apprGubn == '사용자지정') {
                this.apprGrid.deleteRow(selectedRow);
            } else {
                await Alert.meg(this.l10n("M03459", "사용자지정 승인자만 삭제 할 수 있습니다."));
                return;    
            }
        } else {
            await Alert.meg(this.l10n("M02418", "삭제할 항목을 선택해주세요."));
            return;
        }
    }

    onChangeAllDML = () => {
        if (this.masterP.dmlYn == 'Y') {
            this.masterP.select = 'Y';
            this.masterP.delete = 'Y';
            this.masterP.insert = 'Y';
            this.masterP.update = 'Y';
        }
        if (this.masterP.dmlYn == 'N') {
            this.masterP.select = 'N';
            this.masterP.delete = 'N';
            this.masterP.insert = 'N';
            this.masterP.update = 'N';
        }
    }

    onChangeAllDDL = () => {
        if (this.masterP.ddlYn == 'Y') {
            this.masterP.drop = 'Y';
            this.masterP.alter = 'Y';
            this.masterP.create = 'Y';
            this.masterP.rename = 'Y';
            this.masterP.truncate = 'Y';
        }
        if (this.masterP.ddlYn == 'N') {
            this.masterP.drop = 'N';
            this.masterP.alter = 'N';
            this.masterP.create = 'N';
            this.masterP.rename = 'N';
            this.masterP.truncate = 'N';
        }
    }

    onChangeAllDCL = () => {
        if (this.masterP.dclYn == 'Y') {
            this.masterP.grant = 'Y';
            this.masterP.revoke = 'Y';
        }
        if (this.masterP.dclYn == 'N') {
            this.masterP.grant = 'N';
            this.masterP.revoke = 'N';
        }
    }

    onChangeDML = () => {
        if (this.masterP.select == 'Y' && this.masterP.delete == 'Y'
            && this.masterP.insert == 'Y' && this.masterP.update == 'Y') {
            this.masterP.dmlYn = 'Y';
        }
        else this.masterP.dmlYn = 'N';
    }

    onChangeDDL = () => {
        if (this.masterP.drop == 'Y' && this.masterP.alter == 'Y' && this.masterP.create == 'Y'
            && this.masterP.rename == 'Y' && this.masterP.truncate == 'Y') {
            this.masterP.ddlYn = 'Y';
        }
        else this.masterP.ddlYn = 'N';
    }

    onChangeDCL = () => {
        if (this.masterP.grant == 'Y' && this.masterP.revoke == 'Y') {
            this.masterP.dclYn = 'Y';
        }
        else this.masterP.dclYn = 'N';
    }
}

@observer
export default class SvcReqAuthReqSaveP extends BaseComponent {

    sRender() {
        return (
            <React.Fragment>
                <ContentsMiddleTemplate>
                    <span style={{ display: this.store.popupMode != "R" ? "" : "none" }}>
                        <SButton buttonName={this.l10n("M00004", "저장")} onClick={this.store.handleSave} type={'save'} />
                    </span>
                </ContentsMiddleTemplate>

                <ContentsMiddleTemplate subTitle={"승인자 목록"}>
                    {
                        this.store.popupMode == "R" ?
                            null
                            :
                            <div>
                                <SButton buttonName={this.l10n("M03456", "승인자 추가")} onClick={this.store.handleApprAdd} type={'add'} />
                                <SButton buttonName={this.l10n("M03441", "선택 삭제")} onClick={this.store.handleApprDelete} type={'delete'} />
                            </div>
                    }
                </ContentsMiddleTemplate>
                <ContentsTemplate shadow={true} height={"120px"}>
                    <SGrid grid={"apprGrid"} gridApiCallBack={this.store.setApprGridApi} editable={false} />
                </ContentsTemplate>

                <ContentsMiddleTemplate subTitle={"권한 신청 대상자"}>
                    {
                        this.store.popupMode == "R" ?
                            null
                            :
                            <div>
                                <SButton buttonName={this.l10n("M03440", "대상자 추가")} onClick={this.store.handleAdd} type={'add'} />
                                <SButton buttonName={this.l10n("M03441", "선택 삭제")} onClick={this.store.handleDelete} type={'delete'} />
                            </div>
                    }
                </ContentsMiddleTemplate>

                <ContentsTemplate shadow={true} height={"120px"}>
                    <SGrid grid={"grid"} gridApiCallBack={this.store.setGridApi} editable={false} />
                </ContentsTemplate>
                {
                    this.store.srProcDtlType == '01' ?
                        <div>
                            <ContentsMiddleTemplate subTitle={this.l10n("M03443", "서버 접근 권한 신청 내용")} />
                            <table id={'table'} style={{ width: "100%" }}>
                                <tbody>
                                    <tr>
                                        <th className="essential">{this.l10n("M00233", "선택")}</th>
                                        <td colSpan="3">
                                            <SCheckBox id={"masterP.newYn"} title={"서버 보안 인증서 신규 발급"} readOnly={this.store.readOnly} />
                                        </td>
                                    </tr>
                                    <tr>
                                        <th className="essential">{this.l10n("M02456", "시스템")}</th>
                                        <td colSpan="3">
                                            <CodeHelper helperCode={"masterP.sysId"} helperCodeNm={"masterP.sysNm"} mainStore={this.store} business={"sysMt"}
                                                readOnly={this.store.readOnly} codeVisible={false} />
                                        </td>
                                    </tr>
                                    <tr>
                                        <th className="essential">{this.l10n("M03341", "서버")}</th>
                                        <td>
                                            <SInput id={"masterP.svrNm"} readOnly={this.store.readOnly} />
                                        </td>
                                        <th>IP</th>
                                        <td>
                                            <SInput id={"masterP.ip"} readOnly={this.store.readOnly} />
                                        </td>
                                    </tr>
                                    <tr>
                                        <th className="essential">{this.l10n("M02669", "사용기간")}</th>
                                        <td colSpan="3">
                                            <SDatePicker id={"masterP.startDtm"} readOnly={this.store.readOnly} contentWidth={80} />
                                            <label className="item_lb">~</label>
                                            <SDatePicker id={"masterP.expiredDtm"} readOnly={this.store.readOnly} contentWidth={80} />
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        :
                        null
                }
                {
                    this.store.srProcDtlType == '02' ?
                        <div>
                            <ContentsMiddleTemplate subTitle={this.l10n("M03444", "DB 접근 권한 신청 내용")} />
                            <table id={'table'} style={{ width: "100%" }}>
                                <tbody>
                                    <tr>
                                        <th className="essential">{this.l10n("M00233", "선택")}</th>
                                        <td colSpan="6">
                                            <SCheckBox id={"masterP.newYn"} title={"DB SAFER 신규 발급"} readOnly={this.store.readOnly} />
                                        </td>
                                    </tr>
                                    {/* <tr>
                                        <th className="essential">{this.l10n("M02456", "시스템")}</th>
                                        <td colSpan="6">
                                            <CodeHelper helperCode={"masterP.sysId"} helperCodeNm={"masterP.sysNm"} mainStore={this.store}
                                            business={"sysMt"} readOnly={this.store.readOnly} codeVisible={false} />
                                        </td>
                                    </tr> */}
                                    <tr>
                                        <th className="essential">{this.l10n("M03341", "서버")}</th>
                                        <td colSpan="2">
                                            <SInput id={"masterP.svrNm"} readOnly={this.store.readOnly} />
                                        </td>
                                        <th>IP</th>
                                        <td>
                                            <SInput id={"masterP.ip"} readOnly={this.store.readOnly} />
                                        </td>
                                        <th className="essential">{this.l10n("M03448", "DB 인스턴스")}</th>
                                        <td>
                                            <SInput id={"masterP.dbNm"} readOnly={this.store.readOnly} />
                                        </td>
                                    </tr>
                                    <tr>
                                        <th className="essential" rowSpan="3">{this.l10n("M01034", "권한")}</th>
                                        <th><SCheckBox id={"masterP.dmlYn"} title={"DML"} onChange={this.store.onChangeAllDML} readOnly={this.store.readOnly} /></th>
                                        <td colSpan="5">
                                            <SCheckBox id={"masterP.select"} title={"SELECT"} onChange={this.store.onChangeDML} readOnly={this.store.readOnly} />
                                            <SCheckBox id={"masterP.delete"} title={"DELETE"} onChange={this.store.onChangeDML} readOnly={this.store.readOnly} />
                                            <SCheckBox id={"masterP.insert"} title={"INSERT"} onChange={this.store.onChangeDML} readOnly={this.store.readOnly} />
                                            <SCheckBox id={"masterP.update"} title={"UPDATE"} onChange={this.store.onChangeDML} readOnly={this.store.readOnly} />
                                        </td>
                                    </tr>
                                    <tr>
                                        <th><SCheckBox id={"masterP.ddlYn"} title={"DDL"} onChange={this.store.onChangeAllDDL} readOnly={this.store.readOnly} /></th>
                                        <td colSpan="5">
                                            <SCheckBox id={"masterP.drop"} title={"DROP"} onChange={this.store.onChangeDDL} readOnly={this.store.readOnly} />
                                            <SCheckBox id={"masterP.alter"} title={"ALTER"} onChange={this.store.onChangeDDL} readOnly={this.store.readOnly} />
                                            <SCheckBox id={"masterP.create"} title={"CREATE"} onChange={this.store.onChangeDDL} readOnly={this.store.readOnly} />
                                            <SCheckBox id={"masterP.rename"} title={"RENAME"} onChange={this.store.onChangeDDL} readOnly={this.store.readOnly} />
                                            <SCheckBox id={"masterP.truncate"} title={"TRUNCATE"} onChange={this.store.onChangeDDL} readOnly={this.store.readOnly} />
                                        </td>
                                    </tr>
                                    <tr>
                                        <th><SCheckBox id={"masterP.dclYn"} title={"DCL"} onChange={this.store.onChangeAllDCL} readOnly={this.store.readOnly} /></th>
                                        <td colSpan="5">
                                            <SCheckBox id={"masterP.grant"} title={"GRANT"} onChange={this.store.onChangeDCL} readOnly={this.store.readOnly} />
                                            <SCheckBox id={"masterP.revoke"} title={"REVOKE"} onChange={this.store.onChangeDCL} readOnly={this.store.readOnly} />
                                        </td>
                                    </tr>
                                    <tr>
                                        <th className="essential">{this.l10n("M02669", "사용기간")}</th>
                                        <td colSpan="6">
                                            <SDatePicker id={"masterP.startDtm"} readOnly={this.store.readOnly} contentWidth={80} />
                                            <label className="item_lb">~</label>
                                            <SDatePicker id={"masterP.expiredDtm"} readOnly={this.store.readOnly} contentWidth={80} />
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        :
                        null
                }

                {
                    this.store.srProcDtlType == '04' ?
                        <div>
                            <ContentsMiddleTemplate subTitle={this.l10n("M03461", "ERP 권한신청 내용")} />
                            <table id={'table'} style={{ width: "100%" }}>
                                <tbody>
                                    <tr>
                                        <th className="essential">{this.l10n("M03455", "권한요청 회사")}</th>
                                        <td>
                                            <SInput id={"masterP.compNm"} />
                                        </td>
                                    </tr>
                                    <tr>
                                        <th className="essential">{this.l10n("M03454", "요청권한")}</th>
                                        <td>
                                            <SInput id={"masterP.reqAuthNm"} />
                                        </td>
                                    </tr>
                                    <tr>
                                        <th className="essential">{this.l10n("M02669", "사용기간")}</th>
                                        <td>
                                            <SDatePicker id={"masterP.startDtm"} readOnly={this.store.readOnly} contentWidth={80} />
                                            <label className="item_lb">~</label>
                                            <SDatePicker id={"masterP.expiredDtm"} readOnly={this.store.readOnly} contentWidth={80} />
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        :
                        null
                }

                {
                    this.store.srProcDtlType == '05' ?
                        <div>
                            <ContentsMiddleTemplate subTitle={this.l10n("M03446", "EXCEL MACRO 권한 신청 내용")} />
                            <table id={'table'} style={{ width: "100%" }}>
                                <tbody>
                                    <tr>
                                        <th className="essential">{this.l10n("M00233", "선택")}</th>
                                        <td>
                                            <SCheckBox id={"masterP.newYn"} title={"DB SAFER 신규 발급"} readOnly={this.store.readOnly} />
                                        </td>
                                    </tr>
                                    <tr>
                                        <th className="essential">{this.l10n("M02453", "회사")}</th>
                                        <td>
                                            <SInput id={"masterP.compNm"} />
                                        </td>
                                    </tr>
                                    <tr>
                                        <th className="essential">{this.l10n("M02669", "사용기간")}</th>
                                        <td>
                                            <SDatePicker id={"masterP.startDtm"} readOnly={this.store.readOnly} contentWidth={80} />
                                            <label className="item_lb">~</label>
                                            <SDatePicker id={"masterP.expiredDtm"} readOnly={this.store.readOnly} contentWidth={80} />
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        :
                        null
                }

                {
                    this.store.srProcDtlType == '06' ?
                        <div>
                            <ContentsMiddleTemplate subTitle={this.l10n("M03447", "LIMS 권한 신청 내용")} />
                            <table id={'table'} style={{ width: "100%" }}>
                                <tbody>
                                    <tr>
                                        <th className="essential">{this.l10n("M00233", "선택")}</th>
                                        <td>
                                            <SCheckBox id={"masterP.newYn"} title={"DB SAFER 신규 발급"} readOnly={this.store.readOnly} />
                                        </td>
                                    </tr>
                                    <tr>
                                        <th className="essential">{this.l10n("M02669", "사용기간")}</th>
                                        <td>
                                            <SDatePicker id={"masterP.startDtm"} readOnly={this.store.readOnly} contentWidth={80} />
                                            <label className="item_lb">~</label>
                                            <SDatePicker id={"masterP.expiredDtm"} readOnly={this.store.readOnly} contentWidth={80} />
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        :
                        null
                }
            </React.Fragment>
        )
    }
}
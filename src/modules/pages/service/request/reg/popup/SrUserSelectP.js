import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { observable, toJS } from 'mobx';
import { BaseComponent } from "utils/base/BaseComponent";
import { BaseStore, gridTrigger } from 'utils/base/BaseStore';
import ContentsTemplate from 'components/template/ContentsTemplate';
import SButton from 'components/atoms/button/SButton';
import ContentsMiddleTemplate from 'components/template/ContentsMiddleTemplate';
import SGrid from 'components/override/grid/SGrid';
import SrUserSelectPModel from "modules/pages/service/request/reg/popup/SrUserSelectPModel";
import STree from 'components/override/tree/STree';
export { SrUserSelectP, SrUserSelectPStore }

class SrUserSelectPStore extends BaseStore {

    @observable masterP = new SrUserSelectPModel();
    @observable companyList = [];
    @observable apprEmpList = [];
    @observable treeData = [];

    initialize = async(param) => {
        if(param) {
            if (param.apprEmpList.length > 0) {
                this.apprEmpList = toJS(param.apprEmpList.filter(x => x.editYn == 'Y'));
            }
        }
    }

    loaded = async () => {
        if(this.apprEmpList.length) {
            this.apprGrid.setRowData(this.apprEmpList);
        }
        await this.handleDeptSearch();
    }

    // 부서 트리 가져오기
    handleDeptSearch = async () => {
        const params = toJS(this.masterP);
        const data = await this.execute(`/service/request/reg/dept/get`, params);
        
        data.data.forEach(element => {
            if(element.parentDeptCd =='Top') {
                element.parentDeptCd = element.detpCd;
            }
        });
        
        if(data.data) {
            this.treeData = data.data.filter(x => x.deptNm != "LS");
        }
    }

    handleTreeSelect = async (key, info) => {
        const param = {
            compCd: info.compCd,
            deptCd: info.deptCd
        }

        if (key.length == 0) {
            return;
        }

        const result = await this.execute(`/service/request/reg/user/get`, param);
        
        if(result.success) {
            await this.setUserValidation(result.data);
        }
    }

    setUserValidation = async(param) => {
        const excludeData = [];
        this.apprEmpList.forEach(element => {
            const tempObj = {
                empNo : element.apprEmpNo
            }

            excludeData.push(tempObj);
        });

        if (param.length > 0) {
            let bindingData;

            if (excludeData) {
                bindingData = [];

                param.map(item => {
                    if (excludeData.some(x => this.apprAddGrid.getKeyValue(item) == this.apprAddGrid.getKeyValue(x))) {
                        
                    }
                    else {
                        bindingData.push(item);
                    }
                });
            } else {
                bindingData = param;
            }
            this.apprAddGrid.setRowData(bindingData);
        } else {
            this.apprAddGrid.clear();
        }
    }

    setApprAddGridApi = async (grid) => {
        this.apprAddGrid = grid;
        
        const columnDefs = [
             { name: "", field: "check", width: 32, type: 'checkbox'}
           , { word: "M00674", name: "사번", field: "empNo", width: 80, align: 'center', key: true, heide: true}
           , { word: "M00979", name: "이름", field: "userNm", width: 80, align: 'center' }
           , { word: "M00677", name: "부서명", field: "deptNm", width: 150}
           , { word: "M00420", name: "이메일", field: "email", width: 130 }
           , { word: "M00679", name: "전화번호", field: "officeNo", width: 100 }
        ];

        this.apprAddGrid.setColumnDefs(columnDefs);
    }

    setApprGridApi = async (grid) => {
        this.apprGrid = grid;

        const columnDefs = [
              { name: "", field: "check", width: 32, type: 'checkbox'}
              , { word: "M00979", name: "이름", field: "apprEmpNm", width: 80, align: 'center' }
              , { word: "M00677", name: "부서명", field: "deptNm", width: 150}
              , { word: "M00621", name: "부서", field: "deptNm", width: 120}
              , { word: "M03371", name: "승인순번", field: "sort", width: 80, align: 'center', editable: true, essential: true, type: 'number'}, 
              , { word: "M00674", name: "사번", field: "apprEmpNo", width: 80, align: 'center', key: true, hide: true }
            
        ];

        this.setOption(columnDefs, "sort", gridTrigger.onCellValueChanged, this.headValueChanged);

        this.apprGrid.setColumnDefs(columnDefs);
    }

    headValueChanged = (param) => {

        const lastSort =  Math.max(...this.apprGrid.getRows().map(o => o.sort), 0);
        const maxSort  =  this.apprGrid.getRows().length * 10;

        this.apprGrid.getRows().forEach(element => {
            if(element.sort >= param.data.sort && param.data.apprEmpNo != element.apprEmpNo) {
                if (lastSort == maxSort && lastSort == element.sort) {
                    return;
                }
                element.sort += 10;
            }
        });

        const apprEmpList = this.apprGrid.getRows().sort((a,b) => (a.sort > b.sort) ? 1 : -1 ); 
        this.apprGrid.setRowData(apprEmpList);
    }

    handleDeleteAppr = () => {
        this.apprGrid.deleteCheckedRows();
    }

    handleSaveAppr = () => {
        this.close(toJS(this.apprGrid.getRows()));
    }

    handleApprAdd = () => {
        const apprAddList = toJS(this.apprAddGrid.getCheckedRows());

        if(apprAddList.length > 0) {
            let maxSort = this.apprGrid.getRows().length > 0 ? this.apprGrid.getRows().map(obj => {
              return obj.sort;
            }).reduce((a, b) => (a.sort > b.sort ? a : b)) : 0;

            apprAddList.forEach(element => {
                maxSort += 1;
                element.sort = maxSort;

                const apprObj = {
                    apprEmpNo : element.empNo,
                    apprEmpNm : element.userNm,
                    deptNm    : element.deptNm,
                    sort      : maxSort,
                    editYn    : 'Y'
                }

                this.apprEmpList.push(apprObj);
            });

            this.apprGrid.setRowData(this.apprEmpList);
            const apprValidationList = this.apprAddGrid.getRows();
            this.setUserValidation(toJS(apprValidationList));
        }
    }
}



@observer
export default class SrUserSelectP extends BaseComponent {
    sRender() {
        return (
            <React.Fragment>
                <div className='ratio_w40 ratio_h100'>
                    <ContentsMiddleTemplate subTitle={"부서목록"}/>
                    <STree
                        treeData={this.store.treeData}
                        keyMember={"deptCd"}
                        pIdMember={"parentDeptCd"}
                        titleMember={"deptNm"}
                        onSelect={this.store.handleTreeSelect}
                        selectedKeyId={"selectedTreeKey"}
                        expandedKeysId={"expandedTreeKeys"}
                        border={true}
                    />
                    </div>
                <div className='ratio_w60 ratio_h100'>
                    <div className='ratio_w100 ratio_h50'>
                        <ContentsMiddleTemplate subTitle={this.l10n("M03432", "승인자목록")}>
                            <SButton buttonName={this.l10n("M00983", "추가")} onClick={this.store.handleApprAdd} type={'default'} />
                        </ContentsMiddleTemplate>
                        <ContentsTemplate id={"apprAdd"} height={"300px"}>
                            <SGrid grid={"apprAddGrid"} gridApiCallBack={this.store.setApprAddGridApi} editable={false}/>
                        </ContentsTemplate>
                    </div>
                    <div className='ratio_w100 ratio_h50'>
                        <ContentsMiddleTemplate subTitle={this.l10n("M03370", "승인자목록")}>
                            <SButton buttonName={this.l10n("M00004", "저장")} onClick={this.store.handleSaveAppr} type={'default'} />
                            <SButton buttonName={this.l10n("M00003", "삭제")} onClick={this.store.handleDeleteAppr} type={'default'} />
                        </ContentsMiddleTemplate>
                        <ContentsTemplate id={"appr"} height={"300px"}>
                            <SGrid grid={"apprGrid"} gridApiCallBack={this.store.setApprGridApi} editable={false} />
                        </ContentsTemplate>
                    </div>
                </div>
            </React.Fragment>
        )
    }
}
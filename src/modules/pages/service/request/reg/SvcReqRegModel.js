import { observable } from 'mobx';
import { BaseModel, essential } from 'utils/base/BaseModel'
import moment from 'moment';
import CommonStore from 'modules/pages/common/service/CommonStore';

export default class SvcReqRegModel extends BaseModel {
    @observable reqEmpNm = CommonStore.loginNm;
    @observable reqEmpNo = CommonStore.userInfo.bempNo;
    @observable officeNo = CommonStore.userInfo.bofficeNo;
    @observable srState;
    @observable reqTitle;
    @observable reqFromDtm = moment().add(-30, 'days').format("YYYYMMDD");
    @observable reqToDtm = moment().format('YYYYMMDD');
}
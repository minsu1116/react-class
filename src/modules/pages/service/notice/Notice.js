import React from 'react';
import { BaseStore, model, gridTrigger } from "utils/base/BaseStore";
import { inject, observer } from "mobx-react";
import { BaseComponent } from "utils/base/BaseComponent";

import ContentsMiddleTemplate from 'components/template/ContentsMiddleTemplate';
import SearchTemplate from 'components/template/SearchTemplate';
import ContentsTemplate from 'components/template/ContentsTemplate';
import SInput from 'components/atoms/input/SInput';
import ModalManage from 'utils/base/ModalManage';

import SButton from 'components/atoms/button/SButton';
import SGrid from 'components/override/grid/SGrid';
import NoticeModel from 'modules/pages/service/notice/NoticeModel';
import CommonStore from 'modules/pages/common/service/CommonStore';


import { observable, toJS } from 'mobx';

export { Notice, NoticeStore }

class NoticeStore extends BaseStore {

    @observable @model master = new NoticeModel();

    noticeAddGroup = CommonStore.codeObject["NOTI_ADD_GROUP"];
    isAdmin = false;

    initialize = async () => {
        if(this.noticeAddGroup.filter(x => x.cd == CommonStore.userInfo.bempNo).length > 0){
            this.isAdmin = true;
        }
    }

    loaded = async () => {
        await this.handleSearchClick();
    }

    setNoticeGridApi = async (grid) => {
        this.noticeRegGrid = grid;

        let columnDefs = [];
        if(this.isAdmin) {
            columnDefs = [
                { name: "", field: "check", width: 32, type: 'checkboxAll' },
                { word: "M00348", name: "제목", field: "title", width: 300, align: 'center' },
                { word: "M03296", field: "조회수", field: "rowCnt", width: 80, type: "number" },
                { word: "M01183", name: "등록일시", field: "inputDtm", width: 120, align: 'center', type: 'dateTimeFull' }
            ];
        } else {
            columnDefs = [
                { word: "M00348", name: "제목", field: "title", width: 300, align: 'center' },
                { word: "M03296", field: "조회수", field: "rowCnt", width: 80, type: "number" },
                { word: "M01183", name: "등록일시", field: "inputDtm", width: 120, align: 'center', type: 'dateTimeFull' }
            ];
        }

        this.noticeRegGrid.setColumnDefs(columnDefs);
    }

    handleSearchClick = async () => {
        const param = toJS(this.master);
        await this.noticeRegGrid.search(`/service/common/notice/get`, param);
    }

    handelNoticeDelete = async () => {
        const params = toJS(this.master);
        await this.noticeRegGrid.deleteCheckedItems(`/service/common/notice/delete`, params);
    }    

    handleOpenPopup = async (params) => {
        const param = params?.data;

        const modal = new ModalManage();
        modal.id = "service_notice_save";
        modal.path = "/service/notice/popup/NoticeSaveP";
        modal.title = this.l10n("T00132", "공지사항 등록");
        if(param) {
            if(this.isAdmin) {
                modal.parameter = {readOnlyYn : "N", masterP : param};
            } else {
                modal.parameter = {readOnlyYn : "Y", masterP : param};
            }
            
        } else {
            modal.parameter = {readOnlyYn : "N"};
        }
        modal.width = 1024;

        const result = await this.showModal(modal);

        if(result) {
            this.noticeRegGrid.setRowData(result.data);
        }
    }
}

@observer
class Notice extends BaseComponent {
    sRender() {
        return (
            <div className="basic">
                <React.Fragment>
                    <SearchTemplate>
                        <div className='search_item_btn'>
                            <SButton buttonName={this.l10n("M00011", "조회")} onClick={this.store.handleSearchClick} type={'search'} />
                        </div>
                        <SInput id={"master.title"}  isFocus={true} enter={this.store.handleSearchClick} title={this.l10n("M00348", "제목")}/>
                    </SearchTemplate>
                    <ContentsTemplate id={"content"}>
                        <ContentsMiddleTemplate subTitle={this.l10n("M03322", "공지사항 목록")} >
                            <SButton buttonName={this.l10n("M00001", "등록")} onClick={this.store.handleOpenPopup} type={'save'} />
                            <SButton buttonName={this.l10n("M00003", "삭제")} onClick={this.store.handelNoticeDelete} type={'delete'} />
                        </ContentsMiddleTemplate>
                        <ContentsTemplate>
                            <SGrid grid={'noticeGrid'} gridApiCallBack={this.store.setNoticeGridApi} rowDoubleClick={this.store.handleOpenPopup} editable={false} />
                        </ContentsTemplate>
                    </ContentsTemplate>
                </React.Fragment>
            </div>
        )
    }
}
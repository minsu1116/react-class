import { BaseModel, essential} from "utils/base/BaseModel";
import { observable } from "mobx";
import moment from "moment";

export default class NoticeSavePModel extends BaseModel {
    
    @observable @essential("공지기간") startDtm = moment().format('YYYYMMDD');
    @observable @essential("공지기간") endDtm = moment().add(+7, "days").format("YYYYMMDD");
    @observable @essential("내용") contents = "";
    @observable @essential("제목") title = "";
    @observable notiGroup = '';
    @observable notiId;
    @observable fileGrpId;
    @observable fileContsId;
    
}
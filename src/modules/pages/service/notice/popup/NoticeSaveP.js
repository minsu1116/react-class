import React from 'react';
import { BaseStore, model} from "utils/base/BaseStore";
import { observer } from "mobx-react";
import { BaseComponent } from "utils/base/BaseComponent";

import ContentsMiddleTemplate from 'components/template/ContentsMiddleTemplate';
import ContentsTemplate from 'components/template/ContentsTemplate';
import SDatePicker from 'components/override/datepicker/SDatePicker';
import SEditor from 'components/override/editor/SEditor';
import SButton from 'components/atoms/button/SButton';
import SInput from 'components/atoms/input/SInput';
import SSelectBox from 'components/atoms/selectbox/SSelectBox';
import SFileMulti from 'components/atoms/file/SFileMulti';
import NoticeSavePModel from 'modules/pages/service/notice/popup/NoticeSavePModel';
import { observable, toJS } from 'mobx';

export { NoticeSaveP, NoticeSavePStore }

class NoticeSavePStore extends BaseStore {

    @observable @model masterP = new NoticeSavePModel();

    readOnlyYn = "Y";
    fileCheck = false

    initialize = async (params) => {
        if(this.readOnlyYn  == 'Y') {
            if(params.masterP.fileGrpId && params.masterP.fileGrpId != 0) {
                this.fileCheck = true;
            }    
        }
    }

    handleSaveClick = async () => {

        const fileGrpId = await this.fileGroupSave();

        if (fileGrpId != false || fileGrpId != "undefined") {
            this.masterP.fileGrpId = fileGrpId;
         }
         
        if (await this.validation("masterP")) {
            const param = toJS(this.masterP);
            const result = await this.execute(`/service/common/notice/save`, param);

            if(result) {
                this.close(result);
            }
        }
    }

    handleUnvisibleUpdate = async () => {
        const param = toJS(this.masterP);
        await this.execute(`/pass/notice/update`, param);
        this.close();
    }

}

@observer
class NoticeSaveP extends BaseComponent {

    sRender() {
        return (
            <div className="basic">
                <React.Fragment>
                    {
                        this.store.readOnlyYn != "Y" ?
                            <div>
                                <ContentsMiddleTemplate>
                                    <SButton buttonName={this.l10n("M00004", "저장")} onClick={this.store.handleSaveClick} type={'default'} />
                                </ContentsMiddleTemplate>
                                <ContentsTemplate>
                                    <table id={'table'} style={{ width: "100%" }} tabletype="th_w100">
                                        <tbody>
                                            <tr>
                                                <th className="essential">{this.l10n("M00348", "제목")}</th>
                                                <td>
                                                    <SInput id={"masterP.title"} width={300} isFocus={true} enter={this.store.handleSearchClick} />
                                                </td>
                                            </tr>
                                            <tr>
                                                <th className="essential">{this.l10n("M03298", "공지기간")}</th>
                                                <td>
                                                    <SDatePicker id={"masterP.startDtm"} />
                                                    <SDatePicker id={"masterP.endDtm"} title={'~'} />
                                                </td>
                                            </tr>
                                            <tr>
                                            <th className="essential">{this.l10n("M03320", "공지대상")}</th>
                                                <td>
                                                    <SSelectBox id={"masterP.notiGroup"} codeGroup={"LOC_CD"}  addOption={this.l10n("M00322", "전체")} />
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </ContentsTemplate>
                                <ContentsTemplate id={"noti_contents"} height={500} isShadow={true}>
                                    <SEditor id={`masterP.contents`}/>
                                </ContentsTemplate>
                                <ContentsTemplate id={"contents"} isShadow={true}>
                                    <SFileMulti fileGrpId={"masterP.fileGrpId"}  height={150}/>
                                </ContentsTemplate>
                            </div>
                            :
                            <div>
                                <ContentsTemplate>
                                    <table id={'table'} style={{ width: "100%" }} tabletype="th_w100">
                                        <tbody>
                                            <tr>
                                                <th>{this.l10n("M00348", "제목")}</th>
                                                <td>
                                                    <SInput id={"masterP.title"} readOnly={true} width={300}/>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </ContentsTemplate>
                                <ContentsTemplate id={"contents"} height={500} isShadow={true}>
                                    <SEditor id={`masterP.contents`} readOnly={true} />
                                </ContentsTemplate>
                                {
                                    this.store.fileCheck ?
                                    <ContentsTemplate id={"contents"} isShadow={true}>
                                        <SFileMulti fileGrpId={"masterP.fileGrpId"} readOnly={true} useFileAttach={false} useFileDelete={false}  height={150}/>
                                    </ContentsTemplate> :
                                    null 
                                }
                                <ContentsTemplate>
                                    <a className="noti" style={{textDecoration: 'underline', float: 'right', marginTop: '8px', marginRight: '3px', fontWeight: 900}} onClick={this.store.handleUnvisibleUpdate}>{this.l10n("M03382", "그만보기")}</a>
                                </ContentsTemplate>
                            </div>
                    }

                </React.Fragment>
            </div>
        )
    }
}
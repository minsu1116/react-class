import { BaseModel } from "utils/base/BaseModel";
import { observable } from "mobx";
import moment from "moment";

export default class NoticeModel extends BaseModel {
    
    @observable title = '';
    
}
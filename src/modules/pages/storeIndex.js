// layouts
import AuthStore from 'modules/layouts/main/service/AuthStore';
import MainStore from 'modules/layouts/main/service/MainStore';
import LoginStore from 'modules/layouts/login/service/LoginStore';


// pages
import CommonStore from 'modules/pages/common/service/CommonStore';

class RootStore {
    constructor() {
        this.authStore = AuthStore;
        this.mainStore = MainStore;
        this.commonStore = CommonStore;
        this.loginStore = LoginStore;

    }

    reloadStore = (formClass) => {
        // if (!formClass) {
        //     return;
        // }

        // const className = formClass.substring(0, 1).toLowerCase() + formClass.substring(1, formClass.length) + "Store"

        // try {
        //     this[className].reset();
        // }
        // catch (exception) {
        //     console.warn(`Store 초기화 실패: ${className}`);
        //     console.warn("가이드 확인 후 수정 바랍니다.");
        // }
    }
}

//Singleton
const instance = new RootStore()
export default instance;
import React from 'react';
import { observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import { BaseComponent } from 'utils/base/BaseComponent'
import { BaseStore, model } from 'utils/base/BaseStore'
import SDatePicker from 'components/override/datepicker/SDatePicker';
import CodeHelper from 'modules/pages/common/codehelper/CodeHelper';
import ContentsMiddleTemplate from 'components/template/ContentsMiddleTemplate';
import ApprChangePModel from 'modules/pages/home/popup/ApprChangePModel'
import CommonStore from 'modules/pages/common/service/CommonStore';
import SButton from 'components/atoms/button/SButton';
import Alert from 'components/override/alert/Alert';
import moment from 'moment';

export { ApprChangeP, ApprChangePStore }

class ApprChangePStore extends BaseStore {


    @observable @model masterP = new ApprChangePModel();

    loaded = async () => {
        const result = await this.execute(`/work/work-appr-mng/temp-appr-emp/change/get`);
        if (result.success && result.data) {
            this.masterP.expiredFrom = result.data.expiredFrom;
            this.masterP.expiredTo = result.data.expiredTo;
            this.masterP.tempApprEmpNo = result.data.tempApprEmpNo;
            this.masterP.tempApprEmpNm = result.data.tempApprEmpNm;
        }

    }
    
    handleSave = async () => {

        const validationTime = moment().format("YYYYMMDD");

        if(this.masterP.expiredFrom < validationTime) {
            this.masterP.expiredFrom = validationTime;
        }

        if(this.masterP.expiredFrom > this.masterP.expiredTo) {
            this.masterP.expiredTo = this.masterP.expiredFrom;
        }

        if (await this.validation("masterP")) {
            const confirmResult = await Alert.confirm(this.l10n("M01490", "저장하시겠습니까?"));
            if (!confirmResult) {
                return;
            }

            const param = toJS(this.masterP);
            const result = await this.execute(`/work/work-appr-mng/temp-appr-emp/change/save`, param);
            
            if (result.success) {
                this.close()
            }
        }
    }

    handleDelete = async () => {
        this.masterP.tempApprEmpNo = CommonStore.userInfo.bempNo;

        if (await this.validation("masterP")) {
            const confirmResult = await Alert.confirm(this.l10n("M01492", "삭제하시겠습니까?"));
            if (!confirmResult) {
                return;
            }

            const param = toJS(this.masterP);
            const result = await this.execute(`/work/work-appr-mng/temp-appr-emp/change/save`, param);
            
            if (result.success) {
                this.close()
            }
        }
    }
}

@observer
class ApprChangeP extends BaseComponent {

    sRender() {
        return (
            <React.Fragment>
                <ContentsMiddleTemplate>
                    <SButton buttonName={this.l10n("M00004", "저장")} type={"default"} onClick={this.store.handleSave} />
                    <SButton buttonName={this.l10n("M00003", "삭제")} type={"default"} onClick={this.store.handleDelete} />
                </ContentsMiddleTemplate>
                <table id={'table'} style={{ width: "100%" }} >
                    <tbody>
                        <tr>
                            <th>{this.l10n("M03474", "대리인")}</th>
                            <td>
                                <CodeHelper helperCode={"masterP.tempApprEmpNo"} codeNmWidth={120} helperCodeNm={"masterP.tempApprEmpNm"} mainStore={this.store} business={"user"} codeVisible={false} />
                            </td>
                            <th>{this.l10n("M03473", "대리기간")}</th>
                            <td>
                                <SDatePicker id={"masterP.expiredFrom"} />
                                <SDatePicker id={"masterP.expiredTo"} title={'~'} />
                            </td>
                        </tr>
                    </tbody>
                </table>
            </React.Fragment>
        )
    }
}
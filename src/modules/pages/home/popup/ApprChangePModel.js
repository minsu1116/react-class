import { observable } from 'mobx';
import { BaseModel, essential } from 'utils/base/BaseModel'
import moment from 'moment';

export default class ApprChangePModel extends BaseModel {
    @observable @essential("대리인") tempApprEmpNo;
    @observable @essential("대리기간") expiredFrom = moment().format("YYYYMMDD");
    @observable @essential("대리기간") expiredTo = moment().add(1, 'days').format("YYYYMMDD");
    @observable tempApprEmpNm;
}
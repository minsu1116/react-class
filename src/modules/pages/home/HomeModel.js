import { BaseModel } from "utils/base/BaseModel";
import { observable } from "mobx";

export default class HomeModel extends BaseModel {
    
    @observable sysNm;
    @observable categoryNm;
    @observable useYn = '';
}

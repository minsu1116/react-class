import React from 'react';
import { BaseStore, gridTrigger, model } from "utils/base/BaseStore";
import { inject, observer } from "mobx-react";
import { BaseComponent } from "utils/base/BaseComponent";
import ContentsTemplate from 'components/template/ContentsTemplate';
import { SChart, SChartXAxis, SChartYAxis, SChartTooltip, SChartLegend, SChartBar, SChartLine, SPie, SPieChart, SCell } from 'components/override/chart/SChart';
import SWeather from 'components/override/weather/SWeather';
import ModalManage from 'utils/base/ModalManage';
import SrRepository from 'modules/layouts/main/service/SrRepository';
import { Label, Text } from 'recharts'
import CommonStore from 'modules/pages/common/service/CommonStore';
import { observable, toJS } from 'mobx';
import moment from 'moment';
import SvcReqRegModel from 'modules/pages/service/request/reg/SvcReqRegModel'
import request from 'style/img/ico_request.png';
import change from 'style/img/ico_change.png';
import ReactTooltip from 'react-tooltip';
import MainStore from 'modules/layouts/main/service/MainStore';
import { SrDetailMainP, SrDetailMainPStore } from 'modules/pages/service/request/todo/popup//SrDetailMainP';

import enter from 'style/img/ico_enter.png';

export { Home, HomeStore }
class HomeStore extends BaseStore {

    @observable master = new SvcReqRegModel();
    @observable currWorkNm = "";
    @observable chartData = [];
    @observable reqData = [];
    @observable todoList = [];
    @observable weatherList = [];
    @observable noticeList = [];

    todoColorList = ["#8bba81", "#fccd14", "#d87373", "#2ea3d1"];
    reqColorList = ["#8bba81", "#fccd14", "#d87373", "#2d74f0", "#2ea3d1", "#8de6dd", "#9c9cd5"];
    cityGroup = undefined;

    topMenuList = MainStore.topMenuList;

    reqCtn = '';
    todoCtn = '';
    srModel = undefined;

    intervalWeather = undefined;
    intervalContents = undefined;

    loaded = async () => {
        if (CommonStore.workCategory_sr) {
            await this.modalOpenAccept(CommonStore.workCategory_sr);
        }
    }

    initialize = async () => {
        this.handleReqSearch();
        this.getWeather();
        this.getNotice();
        await this.handleTodoSearch();

        this.intervalWeather = setInterval(this.getWeather, 1500000);
        // this.intervalContents = setInterval(this.getContents, 120000);
    }

    getContents = () => {
        this.handleTodoSearch();
        this.handleReqSearch();
    }

    getNotice = async () => {
        const param = {};
        const result = await this.execute(`/service/common/notice/get`, param);

        if (result.success) {
            this.noticeList = result.data;
        }
    }

    getWeather = async () => {
        const cityCode = CommonStore.codeObject["CITY_GROUP"];
        this.cityGroup = cityCode;
        const param = {
            cityNm: 'anyang-si,seoul,gumi,donghae-si'
        }
        const result = await this.execute(`/pass/weather/get`, param);

        if (result.data && result.data.length > 0) {
            this.weatherList = result.data;
        }

    }

    handleReqSearch = async () => {

        this.reqData = [];
        const srState = ['110', '200', '300', '310', '400', '900', '910'];

        const params = {
            'reqFromDtm': moment().add(-30, 'days').format("YYYYMMDD"),
            'reqToDtm': moment().format('YYYYMMDD'),
            'reqEMpNo': CommonStore.userInfo.bempNo
        }

        const reqList = await this.execute(`/service/request/reg/get`, params);
        this.reqCtn = reqList.data.length;

        const srTypeMap = CommonStore.codeObject["SR_PROC_STAT"];
        if (reqList.data && reqList.data.length > 0) {
            srState.forEach(element => {
                const key = srTypeMap.find(x => x.cd == element).cdNm;
                this.reqData.push(
                    {
                        'name': `${key}(${reqList.data.filter(x => x.srState == element).length}건)`,
                        'value': reqList.data.filter(x => x.srState == element).length
                    }
                )
            });
        } else {
            this.reqData.push(
                {
                    'name': '없음',
                    'value': 0
                }
            )
        }
    }

    handleTodoSearch = async () => {

        this.chartData = [];

        const srState = ['110', '200', '300', '400'];
        const param = {
            'srStateList': srState
        };

        const result = await this.execute(`/service/request/todo/todo/all/get`, param);
        this.todoCtn = result.data.length;
        const srTypeMap = CommonStore.codeObject["SR_PROC_STAT"];
        if (result.success) {

            this.todoList = result.data;

            srState.forEach(element => {

                if (element != '300') {
                    const key = srTypeMap.find(x => x.cd == element).cdNm;
                    this.chartData.push(
                        {
                            'name': `${key}(${result.data.filter(x => x.srState == element).length}건)`,
                            'value': result.data.filter(x => x.srState == element).length
                        }
                    )
                } else {
                    const key = srTypeMap.find(x => x.cd == element).cdNm;
                    this.chartData.push(
                        {
                            'name': `${key}(${result.data.filter(x => x.srState == element || x.srState == '310').length}건)`,
                            'value': result.data.filter(x => x.srState == element || x.srState == '310').length
                        }
                    )
                }
            });
        } else {
            this.chartData.push(
                {
                    'name': '없음',
                    'value': 0
                }
            )
        }
    }

    handleRegSr = async () => {
        const modal = new ModalManage();
        this.master.popupMode = "C";
        modal.id = "service_request_reg_save_p";
        modal.path = "/service/request/reg/popup/SvcReqRegSaveP";
        modal.title = this.l10n("M03339"); //서비스요청 등록
        modal.parameter = { 'masterP': this.master };
        modal.width = 1133;

        await this.showModal(modal);

        await this.handleTodoSearch();
        await this.handleReqSearch();
        
    }

    handleNotiDtlOpen = async (param) => {
        const modal = new ModalManage();
        modal.id = "service_notice_save";
        modal.path = "/service/notice/popup/NoticeSaveP";
        modal.title = this.l10n("T00134", "공지사항");
        modal.parameter = { readOnlyYn: "Y", masterP: param };
        modal.width = 1024;

        await this.showModal(modal);
    }

    handleTodoRowClick = async (obj) => {
        const modal = new ModalManage();
        modal.id = "service_request_todo_sr_detail_main_p";
        modal.path = "/service/request/todo/popup/SrDetailMainP";

        // modal.path = "/service/request/reg/popup/SvcReqRegSaveP";
        modal.parameter = obj;
        modal.width = 1133;
        modal.title = this.l10n("M03441", "서비스 요청상세")
        const result = await this.showModal(modal);

        const param = {};
        if (result) {
            const saveList = [];
            let params = undefined;

            if (obj) {
                result.srId = obj.srId;
                result.categoryId = obj.categoryId;
            }

            switch (result.srState) {
                case '110':
                    // 승인 처리
                    result.reviewGubn = result.gubn;
                    saveList.push(result);
                    params = JSON.stringify({ param1: saveList, param2: param });
                    await this.execute(`/service/request/todo/appr-todo/save`, params)
                    break;
                case '300':
                    // 접수
                    params = JSON.stringify({ param1: result, param2: param });
                    await this.execute(`/service/request/todo/mng-todo/save`, params);
                    break;
                case '400':
                    // 접수
                    params = JSON.stringify({ param1: result, param2: param });
                    await this.execute(`/service/request/todo/proc-todo/save`, params);
                    break;
                case '900':
                case '310':
                    params = JSON.stringify({ param1: result, param2: param });
                    await this.execute(`/service/request/todo/complt-todo/save`, params);
                    break;
                default:
                    break;
            }
            await this.handleTodoSearch();
            await this.handleReqSearch();
        }
    }

    openTodoMenu = (tabIndex) => {
        MainStore.addMenu(this.topMenuList[0]?.subMenu[0]?.subMenu[1], { 'todoIndex': tabIndex }, false);
    }

    modalOpenAccept = async (srId) => {
        try {
            if (!srId) {
                await Alert.meg("서비스요청 정보가 전달되지 않았습니다.<br/>관리자에게 문의해주세요.");
                return;
            }

            const param = {
                'srId': srId
            }

            const result = await SrRepository.getSr(param);

            this.srModel = result.data.data.length > 0 ? result.data.data[0] : null;
            const modal = new ModalManage();
            // modal.path = "/service/request/reg/popup/SvcReqRegSaveP";
            modal.parameter = result.data.data && result.data.data.length > 0 ? result.data.data[0] : null;
            modal.width = 1133;
            modal.height = 950;
            modal.title = this.l10n("M03441", "서비스 요청상세");
            modal.id = "sr/" + srId;
            modal.isOpen = true;
            modal.contents = <SrDetailMainP store={new SrDetailMainPStore()} _closeHandler={this._closeSrModal} isModal={true} _parameter={modal.parameter} />
            this._modalManage = modal;
        }
        catch (e) {
            console.warn("modalOpenAccept ERROR: ", e);
        }
        finally {
            window.localStorage.removeItem('popupYn');
            if (CommonStore.workCategory_sr) {
                window.localStorage.removeItem('workCategory_sr');
            }
        }
    }

    _closeSrModal = async (result) => {
        this._modalManage = null;
        const param = {};
        if (result) {
            const saveList = [];
            let params = undefined;

            if (this.srModel) {
                result.srId = this.srModel.srId;
                result.categoryId = this.srModel.categoryId;
            }

            switch (result.srState) {
                case '110':
                    // 승인 처리
                    result.reviewGubn = result.gubn;
                    saveList.push(result);
                    params = JSON.stringify({ param1: saveList, param2: param });
                    await this.execute(`/service/request/todo/appr-todo/save`, params)
                    break;
                case '300':
                    // 접수
                    params = JSON.stringify({ param1: result, param2: param });
                    await this.execute(`/service/request/todo/mng-todo/save`, params);
                    break;
                case '400':
                    // 접수
                    params = JSON.stringify({ param1: result, param2: param });
                    await this.execute(`/service/request/todo/proc-todo/save`, params);
                    break;
                case '900':
                case '310':
                    params = JSON.stringify({ param1: result, param2: param });
                    await this.execute(`/service/request/todo/complt-todo/save`, params);
                    break;
                default:
                    break;
            }
            await this.handleTodoSearch();
            await this.handleReqSearch();
        }

    }


    openNotice = () => {
        MainStore.addMenu(this.topMenuList[1]?.subMenu[0]?.subMenu[0], null, false);
    }

    handleApprChg = async () => {
        const modal = new ModalManage();
        modal.id = "home_ApprChange_P";
        modal.path = "/home/popup/ApprChangeP";
        modal.title = this.l10n("M03475", "대결자 등록");
        modal.width = 555;

        await this.showModal(modal);
    }
}

@observer
class Home extends BaseComponent {
    sRender() {
        const { chartData, reqData, todoColorList, reqColorList, handleTodoRowClick } = this.store;

        const weatherCoponent = [];
        if (this.store.weatherList.length > 0) {
            this.store.weatherList.forEach(element => {
                weatherCoponent.push(
                    <SWeather model={element} cityGroup={this.store.cityGroup} key={element.name} />
                )
            });
        }

        const noti_component = [];

        if (this.store.noticeList && this.store.noticeList.length > 0) {

            for (let index = 0; index < this.store.noticeList.length; index++) {
                const element = this.store.noticeList[index];
                let isOdd = false;
                if (noti_component.length % 2 != 0) {
                    isOdd = true;
                }
                
                noti_component.push(
                    <div key={`noti_${index}`} className={`main_noti`} data-tip='' data-for={`noti_${index}`} onClick={() => this.store.handleNotiDtlOpen(element)}>
                        <ReactTooltip place={'bottom'} html={true} backgroundColor={"white"} textColor={'black'} borderColor={'green'} border={true} id={`noti_${index}`}>
                            {
                                `
                                    <div class="noti_tip">
                                        <div class="tip">
                                            <div class="tip_tit">${this.store.l10n("M00360", "등록자")}</div>
                                            <div class="tip_txt">${element.inputEmpNm}</div>
                                        </div>
                                        <div class="tip">
                                            <div class="tip_tit">${this.store.l10n("M00348", "제목")}</div>
                                            <div class="tip_txt">${element.title}</div>
                                        </div>
                                        <div class="tip">
                                            <div class="tip_tit">${this.store.l10n("M00406", "내용")}</div>
                                        </div>
                                        <div class="tip">
                                            <div class="tip_txt_contents"> ${element.contents}</div>
                                        </div>
                                    </div>
                                    `
                            }
                        </ReactTooltip>

                        <div className={"main_todo_txt"} style={{ float: 'left' }}>{element.title}</div>
                        <div className={"main_todo_txt"} style={{ float: 'right', color: '#76abe0bd' }}>
                            {`${element.inputEmpNm}`}
                        </div>
                        <div className={"main_todo_txt"} style={{ float: 'right', color: '#76abe0bd' }}>
                            {element.inputDtm}
                        </div>
                    </div>
                )
            };
        }


        const srStateList = ['110', '200', '300', '310', '400'];
        const todo_110_component = [];
        const todo_200_component = [];
        const todo_300_component = [];
        const todo_400_component = [];


        srStateList.forEach(element => {
            let todoList = this.store.todoList.filter(x => x.srState == element);
            switch (element) {
                case '110':
                    srTdoRender(todoList, todo_110_component, this);
                    break;
                case '200':
                    srTdoRender(todoList, todo_200_component, this);
                    break;
                case '300':
                case '310':
                    srTdoRender(todoList, todo_300_component, this);
                    break;
                case '400':
                    srTdoRender(todoList, todo_400_component, this);
                    break;

            }
        });

        function srTdoRender(todoList, todoComponent, store) {
            todoList.forEach(element => {
                let isOdd = false;
                if (todoComponent.length % 2 != 0) {
                    isOdd = true;
                }
                todoComponent.push(
                    <div className={`main_todo_${!isOdd ? 'odd' : 'even'}`} key={`todo${element.srId}`} data-tip='' data-for={`todo${element.srId}`} onClick={() => handleTodoRowClick(element)}>
                        <ReactTooltip type={'info'} place={'top'} html={true} backgroundColor={"white"} textColor={'black'} borderColor={'green'} border={true} id={`todo${element.srId}`}>
                            {
                                `
                                    <div>
                                        <div class="tip">
                                            <div class="tip_tit">${store.l10n("M03327", "요청자")}</div>
                                            <div class="tip_txt">${element.reqEmpNm}</div>
                                        </div>
                                        <div class="tip">
                                            <div class="tip_tit">${store.l10n("M03328", "IT업무")}</div>
                                            <div class="tip_txt">${element.categoryNm}</div>
                                        </div>
                                        <div class="tip">
                                            <div class="tip_tit">${store.l10n("M00348", "제목")}</div>
                                            <div class="tip_txt">${element.reqTitle}</div>
                                        </div>
                                        <div class="tip">
                                            <div class="tip_tit">${store.l10n("M00406", "내용")}</div>
                                        </div>
                                        <div class="tip">
                                            <div class="tip_txt_contents"> ${element.reqContents}</div>
                                        </div>
                                    </div>
                                    `
                            }
                        </ReactTooltip>
                        <span className={"main_todo_txt"}>{`[${element.categoryNm}] ${element.reqTitle}`}</span>
                    </div>
                )
            });
        }

        return (
            <React.Fragment>
                <div className="ratio_w100 ratio_h30 shadow main_top">
                    <ContentsTemplate id={"mainTop"}>
                        <div className='user_txt_box'>
                            <p className="user_dept">{CommonStore.userInfo.bdeptNm}</p><p className="user_nm"> {CommonStore.loginNm}</p>
                        </div>
                        <div className="weather">
                            <div className="home_contents">
                                <div className="home_item" onClick={this.store.handleRegSr} >
                                    <div>
                                        <img className="main_req_img" src={request} />
                                    </div>
                                    <div style={{ margin: "2px 0px 0px 42px" }}>
                                        <span className="main_req_txt">{this.l10n("M03463", "서비스 요청")}</span>
                                    </div>
                                </div>
                                <div className="home_item" onClick={this.store.handleApprChg} >
                                    <div>
                                        <img className="main_req_img" src={change} />
                                    </div>
                                    <div style={{ margin: "2px 0px 0px 25px" }}>
                                        <span className="main_req_txt">{this.l10n("M03472", "대리 승인자 지정")}</span>
                                    </div>
                                </div>
                            </div>
                            {
                                weatherCoponent.length > 0 ?
                                    weatherCoponent : null
                            }
                        </div>
                    </ContentsTemplate>
                </div>
                <div className="ratio_w100 ratio_h35 shadow home_layout">
                    <div className="ratio_w50 ratio_h100 shadow">
                        <ContentsTemplate>
                            <div className="main_tit">
                                <span>{this.l10n("M03464", "공지사항")}</span>
                                <img className={"main_enter_img"} src={enter} onClick={this.store.openNotice} />
                            </div>
                            <div className={noti_component.length > 0 ? "main_middle" :"main_middle empty"} style={{ padding: '3px 0px 0px 0px', display: 'grid' }}>
                                {
                                    noti_component.length > 0
                                        ? noti_component :
                                        null
                                }
                            </div>
                        </ContentsTemplate>
                    </div>
                    <div className="ratio_w25 ratio_h100 shadow">
                        <ContentsTemplate>
                            {chartData.length > 0 ?
                                <SChart id="todo" background={"transparent"} anotherData={true} subTitle={`TO-DO Chart (${this.store.todoCtn}${this.l10n("M03483", "건")})`}>
                                    <SPieChart width={300} height={300}>
                                        <SChartTooltip />
                                        <SChartLegend verticalAlign='middle' align='right' layout='vertical' />
                                        <SPie data={toJS(chartData)} dataKey={"value"} cx={"45%"} cy={"45%"} fill="#8884d8" innerRadius={40} outerRadius={115}>
                                            <Label value={""} position="center" className='total_contents' />
                                            {
                                                chartData.map((entry, i) => <SCell key={`cell-${i}`} fill={todoColorList[i % todoColorList.length]} />)
                                            }
                                        </SPie>
                                    </SPieChart>
                                </SChart> : null
                            }
                        </ContentsTemplate>
                    </div>
                    <div className="ratio_w25 ratio_h100 shadow">
                        <ContentsTemplate>
                            {reqData.length > 0 ?
                                <SChart id="todo" background={"transparent"} anotherData={true} subTitle={`요청현황 Chart (${this.store.reqCtn}${this.l10n("M03483", "건")})`}>
                                    <SPieChart width={300} height={300}>
                                        <SChartTooltip />
                                        <SChartLegend verticalAlign='middle' align='right' layout='vertical' />
                                        <SPie data={toJS(reqData)} dataKey={"value"} cx={"45%"} cy={"45%"} fill="#8884d8" innerRadius={40} outerRadius={115}>
                                            <Label value={""} position="center" className='total_contents' />
                                            {
                                                reqData.map((entry, i) => <SCell key={`cell-${i}`} fill={reqColorList[i % reqColorList.length]} />)
                                            }
                                        </SPie>
                                    </SPieChart>
                                </SChart> : null
                            }
                        </ContentsTemplate>
                    </div>
                </div>
                <div className="ratio_w100 ratio_h35 shadow home_layout">
                    <div className="ratio_w25 ratio_h100 shadow">
                        <ContentsTemplate>
                            <div className="main_tit">
                                <span>{`${this.l10n("M03459", "승인요청 목록")}(${this.store.todoList.filter(x => x.srState == '110').length}${this.l10n("M03483", "건")})`}</span>
                                <img className={"main_enter_img"} src={enter} onClick={() => this.store.openTodoMenu(0)} />
                            </div>
                            <div className={todo_110_component.length >  0 ? "main_middle" : "main_middle empty" }>
                                {todo_110_component.length > 0
                                    ? todo_110_component :
                                    null
                                }
                            </div>
                        </ContentsTemplate>
                    </div>
                    <div className="ratio_w25 ratio_h100 shadow">
                        <ContentsTemplate>
                            <div className="main_tit">
                                <span>{`${this.l10n("M03462", "접수요청 목록")}(${this.store.todoList.filter(x => x.srState == '200').length}${this.l10n("M03483", "건")})`}</span>
                                <img className={"main_enter_img"} src={enter} onClick={() => this.store.openTodoMenu(1)} />
                            </div>
                            <div className={todo_200_component.length >  0 ? "main_middle" : "main_middle empty" }>
                                {todo_200_component.length > 0
                                    ? todo_200_component :
                                    null
                                }
                            </div>
                        </ContentsTemplate>
                    </div>
                    <div className="ratio_w25 ratio_h100 shadow">
                        <ContentsTemplate>
                            <div className="main_tit">
                                <span>{`${this.l10n("M03461", "처리요청 목록")}(${this.store.todoList.filter(x => x.srState == '300' || x.srState == '310').length}${this.l10n("M03483", "건")})`}</span>
                                <img className={"main_enter_img"} src={enter} onClick={() => this.store.openTodoMenu(2)} />
                            </div>
                            <div className={todo_300_component.length >  0 ? "main_middle" : "main_middle empty" }>
                                {todo_300_component.length > 0
                                    ? todo_300_component :
                                    null
                                }
                            </div>
                        </ContentsTemplate>
                    </div>
                    <div className="ratio_w25 ratio_h100 shadow">
                        <ContentsTemplate>
                            <div className="main_tit">
                                <span>{`${this.l10n("M03460", "처리확인요청 목록")}(${this.store.todoList.filter(x => x.srState == '400').length}${this.l10n("M03483", "건")})`}</span>
                                <img className={"main_enter_img"} src={enter} onClick={() => this.store.openTodoMenu(3)} />
                            </div>
                            <div className={todo_400_component.length >  0 ? "main_middle" : "main_middle empty" }>
                                {todo_400_component.length > 0
                                    ? todo_400_component :
                                    null
                                }
                            </div>
                        </ContentsTemplate>
                    </div>
                </div>
            </React.Fragment>
        )
    }
}
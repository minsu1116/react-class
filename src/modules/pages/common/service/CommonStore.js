import { observable, action, reaction, toJS } from 'mobx';
import CommonRepository from "modules/pages/common/service/CommonRepository"
import LoginRepository from "modules/layouts/login/service/LoginRepository"
import MainStore from "modules/layouts/main/service/MainStore"
import API from 'components/override/api/API'
import { setLocale } from 'utils/word/locale'
import RootStore from 'modules/pages/storeIndex'
import Alert from 'components/override/alert/Alert';

class CommonStore {

    @observable token = window.localStorage.getItem('jwt');
    @observable loginId = window.localStorage.getItem('loginId');
    @observable loginNm = window.localStorage.getItem('loginNm');
    @observable deptNm = window.localStorage.getItem('deptNm');
    @observable locale = window.localStorage.getItem('locale');
    @observable compCd = window.localStorage.getItem('compCd');
    @observable pcAdminAuth = window.localStorage.getItem('pcAdminAuth');
    @observable compList = JSON.parse(toJS(window.sessionStorage.getItem('compList')));
    @observable deptList = JSON.parse(toJS(window.sessionStorage.getItem('deptList')));
    @observable lang = window.localStorage.getItem('lang');
    @observable isSystem; //관리자 여부
    @observable userInfo = null; //window.sessionStorage.getItem('userInfo') ? JSON.parse(window.sessionStorage.getItem('userInfo')) : null;
    @observable empPhoto = window.localStorage.getItem('empPhoto');
    @observable helperInfo;
    @observable familySite = [];
    @observable popupYn = window.localStorage.getItem('popupYn');
    @observable workCategory = window.localStorage.getItem('workCategory');
    @observable workCategory_sr = window.localStorage.getItem('workCategory_sr');
    codeObject;
    codeHelper = JSON.parse(window.localStorage.getItem('codeHelper'));
    allServer = JSON.parse(window.localStorage.getItem('allServer')) || [];
    mgrServer = JSON.parse(window.localStorage.getItem('mgrServer')) || [];

    homeEvent = [];

    constructor() {

        reaction(() => this.token, token => {
            if (token) {
                window.localStorage.setItem('jwt', token);
            } else {
                window.localStorage.removeItem('jwt');
            }
        });

        reaction(() => this.loginId, loginId => {
            if (loginId) {
                window.localStorage.setItem('loginId', loginId);
            } else {
                window.localStorage.removeItem('loginId');
            }
        });

        reaction(() => this.loginNm, loginNm => {
            if (loginNm) {
                window.localStorage.setItem('loginNm', loginNm);
            } else {
                window.localStorage.removeItem('loginNm');
            }
        });

        reaction(() => this.deptNm, deptNm => {
            if (deptNm) {
                window.localStorage.setItem('deptNm', deptNm);
            } else {
                window.localStorage.removeItem('deptNm');
            }
        });

        reaction(() => this.compCd, compCd => {
            if (compCd) {
                window.localStorage.setItem('compCd', compCd);
            } else {
                window.localStorage.removeItem('compCd');
            }
        });

        reaction(() => this.workCategory, workCategory => {
            if (workCategory) {
                window.localStorage.setItem('workCategory', workCategory);
            } else {
                window.localStorage.removeItem('workCategory');
            }
        });

        reaction(() => this.popupYn, popupYn => {
            if (popupYn) {
                window.localStorage.setItem('popupYn', popupYn);
            } else {
                window.localStorage.removeItem('popupYn');
            }
        });

        reaction(() => this.codeObject, codeObject => {
            if (codeObject) {
                window.localStorage.setItem('codeObject', codeObject);
            } else {
                window.localStorage.removeItem('codeObject');
            }
        });

        reaction(() => this.codeHelper, codeHelper => {
            if (codeHelper) {
                window.localStorage.setItem('codeHelper', codeHelper);
            } else {
                window.localStorage.removeItem('codeHelper');
            }
        });
    }

    @action.bound
    async checkToken() {
        const { data, status } = await CommonRepository.checkToken();

        if (status == 200) {
            return data;
        } else {
            return null;
        }
    }

    @action
    setUserInfo(userInfo) {
        this.userInfo = userInfo;
        window.sessionStorage.setItem('userInfo', JSON.stringify(userInfo));
        this.isSystem = userInfo.bisSystem ? userInfo.bisSystem : false;
    }

    @action
    setCodeObject(codeObject) {

        this.codeObject = codeObject;
        window.localStorage.setItem('codeObject', codeObject);
    }

    @action
    setPopupYn(popupYn) {
        this.popupYn = popupYn;
        window.localStorage.setItem('popupYn', popupYn);
    }

    @action
    setWorkCategory(workCategory) {
        this.workCategory = workCategory;
        window.localStorage.setItem('workCategory', workCategory);
    }

    @action
    setWorkCategory_sr(workCategory_sr) {
        this.workCategory_sr = workCategory_sr;
        window.localStorage.setItem('workCategory_sr', workCategory_sr);
    }

    @action
    setToken(token) {
        this.token = token;
        window.localStorage.setItem('jwt', token);
    }

    @action
    setLoginId(loginId) {
        this.loginId = loginId;
        window.localStorage.setItem('loginId', loginId);
    }

    @action
    setLoginNm(loginNm) {
        this.loginNm = loginNm;
        window.localStorage.setItem('loginNm', loginNm);
    }

    @action
    setDeptNm(deptNm) {
        this.deptNm = deptNm;
        window.localStorage.setItem('deptNm', deptNm);
    }

    @action
    setLocale(locale) {
        if (locale) {
            this.locale = locale;
        } else {
            this.locale = "KR";
        }
        window.localStorage.setItem('locale', this.locale);
    }

    @action
    setCompCd(compCd) {
        this.compCd = compCd;
        window.localStorage.setItem('compCd', compCd);
    }

    @action
    setPcAdminAuth(pcAdminAuth) {
        this.pcAdminAuth = pcAdminAuth;
        window.localStorage.setItem('pcAdminAuth', pcAdminAuth);
    }

    @action
    setCompList(compList) {
        this.compList = compList;
        window.sessionStorage.setItem('compList', JSON.stringify(compList));
    }

    @action
    setDeptList(deptList) {
        this.deptList = deptList;
        window.sessionStorage.setItem('deptList', JSON.stringify(deptList));
    }

    @action
    setCodeHelper(codeHelper) {
        this.codeHelper = codeHelper;
        window.localStorage.setItem('codeHelper', codeHelper);
    }

    @action
    setEmpPhto(empPhoto) {
        this.empPhoto = empPhoto == undefined ? '' : empPhoto;
        window.localStorage.setItem('empPhoto', empPhoto == undefined ? '' : empPhoto);
    }

    @action clearLocalStorage() {
        this.loginId = undefined;
        this.loginNm = undefined;
        this.deptNm = undefined;
        this.compCd = undefined;
        this.pcAdminAuth = undefined;
        this.compList = undefined;
        this.deptList = undefined;
        this.token = undefined;
        this.codeObject = undefined;
        this.empPhoto = undefined;
        this.mgrServer = undefined;
        window.localStorage.removeItem('loginId');
        window.localStorage.removeItem('loginNm');
        window.localStorage.removeItem('deptNm');
        window.localStorage.removeItem('plnt');
        window.localStorage.removeItem('bpCd');
        window.localStorage.removeItem('compCd');
        window.localStorage.removeItem('pcAdminAuth');
        window.localStorage.removeItem('jwt');
        window.localStorage.removeItem('codeObject');
        window.localStorage.removeItem('empPhoto');
        window.localStorage.removeItem('mgrServer');
        window.localStorage.clear();

        MainStore.clearMenu();
        window.sessionStorage.clear();
    }

    /**
     * 공통코드 가져오기
     * @cdType 가져올 CD_TYPE
     */
    getCdType = (cdType) => {
        const array = this.codeObject[cdType];
        const result = {};
        array.map(x => {
            result[x.cd] = x;
        });
        return result;
    }

    @action
    async getCodeObject() {
        if (!this.codeObject) {
            await this.requestGetCodes();
        }

        const arrayData = this.codeObject;
        return arrayData;
    }

    @action
    async getCodeObjectSelectBox(codeNameGroup) {
        if (!this.codeObject) {
            await this.requestGetCodes();
        }

        let resultArr = {};

        for (let i = 0; i < codeNameGroup.length; i++) {
            const element = codeNameGroup[i];
            let codeArr = this.codeObject[element.code].concat();
            if (element.allAdd) {
                codeArr.unshift({ cd: '', cdNm: "ALL" });
            }
            resultArr[element.code] = codeArr;
        }

        return resultArr;
    }

    @action
    async getCodeObjectMultiSelect(code, addAll) {
        if (!this.codeObject) {
            await this.requestGetCodes();
        }

        const options = this.codeObject[code];
        const resultArr = [];
        for (let i = 0; i < options.length; i++) {
            const element = options[i];
            resultArr.push({ value: element.cd, label: element.cdNm });
        }

        // if(addAll) {
        //   resultArr.unshift({value:'', label:"ALL"});
        // }

        return resultArr;
    }

    @action
    async requestGetCodes() {
        const params = {
            cdGubn: '',
            useFlag: 'Y',
            compCd: this.compCd,
        };

        const { data, status } = await CommonRepository.getCodes(params);

        if (data.success) {
            this.codeObject = data.data;
            window.localStorage.setItem('codeObject', JSON.stringify(data.data));
            // FAMILY SITE 설정
            const familySite = this.codeObject["FAMILY_GROUP"];
            this.familySite = familySite;

        } else {
            //alert('공통 코드를 가져오지 못하였습니다.');
        }
    }

    @action
    async commonUserInfo() {

        const params = {
            bempNo: this.loginId,
        };

        const { data, status } = await LoginRepository.getLoginUserInfo(params);

        if (status == 200) {
            this.setUserInfo(data.data);
        }
    }

    async getUserWord() {
        const params = {
            langCd: this.userInfo.langCd
        }
        
        const result = await API.request.post(encodeURI(`/api/pass/login/word`), params);

        setLocale(result.data.data);

        // console.log("localeKR", localeKR)
        // localeKR = result.data.data;

        // if (this.isIE()) {
        //     this.saveFile_IE("json.txt", result.data.data);
        // } else {
        //     this.saveFile_chrome("json.txt", result.data.data);
        // }
    }

    saveFile_chrome(fileName, content) {
        var blob = new Blob([JSON.stringify(content)], { type: 'text/plain' });

        const objURL = window.URL.createObjectURL(blob);

        // 이전에 생성된 메모리 해제
        if (window.__Xr_objURL_forCreatingFile__) {
            window.URL.revokeObjectURL(window.__Xr_objURL_forCreatingFile__);
        }
        window.__Xr_objURL_forCreatingFile__ = objURL;

        const a = document.createElement('a');

        a.download = fileName;
        a.href = objURL;
        a.click();
    }

    saveFile_IE(fileName, content) {
        var blob = new Blob([JSON.stringify(content)], { type: "text/plain", endings: "native" });

        window.navigator.msSaveBlob(blob, fileName);
    }

    isIE() {
        return (navigator.appName === 'Netscape' && navigator.userAgent.search('Trident') !== -1) ||
            navigator.userAgent.toLowerCase().indexOf("msie") !== -1;
    }

    getCompList = async () => {
        const result = await API.request.post(encodeURI(`/api/pass/login/comp`));
        if (result.data.success) {
            this.setCompList(toJS(result.data.data));
        }
    }

    getDeptList = async () => {
        const result = await API.request.post(encodeURI(`/api/pass/login/dept`));
        if (result.data.success) {
            this.setDeptList(toJS(result.data.data));
        }
    }

    /**
     * 부서정보 가져오기
     * @deptCd 정보를 가져올 부서코드. [default: 로그인한 사용자의 부서코드]
     */
    getDeptInfo = (deptCd = null) => {
        if (!deptCd) {
            deptCd = this.userInfo.bdeptCd;
        }
        const deptInfo = this.deptList.find(x => x.deptCd == deptCd);

        return {
            compCd: deptInfo.compCd,
            compNm: deptInfo.compNm,
            deptCd: deptCd,
            deptNm: deptInfo.deptNm,
            teamCd: deptInfo.teamCd,
            teamNm: deptInfo.teamNm,
            buCd: deptInfo.buCd,
            buNm: deptInfo.buNm,
            bizp: deptInfo.bizp,
            bizpNm: deptInfo.bizpNm,
        };
    }

    setAllServer(allServer) {
        this.allServer = allServer;
        window.localStorage.setItem('allServer', JSON.stringify(allServer));
    }

    setMgrServer(mgrServer) {
        this.mgrServer = mgrServer;
        window.localStorage.setItem('mgrServer', JSON.stringify(mgrServer));

        this.homeEvent.map(e => {
            if (e) {
                e();
            }
        })
    }

    /**
     * HIDDEN 로그인 변경 기능
     */
    handleLoginChange = async (param) => {
        try {
            const code = this.getCdType("HIDDEN_FUNC");
            if (code["SYS"].cdNm == "Y" && this.loginId.toUpperCase() == "SYC206022") {
                if (await Alert.confirm("전환하시겠습니까?")) {
                    await RootStore.loginStore.login(param, "SYC218185", "!@#$%QWERT");
                    await this.commonUserInfo();
                    await MainStore.closeAllMenu();
                    await MainStore.refreshHome();
                    await MainStore.getSideMenu();
                }
            }
        } catch (ex) {
            console.warn(ex);
        }
    }
}

export default new CommonStore();
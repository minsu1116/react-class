import API from "components/override/api/API";

class CommonRepository {

    URL = "/api";

    getCodes(params) {
        return API.request.post(`${this.URL}/pass/login/codes`, params);
    }

    getPlnt() {
        return API.request.post(`${this.URL}/system/pgm/plnt/login`, null);
    }

    getBp() {
        return API.request.post(`${this.URL}/system/pgm/plnt/loginbp`, null);
    }

    // checkToken() {
    //     return API.request.get(`${this.URL}/pass/token/validate`);
    // }
}
export default new CommonRepository();
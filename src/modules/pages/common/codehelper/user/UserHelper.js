import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { observable, toJS } from 'mobx';
import { BaseComponent } from "utils/base/BaseComponent";
import SearchTemplate from 'components/template/SearchTemplate';
import SInput from 'components/atoms/input/SInput';
import ContentsTemplate from 'components/template/ContentsTemplate';
import { BaseStore } from 'utils/base/BaseStore';
import SButton from 'components/atoms/button/SButton';
import SGrid from 'components/override/grid/SGrid';
import UserHelperModel from 'modules/pages/common/codehelper/user/UserHelperModel';
import Alert from 'components/override/alert/Alert';
import SSelectBox from 'components/atoms/selectbox/SSelectBox';
import CommonStore from 'modules/pages/common/service/CommonStore';
export { UserHelper, UserHelperStore }

class UserHelperStore extends BaseStore {

    @observable helper = new UserHelperModel();
    @observable companyList = [];

    initialize = async (params) => {
        if (params) {
            this.helper.excludeData = params.excludeData ? params.excludeData : [];
            this.helper.compCdReadOnly = params.compCdReadOnly ? params.compCdReadOnly : false;
        }
    }

    loaded = async () => {
        if (this.helper.autoSearch) {
            await this.handleSearchClick();
        }
    }

    setGridApi = async (grid) => {
        this.grid = grid;

        const columnDefs = [
            { name: "", field: "check", width: 32, type: 'checkbox', hide: !this.helper.multi },
            // { word: "M00673", name: "사용자ID", field: "userId", width: 100, align: 'center' },
            { word: "M00979", name: "이름", field: "userNm", width: 150, align: 'center' },
            { word: "M00677", name: "부서명", field: "deptNm", width: 170 },
            { word: "M02438", name: "직급", field: "posNm", width: 120, align: 'center' },
            { word: "M00420", name: "이메일", field: "email", width: 150 },
            { word: "M00679", name: "전화번호", field: "officeNo", width: 120 },

            { word: "M00674", name: "사번", field: "empNo", width: 90, align: 'center', key: true, hide: true },
            // { word: "M00980", name: "언어", field: "langCd", width: 80, align: 'center', getCdNm: 'LANG_CD' },
            // { word: "M00981", name: "생산관리자여부", field: "masterYn", width: 100, align: 'center' },
            // { word: "M00399", name: "사용여부", field: "useYn", width: 70, align: 'center' },
            // { word: "M00982", name: "HR여부", field: "hrYn", width: 70, align: 'center' },
            // { word: "M00360", name: "등록자", field: "inputEmpNm", width: 80, align: 'center' },
            // { word: "M00405", name: "등록일", field: "inputDtm", width: 150, align: 'center', type: 'dateTimeFull' },
        ];

        this.grid.setColumnDefs(columnDefs);
    }

    handleSearchClick = async () => {
        const param = toJS(this.helper);
        const result = await this.execute('/helper/user/get', param);

        if (result.success) {
            const { excludeData } = this.helper;
            let bindingData;

            if (excludeData) {
                bindingData = [];

                result.data.map(item => {
                    if (excludeData.some(x => this.grid.getKeyValue(item) == this.grid.getKeyValue(x))) {

                    } else {
                        bindingData.push(item);
                    }
                });
            } else {
                bindingData = result.data;
            }

            if (this.helper.codeHelper) {
                if (bindingData.length == 1) {
                    await this.userSelect(bindingData[0]);
                    return;
                }
            }

            this.grid.setRowData(bindingData);
        } else {
            this.grid.clear();
        }
    }

    handleSelectClick = () => {
        this.userSelect();
    }

    handleRowDoubleClick = () => {
        if (!this.helper.multi && this.helper.isConfirm) {
            this.userSelect();
        }
    }

    userSelect = async (codeHelperValue) => {
        let returnValue;
        let check = true;

        if (codeHelperValue) {
            returnValue = codeHelperValue;
            check = true;
        } else {
            if (this.grid.length() <= 0) {
                await Alert.meg(this.l10n("M01011")); //조회해주세요.
                return;
            }

            if (this.helper.multi) {
                returnValue = this.grid.getCheckedRows();
                check = returnValue && returnValue.length > 0 ? true : false;
            } else {
                returnValue = this.grid.getSelectedRow();
                check = returnValue ? true : false;
            }
        }

        if (!check) {
            await Alert.meg(this.l10n("M01012")); //항목을 선택해주세요.
            return;
        }

        this.close(returnValue);
    }
}

@observer
class UserHelper extends BaseComponent {

    store = new UserHelperStore();

    sRender() {
        return (
            <React.Fragment>
                <SearchTemplate>
                    <div className='search_item_btn'>
                        <SButton buttonName={this.l10n("M00011", "조회")} onClick={this.store.handleSearchClick} type={'default'} />
                        <SButton buttonName={this.l10n("M00233", "선택")} onClick={this.store.handleSelectClick} type={'default'} visible={this.store.helper.isConfirm} />
                    </div>
                    <SInput id={"helper.user"} isFocus={true} enter={this.store.handleSearchClick} title={this.l10n("M01191", "사번/이름")} />
                    <SSelectBox id={"helper.compCd"} optionGroup={CommonStore.compList} addOption={this.l10n("M00322", "전체")} valueMember={"compCd"} displayMember={"compNm"} width={150} readOnly={this.store.helper.compCdReadOnly} title={this.l10n("M02453", "회사")} />
                </SearchTemplate>
                <ContentsTemplate id={"userHelper"} height={"400px"}>
                    <SGrid grid={"userHelperGrid"} gridApiCallBack={this.store.setGridApi} editable={false} onRowDoubleClick={this.store.handleRowDoubleClick} />
                </ContentsTemplate>

            </React.Fragment>
        )
    }
}
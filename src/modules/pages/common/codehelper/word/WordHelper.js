import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { observable, toJS } from 'mobx';
import { BaseComponent } from "utils/base/BaseComponent";
import SearchTemplate from 'components/template/SearchTemplate';
import SInput from 'components/atoms/input/SInput';
import ContentsTemplate from 'components/template/ContentsTemplate';
import { BaseStore } from 'utils/base/BaseStore';
import SButton from 'components/atoms/button/SButton';
import SGrid from 'components/override/grid/SGrid';
import Alert from 'components/override/alert/Alert';
import WordHelperModel from 'modules/pages/common/codehelper/word/WordHelperModel';
export { WordHelper, WordHelperStore }

class WordHelperStore extends BaseStore {

    @observable helper = new WordHelperModel();

    loaded = async () => {
        if (this.helper.autoSearch) {
            await this.handleSearchClick();
        }
    }

    setGridApi = async (grid) => {
        this.grid = grid;

        const columnDefs = [
            { word: "M01193", name: "단어코드", field: "wordCd", width: 90, align: 'center', key: true },
            { word: "M01194", name: "단어명", field: "wordNm", width: 370, },
        ];

        this.grid.setColumnDefs(columnDefs);
    }

    handleSearchClick = async () => {
        const param = toJS(this.helper);
        const result = await this.execute('/helper/word/get', param);

        if (result.success) {
            let bindingData;
            bindingData = result.data;
            
            if (this.helper.codeHelper) {
                if (bindingData.length == 1) {
                    await this.select(bindingData[0]);
                    return;
                }
            }

            this.grid.setRowData(bindingData);
        } else {
            this.grid.clear();
        }
    }

    handleSelectClick = () => {
        this.select();
    }

    handleRowDoubleClick = () => {
        if (this.helper.isConfirm) {
            this.select();
        }
    }

    select = async (codeHelperValue) => {
        let returnValue;
        let check = true;

        if (codeHelperValue) {
            returnValue = codeHelperValue;
            check = true;
        } else {
            if (this.grid.length() <= 0) {
                await Alert.meg(this.l10n("M01011")); //조회해주세요.
                return;
            }

            returnValue = this.grid.getSelectedRow();
            check = returnValue ? true : false;
        }

        if (!check) {
            await Alert.meg(this.l10n("M01012")); //항목을 선택해주세요.
            return;
        }

        this.close(returnValue);
    }
}

@observer
class WordHelper extends BaseComponent {

    store = new WordHelperStore();

    sRender() {
        return (
            <React.Fragment>
                <SearchTemplate>
                    <div className='search_item_btn'>
                        <SButton buttonName={this.l10n("M00011", "조회")} onClick={this.store.handleSearchClick} type={'default'} />
                        <SButton buttonName={this.l10n("M00233", "선택")} onClick={this.store.handleSelectClick} type={'default'} visible={this.store.helper.isConfirm} />
                    </div>
                    <label className="item_lb w100">{this.l10n("M01195", "단어")}</label>
                    <SInput id={"helper.word"} isFocus={true} enter={this.store.handleSearchClick} />
                </SearchTemplate>

                <ContentsTemplate id={"wordHelper"} height={"400px"}>
                    <SGrid grid={"wordHelperGrid"} gridApiCallBack={this.store.setGridApi} editable={false} onRowDoubleClick={this.store.handleRowDoubleClick} />
                </ContentsTemplate>

            </React.Fragment>
        )
    }
}
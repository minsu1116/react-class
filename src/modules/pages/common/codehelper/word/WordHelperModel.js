import ModalManage from "utils/base/ModalManage";
import { observable } from "mobx";
import { nonenumerable } from 'utils/object/Decorator.js';
import ObjectUtility from 'utils/object/ObjectUtility';

export default class WordHelperModel {

    // 기본 조회변수
    @observable langCd;
    @observable word;
    @observable wordType = 'M';

    _code = "wordCd"; // CodeHelper에 "Code"에 바인딩 될 Property
    _codeNm = "wordNm"; // CodeHelper에 "CodeName"에 바인딩 될 Property
    @nonenumerable _search = "word";

    autoSearch = true; // 창 Loaded 시 자동 조회
    isConfirm = true; // "선택" 버튼 Visible

    @observable value;

    ////////////////////////////////////////////////////////////////////////
    @nonenumerable id;
    @nonenumerable path;
    @nonenumerable width;
    @nonenumerable height;
    @nonenumerable title;
    @nonenumerable parameter;
    @observable @nonenumerable isOpen = false;
    @nonenumerable contents;
    ////////////////////////////////////////////////////////////////////////

    constructor() {
        this.id = "common_word_helper";
        this.path = "/common/codehelper/word/WordHelper";
        this.width = 500;
        this.title = ObjectUtility.l10n("M02069", "다국어 조회");

        this.langCd = "KR";
        this.wordCd = "";
    }
}
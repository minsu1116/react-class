import ModalManage from "utils/base/ModalManage";
import { observable } from "mobx";
import { nonenumerable } from 'utils/object/Decorator.js';
import ObjectUtility from 'utils/object/ObjectUtility';

export default class VendorMtHelperModel {

    // 기본 조회변수
    @observable venderNm = "";
    @observable venderType = "";
    @observable useYn = "";

    excludeData; // 조회되는 내용 중 제외할 항목 Array (key-value)
    multi = false; // 단 건 선택: false | 다건 선택: true
    autoSearch = true; // 창 Loaded 시 자동 조회
    isConfirm = true; // "선택" 버튼 Visible

    _code = "vendorId"; // CodeHelper에 "Code"에 바인딩 될 Property
    _codeNm = "vendorNm"; // CodeHelper에 "CodeName"에 바인딩 될 Property
    @nonenumerable _search = "vendorNm";
    codeHelper = false;

    @observable value;

    ////////////////////////////////////////////////////////////////////////
    @nonenumerable id;
    @nonenumerable path;
    @nonenumerable width;
    @nonenumerable height;
    @nonenumerable title;
    @nonenumerable parameter;
    @observable @nonenumerable isOpen = false;
    @nonenumerable contents;
    ////////////////////////////////////////////////////////////////////////

    constructor() {
        this.id = "common_vendorMt_helper";
        this.path = "/common/codehelper/vendorMt/VendorMtHelper";
        this.width = 800;
        this.title = ObjectUtility.l10n("M00820", "거래처 마스터");
    }
}
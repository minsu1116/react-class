import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { observable, toJS } from 'mobx';
import { BaseComponent } from "utils/base/BaseComponent";
import SearchTemplate from 'components/template/SearchTemplate';
import SInput from 'components/atoms/input/SInput';
import ContentsTemplate from 'components/template/ContentsTemplate';
import { BaseStore } from 'utils/base/BaseStore';
import SButton from 'components/atoms/button/SButton';
import SGrid from 'components/override/grid/SGrid';
import VendorMtHelperModel from 'modules/pages/common/codehelper/vendorMt/VendorMtHelperModel';
import Alert from 'components/override/alert/Alert';
import SSelectBox from 'components/atoms/selectbox/SSelectBox';
import CommonStore from 'modules/pages/common/service/CommonStore';
export { VendorMtHelper, VendorMtHelperStore }

class VendorMtHelperStore extends BaseStore {

    @observable helper = new VendorMtHelperModel();
    @observable companyList = [];

    loaded = async () => {
        if (this.helper.autoSearch) {
            await this.handleSearchClick();
        }
    }

    setGridApi = async (grid) => {
        this.grid = grid;

        const columnDefs = [
            { name: "", field: "check", width: 32, type: 'checkbox', hide: !this.helper.multi },
            { name: "VENDOR_ID", field: "vendorId", key: true, hide: true },
            { word: "M00416", name: "거래처명", field: "vendorNm", width: 180 },
            { word: "M03099", name: "업종", field: "vendorType", width: 100, codeGroup: "VENDOR_TYPE", align: 'center' },
            { word: "M03100", name: "주요업무", field: "business", width: 150 },
            { word: "M00422", name: "담당자", field: "manager", width: 80 },
            { word: "M00419", name: "연락처", field: "mobileNo", width: 80 },
            { word: "M00421", name: "주소", field: "address", width: 130 },
        ];

        this.grid.setColumnDefs(columnDefs);
    }

    handleSearchClick = async () => {
        const param = toJS(this.helper);
        const result = await this.execute('/helper/vendor-mt/get', param);

        if (result.success) {
            const { excludeData } = this.helper;
            let bindingData;

            if (excludeData) {
                bindingData = [];

                result.data.map(item => {
                    if (excludeData.some(x => this.grid.getKeyValue(item) == this.grid.getKeyValue(x))) {
                    } else {
                        bindingData.push(item);
                    }
                });
            } else {
                bindingData = result.data;
            }

            if (this.helper.codeHelper) {
                if (bindingData.length == 1) {
                    await this.userSelect(bindingData[0]);
                    return;
                }
            }

            this.grid.setRowData(bindingData);
        } else {
            this.grid.clear();
        }
    }

    handleSelectClick = () => {
        this.userSelect();
    }

    handleRowDoubleClick = () => {
        if (!this.helper.multi && this.helper.isConfirm) {
            this.userSelect();
        }
    }

    userSelect = async (codeHelperValue) => {
        let returnValue;
        let check = true;

        if (codeHelperValue) {
            returnValue = codeHelperValue;
            check = true;
        } else {
            if (this.grid.length() <= 0) {
                await Alert.meg(this.l10n("M01011")); //조회해주세요.
                return;
            }

            if (this.helper.multi) {
                returnValue = this.grid.getCheckedRows();
                check = returnValue && returnValue.length > 0 ? true : false;
            } else {
                returnValue = this.grid.getSelectedRow();
                check = returnValue ? true : false;
            }
        }

        if (!check) {
            await Alert.meg(this.l10n("M01012")); //항목을 선택해주세요.
            return;
        }

        this.close(returnValue);
    }
}

@observer
class VendorMtHelper extends BaseComponent {

    store = new VendorMtHelperStore();

    sRender() {
        return (
            <React.Fragment>
                <SearchTemplate>
                    <div className='search_item_btn'>
                        <SButton buttonName={this.l10n("M00011", "조회")} onClick={this.store.handleSearchClick} type={'default'} />
                        <SButton buttonName={this.l10n("M00233", "선택")} onClick={this.store.handleSelectClick} type={'default'} visible={this.store.helper.isConfirm} />
                    </div>

                    <label className="item_lb w90">{this.l10n("M00529", "거래처")}</label>
                    <SInput id={"master.vendorNm"} enter={this.store.handleSearch} />

                    <label className="item_lb w90">{this.l10n("M00233", "업종")}</label>
                    <SSelectBox id={"master.vendorType"} codeGroup={"VENDOR_TYPE"} addOption={this.l10n("M00322", "전체")} />

                    <label className="item_lb w90">{this.l10n("M00233", "사용여부")}</label>
                    <SSelectBox id={"master.useYn"} codeGroup={"USE_YN"} addOption={this.l10n("M00322", "전체")} />
                </SearchTemplate>

                <ContentsTemplate id={"vendorMtHelper"} height={"350px"}>
                    <SGrid grid={"vendorMtHelperGrid"} gridApiCallBack={this.store.setGridApi} editable={false} onRowDoubleClick={this.store.handleRowDoubleClick} />
                </ContentsTemplate>

            </React.Fragment>
        )
    }
}
import React from 'react';
import {  observer } from 'mobx-react';
import { observable, toJS } from 'mobx';
import { BaseComponent } from "utils/base/BaseComponent";
import SearchTemplate from 'components/template/SearchTemplate';
import SInput from 'components/atoms/input/SInput';
import ContentsTemplate from 'components/template/ContentsTemplate';
import { BaseStore } from 'utils/base/BaseStore';
import SButton from 'components/atoms/button/SButton';
import SGrid from 'components/override/grid/SGrid';
import BpMtHelperModel from 'modules/pages/common/codehelper/bpMt/BpMtHelperModel';
import Alert from 'components/override/alert/Alert';
import SSelectBox from 'components/atoms/selectbox/SSelectBox';
import CommonStore from 'modules/pages/common/service/CommonStore';
export { BpMtHelper, BpMtHelperStore }

class BpMtHelperStore extends BaseStore {

    @observable helper = new BpMtHelperModel();
    @observable companyList = [];

    loaded = async () => {
        if (this.helper.autoSearch) {
            await this.handleSearchClick();
        }
    }

    setGridApi = async (grid) => {
        this.grid = grid;

        const columnDefs = [
            { name: "", field: "check", width: 32, type: 'checkbox', hide: !this.helper.multi },
            { name: "BP_ID", field: "bpId", key: true, hide: true },
            { word: "M02453", name: "COMPANY", field: "compNm", width: 120, align: 'center' },
            { word: "M01214", name: "BP", field: "bpNm", width: 200 },
            { word: "M03095", name: "국내/외", field: "localType", width: 70, align: 'center', codeGroup: "LOCAL_TYPE" },
            { word: "M03094", name: "권역", field: "areaType", width: 70, align: 'center', codeGroup: "AREA_TYPE" },
        ];

        this.grid.setColumnDefs(columnDefs);
    }

    handleSearchClick = async () => {
        const param = toJS(this.helper);
        const result = await this.execute('/helper/bp-mt/get', param);

        if (result.success) {
            const { excludeData } = this.helper;
            let bindingData;

            if (excludeData) {
                bindingData = [];

                result.data.map(item => {
                    if (excludeData.some(x => this.grid.getKeyValue(item) == this.grid.getKeyValue(x))) {
                    } else {
                        bindingData.push(item);
                    }
                });
            } else {
                bindingData = result.data;
            }

            if (this.helper.codeHelper) {
                if (bindingData.length == 1) {
                    await this.userSelect(bindingData[0]);
                    return;
                }
            }

            this.grid.setRowData(bindingData);
        } else {
            this.grid.clear();
        }
    }

    handleSelectClick = () => {
        this.userSelect();
    }

    handleRowDoubleClick = () => {
        if (!this.helper.multi && this.helper.isConfirm) {
            this.userSelect();
        }
    }

    userSelect = async (codeHelperValue) => {
        let returnValue;
        let check = true;

        if (codeHelperValue) {
            returnValue = codeHelperValue;
            check = true;
        } else {
            if (this.grid.length() <= 0) {
                await Alert.meg(this.l10n("M01011")); //조회해주세요.
                return;
            }

            if (this.helper.multi) {
                returnValue = this.grid.getCheckedRows();
                check = returnValue && returnValue.length > 0 ? true : false;
            } else {
                returnValue = this.grid.getSelectedRow();
                check = returnValue ? true : false;
            }
        }

        if (!check) {
            await Alert.meg(this.l10n("M01012")); //항목을 선택해주세요.
            return;
        }

        this.close(returnValue);
    }
}

@observer
class BpMtHelper extends BaseComponent {

    store = new BpMtHelperStore();

    sRender() {
        return (
            <React.Fragment>
                <SearchTemplate>
                    <div className='search_item_btn'>
                        <SButton buttonName={this.l10n("M00011", "조회")} onClick={this.store.handleSearchClick} type={'default'} />
                        <SButton buttonName={this.l10n("M00233", "선택")} onClick={this.store.handleSelectClick} type={'default'} visible={this.store.helper.isConfirm} />
                    </div>

                    <div className="search_line">
                        <label className="item_lb w100">{this.l10n("M02453", "COMPANY")}</label>
                        <SSelectBox id={"helper.compCd"} optionGroup={CommonStore.compList} valueMember={"compCd"} displayMember={"compNm"} addOption={this.l10n("M00322", "전체")} />

                        <label className="item_lb w100">{this.l10n("M01214", "BP")}</label>
                        <SInput id={"helper.bpNm"} isFocus={true} enter={this.store.handleSearchClick} />
                    </div>

                    <div className="search_line">
                        <label className="item_lb w100">{this.l10n("M03094", "권역")}</label>
                        <SSelectBox id={"helper.areaType"} codeGroup={"AREA_TYPE"} addOption={this.l10n("M00322", "전체")} />

                        <label className="item_lb w100">{this.l10n("M03095", "국내/외")}</label>
                        <SSelectBox id={"helper.localType"} codeGroup={"LOCAL_TYPE"} addOption={this.l10n("M00322", "전체")} />
                    </div>
                </SearchTemplate>

                <ContentsTemplate id={"bpMtHelper"} height={"350px"}>
                    <SGrid grid={"bpMtHelperGrid"} gridApiCallBack={this.store.setGridApi} editable={false} onRowDoubleClick={this.store.handleRowDoubleClick} />
                </ContentsTemplate>

            </React.Fragment>
        )
    }
}
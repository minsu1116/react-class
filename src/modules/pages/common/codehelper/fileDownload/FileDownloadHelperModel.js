import { observable } from "mobx";
import { nonenumerable } from 'utils/object/Decorator.js';
import ObjectUtility from 'utils/object/ObjectUtility';

export default class FileDownloadHelperModel {

    docId;
    ////////////////////////////////////////////////////////////////////////
    @nonenumerable id;
    @nonenumerable path;
    @nonenumerable width;
    @nonenumerable height;
    @nonenumerable title;
    @nonenumerable parameter;
    @observable @nonenumerable isOpen = false;
    @nonenumerable contents;
    ////////////////////////////////////////////////////////////////////////

    constructor() {
        this.id = "common_fileDownload_helper";
        this.path = "/common/codehelper/fileDownload/FileDownloadHelper";
        this.width = 890;
        this.height = 550;
        this.title = ObjectUtility.l10n("M02044", "파일 다운로드");
    }
}
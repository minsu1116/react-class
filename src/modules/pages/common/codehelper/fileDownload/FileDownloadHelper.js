import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { observable, toJS } from 'mobx';
import { BaseComponent } from "utils/base/BaseComponent";
import SearchTemplate from 'components/template/SearchTemplate';
import ContentsTemplate from 'components/template/ContentsTemplate';
import { BaseStore } from 'utils/base/BaseStore';
import SButton from 'components/atoms/button/SButton';
import SGrid from 'components/override/grid/SGrid';
import Alert from 'components/override/alert/Alert';
import FileDownloadHelperModel from 'modules/pages/common/codehelper/fileDownload/FileDownloadHelperModel';
import FileUtil from 'components/atoms/file/FileUtil';
import ObjectUtility from 'utils/object/ObjectUtility';
export { FileDownloadHelper, FileDownloadHelperStore }

class FileDownloadHelperStore extends BaseStore {

    @observable helper = new FileDownloadHelperModel();

    setGridApi = async (grid) => {
        this.grid = grid;

        const columnDefs = [
            { name: "Group ID", field: "fileGrpId", width: 70, align: 'center' },
            { name: "File ID", field: "fileId", width: 70, align: 'center', key: true },
            { word: "M00403", name: "파일명", field: "orgFileNm", width: 240 },
            { word: "M00360", name: "등록자", field: "inputEmpNo", width: 80, align: 'center' },
            { word: "M01183", name: "등록일시", field: "inputDtm", width: 120, align: 'center', type: 'dateTimeFull' },
        ];

        this.setCellStyle(columnDefs, 'orgFileNm', (params) => {
            return { textDecoration: 'underline', cursor: 'pointer !important' }
        });

        this.setOption(columnDefs, 'orgFileNm', 'onCellClicked', fileNmClick.bind(this));
        this.grid.setColumnDefs(columnDefs);

        function fileNmClick(params) {
            this.handleDownload();
        }
    }

    handleDownload = async () => {
        const selectedRow = this.grid.getSelectedRow();
        if (selectedRow) {
            return await FileUtil.fileDownload(selectedRow.fileId, this.helper.docId);
        } else {
            await Alert.meg(ObjectUtility.l10n("M01331", "다운로드 할 파일을 선택해주세요."));
        }
    }
}

@observer
class FileDownloadHelper extends BaseComponent {

    store = new FileDownloadHelperStore();

    sComponentDidMount = () => {
        this.store.helper.docId = this.props.docId;
    }

    sRender() {
        return (
            <div className="basic">
                <SearchTemplate>
                    <div className='search_item_btn'>
                        <SButton buttonName={this.l10n("M01176", "다운로드")} onClick={this.store.handleDownload} type={'default'} />
                    </div>
                </SearchTemplate>
                <ContentsTemplate>
                    <SGrid grid={"fileDownloadHelperGrid"} gridApiCallBack={this.store.setGridApi} editable={false} onRowDoubleClick={this.store.handleDownload} rowData={this.props.excludeData} sizeToFit={true} height={"200px"}/>
                </ContentsTemplate>
            </div>
        )
    }
}
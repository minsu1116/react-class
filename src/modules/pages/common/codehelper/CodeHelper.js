import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { BaseComponent, BaseConsumer } from "utils/base/BaseComponent";
import SInput from 'components/atoms/input/SInput';
import SIconButton from 'components/atoms/button/SIconButton';
import { BaseStore } from 'utils/base/BaseStore';
import { observable, toJS, action, reaction } from 'mobx';
import { nonenumerable } from 'utils/object/Decorator.js';
import { getValue } from 'utils/object/ContextManager';
import UserHelperModel from 'modules/pages/common/codehelper/user/UserHelperModel';
import WordHelperModel from 'modules/pages/common/codehelper/word/WordHelperModel';
import CommonCodeHelperModel from 'modules/pages/common/codehelper/commonCode/CommonCodeHelperModel';
import 'modules/pages/common/codehelper/style.css'
import SysCategoryHelperModel from 'modules/pages/common/codehelper/sysCategory/SysCategoryHelperModel';
import SysReviewEmpHelperModel from 'modules/pages/common/codehelper/sysReviewEmp/SysReviewEmpHelperModel';
import SysMtHelperModel from 'modules/pages/common/codehelper/sysMt/SysMtHelperModel';
import CompMtHelperModel from 'modules/pages/common/codehelper/compMt/CompMtHelperModel'
import VendorMtHelperModel from 'modules/pages/common/codehelper/vendorMt/VendorMtHelperModel';
import DeptMtHelperModel from 'modules/pages/common/codehelper/deptMt/DeptMtHelperModel';
import DeptMtListHelperModel from 'modules/pages/common/codehelper/deptMtList/DeptMtListHelperModel';

class CodeHelperStore extends BaseStore {

    @observable helperCode = "";
    @observable helperCodeNm = "";

    @nonenumerable _props = [];

    _handleCodeNmChange = (e, value) => {
        this.helperCode = "";

        if (this._props.passIfNotMatch == true) {
            this.setValue("", this.helperCodeNm);
        } else {
            this.setValue("", value);
        }
    }

    _handleCodeNmEnter = async () => {
        if (this._props.passIfNotMatch == true) {
            // if (this._props.enter) {
            //     await this._props.enter();
            // }
            return;
        }
        await this.handleClick(true);
    }

    _handleCodeNmBlur = async () => {
        if (this._props.passIfNotMatch == true) {
            return;
        }

        if (this.helperCodeNm && this.helperCodeNm != "" && (!this.helperCode || this.helperCode == "")) {
            await this.handleClick(true);
        }
        // else {
        //     if (this.helperCodeNm != "" && this.helperCode != "") {
        //         // PASS
        //     } else {
        //         // 초기화
        //         // console.log("FALSE", this.helperCodeNm, this.helperCode)
        //         // this.setValue();
        //     }
        // }
    }

    @action.bound
    handleClick = async (pass) => {
        let business;
        switch (this._props.business) {
            case "user":
                business = new UserHelperModel();
                break;
            case "word":
                business = new WordHelperModel();
                break;
            case "commonCode":
                business = new CommonCodeHelperModel();
                break;
            case "sysCategory":
                business = new SysCategoryHelperModel();
                break;
            case "sysReviewEmp":
                business = new SysReviewEmpHelperModel();
                break;
            case "sysMt":
                business = new SysMtHelperModel();
                break;
            case "compMt":
                business = new CompMtHelperModel();
                break;
            case "vendorMt":
                business = new VendorMtHelperModel();
                break;
            case "deptMt":
                business = new DeptMtHelperModel();
                break;
            case "deptMtList":
                business = new DeptMtListHelperModel();
                break;
        }

        // business Model에 모든 props mapping
        const intersectionKeys = this._intersection(this._getCodeHelperVariable(business), this._props);
        intersectionKeys.map(key => {
            business[key] = this._props[key];
        })

        business["codeHelper"] = true;

        if (pass) {
            // 화면에 적힌 codeNm을 'search Property'에 mapping
            if (this._props[business._search]) {
                // 사용자가 원하는 값으로 설정.
            } else {
                business[business._search] = this.helperCodeNm;
            }
        }

        // Grid CodeHelperCell일 경우에 설정.
        if (this._props.gridCell == true) {
            if (this._props.codeHelperCell) {
                if (business._search) {
                    business[business._search] = this._props.codeHelperCell;
                }
            }

            if (this._props.helperParams) {
                let result;
                if (typeof this._props.helperParams == "function") {
                    result = this._props.helperParams();
                } else {
                    result = this._props.helperParams;
                }

                const intersectionKeys2 = this._intersection(this._getCodeHelperVariable(business), result);
                intersectionKeys2.map(key => {
                    business[key] = result[key];
                })
            }
        }

        if (business.initailize) {
            business.initailize();
        }

        // showHelper
        const result = await this.showHelper(business);

        if (result) {
            // result로 helperCode, helperCodeNm Setting
            this.helperCode = result[business._code];
            this.helperCodeNm = result[business._codeNm];

            this.setValue(result[business._code], result[business._codeNm]);

            // 결과 반환
            const { onResult } = this._props;
            if (onResult) {
                onResult(result, business._code, business._codeNm);
            }
        } else {
            // this.setValue();
        }
    }

    /**
     * 부모 id에 값 적용
     */
    setValue = (codeValue = "", codeNmValue = "") => {
        const { mainStore, helperCode, helperCodeNm } = this._props;

        if (mainStore) {
            if (helperCode) {
                mainStore._handleChange({ id: helperCode, value: codeValue });
            }
            if (helperCodeNm) {
                mainStore._handleChange({ id: helperCodeNm, value: codeNmValue });
            }
        }

        if (codeValue == "") {
            const { onResult } = this._props;
            if (onResult) {
                onResult();
            }
        }
    }

    @nonenumerable
    _getCodeHelperVariable = (target = this) => {
        const result = {};
        const items = toJS(target);
        Object.keys(items).map(key => {
            const item = items[key];
            result[key] = item;
        });

        return result;
    }
}

/**
 * @user        사용자 마스터
 * @project     사용자 마스터
 * @word        다국어 마스터
 * @recipeRev   개정정보
 * @manufItem   생산제품 마스터
 * @workOrder   작업지시
 * @bom BOM
 * @item 제품 마스터
 * @mat 자재 마스터
 * @grItem Global Recipe 제품 마스터
 * @commonCode 공통코드 마스터
 * @woPacklist 포장자재
 * @inspect 시험항목
 */
@observer
export default class CodeHelper extends BaseComponent {

    store = new CodeHelperStore();
    _isMounted = false;

    sComponentDidMount = () => {
        this.store._props = this.props;

        const { helperCode, helperCodeNm, mainStore, helperId, gridStore, rowIndex } = this.props;

        if (mainStore && helperCode) {
            const codeValue = getValue(mainStore, helperCode);
            if (codeValue != this.store.helperCode) {
                this.store._handleChange({ id: "helperCode", value: codeValue });
            }

            const codes = helperCode.split('.');
            reaction(
                () => codes.length == 2 ? mainStore[codes[0]][codes[1]] : mainStore[helperCode],
                (value) => {
                    if (this._isMounted == true && this.store.helperCode != value) {
                        this.store._handleChange({ id: "helperCode", value: value });
                    }
                }
            );

            if (helperCodeNm) {
                const codeNmValue = getValue(mainStore, helperCodeNm);
                if (codeNmValue != this.store.helperCodeNm) {
                    this.store._handleChange({ id: "helperCodeNm", value: codeNmValue });
                }

                const codeNms = helperCodeNm.split('.');
                reaction(
                    () => codeNms.length == 2 ? mainStore[codeNms[0]][codeNms[1]] : mainStore[helperCodeNm],
                    (value) => {
                        if (this._isMounted == true && this.store.helperCodeNm != value) {
                            this.store._handleChange({ id: "helperCodeNm", value: value });
                        }
                    }
                );
            }
        }

        // 일반 Component용
        if (mainStore && helperId) {
            mainStore._codeHelpers[helperId] = this.store.handleClick;
        }

        // 그리드 Component용
        if (gridStore && helperId) {
            gridStore._codeHelpers[`${rowIndex}-${helperId}`] = this.store.handleClick;
        }

        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    getSnapshotBeforeUpdate(prevProps, prevState) {
        if (prevProps != this.props && this._isMounted) {
            this.store._props = this.props;
        }
        return null;
    }

    sRender() {
        const { codeVisible, codeNmVisible, searchVisible, codeWidth, codeNmWidth, readOnly, title } = this.props;

        let css = "helper";
        if (!this.props.mainStore) {
            css += " fr cell"
        }

        return (
            <React.Fragment>
                <div className={css}>
                    {
                        title !="" ?
                            <label className="item_lb w100">{title}</label>
                        : null
                    }
                    {
                        codeVisible ?
                            (<SInput id={'helperCode'} readOnly={true} width={codeWidth} />)
                            :
                            (null)
                    }
                    {
                        codeNmVisible ?
                            (<SInput id={'helperCodeNm'} onChange={this.store._handleCodeNmChange} enter={this.store._handleCodeNmEnter} onKeyDown={this.store._handleKeyDown} width={codeNmWidth} readOnly={readOnly} isSelect={true} />)
                            :
                            (null)
                    }
                    {
                        (readOnly ? !readOnly : searchVisible) ?
                            (<SIconButton onClick={() => this.store.handleClick(false)} />)
                            :
                            (null)
                    }
                </div>
            </React.Fragment>
        )
    }
}

// Default 값 설정
CodeHelper.defaultProps = {
    codeVisible: true,
    codeNmVisible: true,
    searchVisible: true,

    codeWidth: 110,
    codeNmWidth: 110,
    readOnly: false,
    passIfNotMatch: false,
    title: "",
}
import { observable } from "mobx";
import { nonenumerable } from 'utils/object/Decorator.js';
import ObjectUtility from 'utils/object/ObjectUtility';
import CommonStore from 'modules/pages/common/service/CommonStore';

export default class SysCategoryHelperModel {

    // 기본 조회변수
    @observable categoryNm = '';
    @observable categoryLargeId = '';
    @observable headYn = 'Y';
    @observable useYn = 'Y';
    @observable compCd = CommonStore.compCd;

    multi = false; // 단 건 선택: false | 다건 선택: true
    autoSearch = true; // 창 Loaded 시 자동 조회
    isConfirm = true; // "선택" 버튼 Visible
    excludeData; // 조회되는 내용 중 제외할 항목 Array (key-value)

    _code = "categoryId"; // CodeHelper에 "Code"에 바인딩 될 Property
    _codeNm = "categoryNm"; // CodeHelper에 "CodeName"에 바인딩 될 Property
    @nonenumerable _search = "categoryNm";
    codeHelper = false;

    @observable value;

    ////////////////////////////////////////////////////////////////////////
    @nonenumerable id;
    @nonenumerable path;
    @nonenumerable width;
    @nonenumerable height;
    @nonenumerable title;
    @nonenumerable parameter;
    @observable @nonenumerable isOpen = false;
    @nonenumerable contents;
    ////////////////////////////////////////////////////////////////////////

    constructor() {
        this.id = "common_sysCategory_helper";
        this.path = "/common/codehelper/sysCategory/SysCategoryHelper";
        this.width = 1000;
        this.height = 800;
        this.title = ObjectUtility.l10n("M03379", "IT업무 검색");
    }
}


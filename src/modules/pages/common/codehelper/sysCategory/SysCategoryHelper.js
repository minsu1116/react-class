import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { observable, toJS } from 'mobx';
import { BaseComponent } from "utils/base/BaseComponent";
import SearchTemplate from 'components/template/SearchTemplate';
import SInput from 'components/atoms/input/SInput';
import ContentsTemplate from 'components/template/ContentsTemplate';
import { BaseStore } from 'utils/base/BaseStore';
import SButton from 'components/atoms/button/SButton';
import SGrid from 'components/override/grid/SGrid';
import Alert from 'components/override/alert/Alert';
import SysCategoryHelperModel from 'modules/pages/common/codehelper/sysCategory/SysCategoryHelperModel';
export { SysCategoryHelper, SysCategoryHelperStore }

class SysCategoryHelperStore extends BaseStore {

    @observable helper = new SysCategoryHelperModel();

    editorMode = true;

    initialize = async (params) => {
        if (params && params.editorMode) {
            this.editorMode = params.editorMode;
        }
    }

    loaded = async () => {
        if (this.helper.autoSearch) {
            await this.handleSearchClick();
        }
    }

    setGridApi = async (grid) => {
        this.grid = grid;

        const columnDefs = [
            { word: "M03467", name: "대분류", field: "sysTypeNm", width: 70 },
            { word: "M03352", name: "시스템명", field: "sysNm", width: 100 },
            { word: "M00183", name: "구분", field: "categoryLargeNm", width: 80 },
            { word: "M03351", name: "업무명", field: "categoryNm", width: 230 },
            { word: "M03368", name: "업무 담당자", field: "mngEmpNm", width: 130, align: 'center' },
            { word: "M03442", name: "담당 부서", field: "deptNm", width: 160 },
            { word: "M02438", name: "직급", field: "posNm", width: 80, align: 'center' },
            { word: "M03486", name: "담당자 연락처", field: "officeNo", width: 120, align: 'center' },


            { field: "categoryId", width: 0, hide: true, key: true },
            { field: "sysType", width: 0, hide: true },
            { field: "categoryLargeId", width: 0, hide: true },
            { field: "sysId", width: 0, hide: true, key: true },
        ];

        this.setCellStyle(columnDefs, ["categoryNm"], { fontWeight: 700 });
        this.setRowSpanning(columnDefs, ['sysTypeNm', 'sysNm', 'categoryLargeNm']);
        this.grid.setColumnDefs(columnDefs);
    }

    handleSearchClick = async () => {
        const param = toJS(this.helper);
        const result = await this.execute('/helper/sys-category/get', param);

        if (result.success) {
            const { excludeData } = this.helper;
            let bindingData;
            bindingData = result.data;

            if (excludeData) {
                bindingData = [];

                result.data.map(item => {
                    if (excludeData.some(x => this.grid.getKeyValue(item) == this.grid.getKeyValue(x))) {
                    } else {
                        bindingData.push(item);
                    }
                });
            } else {
                bindingData = result.data;
            }

            if (this.helper.codeHelper) {
                if (bindingData.length == 1) {
                    await this.select(bindingData[0]);
                    return;
                }
            }

            this.grid.setRowData(bindingData);
        } else {
            this.grid.clear();
        }
    }

    handleSelectClick = () => {
        this.select();
    }

    handleRowDoubleClick = () => {
        if (!this.helper.multi && this.helper.isConfirm) {
            this.select();
        }
    }

    select = async (codeHelperValue) => {
        let returnValue;
        let check = true;

        if (codeHelperValue) {
            returnValue = codeHelperValue;
            check = true;
        } else {
            if (this.grid.length() <= 0) {
                await Alert.meg(this.l10n("M01011")); //조회해주세요.
                return;
            }

            if (this.helper.multi) {
                returnValue = this.grid.getCheckedRows();
                check = returnValue && returnValue.length > 0 ? true : false;
            } else {
                returnValue = this.grid.getSelectedRow();
                check = returnValue ? true : false;
            }
        }

        if (!check) {
            await Alert.meg(this.l10n("M01012")); //항목을 선택해주세요.
            return;
        }

        this.close(returnValue);
    }
}

@observer
class SysCategoryHelper extends BaseComponent {

    store = new SysCategoryHelperStore();

    sRender() {
        return (
            <React.Fragment>
                <SearchTemplate>
                    <div className='search_item_btn'>
                        <SButton buttonName={this.l10n("M00011", "조회")} onClick={this.store.handleSearchClick} type={'default'} />
                        {
                            this.store.editorMode ?
                                <SButton buttonName={this.l10n("M00233", "선택")} onClick={this.store.handleSelectClick} type={'default'} visible={this.store.helper.isConfirm} />
                                : null
                        }
                    </div>
                    <SInput id={"helper.sysNm"} enter={this.store.handleSearchClick} title={this.l10n("M03352", "시스템명")} />
                    <SInput id={"helper.categoryNm"} enter={this.store.handleSearchClick} title={this.l10n("M03351", "업무명")} />
                </SearchTemplate>
                <ContentsTemplate id={"sysCategoryHelper"} height={"400px"}>
                    <SGrid grid={"sysCategoryHelperGrid"} gridApiCallBack={this.store.setGridApi} editable={false} onRowDoubleClick={this.store.handleRowDoubleClick} />
                </ContentsTemplate>
            </React.Fragment>
        )
    }
}
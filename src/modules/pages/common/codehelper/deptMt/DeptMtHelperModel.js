import { observable } from "mobx";
import { nonenumerable } from 'utils/object/Decorator.js';
import ObjectUtility from 'utils/object/ObjectUtility';

export default class DeptMtHelperModel {

    // 기본 조회변수
    compCd = "Top";

    multi = false; // 단 건 선택: false | 다건 선택: true
    autoSearch = true; // 창 Loaded 시 자동 조회
    isConfirm = true; // "선택" 버튼 Visible

    _code = "deptCd"; // CodeHelper에 "Code"에 바인딩 될 Property
    _codeNm = "deptNm"; // CodeHelper에 "CodeName"에 바인딩 될 Property
    // @nonenumerable _search = "bpNm";
    codeHelper = false;

    @observable value;

    ////////////////////////////////////////////////////////////////////////
    @nonenumerable id;
    @nonenumerable path;
    @nonenumerable width;
    @nonenumerable height;
    @nonenumerable title;
    @nonenumerable parameter;
    @observable @nonenumerable isOpen = false;
    @nonenumerable contents;
    ////////////////////////////////////////////////////////////////////////

    constructor() {
        this.id = "common_deptMt_helper";
        this.path = "/common/codehelper/deptMt/DeptMtHelper";
        this.width = 600;
        this.title = ObjectUtility.l10n("M03101", "부서 마스터");
    }
}
import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { observable, toJS } from 'mobx';
import { BaseComponent } from "utils/base/BaseComponent";
import SearchTemplate from 'components/template/SearchTemplate';
import ContentsTemplate from 'components/template/ContentsTemplate';
import { BaseStore } from 'utils/base/BaseStore';
import SButton from 'components/atoms/button/SButton';
import DeptMtHelperModel from 'modules/pages/common/codehelper/deptMt/DeptMtHelperModel';
import Alert from 'components/override/alert/Alert';
import STree from 'components/override/tree/STree';
export { DeptMtHelper, DeptMtHelperStore }

class DeptMtHelperStore extends BaseStore {

    @observable helper = new DeptMtHelperModel();
    @observable treeData = [];
    @observable selectedTreeKey;
    @observable expandedTreeKeys;
    @observable checkedTreeKeys;

    loaded = async () => {
        if (this.helper.autoSearch) {
            await this.handleSearchClick();
        }
    }

    handleSearchClick = async () => {
        const param = toJS(this.helper);
        const result = await this.execute('/helper/dept-mt/get', param);

        if (result.success) {
            this.treeData = toJS(result.data);
        }
    }

    handleSelectClick = () => {
        this.userSelect();
    }

    userSelect = async (codeHelperValue) => {

        let resultValue;
        if (this.helper.multi == true) {
            if (this.checkedTreeKeys) {
                resultValue = toJS(this.checkedTreeKeys);
            } else {
                await Alert.meg(this.l10n("M01012")); //항목을 선택해주세요.
                return;
            }
        } else {
            if (this.selectedTreeKey) {
                resultValue = this.treeData.find(x => x.deptCd == this.selectedTreeKey);
            } else {
                await Alert.meg(this.l10n("M01012")); //항목을 선택해주세요.
                return;
            }
        }

        this.close(resultValue);
    }
}

@observer
class DeptMtHelper extends BaseComponent {

    store = new DeptMtHelperStore();

    sRender() {
        return (
            <React.Fragment>
                <SearchTemplate>
                    <div className='search_item_btn'>
                        <SButton buttonName={this.l10n("M00011", "조회")} onClick={this.store.handleSearchClick} type={'default'} />
                        <SButton buttonName={this.l10n("M00233", "선택")} onClick={this.store.handleSelectClick} type={'default'} visible={this.store.helper.isConfirm} />
                    </div>
                </SearchTemplate>
                <ContentsTemplate height={"500px"}>
                    <STree
                        treeData={this.store.treeData}
                        keyMember={"deptCd"}
                        pIdMember={"parentDeptCd"}
                        titleMember={"deptNm"}
                        selectedKeyId={"selectedTreeKey"}
                        checkedKeysId={"checkedTreeKeys"}
                        border={true}
                        checkable={this.store.helper ? this.store.helper.multi : false}
                    />
                </ContentsTemplate>
            </React.Fragment>
        )
    }
}
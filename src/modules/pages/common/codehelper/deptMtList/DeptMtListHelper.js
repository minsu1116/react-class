import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { observable, toJS } from 'mobx';
import { BaseComponent } from "utils/base/BaseComponent";
import SearchTemplate from 'components/template/SearchTemplate';
import SInput from 'components/atoms/input/SInput';
import ContentsTemplate from 'components/template/ContentsTemplate';
import { BaseStore } from 'utils/base/BaseStore';
import SButton from 'components/atoms/button/SButton';
import SGrid from 'components/override/grid/SGrid';
import DeptMtListHelperModel from 'modules/pages/common/codehelper/deptMtList/DeptMtListHelperModel';
import Alert from 'components/override/alert/Alert';
export { DeptMtListHelper, DeptMtListHelperStore }

class DeptMtListHelperStore extends BaseStore {

    @observable helper = new DeptMtListHelperModel();
    @observable companyList = [];

    loaded = async () => {
        if (this.helper.autoSearch) {
            await this.handleSearchClick();
        }
    }

    setGridApi = async (grid) => {
        this.grid = grid;

        const columnDefs = [
            { name: "", field: "check", width: 32, type: 'checkbox', hide: !this.helper.multi },
            { word: "M02445", name: "회사코드", field: "compCd", width: 80, align: 'center'},
            { word: "M02437", name: "회사명", field: "compNm", width: 100 },
            { word: "M02937", name: "BU명", field: "buNm", width: 100 },
            { word: "M03104", name: "BIZP명", field: "bizpNm", width: 150 },
            { word: "M00677", name: "부서명", field: "deptNm", width: 150, key: true },
            { word: "M02448", name: "TYPE", field: "deptTypeNm", width: 80, align: 'center' },
            { word: "M00094", name: "비고", field: "remark", width: 110 },
        ];

        this.grid.setColumnDefs(columnDefs);
    }

    handleSearchClick = async () => {
        const param = toJS(this.helper);
        const result = await this.execute('/helper/dept-mt/get', param);

        if (result.success) {
            const { excludeData } = this.helper;
            let bindingData;

            if (excludeData) {
                bindingData = [];

                result.data.map(item => {
                    if (excludeData.some(x => this.grid.getKeyValue(item) == this.grid.getKeyValue(x))) {
                    } else {
                        bindingData.push(item);
                    }
                });
            } else {
                bindingData = result.data;
            }

            if (this.helper.codeHelper) {
                if (bindingData.length == 1) {
                    await this.userSelect(bindingData[0]);
                    return;
                }
            }

            this.grid.setRowData(bindingData);
        } else {
            this.grid.clear();
        }
    }

    handleSelectClick = () => {
        this.userSelect();
    }

    handleRowDoubleClick = () => {
        if (!this.helper.multi && this.helper.isConfirm) {
            this.userSelect();
        }
    }

    userSelect = async (codeHelperValue) => {
        let returnValue;
        let check = true;

        if (codeHelperValue) {
            returnValue = codeHelperValue;
            check = true;
        } else {
            if (this.grid.length() <= 0) {
                await Alert.meg(this.l10n("M01011")); //조회해주세요.
                return;
            }

            if (this.helper.multi) {
                returnValue = this.grid.getCheckedRows();
                check = returnValue && returnValue.length > 0 ? true : false;
            } else {
                returnValue = this.grid.getSelectedRow();
                check = returnValue ? true : false;
            }
        }

        if (!check) {
            await Alert.meg(this.l10n("M01012")); //항목을 선택해주세요.
            return;
        }

        this.close(returnValue);
    }
}

@observer
class DeptMtListHelper extends BaseComponent {

    store = new DeptMtListHelperStore();

    sRender() {
        return (
            <React.Fragment>
                <SearchTemplate>
                    <div className='search_item_btn'>
                        <SButton buttonName={this.l10n("M00011", "조회")} onClick={this.store.handleSearchClick} type={'default'} />
                        <SButton buttonName={this.l10n("M00233", "선택")} onClick={this.store.handleSelectClick} type={'default'} visible={this.store.helper.isConfirm} />
                    </div>

                    <SInput id={"helper.compNm"} enter={this.store.handleSearchClick} title={this.l10n("M02444", "회사코드/명")} />
                    <SInput id={"helper.deptNm"} isFocus={true} enter={this.store.handleSearchClick} title={this.l10n("M00677", "부서명")}/>

                </SearchTemplate>

                <ContentsTemplate id={"deptMtListHelper"} height={"350px"}>
                    <SGrid grid={"deptMtListHelperGrid"} gridApiCallBack={this.store.setGridApi} editable={false} onRowDoubleClick={this.store.handleRowDoubleClick} />
                </ContentsTemplate>

            </React.Fragment>
        )
    }
}
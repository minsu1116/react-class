import { observable } from "mobx";
import { nonenumerable } from 'utils/object/Decorator.js';
import CommonStore from 'modules/pages/common/service/CommonStore';
import ObjectUtility from 'utils/object/ObjectUtility';

export default class CommonCodeHelperModel {

    // 기본 조회변수
    @observable bpCd = CommonStore.bpCd;
    @observable plnt = CommonStore.plnt;
    @observable cdGubn = 'UOM';
    @observable cdNm = "";
    useYn = "Y";
    showDesc = false; // Desc컬럼 보이기 여부

    multi = false; // 단 건 선택: false | 다건 선택: true
    autoSearch = true; // 창 Loaded 시 자동 조회
    isConfirm = true; // "선택" 버튼 Visible

    _code = "cd"; // CodeHelper에 "Code"에 바인딩 될 Property
    _codeNm = "cdNm"; // CodeHelper에 "CodeName"에 바인딩 될 Property
    @nonenumerable _search = "cdNm";

    @observable value;

    ////////////////////////////////////////////////////////////////////////
    @nonenumerable id;
    @nonenumerable path;
    @nonenumerable width;
    @nonenumerable height;
    title;
    @nonenumerable parameter;
    @observable @nonenumerable isOpen = false;
    @nonenumerable contents;
    ////////////////////////////////////////////////////////////////////////

    constructor() {
        this.id = "common_commonCode_helper";
        this.path = "/common/codehelper/commonCode/CommonCodeHelper";
        this.width = 400;
        this.height = 300;
        this.title = ObjectUtility.l10n("M02042", "공통코드 조회");
    }
}
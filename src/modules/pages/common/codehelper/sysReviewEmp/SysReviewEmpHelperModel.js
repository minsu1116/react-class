import ModalManage from "utils/base/ModalManage";
import { observable } from "mobx";
import { nonenumerable } from 'utils/object/Decorator.js';

export default class SysReviewEmpHelperModel {

    // 기본 조회변수
    sysCategoryId;

    excludeData; // 조회되는 내용 중 제외할 항목 Array (key-value)
    multi = false; // 단 건 선택: false | 다건 선택: true
    autoSearch = true; // 창 Loaded 시 자동 조회
    isConfirm = true; // "선택" 버튼 Visible

    _code = "empNo"; // CodeHelper에 "Code"에 바인딩 될 Property
    _codeNm = "userNm"; // CodeHelper에 "CodeName"에 바인딩 될 Property
    // @nonenumerable _search = "user";
    codeHelper = false;

    @observable value;

    ////////////////////////////////////////////////////////////////////////
    @nonenumerable id;
    @nonenumerable path;
    @nonenumerable width;
    @nonenumerable height;
    @nonenumerable title;
    @nonenumerable parameter;
    @observable @nonenumerable isOpen = false;
    @nonenumerable contents;
    ////////////////////////////////////////////////////////////////////////

    constructor() {
        this.id = "common_sysReviewEmp_helper";
        this.path = "/common/codehelper/sysReviewEmp/SysReviewEmpHelper";
        this.width = 650;
        this.title = "서비스 요청 검토자";
        this.user = "";
    }
}
import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { observable, toJS } from 'mobx';
import { BaseComponent } from "utils/base/BaseComponent";
import SearchTemplate from 'components/template/SearchTemplate';
import SInput from 'components/atoms/input/SInput';
import ContentsTemplate from 'components/template/ContentsTemplate';
import { BaseStore } from 'utils/base/BaseStore';
import SButton from 'components/atoms/button/SButton';
import SGrid from 'components/override/grid/SGrid';
import SysMtHelperModel from 'modules/pages/common/codehelper/sysMt/SysMtHelperModel';
import Alert from 'components/override/alert/Alert';
import SSelectBox from 'components/atoms/selectbox/SSelectBox';
export { SysMtHelper, SysMtHelperStore }

class SysMtHelperStore extends BaseStore {

    @observable helper = new SysMtHelperModel();
    @observable companyList = [];

    loaded = async () => {
        if (this.helper.autoSearch) {
            await this.handleSearchClick();
        }
    }

    setGridApi = async (grid) => {
        this.grid = grid;

        const columnDefs = [
            { name: "", field: "check", width: 32, type: 'checkbox', hide: !this.helper.multi },
            { name: "REVIE_ID", field: "sysId", key: true, hide: true },
            { word: "M02471", name: "시스템이름", field: "sysNm", width: 290 },
            { name: "OPEN", field: "sysOpen", width: 90, align: 'center', type: "dateTime" },
            { word: "M00417", name: "Type", field: "sysType", width: 100, align: 'center', codeGroup: "SYS_TYPE" },
        ];

        this.grid.setColumnDefs(columnDefs);
    }

    handleSearchClick = async () => {
        const param = toJS(this.helper);
        const result = await this.execute('/helper/sys-mt/get', param);

        if (result.success) {
            const { excludeData } = this.helper;
            let bindingData;

            if (excludeData) {
                bindingData = [];

                result.data.map(item => {
                    if (excludeData.some(x => this.grid.getKeyValue(item) == this.grid.getKeyValue(x))) {
                    } else {
                        bindingData.push(item);
                    }
                });
            } else {
                bindingData = result.data;
            }

            if (this.helper.codeHelper) {
                if (bindingData.length == 1) {
                    await this.userSelect(bindingData[0]);
                    return;
                }
            }

            this.grid.setRowData(bindingData);
        } else {
            this.grid.clear();
        }
    }

    handleSelectClick = () => {
        this.userSelect();
    }

    handleRowDoubleClick = () => {
        if (!this.helper.multi && this.helper.isConfirm) {
            this.userSelect();
        }
    }

    userSelect = async (codeHelperValue) => {
        let returnValue;
        let check = true;

        if (codeHelperValue) {
            returnValue = codeHelperValue;
            check = true;
        } else {
            if (this.grid.length() <= 0) {
                await Alert.meg(this.l10n("M01011")); //조회해주세요.
                return;
            }

            if (this.helper.multi) {
                returnValue = this.grid.getCheckedRows();
                check = returnValue && returnValue.length > 0 ? true : false;
            } else {
                returnValue = this.grid.getSelectedRow();
                check = returnValue ? true : false;
            }
        }

        if (!check) {
            await Alert.meg(this.l10n("M01012")); //항목을 선택해주세요.
            return;
        }

        this.close(returnValue);
    }
}

@observer
class SysMtHelper extends BaseComponent {

    store = new SysMtHelperStore();

    sRender() {
        return (
            <React.Fragment>
                <SearchTemplate>
                    <div className='search_item_btn'>
                        <SButton buttonName={this.l10n("M00011", "조회")} onClick={this.store.handleSearchClick} type={'default'} />
                        <SButton buttonName={this.l10n("M00233", "선택")} onClick={this.store.handleSelectClick} type={'default'} visible={this.store.helper.isConfirm} />
                    </div>

                    <label className="item_lb w100">{this.l10n("M02471", "시스템명")}</label>
                    <SInput id={"helper.sysNm"} isFocus={true} enter={this.store.handleSearchClick} />

                    <label className="item_lb w100">{this.l10n("M00417", "타입")}</label>
                    <SSelectBox id={"helper.sysType"} codeGroup={"SYS_TYPE"} addOption={this.l10n("M00322", "전체")} readOnly={this.store.helper.readOnlySysType} />
                </SearchTemplate>

                <ContentsTemplate id={"sysMtHelper"} height={"350px"}>
                    <SGrid grid={"sysMtHelperGrid"} gridApiCallBack={this.store.setGridApi} editable={false} onRowDoubleClick={this.store.handleRowDoubleClick} />
                </ContentsTemplate>

            </React.Fragment>
        )
    }
}
import React, { Component, Suspense} from 'react';
import { inject } from 'mobx-react';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
import 'babel-polyfill'; // Promise 오류 관련

import PrivateRoute from 'modules/layouts/main/PrivateRoute'
import Spinner from 'components/atoms/spinner/Spinner';
import NoMatch from 'modules/layouts/match/NoMatch';

import MainLayout from "modules/layouts/main/MainLayout";
import UnauthorizedLayout from "modules/layouts/authorized/UnauthorizedLayout";
import DirectPopupLayout from "modules/layouts/popup/DirectPopupLayout";
import Storage from "modules/pages/system/etc/storage/Storage";

import 'style/font.css';
import 'style/layout.css';
import 'style/main.css'
import 'utils/object/DateFormat'
import 'style/table.css';


@inject(stores => ({
    checkToken: stores.commonStore.checkToken,
    getSSO: stores.loginStore.getSSO,
}))
class App extends Component {
    render() {
        return (
            <React.Fragment>
                <Spinner />
                    <BrowserRouter>
                        <Switch>
                            <Route path="/auth" component={UnauthorizedLayout} />
                            <Route path="/popup" component={DirectPopupLayout} />
                            <Route path="/download/:target" component={Storage} />
                            <Route path="/sr-req/:target" component={MainLayout} />
                            <Route path="/sr-req" component={MainLayout} />
                            <Route path="/sr/:target" component={MainLayout} />
                            <PrivateRoute path="/app" component={MainLayout} />
                            <Redirect from="/" to="/app" />
                            <Route component={NoMatch} />
                        </Switch>
                    </BrowserRouter>
            </React.Fragment>
        );
    }
}

export default App;
